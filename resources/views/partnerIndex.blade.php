@extends('layouts.master')

@section('activeB','Dashboard')

@section('mainContent')

	<div class="page-content">
		<div class="ace-settings-container" id="ace-settings-container">
			<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
				<i class="ace-icon fa fa-cog bigger-130"></i>
			</div>
			<div class="ace-settings-box clearfix" id="ace-settings-box">
				<div class="pull-left width-50">
				</div><!-- /.pull-left -->
				<div class="pull-left width-50">
				</div><!-- /.pull-left -->
			</div><!-- /.ace-settings-box -->
		</div><!-- /.ace-settings-container -->
		<div class="page-header">
			<h1>
				Dashboard
				<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					overview &amp; stats
				</small>
			</h1>
		</div><!-- /.page-header -->

		
		<div class="row">
			@include('notifications')
			<div class="col-xs-12">
				<div class="row">
					<div class="space-6"></div>

					<div class="col-sm-6 infobox-container">
						@forEach($partnerIssues as $case )
							@if($case->value > 0)
								<a href="partner.home">
									<div class="infobox infobox-green">
										<div class="infobox-icon">
											<i class="ace-icon fa fa-file-text" style=""></i>
										</div>
										<div class="infobox-data">
											<div class="infobox-content">
												<small>{{$case->name}}</small> <span class="" style="color:black">{{$case->value}}</span>
											</div>
										</div>
									</div>
								</a>
							@endif
						@endforeach
					</div>
					<div class="vspace-12-sm"></div>
					<div class="col-sm-3" >
						<div class="widget-box" >
							<div class="widget-header widget-header-flat widget-header-small">
								<h5 class="widget-title">
									<i class="ace-icon fa fa-signal"></i>
									Main Issues
								</h5>

								<div class="widget-toolbar no-border">
									<div class="inline dropdown-hover">
										<!--	<button class="btn btn-minier btn-primary">
										This Week
										<i class="ace-icon fa fa-angle-down icon-on-right bigger-110"></i>
									</button> -->
									<ul class="dropdown-menu dropdown-menu-right dropdown-125 dropdown-lighter dropdown-close dropdown-caret">
										<li class="active">
											<a href="#" class="blue">
												<i class="ace-icon fa fa-caret-right bigger-110">&nbsp;</i>
												This Week
											</a>
										</li>

										<li>
											<a href="#">
												<i class="ace-icon fa fa-caret-right bigger-110 invisible">&nbsp;</i>
												Last Week
											</a>
										</li>

										<li>
											<a href="#">
												<i class="ace-icon fa fa-caret-right bigger-110 invisible">&nbsp;</i>
												This Month
											</a>
										</li>

										<li>
											<a href="#">
												<i class="ace-icon fa fa-caret-right bigger-110 invisible">&nbsp;</i>
												Last Month
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main" >
								<div id="piechart-placeholder" style="height:180px"></div>

								<div class="hr hr8 hr-double"></div>

							</div><!-- /.widget-main -->
						</div><!-- /.widget-body -->
					</div><!-- /.widget-box -->

				</div><!-- /.col -->
				<div class="col-sm-3" id="legend1"></div>
			</div><!-- /.row -->

			<div class="hr hr32 hr-dotted"></div>

			<div class="row">
				<div class="col-sm-5">
					<div class="widget-box transparent">
						<div class="widget-header widget-header-flat">
							<h4 class="widget-title lighter">
								<i class="ace-icon fa fa-star orange"></i>
								Popular Cases
							</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main no-padding">
								<table class="table table-bordered table-striped">
									<thead class="thin-border-bottom">
										<tr>
											<th><i class="ace-icon fa fa-caret-right blue"></i>Issue</th>
											<th><i class="ace-icon fa fa-caret-right blue"></i>Total</th>
										</tr>
									</thead>
									<tbody>
										@foreach($partnerIssues as $case)
											@if($case->value > 0)
												<tr>
													<td>{{$case->name}}</td>
													<td><b class="green">{{$case->value}}</b></td>
												</tr>
											@endif
										@endforeach
									</tbody>
								</table>
							</div><!-- /.widget-main -->
						</div><!-- /.widget-body -->
					</div><!-- /.widget-box -->
				</div><!-- /.col -->

				<div class="col-sm-7">
					<div class="widget-box transparent" id="recent-box">
						<div class="widget-header">
							<h4 class="widget-title lighter smaller"><i class="ace-icon fa fa-list orange"></i>RECENT CASES</h4>
							<div class="widget-toolbar no-border">
								<ul class="nav nav-tabs" id="recent-tab">
									<li class="active">
										<a data-toggle="tab" href="#task-tab" style="font-size:18px; font-family:times new romans" title="These are cases referred to partners.">
											<i class="ace-icon fa fa-list blue"></i> Un Resolved
											<span class="badge badge-grey" style="background:blue"><b>{{$totalClosedCases}}</b></span>
										</a>
									</li>
									<li>
										<a data-toggle="tab" href="#resolved-tab" style="font-size:18px; font-family:times new romans" title="These are cases resolved  by  partners.">
											<i class="ace-icon fa fa-list green"></i>Resolved
											<span class="badge badge-grey" style="background:green"><b>{{$totalResolvedCases}}</b></span>
										</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="widget-body">
							<div class="widget-main padding-4">
								<div class="tab-content padding-8">
									<div id="task-tab" class="tab-pane active">
										<h4 class="smaller lighter green"
										style="font-size:18px; padding:2px; font-family:times new romans">
										<i class="ace-icon fa fa-list"></i>
										Referred
									</h4>
									<ul id="tasks" class="item-list">
										@foreach($latestClosedCases as $latestClosedCase)
											<li class="item-orange clearfix">
												<label class="inline">
													<span class="lbl"
													style="font-size:14px; padding:2px; font-family:times new romans">{{ substr(trim($latestClosedCase->comments) , 0, 90)}}...</span>
												</label>
												<div class="pull-right action-buttons">
													<a href="/caseClose?id={{$latestClosedCase->id}}">
														<i class="glyphicon glyphicon-pencil"></i>
													</a>
												</div>
											</li>
										@endforeach
									</ul>
								</div>
								<div id="resolved-tab" class="tab-pane">
									<h4 class="smaller lighter green" style="font-size:18px; padding:2px; font-family:times new romans">
										<i class="ace-icon fa fa-list"></i> Resolved
									</h4>
									<ul id="tasks" class="item-list">
										@foreach($latestResolvedCases as $latestResolvedCase)
											<li class="item-orange clearfix">
												<label class="inline">
													<span class="lbl" style="font-size:14px; padding:2px; font-family:times new romans">{{ substr(trim($latestResolvedCase->comments) , 0, 90)}}...</span>
												</label>
												<div class="pull-right action-buttons">
													<a href="/caseClose?id={{$latestResolvedCase->id}}">
														<i class="glyphicon glyphicon-pencil"></i>
													</a>
												</div>
											</li>
										@endforeach
									</ul>
								</div>
							</div>
						</div><!-- /.widget-main -->
					</div><!-- /.widget-body -->
				</div><!-- /.widget-box -->
			</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
@endsection

