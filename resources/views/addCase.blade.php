@extends('layouts.master')

@section('activeB','Dashboard')

@section('mainContent')

  <div class="page-content">
    <div class="ace-settings-container" id="ace-settings-container">
      <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
        <i class="ace-icon fa fa-cog bigger-130"></i>
      </div>

      <div class="ace-settings-box clearfix" id="ace-settings-box">
        <div class="pull-left width-50">
        </div><!-- /.pull-left -->

        <div class="pull-left width-50">

        </div><!-- /.pull-left -->
      </div><!-- /.ace-settings-box -->
    </div><!-- /.ace-settings-container -->

    <div class="page-header">
      <h1>
        Cases
        <small>
          <i class="ace-icon fa fa-angle-double-right"></i>
          Add a new case.
        </small>
      </h1>
    </div><!-- /.page-header -->
    <!-- the form -->
    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <form class="form-horizontal" role="form" action="/addCase" method="POST">
          {!! csrf_field() !!}
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> <b>Date</b></label>
            <div class="col-sm-9">
              <input type="date" id="form-field-1" name ="date" required>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> <b>Client Name</b> </label>
            <div class="col-sm-9">
              <input type="text" id="form-field-1" name ="client_name" class="col-xs-10 col-sm-5"  required >
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"><b>Gender</b> </label>
            <div class="col-sm-2">
              <b>MALE</b> <Input type="radio" id="form-field-1-1" name="client_gender" value="Male" style=" color:white; text-align:center" />
              <b>FEMALE</b> <Input type="radio" id="form-field-1-1" name="client_gender" value="Female" style=" color:white; text-align:center" />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> <b>Age</b> </label>

            <div class="col-sm-1">
              <input type="number" id="form-field-1-1" name="client_age" class="form-control" required />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> <b>Perpetrator</b> </label>
            <div class="col-sm-1">
              <select name="perpetrator">
                @foreach($perpetrators as $perpetrator)
                  <option type="number" id="form-field-1-1" value="{{$perpetrator->id}}" class="form-control" required >{{$perpetrator->name}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <!-- show status here -->

          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> <b>Nature</b> </label>
            <div class="col-sm-1">
              <select name="nature">
                @foreach($reasons as $reason)
                  <option type="number" id="form-field-1-1" value="{{$reason}}" class="form-control" required>{{$reason}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <!-- show status here -->
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"><b>District</b> </label>

            <div class="col-sm-2">
              <select name="district">
                @foreach($districts as $district)
                  <option type="text" id="form-field-1-1"  style="background-color:green; color:white; text-align:center" value="{{$district->id}}"  class="form-control"><b>{{$district->name}}</b></option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"><b>Traditional Authority</b> </label>

            <div class="col-sm-2">
              <Input type="text" id="form-field-1-1" name="T_A"   class="form-control"/>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1">Partner</label>
            <div class="col-sm-2">
              <span type="text" id="form-field-1-1" name="partner" style="background-color:green; color:white; text-align:center"  class="form-control">{{Auth::user()->username}}</span>
            </div>
          </div>
          <div class="space-4"></div>
          <div class="form-group">
          </div>
          <div class="hr hr-24"></div>
          <div class="row">
            <div class="col-xs-12 col-sm-8">
              <div class="widget-box">
                <div class="widget-header">
                  <h4 class="widget-title">Issues</h4>
                  <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                      <i class="ace-icon fa fa-chevron-up"></i>
                    </a>
                  </div>
                </div>
                <div class="widget-body">
                  <div class="widget-main">
                    <div>
                      @foreach($issues->issues as $issue)
                        <p>{{$issue->name}}</p>
                        @foreach($issue->subissues as $subissue)
                          <div class="ace-settings-item">
                            <input type="radio" name="subissue" class="ace ace-checkbox-2" value="{{$subissue->id}}" id="ace-settings-compact"/>
                            <label class="lbl" for="ace-settings-compact" style="color:black"> <b>{{$subissue->name}}</b></label>
                          </div>
                        @endforeach
                      @endforeach
                    </div>
                  </div>
                </div>
              </div>
            </div><!-- /.span -->
            <div class="space-4"></div>
            <div class="col-xs-12 col-sm-4">
              <div class="widget-box">
                <div class="widget-header">
                  <h4 class="widget-title">CFM Channel</h4>
                  <span class="widget-toolbar">
                    <a href="#" data-action="settings">
                      <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                      </a>
                    </span>
                  </div>
                  <div class="widget-body">
                    <div class="widget-main">
                      <div>
                        @foreach($cfmChannels as $cfmChannel)
                          <div class="ace-settings-item">
                            <input type="radio" name="cfm" class="ace ace-checkbox-2" value="{{$cfmChannel->id}}" id="ace-settings-compact" autocomplete="off" required/>
                            <label class="lbl" for="ace-settings-compact"><b>{{$cfmChannel->Name}}</b></label>
                          </div>
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </div><!-- /.span -->
            </div><!-- /.row -->
            <div class="space-24"></div>
            <hr />
            <div class="row">
              <div class="col-sm-9 col-sm-offset-1" >
                <div class="widget-box">
                  <div class="widget-header">
                    <h4 class="widget-title">Case Summary</h4>
                    <div class="widget-toolbar">
                      <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                      </a>
                    </div>
                  </div>
                  <div class="widget-body">
                    <div class="widget-main">
                      <div class="form-group">
                        <div class="col-xs-12">
                          <textarea type="text" class="form-control" name="case_brief" style="color:#000; font-family: times new romans; font-size:16px;height:150px"></textarea>
                          <p style="font-size:15px"> </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <hr />
            <div class="row">
              <div class="col-sm-9 col-sm-offset-1">
                <div class="widget-box">
                  <div class="widget-header">
                    <h4 class="widget-title"><b >Action Taken Summary<span style="color:red">**</span></b> </h4>
                    <span class="widget-toolbar">
                      <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                      </a>
                    </span>
                  </div>
                  <div class="widget-body">
                    <div class="widget-main">
                      <div class="row">
                        <div class="">
                          <div class=" col-sm-12">
                            <textarea class="form-control" id="" name="action_taken" type="text" style="color:#000; font-family: times new romans; font-size:16px;height:150px"></textarea>
                          </div>
                        </div>
                      </div>

                      <!--<hr />-->
                    </div>
                  </div>


                  <div class="space space-8">  </div>
                </div>
                <div class="widget-box">
                  <div class="widget-header">
                    <h4 class="widget-title"><b > Outcome Of Case <span style="color:red">**</span></b></h4>
                    <span class="widget-toolbar">
                      <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                      </a>
                    </span>
                  </div>
                  <div class="widget-body">
                    <div class="widget-main">
                      <div class="row">
                        <div class="">
                          <div class=" col-sm-12">
                            <textarea class="form-control" id="" name="outcome" type="text" style="color:#000; font-family: times new romans; font-size:16px;height:150px"></textarea>
                          </div>
                        </div>
                      </div>
                      <hr />
                    </div>
                  </div>
                  <div class="space space-4"></div>
                </div>
                <button type="submit" class="btn btn-primary">Save Case</button>
              </div>
              <div class="hr hr-18 dotted hr-double"></div>
            </div>
          </div>
        </div><!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
@stop
