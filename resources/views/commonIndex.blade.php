@extends('layouts.master')

@section('activeB','Dashboard')

@section('mainContent')

  <div class="page-content">
    <div class="ace-settings-container" id="ace-settings-container">
      <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
        <i class="ace-icon fa fa-cog bigger-130"></i>
      </div>
      <div class="ace-settings-box clearfix" id="ace-settings-box">
        <div class="pull-left width-50">
        </div><!-- /.pull-left -->
        <div class="pull-left width-50">
        </div><!-- /.pull-left -->
      </div><!-- /.ace-settings-box -->
    </div><!-- /.ace-settings-container -->
    <div class="page-header">
      <h1>Dashboard<small><i class="ace-icon fa fa-angle-double-right"></i>Today's Cases</small></h1>
    </div><!-- /.page-header -->
    <div class="row">
      @include('notifications')
      <div class="col-md-12">
        <div class="row">
          <div class="col-lg-8">
            <div class="panel panel-primary table-responsive">
              <table class="table table-bordered table-striped">
                <thead class="thin-border-bottom">
                  <tr>
                    <th>Date</th>
                    <th>District</th>
                    <th>Comment</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($cases as $case)
                    <tr>
                      <td>{{$case->date}}</td>
                      <td><b class="green">{{$case->district->name}}</b></td>
                      <td style="font-size:16px">
                        {{ substr(trim($case->comments) , 0, 200)}}...
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div><!-- /.page-content -->
      </div>
    </div>
  </div><!-- /.main-content -->
@stop
