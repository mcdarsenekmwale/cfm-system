@extends('layouts.master')

@section('activeB','Manage Users')

@section('mainContent')

  <div class="page-content">
    <div class="ace-settings-container" id="ace-settings-container">
      <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
        <i class="ace-icon fa fa-cog bigger-130"></i>
      </div>
    </div><!-- /.ace-settings-container -->
    <div class="page-header">
      <h1>Dashboard<small><i class="ace-icon fa fa-angle-double-right"></i> Edit User</small></h1>
    </div><!-- /.page-header -->
    <div class="row">
      @include('notifications')
      <div class="col-md-12">
        <div class="row">
          <div class="col-lg-8">
            <form class="form-horizontal" method="POST" action="/editUser?id={{$user->id}}" enctype="multipart/form-data">
              {!! csrf_field() !!}
              <div class="col-md-6">
                <div class="form-group">
                  <label for="type" class="col-sm-3 control-label">Title:</label>
                  <div class="col-sm-9">
                    <select name="title" id="title" required class="form-control" name="title">
                      <option>--Select title--</option>
                      <option @if($user->type == "Dr") selected @endif value="Dr">Dr.</option>
                        <option @if($user->type == "Mr") selected @endif value="Mr">Mr.</option>
                          <option @if($user->type == "Mrs") selected @endif value="Mrs">Mrs.</option>
                            <option @if($user->type == "Ms") selected @endif value="Ms">Ms</option>
                              <option @if($user->type == "Miss") selected @endif value="Miss">Miss</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="name" class="col-sm-3 control-label">first name:</label>
                            <div class="col-sm-9">
                              <input required placeholder="input first name" value="{{$user->firstname}}" class="form-control" type="text" name="fname" id="fname">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="name" class="col-sm-3 control-label">last name:</label>
                            <div class="col-sm-9">
                              <input required placeholder="input last name" value="{{$user->lastname}}" class="form-control" type="text" name="lname" id="lname">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="name" class="col-sm-3 control-label">username:</label>
                            <div class="col-sm-9">
                              <input required placeholder="input username" value="{{$user->username}}" class=" form-control" type="text" name="name" id="name">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">E-Mail:</label>
                            <div class="col-sm-9">
                              <input required placeholder="input Email" value="{{$user->email}}" class=" form-control" type="email" name="email" id="email">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="type" class="col-sm-3 control-label">Type:</label>
                            <div class="col-sm-9">
                              <select name="type" id="type" required class="form-control" name="type">
                                <option>--Select type--</option>
                                <option @if($user->type == "admin") selected @endif value="admin">Admin</option>
                                  <option @if($user->type == "partner") selected @endif  value="partner">partner</option>
                                    <option @if($user->type == "supervisor") selected @endif  value="supervisor">supervisor</option>
                                      <option @if($user->type == "common") selected @endif  value="common">Common</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="checkbox">
                                    <label><input type="checkbox" @if($user->state == 1) selected @endif value="1" name="active">Active</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-12">
                                    <div class="col-sm-5">
                                      <div class="form-group">
                                        <div class="col-sm-offset-3">
                                          <button type="submit" class=" btn btn-default btn-primary">Edit User</button>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-sm-6">
                                      <div class="form-group">
                                        <div class="" onclick="window.location='passwordReset?id={{$user->id}}'">
                                          <button type="" class=" btn btn-default btn-primary">Reset</button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div><!-- /.page-content -->
                        </div><!-- /.main-content -->
                      </div>
                    @stop
