@extends('layouts.app')

@section('content')
<div class="container">


  <div class="row justify-content-center">
    <div class="col-md-7 col-offset-md-4">
      <h3 class="text-center">
        <span class="badge badg-auth text-muted">{{ __('Create an account') }}</span>
      </h3>

      <div class="card">
        <div class="card-body">
          <div class="card-img">
            <img class="rounded-circle img-card" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
            alt="" >
          </div>

          <form class="form-horizontal form" action="{{ route('register') }}" method="POST">
            @csrf
            <div class="form-group ">

             <div class="">
              <label for="name" class="col-form-label text-md-right sr-only">{{ __('Name') }}</label>
              <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Enter Name" name="name" value="{{ old('name') }}" required autofocus>

              @if ($errors->has('name'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
              @endif

            </div>

            <div class="">
              <label for="email" class="col-form-label text-md-right  sr-only">{{ __('E-Mail Address') }}</label>
              <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Enter Email Address" name="email" value="{{ old('email') }}" required>

              @if ($errors->has('email'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
              @endif
            </div>

            <div class="">
              <label for="password" class=" col-form-label text-md-right  sr-only">{{ __('Password') }}</label>
              <input id="password" type="password" placeholder="Enter Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

              @if ($errors->has('password'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
              @endif
            </div>


          </div>

          <div class="form-group">
            <label for="password-confirm" class="sr-only col-form-label text-md-right">{{ __('Confirm Password') }}</label>

              <input id="password-confirm" type="password" placeholder="Re-enter Password" class="form-control" name="password_confirmation" required>
          </div>

          <div class="form-group row mb-0">
            <div class="division1">
              <button type="submit" class="btn btn-outline-primary btn-block">
               {{ __('Create Account') }}
             </button>

             <div class="btn-group btn-group-sm">
              <a class="btn btn-link login-links" href="{{ route('password.request') }}">
               {{ __('Forgot Your Password?') }}
             </a>
             <a class="btn btn-link login-links" href="{{route('login')}}">
               {{ __('Already have an account') }}
             </a>
           </div>

         </div>
       </div>

     </form>

   </div>

 </div>
</div>
</div>
</div>
@endsection
