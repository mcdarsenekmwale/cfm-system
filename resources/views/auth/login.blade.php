@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-6 col-offset-md-2">
          <h3 class="text-center">
            <span class="badge badg-auth text-muted">{{ __('Sign in to continue') }}</span>
          </h3>

          <div class="card">
            <div class="card-body">
              <div class="card-img">
                <img class="rounded-circle img-card" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
                alt="" >
              </div>

              <form class="form-horizontal form" action="{{ route('login') }}" method="POST">

                        @csrf
                <div class="form-group ">
                  <div class="">
                     <label for="email" class=" col-form-label text-md-right sr-only">{{ __('E-Mail Address') }}</label>
                    <input id="email" type="email" placeholder="name@address.com" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" required autofocus>
                    @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                  </div>

                  <div class=" ">
                    <label for="password" class=" col-form-label text-md-right sr-only">{{ __('Password') }}</label>
                    <input id="password" type="password" placeholder="Enter your password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                    @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                  </div>
                </div>

                <div class="form-group row">
                  <div class=" remember" >
                    <div class="form-check">
                      <input class="form-check-input " type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                      <label class="form-check-label" for="remember">
                         {{ __('Remember Me') }}
                      </label>
                    </div>
                  </div>
                </div>

                <div class="form-group row mb-0">
                  <div class="division">
                    <button type="submit" class="btn btn-outline-primary btn-block mb-3">
                      {{ __('Sign In') }}
                    </button>

                    <div class="btn-group btn-group-sm">
                      <a class="btn btn-link login-links" href="{{ route('password.request') }}">
                         {{ __('Forgot Your Password?') }}
                      </a>
                      <a class="btn btn-link login-links" href="{{route('register')}}">
                         {{ __('Create an account') }}
                      </a>
                    </div>

                  </div>
                </div>

              </form>

            </div>

          </div>
        </div>
</div>
@endsection
