@extends('admin.master.master')

@section('content')

  <!-- .wrapper -->
  <div class="wrapper">
    <!-- .page -->
    <div class="page">
      <!-- .page-inner -->
      <div class="page-inner">
        <!-- .page-title-bar -->
        @include('notifications')
        <header class="page-title-bar">
          <p class="lead">
            <span class="font-weight-bold " style="font-size:15px;">Welcome <span class="text-capitalize">{{Auth::user()->firstname}} {{Auth::user()->lastname}}.</span> </span> <span class="float-right small d-block text-muted">If you need help feel free to contact McDarsene.</span>
          </p>
        </header><!-- /.page-title-bar -->
        <!-- .page-section -->
        <div class="page-section">
          <!-- .section-block -->
          <div class="section-block" >
            <!-- metric row -->
            <div class="metric-row">
              <div class="col-lg-9">
                <div class="metric-row metric-flush">
                  <!-- metric column -->
                  <div class="col ">
                    <!-- .metric -->
                    <a href="{{route('counsellors')}}" class="card hoverable metric metric-bordered align-items-center">
                      <h2 class="metric-label card-title text-uppercase text-muted mb-2"><b>Counsellor</b>  </h2>
                      <p class="metric-value h3">
                        <sub><i class="fa fa-users fa-2x"></i></sub> <span class="value">{{$counsellors_count}}</span>
                      </p>
                    </a> <!-- /.metric -->
                  </div><!-- /metric column -->
                  <!-- metric column -->
                  <div class="col ">
                    <!-- .metric -->
                    <a href="{{route('partner-list')}}" class="card hoverable metric metric-bordered align-items-center">
                      <h2 class="metric-label card-title text-uppercase text-muted mb-2"><b>Partners</b> </h2>
                      <p class="metric-value h3">
                        <sub><i class="fa fa-institution fa-2x"></i></sub> <span class="value">{{$partners_count}}</span>
                      </p>
                    </a> <!-- /.metric -->
                  </div><!-- /metric column -->
                  <!-- metric column -->
                  <div class="col ">
                    <!-- .metric -->
                    <a href="{{route('cases')}}" class="card hoverable metric metric-bordered align-items-center ">
                      <h2 class="metric-label card-title text-uppercase text-muted mb-2"><b>CASES</b> </h2>
                      <p class="metric-value h3">
                        <sub><i class="fa fa-tasks fa-2x" style="position: relative; top:3px;"></i></sub> <span class="value">{{$totalCases}}</span>
                      </p>
                    </a> <!-- /.metric -->
                  </div><!-- /metric column -->
                </div>
              </div><!-- metric column -->
              <div class="col-lg-3">
                <!-- .metric -->
                <a href="{{route('request')}}" class=" card metric metric-bordered hoverable">
                  <div class="metric-badge">
                    <span class="badge badge-lg badge-success text-uppercase"><span class="fa fa-fw fa-circle pulse mr-1"></span> INCOMING REQUEST</span>
                  </div>
                  <p class="metric-value h3">
                    <sub><i class="fa fa-history"></i></sub> <span class="value">8</span>
                  </p>
                </a> <!-- /.metric -->
              </div><!-- /metric column -->
            </div><!-- /metric row -->
          </div><!-- /.section-block -->
          <!-- grid row -->
          <div class="row">
            <!-- grid column -->
            <div class="col-12 col-lg-12 col-xl-4">
              <!-- .card -->
              <section class="card card-fluid">
                <!-- .card-body -->
                <div class="card-body">
                  <!-- .d-flex -->
                  <div class="d-flex align-items-center mb-4">
                    <h3 class="card-title mb-0 text-uppercase"> Case Trending </h3><!-- .card-title-control -->
                    <div class="card-title-control ml-auto">
                      <!-- .dropdown -->
                      <div class="dropdown">
                        <button class="btn btn-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="navDropdown"><span>This Week</span> <i class="fa fa-fw fa-caret-down"></i></button>

                        <div class="dropdown-arrow dropdown-arrow-right"></div><!-- .dropdown-menu -->
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-md stop-propagation"  aria-labelledby="navDropdown" style="position: absolute; will-change: top, left; top: 36px; left: 110px;">
                          <!-- .custom-control -->
                          <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="dpToday" name="dpFilter" value="0"> <label class="custom-control-label d-flex justify-content-between" for="dpToday"><span>Today</span> <span class="text-muted" id="today"</span></label>
                          </div><!-- /.custom-control -->
                          <!-- .custom-control -->
                          <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="dpYesterday" name="dpFilter" value="1"> <label class="custom-control-label d-flex justify-content-between" for="dpYesterday"><span>Yesterday</span> <span class="text-muted" id="yesterday"></span></label>
                          </div><!-- /.custom-control -->
                          <!-- .custom-control -->
                          <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="dpWeek" name="dpFilter" value="2" checked=""> <label class="custom-control-label d-flex justify-content-between" for="dpWeek"><span>This Week</span> <span class="text-muted"  id="month"> 21-27</span></label>
                          </div><!-- /.custom-control -->
                          <!-- .custom-control -->
                          <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="dpMonth" name="dpFilter" value="3"> <label class="custom-control-label d-flex justify-content-between" for="dpMonth"><span>This Month</span> <span class="text-muted" id="month"> 1-31</span></label>
                          </div><!-- /.custom-control -->
                          <!-- .custom-control -->
                          <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="dpYear" name="dpFilter" value="4"> <label class="custom-control-label d-flex justify-content-between" for="dpYear"><span>This Year</span> <span class="text-muted"><?php date_default_timezone_set('Africa/Blantyre'); echo date("Y")?></span></label>
                          </div><!-- /.custom-control -->
                          <!-- .custom-control -->
                          <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="dpCustom" name="dpFilter" value="5"> <label class="custom-control-label" for="dpCustom">Custom</label>
                            <div class="custom-control-hint my-1" for="dpCustom">
                              <!-- datepicker:range -->
                              <input type="text" class="form-control flatpickr-input" data-toggle="flatpickr" data-mode="range" data-disable-mobile="true" data-date-format="Y-m-d" readonly="readonly"> <!-- /datepicker:range -->
                            </div>
                          </div><!-- /.custom-control -->
                        </div><!-- /.dropdown-menu -->
                      </div><!-- /.dropdown -->
                    </div><!-- /.card-title-control -->
                  </div><!-- /.d-flex -->
                  <div class="chartjs" style="height: 291px"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                    <canvas id="completion-tasks" width="826" height="363" class="chartjs-render-monitor" style="display: block; height: 291px; width: 661px;"></canvas>
                  </div>
                </div><!-- /.card-body -->
              </section><!-- /.card -->
            </div><!-- /grid column -->
            <!-- grid column -->
            <div class="col-12 col-lg-6 col-xl-4">
              <!-- .card -->
              <section class="card card-fluid">
                <!-- .card-body -->
                <div class="card-body">
                  <h3 class="card-title text-uppercase"> Tasks Performance </h3><!-- easy-pie-chart -->
                  <div class="text-center pt-4" >
                    <div class="chart chart-inline-group" style="height:214px; " >
                      <div class="easypiechart" data-toggle="easypiechart" data-percent="60" data-size="214" data-bar-color="#346CB0" data-track-color="false"
                      data-scale-color="false" data-rotate="225" style="position:relative; ">
                      </div>
                    </div>
                  </div><!-- /easy-pie-chart -->
                </div><!-- /.card-body -->
                <!-- .card-footer -->
                <div class="card-footer">
                  <div class="card-footer-item">
                    <i class="fa fa-fw fa-circle text-indigo"></i> 100% <div class="text-muted small">  Pending</div>
                  </div>
                  <div class="card-footer-item">
                    <i class="fa fa-fw fa-circle text-purple"></i> 75% <div class="text-muted small"> Unresolved </div>
                  </div>
                  <div class="card-footer-item">
                    <i class="fa fa-fw fa-circle text-teal"></i> 60% <div class="text-muted small"> Resolved </div>
                  </div>
                </div><!-- /.card-footer -->
              </section><!-- /.card -->
            </div><!-- /grid column -->
            <!-- grid column -->
            <div class="col-12 col-lg-6 col-xl-4">
              <!-- .card -->
              <section class="card card-fluid">
                <!-- .card-body -->
                <div class="card-body pb-0">
                  <h3 class="card-title text-uppercase">Counsellor Leaderboard </h3><!-- legend -->
                  <ul class="list-inline small">
                    <li class="list-inline-item">
                      <i class="fa fa-fw fa-phone text-success"></i><b>Calls</b>  </li>
                    <li class="list-inline-item">
                        <i class="fa fa-fw fa-circle pulse text-red"></i> Incomplete </li>
                    <li class="list-inline-item">
                      <i class="fa fa-fw fa-circle pulse  text-teal"></i> Completed </li>


                  </ul><!-- /legend -->
                </div><!-- /.card-body -->
                <!-- .list-group -->
                <div class="list-group list-group-flush">
                  <!-- .list-group-item -->
                  <div class="list-group-item">
                    <!-- .list-group-item-figure -->
                    <div class="list-group-item-figure">
                      <a href="http://uselooper.com/user-profile.html" class="user-avatar" data-toggle="tooltip" title="" data-original-title="Martha Myers"><img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt=""></a>
                    </div><!-- /.list-group-item-figure -->
                    <!-- .list-group-item-body -->
                    <div class="list-group-item-body">
                      <!-- .progress -->
                      <div class="progress progress-animated bg-transparent rounded-0" data-toggle="tooltip" data-html="true" title=""
                      data-original-title="<div class=&quot;text-left small&quot;>
                        <i class=&quot;fa fa-fw fa-circle text-teal&quot;>
                        </i> 231<br><i class=&quot;fa fa-fw fa-circle text-red&quot;></i> 54
                      </div>">
                        <div class="progress-bar bg-red" role="progressbar" aria-valuenow="73.46140163642832" aria-valuemin="0" aria-valuemax="100" style="width: 73.46140163642832%">
                          <span class="sr-only">73.46140163642832% Complete</span>
                        </div>
                        <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="8.217716115261473" aria-valuemin="0" aria-valuemax="100" style="width: 8.217716115261473%">
                          <span class="sr-only">10.217716115261473% Complete</span>
                        </div>
                      </div><!-- /.progress -->
                    </div><!-- /.list-group-item-body -->
                  </div><!-- /.list-group-item -->
                  <!-- .list-group-item -->
                  <div class="list-group-item">
                    <!-- .list-group-item-figure -->
                    <div class="list-group-item-figure">
                      <a href="http://uselooper.com/user-profile.html" class="user-avatar" data-toggle="tooltip" title="" data-original-title="Tammy Beck"><img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt=""></a>
                    </div><!-- /.list-group-item-figure -->
                    <!-- .list-group-item-body -->
                    <div class="list-group-item-body">
                      <!-- .progress -->
                      <div class="progress progress-animated bg-transparent rounded-0" data-toggle="tooltip" data-html="true" title="" data-original-title="<div class=&quot;text-left small&quot;><i class=&quot;fa fa-fw fa-circle text-teal&quot;></i>
                         406<br><i class=&quot;fa fa-fw fa-circle text-red&quot;></i> 49</div>">
                        <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="54.180855088914115" aria-valuemin="0" aria-valuemax="100" style="width: 54.180855088914115%">
                          <span class="sr-only">54.180855088914115% Complete</span>
                        </div>
                        <div class="progress-bar bg-red" role="progressbar" aria-valuenow="17.361331819901627" aria-valuemin="0" aria-valuemax="100" style="width: 15.361331819901627%">
                          <span class="sr-only">17.361331819901627% Complete</span>
                        </div>

                      </div><!-- /.progress -->
                    </div><!-- /.list-group-item-body -->
                  </div><!-- /.list-group-item -->
                  <!-- .list-group-item -->
                  <div class="list-group-item">
                    <!-- .list-group-item-figure -->
                    <div class="list-group-item-figure">
                      <a href="http://uselooper.com/user-profile.html" class="user-avatar" data-toggle="tooltip" title="" data-original-title="Susan Kelley"><img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt=""></a>
                    </div><!-- /.list-group-item-figure -->
                    <!-- .list-group-item-body -->
                    <div class="list-group-item-body">
                      <!-- .progress -->
                      <div class="progress progress-animated bg-transparent rounded-0" data-toggle="tooltip" data-html="true" title="" data-original-title="<div class=&quot;text-left small&quot;><i class=&quot;fa fa-fw fa-circle text-teal&quot;></i> 1271<br><i class=&quot;fa fa-fw fa-circle text-red&quot;></i> 163</div>">
                        <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="52.13289581624282" aria-valuemin="0" aria-valuemax="100" style="width: 52.13289581624282%">
                          <span class="sr-only">52.13289581624282% Complete</span>
                        </div>
                        <div class="progress-bar bg-red" role="progressbar" aria-valuenow="7.568498769483183" aria-valuemin="0" aria-valuemax="100" style="width: 3.568498769483183%">
                          <span class="sr-only">7.568498769483183% Complete</span>
                        </div>

                      </div><!-- /.progress -->
                    </div><!-- /.list-group-item-body -->
                  </div><!-- /.list-group-item -->
                  <!-- .list-group-item -->
                  <div class="list-group-item">
                    <!-- .list-group-item-figure -->
                    <div class="list-group-item-figure">
                      <a href="http://uselooper.com/user-profile.html" class="user-avatar" data-toggle="tooltip" title="" data-original-title="Albert Newman"><img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt=""></a>
                    </div><!-- /.list-group-item-figure -->
                    <!-- .list-group-item-body -->
                    <div class="list-group-item-body">
                      <!-- .progress -->
                      <div class="progress progress-animated bg-transparent rounded-0" data-toggle="tooltip" data-html="true" title="" data-original-title="<div class=&quot;text-left small&quot;><i class=&quot;fa fa-fw fa-circle text-teal&quot;></i> 1527<br><i class=&quot;fa fa-fw fa-circle text-red&quot;></i> 356</div>">
                        <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="75.18463810930577" aria-valuemin="0" aria-valuemax="100" style="width: 75.18463810930577%">
                          <span class="sr-only">75.18463810930577% Complete</span>
                        </div>
                        <div class="progress-bar bg-red" role="progressbar" aria-valuenow="17.593549975381585" aria-valuemin="0" aria-valuemax="100" style="width: 10.093549975381585%">
                          <span class="sr-only">17.593549975381585% Complete</span>
                        </div>
                      </div><!-- /.progress -->
                    </div><!-- /.list-group-item-body -->
                  </div><!-- /.list-group-item -->
                  <!-- .list-group-item -->
                  <div class="list-group-item">
                    <!-- .list-group-item-figure -->
                    <div class="list-group-item-figure">
                      <a href="http://uselooper.com/user-profile.html" class="user-avatar" data-toggle="tooltip" title="" data-original-title="Kyle Grant"><img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt=""></a>
                    </div><!-- /.list-group-item-figure -->
                    <!-- .list-group-item-body -->
                    <div class="list-group-item-body">
                      <!-- .progress -->
                      <div class="progress progress-animated bg-transparent rounded-0" data-toggle="tooltip" data-html="true" title="" data-original-title="<div class=&quot;text-left small&quot;><i class=&quot;fa fa-fw fa-circle text-teal&quot;></i> 643<br><i class=&quot;fa fa-fw fa-circle text-red&quot;></i> 347</div>">
                        <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="36.89041881812966" aria-valuemin="0" aria-valuemax="100" style="width: 36.89041881812966%">
                          <span class="sr-only">36.89041881812966% Complete</span>
                        </div>
                        <div class="progress-bar bg-red" role="progressbar" aria-valuenow="22.203671830177854" aria-valuemin="0" aria-valuemax="100" style="width: 15.203671830177854%">
                          <span class="sr-only">22.503671830177854% Complete</span>
                        </div>
                      </div><!-- /.progress -->
                    </div><!-- /.list-group-item-body -->
                  </div><!-- /.list-group-item -->
                </div><!-- /.list-group -->
              </section><!-- /.card -->
            </div><!-- /grid column -->
          </div><!-- /grid row -->
          <!-- section-deck -->
          <div class="section-deck">
            <!-- .card -->
            <div class="card card-fluid pb-3">
              <header class="card-header text-uppercase">Active Partners </header><!-- .lits-group -->
              <div class="lits-group list-group-flush">
                <!-- .lits-group-item -->
                <?php   $colors = ['blue', 'indigo', 'purple', 'pink', 'orange','red','green','yellow', 'teal']; ?>
                @foreach($issues as $activepartner)
                  <div class="list-group-item">
                    <?php $index = rand(0, 8); ?>
                    <!-- .lits-group-item-figure -->
                    <div class="list-group-item-figure">
                      <div class="has-badge">
                        <a href="{{route('activepartner')}}?id={{$activepartner->id}}" class="tile tile-md bg-{{$colors[$index]}}">
                          <?php
                                $string = $activepartner->name;
                                $expr = '/(?<=\s|^)[a-z]/i';
                                preg_match_all($expr, $string, $matches);
                                $result = implode('', $matches[0]);
                                $result = strtoupper($result);
                           ?>
                           {{$result}}
                        </a> <a href="#team" class="user-avatar user-avatar-xs"><img src="" alt=""></a>
                      </div>
                    </div><!-- .lits-group-item-figure -->
                    <!-- .lits-group-item-body -->
                    <div class="list-group-item-body">
                      <h5 class="card-title">
                        <a href="{{route('activepartner')}}?id={{$activepartner->id}}" class="text-capitalize">{{$activepartner->name}}</a>
                      </h5>
                      <p class="card-subtitle text-muted mb-1"> Progress in
                          <?php
                            $percentage = ($activepartner->res/$activepartner->tot)*100;
                           ?>
                           {{substr($percentage,0,2)}}
                          %  </p><!-- .progress -->
                      <div class="progress progress-xs bg-transparent">
                        <div class="progress-bar bg-{{$colors[$index]}}" role="progressbar" aria-valuenow="2181" aria-valuemin="0" aria-valuemax="100" style="width: {{$percentage}}%">
                          <span class="sr-only">{{$percentage}}% Complete</span>
                        </div>
                      </div><!-- /.progress -->
                    </div><!-- .lits-group-item-body -->
                  </div><!-- /.lits-group-item -->
                    <?php $index = $index+1; ?>
                @endforeach

              </div><!-- /.lits-group -->
            </div><!-- /.card -->
            <!-- .card -->
            <div class="card card-fluid">
              <header class="card-header"> Active Issues </header><!-- .card-body -->
              <div class="card-body">
                <!-- .todo-list -->
                <div class="todo-list">

                </div>
              </div><!-- /.card-body -->
              <!-- .card-footer -->
              <footer class="card-footer">
                <a href="#!" class="card-footer-item">View all <i class="fa fa-fw fa-angle-right"></i></a>
              </footer><!-- /.card-footer -->
            </div><!-- /.card -->
          </div><!-- /section-deck -->
        </div><!-- /.page-section -->
        <br><br>
      </div><!-- /.page-inner -->
    </div><!-- /.page -->
  </div><!-- .app-footer -->

  <!-- /.wrapper -->

@endsection
