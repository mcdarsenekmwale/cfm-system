@extends('admin.master.master')

@section('content')

<div class="wrapper">
          <!-- .page -->
          <div class="page">
            <!-- .page-inner -->
            <div class="page-inner">
              <!-- .page-title-bar -->
              <header class="page-title-bar">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                      <a href="#!"><i class="breadcrumb-icon fa fa-angle-left mr-2"></i>Collections</a>
                    </li>
                  </ol>
                </nav>
                <h5 class="page-title mr-sm-auto">Case Mapping and Heatmaps</h5>
                <p class="text-muted">. </p>
              </header><!-- /.page-title-bar -->
              <!-- .page-section -->
              <div class="page-section">
                <!-- grid row -->
                <div class="row">
                  <!-- grid column -->
                  <div class="col-lg-6">
                    <!-- .card -->
                    <section class="card card-fluid">
                      <!-- .card-body -->
                      <div class="card-body">
                        <h3 class="card-title"> World </h3>
                      </div><!-- /.card-body -->
                      <div id="mapworld" style="height: 300px; position: relative; overflow: hidden;">
                    </section><!-- /.card -->
                  </div><!-- /grid column -->
                  <!-- grid column -->
                  <div class="col-lg-6">
                    <!-- .card -->
                    <section class="card card-fluid">
                      <!-- .card-body -->
                      <div class="card-body">
                        <h3 class="card-title"> Africa </h3>
                      </div>
                      <div class="" id="africamap" style="height: 300px; position: relative; overflow: hidden;">

                      </div>

                      </div>
                    </section><!-- /.card -->
                  </div><!-- /grid column -->
                  <!-- grid column -->
                  <div class="col-lg-6">
                    <!-- .card -->
                    <section class="card card-fluid">
                      <!-- .card-body -->
                      <div class="card-body">
                        <h3 class="card-title"> Malawi </h3>
                      </div>
                    <div id="chart-container" style="height: 400px; position: relative; overflow: hidden;"></div>
                    </section><!-- /.card -->
                  </div><!-- /grid column -->
                  <!-- grid column -->
                  <div class="col-lg-6">
                    <!-- .card -->
                    <section class="card card-fluid">
                      <!-- .card-body -->
                      <div class="card-body">
                        <h3 class="card-title"> chart </h3>

                      </div>

                    </section><!-- /.card -->
                  </div><!-- /grid column -->


                </div><!-- /grid row -->
              </div><!-- /.page-section -->
            </div><!-- /.page-inner -->
          </div><!-- /.page -->
          <br><br>
        </div>
@endsection
