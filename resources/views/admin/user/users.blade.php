@extends('admin.master.master')

@section('content')

<div class="wrapper">
          <!-- .page -->
          <div class="page">
            <!-- .page-cover -->
            <header class="page-cover">
              <div class="text-center">
                <a href="{{route('admin.profile')}}" class="user-avatar user-avatar-xl"><img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt=""></a>
                <h2 class="h4 mt-2 mb-0"> {{Auth::user()->firstname}} {{Auth::user()->lastname}}</h2>
                <div class="my-1">
                  @if(Auth::user()->username == 'McDarsenek')
                    <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i>
                  @else
                  <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i> <i class="fa fa-star-o text-yellow"></i> <i class="fa fa-star-o text-yellow"></i> <i class="fa fa-star-o text-yellow"></i>
                  @endif
                </div>
                <p class="text-muted"> Administrator </p>
                <p>  </p>
              </div><!-- .cover-controls --><!-- /.cover-controls -->
              <div class="cover-controls cover-controls-bottom">
                <a href="#!" class="btn btn-light" data-toggle="modal" data-target="#RequestModal"><span class="fa fa-fw fa-bell pulse"></span> 2,159 Notifications</a>
                 <a href="#!" class="btn btn-light" data-toggle="modal" data-target="#MessagesModal"><span class="fa fa-fw fa-envelope pulse"></span>136 Messages</a>
              </div><!-- /.cover-controls -->
            </header><!-- /.page-cover -->
            <!-- Followers Modal -->
            <!-- .modal -->
            <div class="modal fade " id="RequestModal" tabindex="-1" role="dialog" aria-labelledby="RequestModalLabel" aria-hidden="true">
              <!-- .modal-dialog -->
              <div class="modal-dialog modal-dialog-vert" role="document">
                <!-- .modal-content -->
                <div class="modal-content">
                  <!-- .modal-header -->
                  <div class="modal-header">
                    <h6 class="modal-title"> Followers </h6>
                  </div><!-- /.modal-header -->
                  <!-- .modal-body -->
                  <div class="modal-body px-0">
                    <!-- .list-group -->
                    <div class="list-group list-group-flush list-group-divider border">

                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces7.jpg" alt="Julia Silva"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Julia Silva</a>
                          </h4>
                          <p class="list-group-item-text"> Photographer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button type="button" class="btn btn-sm btn-primary">Follow</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces1.jpg" alt="Joe Hanson"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Joe Hanson</a>
                          </h4>
                          <p class="list-group-item-text"> Logistician </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button type="button" class="btn btn-sm btn-secondary">Follow</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces11.jpg" alt="Brenda Griffin"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Brenda Griffin</a>
                          </h4>
                          <p class="list-group-item-text"> Medical Assistant </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button type="button" class="btn btn-sm btn-primary">Follow</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces12.jpg" alt="Ryan Jimenez"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Ryan Jimenez</a>
                          </h4>
                          <p class="list-group-item-text"> Photographer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button type="button" class="btn btn-sm btn-primary">Follow</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces13.jpg" alt="Bryan Hayes"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Bryan Hayes</a>
                          </h4>
                          <p class="list-group-item-text"> Computer Systems Analyst </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button type="button" class="btn btn-sm btn-primary">Follow</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces14.jpg" alt="Cynthia Clark"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Cynthia Clark</a>
                          </h4>
                          <p class="list-group-item-text"> Web Developer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button type="button" class="btn btn-sm btn-primary">Follow</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->


                    </div><!-- /.list-group -->
                    <!-- .loading -->
                    <div class="loading border-bottom">
                      <div class="loader loader-sm fa-spin"></div>
                      <div class="sr-only"> Loading more content </div>
                    </div><!-- /.loading -->
                  </div><!-- /.modal-body -->
                  <!-- .modal-footer -->
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger btn-sm" data-dismiss="modal">Close</button>
                  </div><!-- /.modal-footer -->
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <!-- /Followers Modal -->
            <!-- .modal -->
            <div class="modal fade " id="MessagesModal" tabindex="-1" role="dialog" aria-labelledby="MessagesModalLabel" aria-hidden="true">
              <!-- .modal-dialog -->
              <div class="modal-dialog modal-dialog-vert" role="document">
                <!-- .modal-content -->
                <div class="modal-content">
                  <!-- .modal-header -->
                  <div class="modal-header">
                    <h6 class="modal-title"> Messages </h6>
                  </div><!-- /.modal-header -->
                  <!-- .modal-body -->
                  <div class="modal-body px-0">
                    <!-- .list-group -->
                    <div class="list-group list-group-flush list-group-divider border">
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces2.jpg" alt="Johnny Day"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Johnny Day</a>
                          </h4>
                          <p class="list-group-item-text"> Computer Hardware Engineer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button class="btn btn-sm btn-secondary">Following</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces3.jpg" alt="Sarah Bishop"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Sarah Bishop</a>
                          </h4>
                          <p class="list-group-item-text"> Designer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button class="btn btn-sm btn-secondary">Following</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces13.jpg" alt="Bryan Hayes"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Bryan Hayes</a>
                          </h4>
                          <p class="list-group-item-text"> Computer Systems Analyst </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button class="btn btn-sm btn-secondary">Following</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces14.jpg" alt="Cynthia Clark"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Cynthia Clark</a>
                          </h4>
                          <p class="list-group-item-text"> Web Developer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button class="btn btn-sm btn-secondary">Following</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces6.jpg" alt="Martha Myers"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Martha Myers</a>
                          </h4>
                          <p class="list-group-item-text"> Writer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button class="btn btn-sm btn-secondary">Following</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces5.jpg" alt="Tammy Beck"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Tammy Beck</a>
                          </h4>
                          <p class="list-group-item-text"> Designer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button class="btn btn-sm btn-secondary">Following</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces7.jpg" alt="Susan Kelley"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Susan Kelley</a>
                          </h4>
                          <p class="list-group-item-text"> Web Developer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button class="btn btn-sm btn-secondary">Following</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces1.jpg" alt="Albert Newman"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Albert Newman</a>
                          </h4>
                          <p class="list-group-item-text"> Web Developer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button class="btn btn-sm btn-secondary">Following</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces9.jpg" alt="Kyle Grant"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Kyle Grant</a>
                          </h4>
                          <p class="list-group-item-text"> Designer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button class="btn btn-sm btn-secondary">Following</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                    </div><!-- /.list-group -->
                    <!-- .loading -->
                    <div class="loading border-bottom">
                      <div class="loader loader-sm"></div>
                      <div class="sr-only"> Loading more content </div>
                    </div><!-- /.loading -->
                  </div><!-- /.modal-body -->
                  <!-- .modal-footer -->
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger btn-sm" data-dismiss="modal">Close</button>
                  </div><!-- /.modal-footer -->
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <!-- /Following Modal -->
            <nav class="page-navs">
              <!-- .nav-scroller -->
              <div class="nav-scroller">
                <!-- .nav -->
                <div class="nav nav-center nav-tabs">
                  <a class="nav-link" href="{{route('admin.profile')}}">Profile Overview</a>
                  <a class="nav-link " href="javascript: window.location.reload();">Request <span class="badge">16</span></a>
                   <a class="nav-link" href="{{route('partner-list')}}">Partners</a>
                   <a class="nav-link active" href="javascript: window.location.reload();">Users</a>
                   <a class="nav-link" href="{{route('notifications')}}">Notifications</a>
                   <a class="nav-link " href="{{route('account')}}">Settings</a>
                </div><!-- /.nav -->
              </div><!-- /.nav-scroller -->
            </nav><!-- /.page-navs -->
            <!-- .page-inner -->
            <!-- .page-inner -->
            <div class="page-inner">
              <!-- .page-title-bar -->
              <header class="page-title-bar">
                <!-- .breadcrumb -->

                <div class="d-md-flex align-items-md-start  ">
                  <div class="small text-muted text-center page-title mr-sm-auto">
                  </div>
                  <div class="btn-toolbar">
                    <button type="button" class="btn btn-light"><i class="oi oi-data-transfer-download"></i> <span class="ml-1">Export</span></button> <button type="button" class="btn btn-light"><i class="oi oi-data-transfer-upload"></i> <span class="ml-1">Import</span></button>
                    <div class="dropdown">
                      <button type="button" class="btn btn-light" data-toggle="dropdown"><span>More</span> <span class="fa fa-caret-down"></span></button>
                      <div class="dropdown-arrow dropdown-arrow-right"></div>
                      <div class="dropdown-menu dropdown-menu-right">
                        <a href="{{route('newuser')}}" class="dropdown-item">Add User</a>
                         <a href="#!" class="dropdown-item">Check Cases</a>
                        <div class="dropdown-divider"></div><a href="#!" class="dropdown-item">Share</a> <a href="#!" class="dropdown-item">Archive</a> <a href="#!" class="dropdown-item">Remove</a>
                      </div>
                    </div>
                  </div><!-- /.btn-toolbar -->
                </div><!-- /title and toolbar -->
                <!-- floating action -->
                <button type="button"id="newuser" class="btn btn-success btn-floated"><span class="fa fa-plus"></span></button> <!-- /floating action -->
                <!-- title and toolbar -->

              </header><!-- /.page-title-bar -->
              <!-- .page-section -->
              <div class="page-section">
                <!-- .card -->
                <div class="col-xl-12">
                  <!-- .card -->
                  <section class="card card-fluid">
                    <!-- .card-header -->
                    <header class="card-header border-0">
                      <div class="d-flex align-items-center">
                        <span class="mr-auto">All Users</span> <button type="button" class="btn btn-icon btn-light"><i class="fa fa-copy" title="copy"></i></button> <button type="button" class="btn btn-icon btn-light"><i class="fa fa-download" title="download"></i></button>
                      </div>
                    </header><!-- /.card-header -->
                    <div class="text-muted px-3"> Showing 1 to {!!$data['users']->count()!!} of {!!$data['users']->total()!!} users </div>
                    <!-- .table-responsive -->
                    <div class="table-responsive">
                      <!-- .table -->
                      <table class="table table-sm mb-0">
                        <!-- thead -->
                        <thead class="thead-light">
                          <tr>
                            <th style="min-width:200px">
                              <a href="#" class="text-muted sort small" data-sort="goal-name">
                              Name
                              </a>
                            </th>
                            <th style="min-width:200px">
                              <a href="#" class="text-muted sort small" data-sort="goal-username">
                              Username
                              </a>
                            </th>
                            <th>
                              <a href="#" class="text-muted sort small" data-sort="goal-email">
                              Email
                              </a>
                             </th>
                             <th>
                               <a href="#" class="text-muted sort small" data-sort="goal-type">
                               Type
                               </a>
                              </th>
                            <th>
                              <a href="#" class="text-muted sort small" data-sort="goal-status">
                               Status
                             </a>
                             </th>
                             <th style="width:150px; min-width:100px;"> &nbsp; </th>
                          </tr>
                        </thead><!-- /thead -->
                        <!-- tbody -->
                        <tbody>
                          @if($data['users']->total() == 0)
                          <tr class="card-body" style="height:10rem; width:56rem;"></tr>
                          <tr class="alert  align-items-center" role="alert " >
                            <td class="alert-text text-center " colspan="3" >
                              No recent partner with a case data yet
                              <b>{{Auth::user()->firstname}}  {{Auth::user()->lastname}} </b>
                            </td>
                          </tr>
                          <tr class="card-body" style="height:10.7rem; width:56rem;"></tr>
                          @else
                            @foreach($users as $user)
                                <!-- tr -->
                                <tr class="hoverable">
                                  <td> {{$user->firstname}} {{$user->lastname}} </td>
                                  <td> {{$user->username}} </td>
                                  <td> {{$user->email}} </td>
                                  <td> {{$user->type}} </td>
                                  <td>
                                    @if($user->status=='active')
                                      <span class="text-success">●</span> active
                                    @else
                                      <span class="text-secondary">●</span> inactive
                                    @endif
                                  </td>

                                  <td class="align-middle text-right">
                                    <a href="/userEdit?id={{$user->id}}" data-num1="{{$user->id}}" class="btn btn-sm btn-icon btn-success btn-sm"><i class="fa fa-pencil" title="edit"></i> <span class="sr-only">Edit</span></a>
                                    <a href="/userEdit?id={{$user->id}}" data-num="{{$user->id}}" class="btn btn-sm btn-icon btn-danger btn-sm"><i class="fa fa-trash" title="delete"></i> <span class="sr-only">Remove</span></a>
                                  </td>
                                </tr><!-- /tr -->
                                <!-- tr -->
                            @endforeach

                          @endif
                        </tbody><!-- /tbody -->
                      </table><!-- /.table -->
                    </div><!-- /.table-responsive -->
                    <div class="card-body">
                      <div class="row">
                        <b class="col-11"></b>
                        <b class="col-1 float-right" >
                          <a class="text-muted " href="{!! $data['users']->url($data['users']->currentPage()-1)!!}" aria-label="Previous">
                            <span class="fa fa-chevron-circle-left fa-lg"></span>
                            <span class="sr-only">Previous</span>
                          </a>

                          <a class="text-muted " href="{!! $data['users']->url($data['users']->currentPage()+1)!!}" aria-label="Next">
                            <span class="fa fa-chevron-circle-right fa-lg"></span>
                            <span class="sr-only">Next</span>
                          </a>
                        </b>
                      </div>

                    </div>
                  </section><!-- /.card -->
                </div><!-- /grid column -->
              </div><!-- /.page-section -->
              <br><br><br>
            </div><!-- /.page-inner -->
          </div><!-- /.page -->
        </div>
@endsection
