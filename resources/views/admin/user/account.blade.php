@extends('admin.master.master')

@section('content')
<div class="wrapper">
          <!-- .page -->
          <div class="page">
            <!-- .page-cover -->
            <header class="page-cover">
              <div class="text-center">
                <a href="{{route('admin.profile')}}" class="user-avatar user-avatar-xl"><img src="http://uselooper.com/assets/images/avatars/uifaces9.jpg" alt=""></a>
                <h2 class="h4 mt-2 mb-0"> {{Auth::user()->firstname}} {{Auth::user()->lastname}}</h2>
                <div class="my-1">
                  @if(Auth::user()->username == 'McDarsenek')
                    <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i>
                  @else
                  <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i> <i class="fa fa-star-o text-yellow"></i> <i class="fa fa-star-o text-yellow"></i> <i class="fa fa-star-o text-yellow"></i>
                  @endif
                </div>
                <p class="text-muted"> Administrator </p>
                <p>  </p>
              </div><!-- .cover-controls --><!-- /.cover-controls -->
              <div class="cover-controls cover-controls-bottom">
                <a href="#!" class="btn btn-light" data-toggle="modal" data-target="#OpenCasesModal">
                  @if($totalOpenCases !== 0)
                      {{$totalOpenCases}} Open Cases
                      </a>
                  @else
                      {{$totalOpenCases}} Open Cases
                      </a>
                  @endif
                </a>
                <a href="#!" class="btn btn-light" data-toggle="modal" data-target="#CosedCasesModal">
                  @if($totalClosedCases != 0)
                          {{$totalClosedCases}} Closed Cases
                  @else
                          {{$totalClosedCases}} Closed Cases
                  @endif
                </a>
              </div><!-- /.cover-controls -->
            </header><!-- /.page-cover -->
            <!-- Followers Modal -->
            <!-- .modal -->
            <div class="modal fade fixed-left" id="OpenCasesModal" tabindex="-1" role="dialog" aria-labelledby="OpenCasesModalLabel" aria-hidden="true">
              <!-- .modal-dialog -->
              <div class="modal-dialog modal-dialog-vert" role="document">
                <!-- .modal-content -->
                <div class="modal-content">
                  <!-- .modal-header -->
                  <div class="modal-header">
                    <h6 class="modal-title"> Open Cases </h6>
                  </div><!-- /.modal-header -->
                  <!-- .modal-body -->
                  <div class="modal-body px-0">
                    <!-- .list-group -->
                    <div class="list-group list-group-flush list-group-divider border">
                      <!-- .list-group-item -->
                      @foreach($latestOpenCases  as $case)
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt=""></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">{{$case->client}} </a>
                          </h4>
                          <p class="list-group-item-text"> {{ substr(trim($case->comments) , 0, 35)}} </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a type="button" href="/viewCases/?status=1" class="btn btn-sm btn-primary">Open</a>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      @endforeach
                    </div><!-- /.list-group -->
                    <!-- .loading -->
                    <div class="loading border-bottom">
                      <div class="loader loader-sm"></div>
                      <div class="sr-only"> Loading more content </div>

                    </div><!-- /.loading -->
                  </div><!-- /.modal-body -->
                  <!-- .modal-footer -->
                  <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                  </div><!-- /.modal-footer -->
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <!-- /Followers Modal -->
            <!-- Following Modal -->
            <!-- .modal -->
            <div class="modal fade fixed-left" id="CosedCasesModal" tabindex="-1" role="dialog" aria-labelledby="CosedCasesModalLabel" aria-hidden="true">
              <!-- .modal-dialog -->
              <div class="modal-dialog modal-dialog-vert" role="document">
                <!-- .modal-content -->
                <div class="modal-content">
                  <!-- .modal-header -->
                  <div class="modal-header">
                    <h6 class="modal-title"> Closed Cases </h6>
                  </div><!-- /.modal-header -->
                  <!-- .modal-body -->
                  <div class="modal-body px-0">
                    <!-- .list-group -->
                    <div class="list-group list-group-flush list-group-divider border">
                      <!-- .list-group-item -->
                      @foreach($latestClosedCases  as $case)
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt=""></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">{{Auth::user()->firstname}} </a>
                          </h4>
                          <p class="list-group-item-text"> {{ substr(trim($case->comments) , 0, 35)}} </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="/viewCases/?status=2" type="button" class="btn btn-sm btn-primary">Open</a>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      @endforeach
                    </div><!-- /.list-group -->
                    <!-- .loading -->
                    <div class="loading border-bottom">
                      <div class="loader loader-sm"></div>
                      <div class="sr-only"> Loading more content </div>

                    </div><!-- /.loading -->
                  </div><!-- /.modal-body -->
                  <!-- .modal-footer -->
                  <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                  </div><!-- /.modal-footer -->
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <!-- /Following Modal -->
            <!-- .page-navs -->
            <nav class="page-navs">
              <!-- .nav-scroller -->
              <div class="nav-scroller">
                <!-- .nav -->
                <div class="nav nav-center nav-tabs">
                  <a class="nav-link" href="{{route('admin.profile')}}">Profile Overview</a>
                  <a class="nav-link " href="{{route('request')}}">Request <span class="badge">16</span></a>
                   <a class="nav-link" href="{{route('partner-list')}}">Partners</a>
                   <a class="nav-link" href="{{route('users')}}">Users</a>
                   <a class="nav-link" href="{{route('notifications')}}">Notifications</a>
                   <a class="nav-link active" href="javascript: window.location.reload();">Settings</a>
                </div><!-- /.nav -->
              </div><!-- /.nav-scroller -->
            </nav><!-- /.page-navs -->
            <!-- .page-inner -->
            <div class="page-inner">
              <!-- .page-title-bar -->
              <header class="page-title-bar">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                      <a href="user-profile.html"><i class="breadcrumb-icon fa fa-angle-left mr-2"></i>Overview</a>
                    </li>
                  </ol>
                </nav>
              </header><!-- /.page-title-bar -->
              <!-- .page-section -->
              <div class="page-section">
                <!-- grid row -->
                <div class="row">
                  <!-- grid column -->
                  <div class="col-lg-4">
                    <!-- .card -->
                    <div class="card card-fluid">
                      <h6 class="card-header"> Your Details </h6><!-- .nav -->
                      <nav class="nav nav-tabs flex-column border-0">
                        <a href="#" class="nav-link active">Account</a>
                        <a href="#" class="nav-link">Roles</a>
                        
                      </nav><!-- /.nav -->
                    </div><!-- /.card -->
                  </div><!-- /grid column -->
                  <!-- grid column -->
                  <div class="col-lg-8">
                    <!-- .card -->
                    <div class="card card-fluid">
                      <h6 class="card-header text-white text-uppercase" style="background-color:#343a40;"> Account </h6><!-- .card-body -->
                      <div class="card-body">
                        <!-- form -->
                        <form method="post"  name="formupdate" target="_parent" action="{{route('updateaccount')}}">
                          <!-- form row -->
                          <div class="form-row">
                            <!-- form column -->
                            <div class="col-md-6 mb-3">
                              <label class="form-control-label text-muted">First Name</label> <input type="text" class="form-control" name="firstname"  value="{{Auth::user()->firstname}}" required="">
                            </div><!-- /form column -->
                            <!-- form column -->
                            <div class="col-md-6 mb-3">
                              <label class="form-control-label text-muted">Last Name</label> <input type="text" class="form-control" name="lastname" value="{{Auth::user()->lastname}}" required="">
                            </div><!-- /form column -->
                          </div><!-- /form row -->
                          <!-- .form-group -->
                          <div class="form-group">
                            <label class="form-control-label text-muted">Email</label> <input type="email" class="form-control" name="email" value="{{Auth::user()->email}}" required="">
                          </div><!-- /.form-group -->
                          <!-- .form-group -->
                          <div class="form-group">
                            <label class="form-control-label text-muted">New Password</label> <input type="password" class="form-control" name="newpassword" value="{{old('password')}}" required="">
                          </div><!-- /.form-group -->
                          <!-- .form-group -->
                          <div class="form-group">
                            <label class="form-control-label text-muted">Username</label> <input type="text" class="form-control" name="username" value="{{Auth::user()->username}}" required="">
                          </div><!-- /.form-group -->
                          <hr>
                          <!-- .form-actions -->
                          <div class="form-actions">
                            <!-- enable submit btn when user type their current password -->
                            <input type="password" class="form-control ml-auto mr-3" name="oldpassword" placeholder="Enter Current Password" required=""> <button type="submit" class="btn btn-outline-success">Update Account</button>
                          </div><!-- /.form-actions -->
                        </form><!-- /form -->
                      </div><!-- /.card-body -->
                    </div><!-- /.card -->
                  </div><!-- /grid column -->
                </div><!-- /grid row -->
              </div><!-- /.page-section -->

            </div><!-- /.page-inner -->
          </div><!-- /.page -->
          <br><br><br><br>
        </div>
    @endsection
