@extends('admin.master.master')

@section('content')
<div class="wrapper">
          <!-- .page -->
          <div class="page">
            <!-- .page-inner -->
            <div class="page-inner">
              <!-- .page-title-bar -->
              <header class="page-title-bar">
                <div class="small text-muted"> <b>NEW PARTNER
                  <span class="fa fa-angle-right fa-lg" style="position: relative; right: -10px;"></span><span class="fa fa-angle-right fa-lg " style="position: relative; right: -10px;"></span> </b>
                  <span class="" style="position:absolute; right: 12em;">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">
                            <a href="{{route('newcall')}}" class="small text-muted active"> <b>New call </b></a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="{{route('newcase')}}" class="small text-muted"> <b>New Case </b></a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="#" class="small text-muted"> <b>New Partner </b></a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="{{route('newuser')}}" class="small text-muted"> <b>New User </b></a>
                        </li>
                    </ol>
                  </span>
                </div>
              </header><!-- /.page-title-bar -->
              <!-- .page-section -->
              <div class="page-section">
                <!-- .section-deck -->
                <div class="section-deck">
                  <!-- .card -->
                  <section class="card card-fluid">
                    <!-- .card-body -->
                    <div class="card-body">
                      <h4 class="card-title"> Select2 </h4>
                      <h6 class="card-subtitle mb-4"> A jQuery based replacement for select boxes. </h6><!-- form -->
                      <form>
                        <!-- .form-group -->
                        <div class="form-group">
                          <label class="control-label" for="select2-single">Single select boxes</label> <select id="select2-single" class="form-control select2-hidden-accessible" data-toggle="select2" data-placeholder="Select a state" data-allow-clear="true" data-select2-id="select2-single" tabindex="-1" aria-hidden="true">

                        <optgroup label="Alaskan/Hawaiian Time Zone">
                          <option value="AK"> Alaska </option>
                          <option value="HI"> Hawaii </option>
                        </optgroup>
                        <optgroup label="Pacific Time Zone">
                          <option value="CA"> California </option>
                          <option value="NV"> Nevada </option>
                          <option value="OR"> Oregon </option>
                          <option value="WA"> Washington </option>
                        </optgroup>
                        <optgroup label="Mountain Time Zone">
                          <option value="AZ"> Arizona </option>
                          <option value="CO"> Colorado </option>
                          <option value="ID"> Idaho </option>
                          <option value="MT"> Montana </option>
                          <option value="NE"> Nebraska </option>
                          <option value="NM"> New Mexico </option>
                          <option value="ND"> North Dakota </option>
                          <option value="UT"> Utah </option>
                          <option value="WY"> Wyoming </option>
                        </optgroup>
                        <optgroup label="Central Time Zone">
                          <option value="AL"> Alabama </option>
                          <option value="AR"> Arkansas </option>
                          <option value="IL"> Illinois </option>
                          <option value="IA"> Iowa </option>
                          <option value="KS"> Kansas </option>
                          <option value="KY"> Kentucky </option>
                          <option value="LA"> Louisiana </option>
                          <option value="MN"> Minnesota </option>
                          <option value="MS"> Mississippi </option>
                          <option value="MO"> Missouri </option>
                          <option value="OK"> Oklahoma </option>
                          <option value="SD"> South Dakota </option>
                          <option value="TX" disabled="disabled"> Texas </option>
                          <option value="TN"> Tennessee </option>
                          <option value="WI"> Wisconsin </option>
                        </optgroup>
                        <optgroup label="Eastern Time Zone">
                          <option value="CT"> Connecticut </option>
                          <option value="DE"> Delaware </option>
                          <option value="FL"> Florida </option>
                          <option value="GA"> Georgia </option>
                          <option value="IN"> Indiana </option>
                          <option value="ME"> Maine </option>
                          <option value="MD"> Maryland </option>
                          <option value="MA"> Massachusetts </option>
                          <option value="MI"> Michigan </option>
                          <option value="NH"> New Hampshire </option>
                          <option value="NJ"> New Jersey </option>
                          <option value="NY"> New York </option>
                          <option value="NC" disabled="disabled"> North Carolina </option>
                          <option value="OH"> Ohio </option>
                          <option value="PA"> Pennsylvania </option>
                          <option value="RI"> Rhode Island </option>
                          <option value="SC"> South Carolina </option>
                          <option value="VT"> Vermont </option>
                          <option value="VA"> Virginia </option>
                          <option value="WV"> West Virginia </option>
                        </optgroup>
                      </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="1" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-select2-single-container"><span class="select2-selection__rendered" id="select2-select2-single-container" role="textbox" aria-readonly="true"><span class="select2-selection__placeholder">Select a state</span></span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                        </div><!-- /.form-group -->
                        <!-- .form-group -->
                        <div class="form-group">
                          <label class="control-label" for="select2-multiple">Multiple select boxes</label> <select id="select2-multiple" class="form-control select2-hidden-accessible" data-toggle="select2" data-placeholder="Select a state" data-maximum-selection-length="4" multiple="" data-select2-id="select2-multiple" tabindex="-1" aria-hidden="true">

                        <optgroup label="Alaskan/Hawaiian Time Zone">
                          <option value="AK"> Alaska </option>
                          <option value="HI"> Hawaii </option>
                        </optgroup>
                        <optgroup label="Pacific Time Zone">
                          <option value="CA"> California </option>
                          <option value="NV"> Nevada </option>
                          <option value="OR"> Oregon </option>
                          <option value="WA"> Washington </option>
                        </optgroup>
                        <optgroup label="Mountain Time Zone">
                          <option value="AZ"> Arizona </option>
                          <option value="CO"> Colorado </option>
                          <option value="ID"> Idaho </option>
                          <option value="MT"> Montana </option>
                          <option value="NE"> Nebraska </option>
                          <option value="NM"> New Mexico </option>
                          <option value="ND"> North Dakota </option>
                          <option value="UT"> Utah </option>
                          <option value="WY"> Wyoming </option>
                        </optgroup>
                        <optgroup label="Central Time Zone">
                          <option value="AL"> Alabama </option>
                          <option value="AR"> Arkansas </option>
                          <option value="IL"> Illinois </option>
                          <option value="IA"> Iowa </option>
                          <option value="KS"> Kansas </option>
                          <option value="KY"> Kentucky </option>
                          <option value="LA"> Louisiana </option>
                          <option value="MN"> Minnesota </option>
                          <option value="MS"> Mississippi </option>
                          <option value="MO"> Missouri </option>
                          <option value="OK"> Oklahoma </option>
                          <option value="SD"> South Dakota </option>
                          <option value="TX" disabled="disabled"> Texas </option>
                          <option value="TN"> Tennessee </option>
                          <option value="WI"> Wisconsin </option>
                        </optgroup>
                        <optgroup label="Eastern Time Zone">
                          <option value="CT"> Connecticut </option>
                          <option value="DE"> Delaware </option>
                          <option value="FL"> Florida </option>
                          <option value="GA"> Georgia </option>
                          <option value="IN"> Indiana </option>
                          <option value="ME"> Maine </option>
                          <option value="MD"> Maryland </option>
                          <option value="MA"> Massachusetts </option>
                          <option value="MI"> Michigan </option>
                          <option value="NH"> New Hampshire </option>
                          <option value="NJ"> New Jersey </option>
                          <option value="NY"> New York </option>
                          <option value="NC" disabled="disabled"> North Carolina </option>
                          <option value="OH"> Ohio </option>
                          <option value="PA"> Pennsylvania </option>
                          <option value="RI"> Rhode Island </option>
                          <option value="SC"> South Carolina </option>
                          <option value="VT"> Vermont </option>
                          <option value="VA"> Virginia </option>
                          <option value="WV"> West Virginia </option>
                        </optgroup>
                      </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--multiple" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="-1"><ul class="select2-selection__rendered"><li class="select2-search select2-search--inline"><input class="select2-search__field" type="search" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="textbox" aria-autocomplete="list" placeholder="Select a state" style="width: 563.6px;"></li></ul></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                        </div><!-- /.form-group -->
                        <!-- .form-group -->
                        <div class="form-group">
                          <label class="control-label" for="select2-data-array">Loading array data</label> <select id="select2-data-array" class="form-control select2-hidden-accessible" data-toggle="select2" data-data="[ {&quot;id&quot;: 0, &quot;text&quot;: &quot;Visa&quot;}, {&quot;id&quot;: 1, &quot;text&quot;: &quot;Discover Card&quot;}, {&quot;id&quot;: 2, &quot;text&quot;: &quot;American Express&quot;}, {&quot;id&quot;: 3, &quot;text&quot;: &quot;MasterCard&quot;}, {&quot;id&quot;: 4, &quot;text&quot;: &quot;American Express&quot;} ]" data-minimum-results-for-search="Infinity" data-select2-id="select2-data-array" tabindex="-1" aria-hidden="true">
                          <option value="0" data-select2-id="3">Visa</option><option value="1" data-select2-id="4">Discover Card</option><option value="2" data-select2-id="5">American Express</option><option value="3" data-select2-id="6">MasterCard</option><option value="4" data-select2-id="7">American Express</option></select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="8" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-select2-data-array-container"><span class="select2-selection__rendered" id="select2-select2-data-array-container" role="textbox" aria-readonly="true" title="Visa">Visa</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                        </div><!-- /.form-group -->
                        <!-- .form-group -->
                        <div class="form-group">
                          <label class="control-label" for="select2-data-remote">Loading remote data</label> <select id="select2-data-remote" class="form-control select2-hidden-accessible" data-select2-id="select2-data-remote" tabindex="-1" aria-hidden="true">
                          </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="159" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-select2-data-remote-container"><span class="select2-selection__rendered" id="select2-select2-data-remote-container" role="textbox" aria-readonly="true"></span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                        </div><!-- /.form-group -->
                        <!-- .form-group -->
                        <div class="form-group">
                          <!-- .control-label -->
                          <label class="control-label" for="select2-tagging">Tagging support</label> <!-- you can put options directly in data attribute -->
                          <select id="select2-tagging" class="form-control select2-hidden-accessible" data-toggle="select2" data-options="{ &quot;tags&quot;: [&quot;SandyBrown&quot;, &quot;GhostWhite&quot;, &quot;LightSalmon&quot;, &quot;Bisque&quot;, &quot;LightSlateGray&quot;, &quot;PaleTurquoise&quot;, &quot;MediumVioletRed&quot;, &quot;LightSteelBlue&quot;, &quot;MidnightBlue&quot;, &quot;Peru&quot;, &quot;CornflowerBlue&quot;, &quot;DimGray&quot;, &quot;LightPink&quot;, &quot;Lime&quot;, &quot;Cornsilk&quot;, &quot;Cyan&quot;, &quot;DeepPink&quot;, &quot;BurlyWood&quot;, &quot;LightBlue&quot;, &quot;Fuchsia&quot;, &quot;LightGoldenRodYellow&quot;, &quot;PaleGoldenRod&quot;, &quot;DarkSalmon&quot;, &quot;Darkorange&quot;, &quot;Orange&quot;, &quot;FloralWhite&quot;, &quot;Ivory&quot;, &quot;Pink&quot;, &quot;Teal&quot;, &quot;Tan&quot;, &quot;LightCoral&quot;, &quot;ForestGreen&quot;, &quot;LimeGreen&quot;, &quot;Chocolate&quot;, &quot;Linen&quot;, &quot;RosyBrown&quot;, &quot;DarkTurquoise&quot;, &quot;DarkOrchid&quot;, &quot;DarkBlue&quot;, &quot;Magenta&quot;, &quot;SeaGreen&quot;, &quot;DarkRed&quot;, &quot;DarkSlateGray&quot;, &quot;SaddleBrown&quot;, &quot;DarkMagenta&quot;, &quot;Gray&quot;, &quot;Azure&quot;, &quot;Black&quot;, &quot;DarkKhaki&quot;, &quot;Lavender&quot;, &quot;Maroon&quot;, &quot;Orchid&quot;, &quot;DarkSeaGreen&quot;, &quot;Gainsboro&quot;, &quot;Brown&quot;, &quot;Khaki&quot;, &quot;MediumSeaGreen&quot;, &quot;LightYellow&quot;, &quot;Salmon&quot;, &quot;MediumTurquoise&quot;, &quot;IndianRed&quot;, &quot;AntiqueWhite&quot;, &quot;SpringGreen&quot;, &quot;MistyRose&quot;, &quot;DarkOliveGreen&quot;, &quot;Thistle&quot;, &quot;Violet&quot;, &quot;Olive&quot;, &quot;Crimson&quot;, &quot;BlanchedAlmond&quot;, &quot;PowderBlue&quot;, &quot;SlateGray&quot;, &quot;LawnGreen&quot;, &quot;MintCream&quot;, &quot;LightGreen&quot;, &quot;LightSkyBlue&quot;, &quot;Yellow&quot;, &quot;Indigo&quot;, &quot;HotPink&quot;, &quot;WhiteSmoke&quot;, &quot;Gold&quot;, &quot;BlueViolet&quot;, &quot;LavenderBlush&quot;, &quot;OliveDrab&quot;, &quot;PeachPuff&quot;, &quot;OldLace&quot;, &quot;GreenYellow&quot;, &quot;Navy&quot;, &quot;Aquamarine&quot;, &quot;DarkSlateBlue&quot;, &quot;Purple&quot;, &quot;PaleGreen&quot;, &quot;SteelBlue&quot;, &quot;Blue&quot;, &quot;Coral&quot;, &quot;PaleVioletRed&quot;, &quot;RoyalBlue&quot;, &quot;Turquoise&quot;, &quot;MediumOrchid&quot;, &quot;Green&quot;, &quot;Sienna&quot;, &quot;DarkGray&quot;, &quot;DodgerBlue&quot;, &quot;SlateBlue&quot;, &quot;LightGray&quot;, &quot;DarkGoldenRod&quot;, &quot;SkyBlue&quot;, &quot;LightSeaGreen&quot;, &quot;GoldenRod&quot;, &quot;Snow&quot;, &quot;YellowGreen&quot;, &quot;CadetBlue&quot;, &quot;PapayaWhip&quot;, &quot;DeepSkyBlue&quot;, &quot;LemonChiffon&quot;, &quot;DimGrey&quot;, &quot;MediumSpringGreen&quot;, &quot;HoneyDew&quot;, &quot;Plum&quot;, &quot;Silver&quot;, &quot;MediumBlue&quot;, &quot;Aqua&quot;, &quot;Chartreuse&quot;, &quot;FireBrick&quot;, &quot;Beige&quot;, &quot;SeaShell&quot;, &quot;Wheat&quot;, &quot;AliceBlue&quot;, &quot;MediumPurple&quot;, &quot;OrangeRed&quot;, &quot;DarkGreen&quot;, &quot;Moccasin&quot;, &quot;NavajoWhite&quot;, &quot;DarkCyan&quot;, &quot;MediumAquaMarine&quot;, &quot;Red&quot;, &quot;DarkViolet&quot;, &quot;LightCyan&quot;, &quot;MediumSlateBlue&quot;], &quot;tokenSeparators&quot;: [&quot;,&quot;, &quot; &quot;] }" multiple="" data-select2-id="select2-tagging" tabindex="-1" aria-hidden="true">
                            <option> White </option>
                            <option selected="selected" data-select2-id="149"> Tomato </option>
                          <option value="SandyBrown" data-select2-id="9">SandyBrown</option><option value="GhostWhite" data-select2-id="10">GhostWhite</option><option value="LightSalmon" data-select2-id="11">LightSalmon</option><option value="Bisque" data-select2-id="12">Bisque</option><option value="LightSlateGray" data-select2-id="13">LightSlateGray</option><option value="PaleTurquoise" data-select2-id="14">PaleTurquoise</option><option value="MediumVioletRed" data-select2-id="15">MediumVioletRed</option><option value="LightSteelBlue" data-select2-id="16">LightSteelBlue</option><option value="MidnightBlue" data-select2-id="17">MidnightBlue</option><option value="Peru" data-select2-id="18">Peru</option><option value="CornflowerBlue" data-select2-id="19">CornflowerBlue</option><option value="DimGray" data-select2-id="20">DimGray</option><option value="LightPink" data-select2-id="21">LightPink</option><option value="Lime" data-select2-id="22">Lime</option><option value="Cornsilk" data-select2-id="23">Cornsilk</option><option value="Cyan" data-select2-id="24">Cyan</option><option value="DeepPink" data-select2-id="25">DeepPink</option><option value="BurlyWood" data-select2-id="26">BurlyWood</option><option value="LightBlue" data-select2-id="27">LightBlue</option><option value="Fuchsia" data-select2-id="28">Fuchsia</option><option value="LightGoldenRodYellow" data-select2-id="29">LightGoldenRodYellow</option><option value="PaleGoldenRod" data-select2-id="30">PaleGoldenRod</option><option value="DarkSalmon" data-select2-id="31">DarkSalmon</option><option value="Darkorange" data-select2-id="32">Darkorange</option><option value="Orange" data-select2-id="33">Orange</option><option value="FloralWhite" data-select2-id="34">FloralWhite</option><option value="Ivory" data-select2-id="35">Ivory</option><option value="Pink" data-select2-id="36">Pink</option><option value="Teal" data-select2-id="37">Teal</option><option value="Tan" data-select2-id="38">Tan</option><option value="LightCoral" data-select2-id="39">LightCoral</option><option value="ForestGreen" data-select2-id="40">ForestGreen</option><option value="LimeGreen" data-select2-id="41">LimeGreen</option><option value="Chocolate" data-select2-id="42">Chocolate</option><option value="Linen" data-select2-id="43">Linen</option><option value="RosyBrown" data-select2-id="44">RosyBrown</option><option value="DarkTurquoise" data-select2-id="45">DarkTurquoise</option><option value="DarkOrchid" data-select2-id="46">DarkOrchid</option><option value="DarkBlue" data-select2-id="47">DarkBlue</option><option value="Magenta" data-select2-id="48">Magenta</option><option value="SeaGreen" data-select2-id="49">SeaGreen</option><option value="DarkRed" data-select2-id="50">DarkRed</option><option value="DarkSlateGray" data-select2-id="51">DarkSlateGray</option><option value="SaddleBrown" data-select2-id="52">SaddleBrown</option><option value="DarkMagenta" data-select2-id="53">DarkMagenta</option><option value="Gray" data-select2-id="54">Gray</option><option value="Azure" data-select2-id="55">Azure</option><option value="Black" data-select2-id="56">Black</option><option value="DarkKhaki" data-select2-id="57">DarkKhaki</option><option value="Lavender" data-select2-id="58">Lavender</option><option value="Maroon" data-select2-id="59">Maroon</option><option value="Orchid" data-select2-id="60">Orchid</option><option value="DarkSeaGreen" data-select2-id="61">DarkSeaGreen</option><option value="Gainsboro" data-select2-id="62">Gainsboro</option><option value="Brown" data-select2-id="63">Brown</option><option value="Khaki" data-select2-id="64">Khaki</option><option value="MediumSeaGreen" data-select2-id="65">MediumSeaGreen</option><option value="LightYellow" data-select2-id="66">LightYellow</option><option value="Salmon" data-select2-id="67">Salmon</option><option value="MediumTurquoise" data-select2-id="68">MediumTurquoise</option><option value="IndianRed" data-select2-id="69">IndianRed</option><option value="AntiqueWhite" data-select2-id="70">AntiqueWhite</option><option value="SpringGreen" data-select2-id="71">SpringGreen</option><option value="MistyRose" data-select2-id="72">MistyRose</option><option value="DarkOliveGreen" data-select2-id="73">DarkOliveGreen</option><option value="Thistle" data-select2-id="74">Thistle</option><option value="Violet" data-select2-id="75">Violet</option><option value="Olive" data-select2-id="76">Olive</option><option value="Crimson" data-select2-id="77">Crimson</option><option value="BlanchedAlmond" data-select2-id="78">BlanchedAlmond</option><option value="PowderBlue" data-select2-id="79">PowderBlue</option><option value="SlateGray" data-select2-id="80">SlateGray</option><option value="LawnGreen" data-select2-id="81">LawnGreen</option><option value="MintCream" data-select2-id="82">MintCream</option><option value="LightGreen" data-select2-id="83">LightGreen</option><option value="LightSkyBlue" data-select2-id="84">LightSkyBlue</option><option value="Yellow" data-select2-id="85">Yellow</option><option value="Indigo" data-select2-id="86">Indigo</option><option value="HotPink" data-select2-id="87">HotPink</option><option value="WhiteSmoke" data-select2-id="88">WhiteSmoke</option><option value="Gold" data-select2-id="89">Gold</option><option value="BlueViolet" data-select2-id="90">BlueViolet</option><option value="LavenderBlush" data-select2-id="91">LavenderBlush</option><option value="OliveDrab" data-select2-id="92">OliveDrab</option><option value="PeachPuff" data-select2-id="93">PeachPuff</option><option value="OldLace" data-select2-id="94">OldLace</option><option value="GreenYellow" data-select2-id="95">GreenYellow</option><option value="Navy" data-select2-id="96">Navy</option><option value="Aquamarine" data-select2-id="97">Aquamarine</option><option value="DarkSlateBlue" data-select2-id="98">DarkSlateBlue</option><option value="Purple" data-select2-id="99">Purple</option><option value="PaleGreen" data-select2-id="100">PaleGreen</option><option value="SteelBlue" data-select2-id="101">SteelBlue</option><option value="Blue" data-select2-id="102">Blue</option><option value="Coral" data-select2-id="103">Coral</option><option value="PaleVioletRed" data-select2-id="104">PaleVioletRed</option><option value="RoyalBlue" data-select2-id="105">RoyalBlue</option><option value="Turquoise" data-select2-id="106">Turquoise</option><option value="MediumOrchid" data-select2-id="107">MediumOrchid</option><option value="Green" data-select2-id="108">Green</option><option value="Sienna" data-select2-id="109">Sienna</option><option value="DarkGray" data-select2-id="110">DarkGray</option><option value="DodgerBlue" data-select2-id="111">DodgerBlue</option><option value="SlateBlue" data-select2-id="112">SlateBlue</option><option value="LightGray" data-select2-id="113">LightGray</option><option value="DarkGoldenRod" data-select2-id="114">DarkGoldenRod</option><option value="SkyBlue" data-select2-id="115">SkyBlue</option><option value="LightSeaGreen" data-select2-id="116">LightSeaGreen</option><option value="GoldenRod" data-select2-id="117">GoldenRod</option><option value="Snow" data-select2-id="118">Snow</option><option value="YellowGreen" data-select2-id="119">YellowGreen</option><option value="CadetBlue" data-select2-id="120">CadetBlue</option><option value="PapayaWhip" data-select2-id="121">PapayaWhip</option><option value="DeepSkyBlue" data-select2-id="122">DeepSkyBlue</option><option value="LemonChiffon" data-select2-id="123">LemonChiffon</option><option value="DimGrey" data-select2-id="124">DimGrey</option><option value="MediumSpringGreen" data-select2-id="125">MediumSpringGreen</option><option value="HoneyDew" data-select2-id="126">HoneyDew</option><option value="Plum" data-select2-id="127">Plum</option><option value="Silver" data-select2-id="128">Silver</option><option value="MediumBlue" data-select2-id="129">MediumBlue</option><option value="Aqua" data-select2-id="130">Aqua</option><option value="Chartreuse" data-select2-id="131">Chartreuse</option><option value="FireBrick" data-select2-id="132">FireBrick</option><option value="Beige" data-select2-id="133">Beige</option><option value="SeaShell" data-select2-id="134">SeaShell</option><option value="Wheat" data-select2-id="135">Wheat</option><option value="AliceBlue" data-select2-id="136">AliceBlue</option><option value="MediumPurple" data-select2-id="137">MediumPurple</option><option value="OrangeRed" data-select2-id="138">OrangeRed</option><option value="DarkGreen" data-select2-id="139">DarkGreen</option><option value="Moccasin" data-select2-id="140">Moccasin</option><option value="NavajoWhite" data-select2-id="141">NavajoWhite</option><option value="DarkCyan" data-select2-id="142">DarkCyan</option><option value="MediumAquaMarine" data-select2-id="143">MediumAquaMarine</option><option value="Red" data-select2-id="144">Red</option><option value="DarkViolet" data-select2-id="145">DarkViolet</option><option value="LightCyan" data-select2-id="146">LightCyan</option><option value="MediumSlateBlue" data-select2-id="147">MediumSlateBlue</option></select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="148" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--multiple" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="-1"><ul class="select2-selection__rendered"><li class="select2-selection__choice" title=" Tomato " data-select2-id="150"><span class="select2-selection__choice__remove" role="presentation">×</span> Tomato </li><li class="select2-search select2-search--inline"><input class="select2-search__field" type="search" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="textbox" aria-autocomplete="list" placeholder="" style="width: 0.75em;"></li></ul></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                        </div><!-- /.form-group -->
                        <!-- .form-group -->
                        <div class="form-group">
                          <label class="control-label">Single select disable</label> <select class="form-control select2-hidden-accessible" data-toggle="select2" data-placeholder="Select a state" disabled="" data-select2-id="151" tabindex="-1" aria-hidden="true">
                          </select><span class="select2 select2-container select2-container--default select2-container--disabled" dir="ltr" data-select2-id="152" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="-1" aria-labelledby="select2-7sr4-container"><span class="select2-selection__rendered" id="select2-7sr4-container" role="textbox" aria-readonly="true"><span class="select2-selection__placeholder">Select a state</span></span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                        </div><!-- /.form-group -->
                        <!-- .form-group -->
                        <div class="form-group">
                          <label class="control-label">Multiple select disable</label> <select class="form-control select2-hidden-accessible" data-toggle="select2" data-placeholder="Select a state" multiple="" disabled="" data-select2-id="153" tabindex="-1" aria-hidden="true">
                            <option selected="selected" data-select2-id="155"> White </option>
                            <option selected="selected" data-select2-id="156"> Tomato </option>
                          </select><span class="select2 select2-container select2-container--default select2-container--disabled" dir="ltr" data-select2-id="154" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--multiple" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="-1"><ul class="select2-selection__rendered"><li class="select2-selection__choice" title=" White " data-select2-id="157"><span class="select2-selection__choice__remove" role="presentation">×</span> White </li><li class="select2-selection__choice" title=" Tomato " data-select2-id="158"><span class="select2-selection__choice__remove" role="presentation">×</span> Tomato </li><li class="select2-search select2-search--inline"><input class="select2-search__field" type="search" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="textbox" aria-autocomplete="list" placeholder="" disabled="" style="width: 0.75em;"></li></ul></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                        </div><!-- /.form-group -->
                      </form><!-- /form -->
                      <!-- default demo source -->
                      <select id="select2-source-states" style="display: none">
                        <optgroup label="Alaskan/Hawaiian Time Zone">
                          <option value="AK"> Alaska </option>
                          <option value="HI"> Hawaii </option>
                        </optgroup>
                        <optgroup label="Pacific Time Zone">
                          <option value="CA"> California </option>
                          <option value="NV"> Nevada </option>
                          <option value="OR"> Oregon </option>
                          <option value="WA"> Washington </option>
                        </optgroup>
                        <optgroup label="Mountain Time Zone">
                          <option value="AZ"> Arizona </option>
                          <option value="CO"> Colorado </option>
                          <option value="ID"> Idaho </option>
                          <option value="MT"> Montana </option>
                          <option value="NE"> Nebraska </option>
                          <option value="NM"> New Mexico </option>
                          <option value="ND"> North Dakota </option>
                          <option value="UT"> Utah </option>
                          <option value="WY"> Wyoming </option>
                        </optgroup>
                        <optgroup label="Central Time Zone">
                          <option value="AL"> Alabama </option>
                          <option value="AR"> Arkansas </option>
                          <option value="IL"> Illinois </option>
                          <option value="IA"> Iowa </option>
                          <option value="KS"> Kansas </option>
                          <option value="KY"> Kentucky </option>
                          <option value="LA"> Louisiana </option>
                          <option value="MN"> Minnesota </option>
                          <option value="MS"> Mississippi </option>
                          <option value="MO"> Missouri </option>
                          <option value="OK"> Oklahoma </option>
                          <option value="SD"> South Dakota </option>
                          <option value="TX" disabled="disabled"> Texas </option>
                          <option value="TN"> Tennessee </option>
                          <option value="WI"> Wisconsin </option>
                        </optgroup>
                        <optgroup label="Eastern Time Zone">
                          <option value="CT"> Connecticut </option>
                          <option value="DE"> Delaware </option>
                          <option value="FL"> Florida </option>
                          <option value="GA"> Georgia </option>
                          <option value="IN"> Indiana </option>
                          <option value="ME"> Maine </option>
                          <option value="MD"> Maryland </option>
                          <option value="MA"> Massachusetts </option>
                          <option value="MI"> Michigan </option>
                          <option value="NH"> New Hampshire </option>
                          <option value="NJ"> New Jersey </option>
                          <option value="NY"> New York </option>
                          <option value="NC" disabled="disabled"> North Carolina </option>
                          <option value="OH"> Ohio </option>
                          <option value="PA"> Pennsylvania </option>
                          <option value="RI"> Rhode Island </option>
                          <option value="SC"> South Carolina </option>
                          <option value="VT"> Vermont </option>
                          <option value="VA"> Virginia </option>
                          <option value="WV"> West Virginia </option>
                        </optgroup>
                      </select> <!-- /default demo source -->
                    </div><!-- /.card-body -->
                  </section><!-- /.card -->
                  <!-- .card -->
                  <section class="card card-fluid">
                    <!-- .card-body -->
                    <div class="card-body">
                      <h4 class="card-title"> Typeahead </h4>
                      <h6 class="card-subtitle mb-4"> A fast and fully-featured autocomplete library. </h6><!-- form -->
                      <form>
                        <!-- .form-group -->
                        <div class="form-group">
                          <label class="control-label" for="the-basics">The Basics</label> <span class="twitter-typeahead" style="position: relative; display: inline-block;"><input type="text" class="form-control tt-hint" readonly="" autocomplete="off" spellcheck="false" tabindex="-1" dir="ltr" style="position: absolute; top: 0px; left: 0px; border-color: transparent; box-shadow: none; opacity: 1; background: none 0% 0% / auto repeat scroll padding-box padding-box rgb(255, 255, 255);"><input id="the-basics" type="text" class="form-control tt-input" placeholder="States of USA" autocomplete="off" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: transparent;"><pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: -apple-system, BlinkMacSystemFont, &quot;Fira Sans&quot;, &quot;Helvetica Neue&quot;, &quot;Apple Color Emoji&quot;, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 400; word-spacing: 0px; letter-spacing: 0px; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre><div class="tt-menu" style="position: absolute; top: 100%; left: 0px; z-index: 100; display: none;"><div class="tt-dataset tt-dataset-states"></div></div></span>
                        </div><!-- /.form-group -->
                        <!-- .form-group -->
                        <div class="form-group">
                          <label class="control-label" for="bloodhound">Bloodhound <i tabindex="0" class="fa fa-info-circle text-gray" data-toggle="tooltip" data-container="body" title="" data-original-title="Suggestion Engine"></i></label> <span class="twitter-typeahead" style="position: relative; display: inline-block;"><input type="text" class="form-control tt-hint" readonly="" autocomplete="off" spellcheck="false" tabindex="-1" dir="ltr" style="position: absolute; top: 0px; left: 0px; border-color: transparent; box-shadow: none; opacity: 1; background: none 0% 0% / auto repeat scroll padding-box padding-box rgb(255, 255, 255);"><input id="bloodhound" type="text" class="form-control tt-input" placeholder="States of USA" autocomplete="off" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: transparent;"><pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: -apple-system, BlinkMacSystemFont, &quot;Fira Sans&quot;, &quot;Helvetica Neue&quot;, &quot;Apple Color Emoji&quot;, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 400; word-spacing: 0px; letter-spacing: 0px; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre><div class="tt-menu" style="position: absolute; top: 100%; left: 0px; z-index: 100; display: none;"><div class="tt-dataset tt-dataset-states"></div></div></span>
                        </div><!-- /.form-group -->
                        <!-- .form-group -->
                        <div class="form-group">
                          <label class="control-label" for="prefetch">Prefetch</label> <span class="twitter-typeahead" style="position: relative; display: inline-block;"><input type="text" class="form-control tt-hint" readonly="" autocomplete="off" spellcheck="false" tabindex="-1" dir="ltr" style="position: absolute; top: 0px; left: 0px; border-color: transparent; box-shadow: none; opacity: 1; background: none 0% 0% / auto repeat scroll padding-box padding-box rgb(255, 255, 255);"><input id="prefetch" type="text" class="form-control tt-input" placeholder="Countries" autocomplete="off" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: transparent;"><pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: -apple-system, BlinkMacSystemFont, &quot;Fira Sans&quot;, &quot;Helvetica Neue&quot;, &quot;Apple Color Emoji&quot;, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 400; word-spacing: 0px; letter-spacing: 0px; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre><div class="tt-menu" style="position: absolute; top: 100%; left: 0px; z-index: 100; display: none;"><div class="tt-dataset tt-dataset-countries"></div></div></span>
                        </div><!-- /.form-group -->
                        <!-- .form-group -->
                        <div class="form-group">
                          <label class="control-label" for="remote">Remote</label> <span class="twitter-typeahead" style="position: relative; display: inline-block;"><input type="text" class="form-control tt-hint" readonly="" autocomplete="off" spellcheck="false" tabindex="-1" dir="ltr" style="position: absolute; top: 0px; left: 0px; border-color: transparent; box-shadow: none; opacity: 1; background: none 0% 0% / auto repeat scroll padding-box padding-box rgb(255, 255, 255);"><input id="remote" type="text" class="form-control tt-input" placeholder="Oscar winners for Best Picture" autocomplete="off" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: transparent;"><pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: -apple-system, BlinkMacSystemFont, &quot;Fira Sans&quot;, &quot;Helvetica Neue&quot;, &quot;Apple Color Emoji&quot;, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 400; word-spacing: 0px; letter-spacing: 0px; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre><div class="tt-menu" style="position: absolute; top: 100%; left: 0px; z-index: 100; display: none;"><div class="tt-dataset tt-dataset-best-pictures"></div></div></span>
                        </div><!-- /.form-group -->
                        <!-- .form-group -->
                        <div class="form-group">
                          <label class="control-label" for="custom-templates">Custom Templates</label> <span class="twitter-typeahead" style="position: relative; display: inline-block;"><input type="text" class="form-control tt-hint" readonly="" autocomplete="off" spellcheck="false" tabindex="-1" dir="ltr" style="position: absolute; top: 0px; left: 0px; border-color: transparent; box-shadow: none; opacity: 1; background: none 0% 0% / auto repeat scroll padding-box padding-box rgb(255, 255, 255);"><input id="custom-templates" type="text" class="form-control tt-input" placeholder="Oscar winners for Best Picture" autocomplete="off" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: transparent;"><pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: -apple-system, BlinkMacSystemFont, &quot;Fira Sans&quot;, &quot;Helvetica Neue&quot;, &quot;Apple Color Emoji&quot;, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 400; word-spacing: 0px; letter-spacing: 0px; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre><div class="tt-menu" style="position: absolute; top: 100%; left: 0px; z-index: 100; display: none;"><div class="tt-dataset tt-dataset-best-pictures"></div></div></span>
                        </div><!-- /.form-group -->
                        <!-- .form-group -->
                        <div class="form-group">
                          <label class="control-label" for="default-suggestions">Default Suggestions</label> <span class="twitter-typeahead" style="position: relative; display: inline-block;"><input type="text" class="form-control tt-hint" readonly="" autocomplete="off" spellcheck="false" tabindex="-1" dir="ltr" style="position: absolute; top: 0px; left: 0px; border-color: transparent; box-shadow: none; opacity: 1; background: none 0% 0% / auto repeat scroll padding-box padding-box rgb(255, 255, 255);"><input id="default-suggestions" type="text" class="form-control tt-input" placeholder="NFL Teams" autocomplete="off" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: transparent;"><pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: -apple-system, BlinkMacSystemFont, &quot;Fira Sans&quot;, &quot;Helvetica Neue&quot;, &quot;Apple Color Emoji&quot;, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 400; word-spacing: 0px; letter-spacing: 0px; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre><div class="tt-menu" style="position: absolute; top: 100%; left: 0px; z-index: 100; display: none;"><div class="tt-dataset tt-dataset-nfl-teams"></div></div></span>
                        </div><!-- /.form-group -->
                        <!-- .form-group -->
                        <div class="form-group">
                          <label class="control-label" for="multiple-datasets">Multiple Datasets</label> <span class="twitter-typeahead" style="position: relative; display: inline-block;"><input type="text" class="form-control tt-hint" readonly="" autocomplete="off" spellcheck="false" tabindex="-1" dir="ltr" style="position: absolute; top: 0px; left: 0px; border-color: transparent; box-shadow: none; opacity: 1; background: none 0% 0% / auto repeat scroll padding-box padding-box rgb(255, 255, 255);"><input id="multiple-datasets" type="text" class="form-control tt-input" placeholder="NBA and NHL teams" autocomplete="off" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: transparent;"><pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: -apple-system, BlinkMacSystemFont, &quot;Fira Sans&quot;, &quot;Helvetica Neue&quot;, &quot;Apple Color Emoji&quot;, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 400; word-spacing: 0px; letter-spacing: 0px; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre><div class="tt-menu" style="position: absolute; top: 100%; left: 0px; z-index: 100; display: none;"><div class="tt-dataset tt-dataset-nba-teams"></div><div class="tt-dataset tt-dataset-nhl-teams"></div></div></span>
                        </div><!-- /.form-group -->
                        <!-- .form-group -->
                        <div class="form-group has-typeahead-scrollable">
                          <label class="control-label" for="scrollable-dropdown-menu">Scrollable Dropdown Menu</label> <span class="twitter-typeahead" style="position: relative; display: inline-block;"><input type="text" class="form-control tt-hint" readonly="" autocomplete="off" spellcheck="false" tabindex="-1" dir="ltr" style="position: absolute; top: 0px; left: 0px; border-color: transparent; box-shadow: none; opacity: 1; background: none 0% 0% / auto repeat scroll padding-box padding-box rgb(255, 255, 255);"><input id="scrollable-dropdown-menu" type="text" class="form-control tt-input" placeholder="Countries" autocomplete="off" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: transparent;"><pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: -apple-system, BlinkMacSystemFont, &quot;Fira Sans&quot;, &quot;Helvetica Neue&quot;, &quot;Apple Color Emoji&quot;, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 400; word-spacing: 0px; letter-spacing: 0px; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre><div class="tt-menu" style="position: absolute; top: 100%; left: 0px; z-index: 100; display: none;"><div class="tt-dataset tt-dataset-countries"></div></div></span>
                        </div><!-- /.form-group -->
                      </form><!-- /form -->
                    </div><!-- /.card-body -->
                  </section><!-- /.card -->
                </div><!-- /.section-deck -->
                <!-- .section-deck -->
                <div class="section-deck">
                  <!-- .card -->
                  <section class="card card-fluid">
                    <!-- .card-body -->
                    <div class="card-body">
                      <h4 class="card-title"> Tribute </h4>
                      <h6 class="card-subtitle mb-4"> A cross-browser @mention engine. </h6><!-- form -->
                      <form>
                        <!-- .form-group -->
                        <div class="form-group">
                          <input type="text" class="form-control" placeholder="Volunteer someone to participate. Start typing with @" autocomplete="off" data-toggle="tribute" data-remote="assets/data/tribute.json" data-tribute="true">
                        </div><!-- /.form-group -->
                        <!-- .form-group -->
                        <div class="form-group">
                          <textarea class="form-control" placeholder="Start typing with @" data-toggle="tribute" data-remote="assets/data/tribute.json" data-item-template="true" data-tribute="true"></textarea>
                        </div><!-- /.form-group -->
                        <!-- .form-group -->
                        <div class="form-group">
                          <div class="border rounded p-3" data-toggle="tribute" data-remote="assets/data/tribute.json" data-item-template="true" data-select-template="true" contenteditable="true" data-tribute="true">
                            <a href="#!" class="mention">@rue74</a> Destroying things is much easier than making them. </div>
                        </div><!-- /.form-group -->
                      </form><!-- /form -->
                    </div><!-- /.card-body -->
                  </section><!-- /.card -->
                  <!-- .card -->
                  <section class="card card-fluid">
                    <!-- .card-body -->
                    <div class="card-body">
                      <h4 class="card-title"> At.js </h4>
                      <h6 class="card-subtitle mb-4"> Add Github like mentions autocomplete to your application. </h6><!-- form -->
                      <form>
                        <!-- .form-group -->
                        <div class="form-group">
                          <textarea id="inputor" class="form-control autosize">At.js, a github-like autocomplete library :s</textarea>
                        </div><!-- /.form-group -->
                        <!-- .form-group -->
                        <div class="form-group">
                          <div id="editable" class="border rounded p-3" contenteditable="true">
                            <p>
                              <strong>And!!</strong> it support <strong style="font-size: 20px">ContentEditable</strong> mode too!! <img src="https://assets-cdn.github.com/images/icons/emoji/smile.png" height="20" width="20"> <img src="https://assets-cdn.github.com/images/icons/emoji/smiley.png" height="20" width="20"> <img src="https://assets-cdn.github.com/images/icons/emoji/coffee.png" height="20" width="20">
                            </p>
                            <p> Try here now! <img src="https://assets-cdn.github.com/images/icons/emoji/point_right.png" height="20" width="20"> <span>emoji :w</span> <span>mention @a</span> <span>issue #</span>
                            </p>
                          </div>
                        </div><!-- /.form-group -->
                      </form><!-- /form -->
                    </div><!-- /.card-body -->
                  </section><!-- /.card -->
                </div><!-- /.section-deck -->

                <br><br>
              </div><!-- /.page-section -->
            </div><!-- /.page-inner -->
          </div><!-- /.page -->
        </div>
@endsection
