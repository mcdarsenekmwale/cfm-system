@extends('admin.master.master')

@section('content')

<div class="wrapper">
          <!-- .page -->
          <div class="page">
            <!-- .page-inner -->
            <div class="page-inner">
              <!-- .page-title-bar -->
              <header class="page-title-bar">
                <!-- .breadcrumb -->

                <div class="d-md-flex align-items-md-start  ">
                  <div class="small text-muted text-center page-title mr-sm-auto">
                  </div>
                  <div class="btn-toolbar">
                    <button type="button" class="btn btn-light"><i class="oi oi-data-transfer-download"></i> <span class="ml-1">Export</span></button> <button type="button" class="btn btn-light"><i class="oi oi-data-transfer-upload"></i> <span class="ml-1">Import</span></button>
                    <div class="dropdown">
                      <button type="button" class="btn btn-light" data-toggle="dropdown"><span>More</span> <span class="fa fa-caret-down"></span></button>
                      <div class="dropdown-arrow dropdown-arrow-right"></div>
                      <div class="dropdown-menu dropdown-menu-right">
                        <a href="{{route('newpartner')}}" class="dropdown-item">Add Partners</a>
                         <a href="#!" class="dropdown-item">Check Cases</a>
                        <div class="dropdown-divider"></div><a href="#!" class="dropdown-item">Share</a> <a href="#!" class="dropdown-item">Archive</a> <a href="#!" class="dropdown-item">Remove</a>
                      </div>
                    </div>
                  </div><!-- /.btn-toolbar -->
                </div><!-- /title and toolbar -->
                <!-- floating action -->
                <button type="button" class="btn btn-success btn-floated"><span class="fa fa-plus"></span></button> <!-- /floating action -->
                <!-- title and toolbar -->

              </header><!-- /.page-title-bar -->
              <!-- .page-section -->
              <div class="page-section">
                <!-- .card -->
                <section class="card card-fluid">
                  <!-- .card-header -->
                  <header class="card-header">
                    <!-- .nav-tabs -->
                    <div class="row align-items-center">
                      <div class="col">

                        <!-- Title -->
                        <h5 class="card-header-title">
                          <span class="card-title text-uppercase text-muted mb-2"> Partners</span>
                        </h5>

                      </div>
                      <div class="col-auto">

                        <!-- Link -->
                        <a href="#!" class=" text-muted">
                          <span class="small">View all</span>
                        </a>
                      </div>
                    </div> <!-- / .row -->
                  </header><!-- /.card-header -->
                  <!-- .card-body -->
                  <div class="card-body">
                    <!-- .form-group -->
                    <div class="form-group">
                      <!-- .input-group -->
                      <div class="input-group input-group-alt">
                        <!-- .input-group-prepend -->
                        <div class="input-group-prepend">
                          <select class="custom-select">
                            <option selected=""> Filter By </option>
                            <option value="1"> name </option>
                            <option value="2"> status </option>
                            <option value="3"> Tags </option>

                          </select>
                        </div><!-- /.input-group-prepend -->
                        <!-- .input-group -->
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><span class="fa fa-magnifying-glass"></span></span>
                          </div><input type="text" class="form-control" placeholder="Search record">
                        </div><!-- /.input-group -->
                      </div><!-- /.input-group -->
                    </div><!-- /.form-group -->
                    <!-- .table-responsive -->
                    <div class="text-muted"> Showing 1 to {!!$data['issues']->count()!!} of {!!$data['issues']->total()!!} partners </div>
                    <div class="table-responsive">
                      <!-- .table -->
                      <table class="table table-sm table-nowrap card-table table-hover" >
        								<thead class="thead bg-dark">

                          <th>
                          </th>
                            <th >
                              <a href="#" class="text-muted sort small" data-sort="goal-partner">
                              PARTNER
                            </a>
                          </th>
                            <th class="text-center">
                              <a href="#" class="text-muted sort small" data-sort="goal-project">
                                Unreferred
                              </a>
                            </th>
                            <th class="text-center">
                              <a href="#" class="text-muted sort small" data-sort="goal-status">
                                Referred
                              </a>
                            </th>

                            <th class="text-center">
                              <a href="#" class="text-muted sort small" data-sort="goal-date">
                                Resolved
                              </a>
                            </th>
                            <th style="width:150px min-width:100px;"> &nbsp; </th>
                          </tr>
                        </thead><!-- /thead -->
                        <!-- tbody -->
                        <tbody>
                          <!-- tr -->
                          @if($data['issues']->total() == 0)
                          <tr class="card-body" style="height:10rem; width:56rem;"></tr>
                          <tr class="alert  align-items-center" role="alert " >
                            <td class="alert-text text-center " colspan="3" >
                              No recent partner with a case data yet
                              <b>{{Auth::user()->username}}  </b>
                            </td>
                          </tr>
                          <tr class="card-body" style="height:10.7rem; width:56rem;"></tr>
                          @else
                          @foreach($issues as $issue)
                          <tr class="hoverable" data-id="{{$issue->id}}"  id="viewcasedetails">
                              <td class="align-middle col-checker">
                                <div class="custom-control custom-control-nolabel custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" name="selectedRow[]" id="p3"> <label class="custom-control-label" for="p3"></label>
                                </div>
                              </td>
                              <td class="align-middle "> <b>{{$issue->name}}</b> </td>
                              <td class="align-middle text-center"> {{$issue->unref}} </td>
                              <td class="align-middle text-center"> {{$issue->ref}} </td>
                              <td class="align-middle text-center"> {{$issue->res}} </td>
                              <td class="align-middle text-right">
                                <a href="/issueEdit?id={{$issue->id}}" data-num1="{{$issue->id}}" class="btn btn-sm btn-icon btn-success"><i class="fa fa-pencil" title="edit"></i> <span class="sr-only">Edit</span></a>
                                <a href="/issueEdit?id={{$issue->id}}" data-num="{{$issue->id}}" class="btn btn-sm btn-icon btn-danger"><i class="fa fa-trash" title="delete"></i> <span class="sr-only">Remove</span></a>
                              </td>
                          </tr>
                          @endforeach

                          @endif
                          <!-- tr -->
                          </tbody><!-- /tbody -->
                        </table><!-- /.table -->
                      </div><!-- /.table-responsive -->

                      <div class="card-body">
                        <div class="row">
                          <b class="col-11"></b>
                          <b class="col-1 float-right" >
                            <a class="text-muted " href="{!! $data['issues']->url($data['issues']->currentPage()-1)!!}" aria-label="Previous">
                              <span class="fa fa-chevron-circle-left fa-lg"></span>
                              <span class="sr-only">Previous</span>
                            </a>

                            <a class="text-muted " href="{!! $data['issues']->url($data['issues']->currentPage()+1)!!}" aria-label="Next">
                              <span class="fa fa-chevron-circle-right fa-lg"></span>
                              <span class="sr-only">Next</span>
                            </a>
                          </b>
                        </div>

                      </div>
                    </section><!-- /.card -->
                  </div><!-- /grid column -->
                </div><!-- /grid row -->
              </div><!-- /.page-section -->
              <br><br><br>
            </div><!-- /.page-inner -->
          </div><!-- /.page -->
        </div>
@endsection
