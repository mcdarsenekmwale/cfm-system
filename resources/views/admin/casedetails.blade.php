@extends('admin.master.master')
@section('content')
<div class="wrapper py-2">
  <!-- .page -->
  <div class="page">
    <!-- .page-inner -->
    <div class="page-inner">
      <!-- .page-title-bar -->
      <header class="page-cover">
        <div class="text-center">
          <a href="{{route('admin.profile')}}" class="user-avatar user-avatar-xl"><img src="http://uselooper.com/assets/images/avatars/uifaces9.jpg" alt=""></a>
          <h2 class="h4 mt-2 mb-0"> {{Auth::user()->firstname}} {{Auth::user()->lastname}}</h2>
          <div class="my-1">
            @if(Auth::user()->username == 'McDarsenek')
              <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i>
            @else
            <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i> <i class="fa fa-star-o text-yellow"></i> <i class="fa fa-star-o text-yellow"></i> <i class="fa fa-star-o text-yellow"></i>
            @endif
          </div>
          <p class="text-muted"> Administrator </p>
          <p>  </p>
        </div><!-- .cover-controls --><!-- /.cover-controls -->
        <div class="cover-controls cover-controls-bottom">
          <a href="#!" class="btn btn-light" $callDetails-toggle="modal" $callDetails-target="#OpenCasesModal">
            @if($totalOpenCases !== 0)
                {{$totalOpenCases}} Open Cases
                </a>
            @else
                {{$totalOpenCases}} Open Cases
                </a>
            @endif
          </a>
          <a href="#!" class="btn btn-light" $callDetails-toggle="modal" $callDetails-target="#CosedCasesModal">
            @if($totalClosedCases != 0)
                    {{$totalClosedCases}} Closed Cases
            @else
                    {{$totalClosedCases}} Closed Cases
            @endif
          </a>
        </div><!-- /.cover-controls -->
      </header><!-- /.page-cover -->
      <!-- Followers Modal -->
      <!-- .modal -->
      <div class="modal fade fixed-left" id="OpenCasesModal" tabindex="-1" role="dialog" aria-labelledby="OpenCasesModalLabel" aria-hidden="true">
        <!-- .modal-dialog -->
        <div class="modal-dialog modal-dialog-vert" role="document">
          <!-- .modal-content -->
          <div class="modal-content">
            <!-- .modal-header -->
            <div class="modal-header" style="background-color:#343a40;">
              <h6 class="modal-title"> <span class=" card-title text-center text-uppercase text-white mb-2 "> <b>Open Cases</b></span> </h6>
            </div><!-- /.modal-header -->
            <!-- .modal-body -->
            <div class="modal-body px-0">
              <!-- .list-group -->
              <div class="list-group list-group-flush list-group-divider border">
                <!-- .list-group-item -->
                @foreach($latestOpenCases  as $case)
                <div class="list-group-item">
                  <!-- .list-group-item-figure -->
                  <div class="list-group-item-figure">
                    <a href="#!" class="user-avatar"><img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt=""></a>
                  </div><!-- /.list-group-item-figure -->
                  <!-- .list-group-item-body -->
                  <div class="list-group-item-body">
                    <h4 class="list-group-item-title">
                      <a href="#!">{{$case->client}} </a>
                    </h4>
                    <p class="list-group-item-text"> {{ substr(trim($case->comments) , 0, 55)}} </p>
                  </div><!-- /.list-group-item-body -->
                  <!-- .list-group-item-figure -->
                  <div class="list-group-item-figure">
                    <button type="button" onclick="javascript:window.location.href='/viewCases/?status=1?id={{$case->id}}';" class="btn btn-sm btn-primary">Open</button>
                  </div><!-- /.list-group-item-figure -->
                </div><!-- /.list-group-item -->
                @endforeach
              </div><!-- /.list-group -->
              <!-- .loading -->
              <div class="loading border-bottom">
                <div class="loader loader-sm"></div>
                <div class="sr-only"> Loading more content </div>

              </div><!-- /.loading -->
            </div><!-- /.modal-body -->
            <!-- .modal-footer -->
            <div class="modal-footer">
              <button type="button" class="btn btn-light" $callDetails-dismiss="modal">Close</button>
            </div><!-- /.modal-footer -->
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <!-- /Followers Modal -->
      <!-- Following Modal -->
      <!-- .modal -->
      <div class="modal fade fixed-left" id="CosedCasesModal" tabindex="-1" role="dialog" aria-labelledby="CosedCasesModalLabel" aria-hidden="true">
        <!-- .modal-dialog -->
        <div class="modal-dialog modal-dialog-vert" role="document">
          <!-- .modal-content -->
          <div class="modal-content">
            <!-- .modal-header -->
            <div class="modal-header" style="background-color:#343a40;">
              <h6 class="modal-title"> <span class=" card-title text-center text-uppercase text-white mb-2 "> <b>Closed Cases</b></span> </h6>
            </div><!-- /.modal-header -->
            <!-- .modal-body -->
            <div class="modal-body px-0">
              <!-- .list-group -->
              <div class="list-group list-group-flush list-group-divider border">
                <!-- .list-group-item -->
                @foreach($latestClosedCases  as $case)
                <div class="list-group-item">
                  <!-- .list-group-item-figure -->
                  <div class="list-group-item-figure">
                    <a href="#!" class="user-avatar"><img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt=""></a>
                  </div><!-- /.list-group-item-figure -->
                  <!-- .list-group-item-body -->
                  <div class="list-group-item-body">
                    <h4 class="list-group-item-title">
                      <a href="#!">{{Auth::user()->firstname}} </a>
                    </h4>
                    <p class="list-group-item-text"> {{ substr(trim($case->comments) , 0, 55)}} </p>
                  </div><!-- /.list-group-item-body -->
                  <!-- .list-group-item-figure -->
                  <div class="list-group-item-figure">
                    <button type="button" onclick="javascript:window.location.href='/viewCases/?status=2?id={{$case->id}}';" class="btn btn-sm btn-primary">Open<//button>
                  </div><!-- /.list-group-item-figure -->
                </div><!-- /.list-group-item -->
                @endforeach
              </div><!-- /.list-group -->
              <!-- .loading -->
              <div class="loading border-bottom">
                <div class="loader loader-sm"></div>
                <div class="sr-only"> Loading more content </div>

              </div><!-- /.loading -->
            </div><!-- /.modal-body -->
            <!-- .modal-footer -->
            <div class="modal-footer">
              <button type="button" class="btn btn-outline-danger" $callDetails-dismiss="modal">Close</button>
            </div><!-- /.modal-footer -->
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <!-- /Following Modal -->
      <!-- .page-navs -->
      <!-- .page-section -->

      <!--  validation error checking-->
      @include('notifications')
      <div class="page-section">

        <!-- Regiter New Case: Form -->
        <div class="card fixed-right" id="modalReg" tabindex="-1" role="dialog" aria-hidden="true"  aria-labelledby="myLargeModalLabel" >
          <div class=" " role="document">
            <div class=" html">
              <div class="card-header  align-items-center" style="background-color:#343a40;">
                <h6 class="card-header-title">
                  <span class=" card-title text-center text-uppercase text-white mb-2 "> <b>View Case</b></span>
                </h6>
              </div>

              <div class="card-body body px-5">
                <div id="contact_form"></div>
                <div class="py-5">
                  <div class="text-left">
                      <div class=" row">

                        <div class=" col-6">
                          <p class="text-muted "><strong class="text-uppercase">Client Name :</strong> <span class="ml-2">{{$callDetails->client->name}}</span></p>
                          <p class="text-muted "><strong class="text-uppercase">Age :</strong> <span class="ml-2">{{$callDetails->client->age}} </span></p>
                          <p class="text-muted "><strong class="text-uppercase">Nature :</strong><span class="ml-2">{{$callDetails->nature}}  </span>
                          </p>
                          <p class="text-muted "><strong class="text-uppercase">Issue :</strong>
                            @foreach($callDetails->subissues as $subissue)
                              <span class="ml-2">{{$subissue->issue->name}}</span>
                            @endforeach

                          </p>
                          <p class="text-muted "><strong class="text-uppercase">Interventions :</strong>

                            @foreach($callDetails->Interventions as $intervention)
                              <span class="ml-2">{{$intervention->name}}</span>
                            @endforeach
                          </p>
                      </div>
                      <div class=" col-6">
                        <p class="text-muted ">
                        <strong class="text-uppercase">   Gender :</strong> <span class="ml-2">{{$callDetails->client->gender}}</span></p>
                         <p class="text-muted "><strong class="text-uppercase">Mobile :</strong><span class="ml-2">(265)  {{$callDetails->client->phonenumber}}</span></p>
                          <p class="text-muted "><strong class="text-uppercase">District :</strong> <span class="ml-2">{{$callDetails->ta->district->name}}</span></p>
                          <p class="text-muted "><strong class="text-uppercase">Subissue :</strong>
                            @foreach($callDetails->subissues as $subissue)
                              <span class="ml-2">{{$subissue->name}}</span>
                            @endforeach
                          </p>
                          <p class="text-muted "><strong class="text-uppercase">Resolved Case:</strong> <span class="ml-2 ">{{date("d/m/Y", strtotime($callDetails->date))}}</span></p>
                      </div>
                    </div>
                      <p class="text-muted mb-0"><strong class="text-uppercase">Comments :</strong>
                      </p>

                    </div>
                    <p class="text-muted font-13">
                       {{$callDetails->comments}}
                    </p>
                    @if($outcomeCount != 0)
                    <p class="text-muted mb-0"><strong class="text-uppercase">Action Taken :</strong>
                    </p>
                    <p class="text-muted font-13">
                      {{$callDetails->outcome->action_taken}}
                     </p>
                    <p class="text-muted mb-0 "><strong class="text-uppercase">Outcome :</strong>
                    </p>
                  <p class="text-muted font-13">
                        {{$callDetails->outcome}}
                  </p>
                  @endif
                  <p class="text-muted small">
                    <p class="text-muted small">
                    <p class="text-muted "><strong></strong> <span class="ml-2"></span></p>
                </div>
                <hr>
                <div class=" ">
                  <button type="button" class="btn btn-outline-secondary btn-sm" onclick="javascript: history.back();">Cancel</button>
                  <button type="button" class="btn btn-outline-danger btn-sm" data-id ="{{$callDetails->id}}" id="adminDelete">Delete </button>
                  <button type="button" class="btn btn-outline-primary btn-sm" onclick="javascript:window.location.href='/caseEdit?id={{$callDetails->id}}';">Edit </button>
                  <button type="button" class="btn btn-outline-success btn-sm">Export </button>
                </div>
                <br>
            </div>

          </div>
        </div>
      </div>
      <br><br>
    </div><!-- /.page-section -->
  </div><!-- /.page-inner -->
</div><!-- /.page -->
</div>
@endsection
