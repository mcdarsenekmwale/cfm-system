@extends('admin.master.master')


@section('content')
<div class="wrapper">
          <!-- .page -->
          <div class="page">
            <!-- .page-inner -->
            <div class="page-inner page-inner-fill">
              <!-- .page-navs -->
              <header class="page-navs shadow-sm pr-3">
                <!-- .btn-account -->
                <a href="page-project.html" class="btn-account">
                  <div class="has-badge">
                    <span class="tile bg-pink text-white">SP</span> <span class="user-avatar user-avatar-xs"><img src="http://uselooper.com/assets/images/avatars/team4.jpg" alt=""></span>
                  </div>
                  <div class="account-summary">
                    <h1 class="card-title"> Syrena Project </h1>
                    <h6 class="card-subtitle text-muted"> 4 deadline · 2 overdue </h6>
                  </div>
                </a> <!-- /.btn-account -->
                <!-- right actions -->
                <div class="ml-auto">
                  <!-- invite members -->
                  <div class="dropdown d-inline-block">
                    <button type="button" class="btn btn-light btn-icon" title="Invite members" data-toggle="dropdown" data-display="static" aria-haspoppup="true" aria-expanded="false"><i class="fa fa-user-plus"></i></button>
                    <div class="dropdown-arrow"></div><!-- .dropdown-menu -->
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-rich stop-propagation dropdown-menu-card">
                      <div class="dropdown-header"> Add members </div>
                      <div class="form-group px-3 py-2 m-0">
                        <input type="text" class="form-control" placeholder="e.g. @mcdarsene" data-toggle="tribute" data-remote="{{route('allusers')}}" data-menu-container="#people-list" data-item-template="true" autofocus="" data-tribute="true"> <small class="form-text text-muted">Search people by username or email address to invite them.</small>
                      </div>
                      <div id="people-list" class="tribute-inline stop-propagation"></div><a href="#!" class="dropdown-footer">Invite member by link <i class="fa fa-clone"></i></a>
                    </div><!-- /.dropdown-menu -->
                  </div><!-- /invite members -->
                  <button type="button" class="btn btn-light btn-icon" data-toggle="page-expander" title="Expand board"><i class="fa fa-arrows-alt fa-rotate-90 fa-fw"></i></button> <button type="button" class="btn btn-light btn-icon" data-toggle="modal" data-target="#modalBoardConfig" title="Show menu"><i class="fa fa-cog fa-fw"></i></button>
                </div><!-- /right actions -->
              </header><!-- /.page-navs -->
              <!-- .board -->
              <div id="board" class="board" data-toggle="sortable" data-draggable=".tasks" data-handle=".task-header" data-delay="100" data-force-fallback="true">
                <!-- .tasks -->
                <section class="tasks">
                  <!-- .tasks-header -->
                  <header class="task-header">
                    <h3 class="task-title mr-auto"> Backlogs <span class="badge text-muted">(3)</span>
                    </h3><!-- .btn -->
                    <button class="btn btn-light btn-icon text-muted" data-toggle="modal" data-target="#modalNewTask" title="Add task"><i class="fa fa-plus-circle"></i></button> <!-- /.btn -->
                    <!-- .dropdown -->
                    <div class="dropdown">
                      <!-- .btn -->
                      <button class="btn btn-light btn-icon text-muted" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button> <!-- /.btn -->
                      <div class="dropdown-arrow"></div><!-- .dropdown-menu -->
                      <div class="dropdown-menu dropdown-menu-right">
                        <a href="#!" class="dropdown-item">Edit</a> <a href="#!" class="dropdown-item">Move</a> <a href="#!" class="dropdown-item">Duplicate</a> <a href="#!" class="dropdown-item">Subcribe</a> <a href="#!" class="dropdown-item">Trash</a>
                      </div><!-- /.dropdown-menu -->
                    </div><!-- /.dropdown -->
                  </header><!-- /.tasks-header -->
                  <!-- .task-body -->
                  <div class="task-body" data-toggle="sortable" data-group="tasks" data-delay="50" data-force-fallback="true">
                    <!-- .task-issue -->
                    <div class="task-issue">
                      <!-- .card -->
                      <div class="card">
                        <!-- .card-header -->
                        <header class="card-header">
                          <div class="task-label-group">
                            <span class="task-label bg-teal"></span> <span class="task-label bg-green"></span>
                          </div><br>
                          <h4 class="card-title">
                            <a  href="#!" data-toggle="modal" data-target="#modalViewTask">Review</a>
                          </h4>
                          <h6 class="card-subtitle text-muted">
                            <time class="text-muted">03:11</time> / <time class="text-muted">6:00</time> <span class="mx-1">·</span> <span class="due-date"><i class="far fa-fw fa-clock"></i> Apr 07</span>
                          </h6>
                        </header><!-- /.card-header -->
                        <!-- .card-body -->
                        <div class="card-body pt-0">
                          <!-- .list-group -->
                          <div class="list-group">
                            <!-- .list-group-item -->
                            <a href="#!" class="list-group-item" data-toggle="modal" data-target="#modalViewTask">
                              <!-- .list-group-item-body -->
                              <div class="list-group-item-body">
                                <!-- members -->
                                <figure class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Johnny Day">
                                  <img src="http://uselooper.com/assets/images/avatars/uifaces2.jpg" alt="Johnny Day">
                                </figure>
                                <figure class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Sarah Bishop">
                                  <img src="http://uselooper.com/assets/images/avatars/uifaces3.jpg" alt="Sarah Bishop">
                                </figure><!-- /members -->
                              </div><!-- /.list-group-item-body -->
                            </a> <!-- /.list-group-item -->
                          </div><!-- /.list-group -->
                        </div><!-- /.card-body -->
                        <!-- .card-footer -->
                        <footer class="card-footer">
                          <a href="#!" class="card-footer-item card-footer-item-bordered text-muted"><i class="fa fa-play text-teal"></i></a>
                        </footer><!-- /.card-footer -->
                      </div><!-- .card -->
                    </div><!-- /.task-issue -->
                    <!-- .task-issue -->
                    <div class="task-issue">
                      <!-- .card -->
                      <div class="card">
                        <!-- .card-header -->
                        <header class="card-header">
                          <h4 class="card-title">
                            <a href="#!" data-toggle="modal" data-target="#modalViewTask">Deploy</a>
                          </h4>
                          <h6 class="card-subtitle text-muted">
                            <time class="text-muted">08:36</time> / <time class="text-muted">12:00</time> <span class="mx-1">·</span> <span class="due-date"><i class="far fa-fw fa-clock"></i> In 2 days</span>
                          </h6>
                        </header><!-- /.card-header -->
                        <!-- .card-body -->
                        <div class="card-body pt-0">
                          <!-- .list-group -->
                          <div class="list-group">
                            <!-- .list-group-item -->
                            <a href="#!" class="list-group-item" data-toggle="modal" data-target="#modalViewTask">
                              <!-- .list-group-item-body -->
                              <div class="list-group-item-body">
                                <!-- members -->
                                <figure class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Johnny Day">
                                  <img src="http://uselooper.com/assets/images/avatars/uifaces2.jpg" alt="Johnny Day">
                                </figure>
                                <figure class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Sarah Bishop">
                                  <img src="http://uselooper.com/assets/images/avatars/uifaces3.jpg" alt="Sarah Bishop">
                                </figure>
                                <figure class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Craig Hansen">
                                  <img src="http://uselooper.com/assets/images/avatars/uifaces5.jpg" alt="Craig Hansen">
                                </figure>
                                <figure class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Jane Barnes">
                                  <img src="http://uselooper.com/assets/images/avatars/uifaces9.jpg" alt="Jane Barnes">
                                </figure><!-- /members -->
                              </div><!-- /.list-group-item-body -->
                            </a> <!-- /.list-group-item -->
                            <!-- .list-group-item -->
                            <a href="#!" class="list-group-item pt-0" data-toggle="modal" data-target="#modalViewTask">
                              <!-- .list-group-item-body -->
                              <div class="list-group-item-body">
                                <div class="progress progress-xs">
                                  <div class="progress-bar bg-success" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                              </div><!-- /.list-group-item-body -->
                              <!-- .list-group-item-figure -->
                              <div class="list-group-item-figure">
                                <span class="todos">0/5</span>
                              </div><!-- /.list-group-item-figure -->
                            </a> <!-- /.list-group-item -->
                          </div><!-- /.list-group -->
                        </div><!-- /.card-body -->
                        <!-- .card-footer -->
                        <footer class="card-footer">
                          <a href="#!" class="card-footer-item card-footer-item-bordered text-muted" data-toggle="modal" data-target="#modalViewTask"><i class="oi oi-comment-square mr-1"></i> 8</a> <a href="#!" class="card-footer-item card-footer-item-bordered text-muted"><i class="fa fa-play text-teal"></i></a>
                        </footer><!-- /.card-footer -->
                      </div><!-- .card -->
                    </div><!-- /.task-issue -->
                    <!-- .task-issue -->
                    <div class="task-issue">
                      <!-- .card -->
                      <div class="card">
                        <!-- .card-header -->
                        <header class="card-header">
                          <h4 class="card-title">
                            <a href="#!" data-toggle="modal" data-target="#modalViewTask">Go live!</a>
                          </h4>
                          <h6 class="card-subtitle text-muted">
                            <time class="text-muted">11:36</time> / <time class="text-muted">12:00</time> <span class="mx-1">·</span> <span class="due-date"><i class="far fa-fw fa-clock"></i> Mar 23</span>
                          </h6>
                        </header><!-- /.card-header -->
                        <!-- .card-body -->
                        <div class="card-body pt-0">
                          <!-- .list-group -->
                          <div class="list-group">
                            <!-- .list-group-item -->
                            <a href="#!" class="list-group-item" data-toggle="modal" data-target="#modalViewTask">
                              <!-- .list-group-item-body -->
                              <div class="list-group-item-body">
                                <!-- members -->
                                <figure class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Johnny Day">
                                  <img src="http://uselooper.com/assets/images/avatars/uifaces2.jpg" alt="Johnny Day">
                                </figure>
                                <figure class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Sarah Bishop">
                                  <img src="http://uselooper.com/assets/images/avatars/uifaces3.jpg" alt="Sarah Bishop">
                                </figure>
                                <figure class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Craig Hansen">
                                  <img src="http://uselooper.com/assets/images/avatars/uifaces5.jpg" alt="Craig Hansen">
                                </figure>
                                <figure class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Jane Barnes">
                                  <img src="http://uselooper.com/assets/images/avatars/uifaces9.jpg" alt="Jane Barnes">
                                </figure><!-- /members -->
                              </div><!-- /.list-group-item-body -->
                            </a> <!-- /.list-group-item -->
                            <!-- .list-group-item -->
                            <a href="#!" class="list-group-item pt-0" data-toggle="modal" data-target="#modalViewTask">
                              <!-- .list-group-item-body -->
                              <div class="list-group-item-body">
                                <div class="progress progress-xs">
                                  <div class="progress-bar bg-success" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                              </div><!-- /.list-group-item-body -->
                              <!-- .list-group-item-figure -->
                              <div class="list-group-item-figure">
                                <span class="todos">0/2</span>
                              </div><!-- /.list-group-item-figure -->
                            </a> <!-- /.list-group-item -->
                          </div><!-- /.list-group -->
                        </div><!-- /.card-body -->
                        <!-- .card-footer -->
                        <footer class="card-footer">
                          <a href="#!" class="card-footer-item card-footer-item-bordered text-muted" data-toggle="modal" data-target="#modalViewTask"><i class="oi oi-comment-square mr-1"></i> 24</a> <a href="#!" class="card-footer-item card-footer-item-bordered text-muted"><i class="fa fa-play text-teal"></i></a>
                        </footer><!-- /.card-footer -->
                      </div><!-- .card -->
                    </div><!-- /.task-issue -->
                  </div><!-- /.task-body -->
                </section><!-- /.tasks -->
                <!-- .tasks -->
                <section class="tasks">
                  <!-- .tasks-header -->
                  <header class="task-header">
                    <h3 class="task-title mr-auto"> In Progress <span class="badge text-muted">(1)</span>
                    </h3><!-- .btn -->
                    <button class="btn btn-light btn-icon text-muted" data-toggle="modal" data-target="#modalNewTask" title="Add task"><i class="fa fa-plus-circle"></i></button> <!-- /.btn -->
                    <!-- .dropdown -->
                    <div class="dropdown">
                      <!-- .btn -->
                      <button class="btn btn-light btn-icon text-muted" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button> <!-- /.btn -->
                      <div class="dropdown-arrow"></div><!-- .dropdown-menu -->
                      <div class="dropdown-menu dropdown-menu-right">
                        <a href="#!" class="dropdown-item">Edit</a> <a href="#!" class="dropdown-item">Move</a> <a href="#!" class="dropdown-item">Duplicate</a> <a href="#!" class="dropdown-item">Subcribe</a> <a href="#!" class="dropdown-item">Trash</a>
                      </div><!-- /.dropdown-menu -->
                    </div><!-- /.dropdown -->
                  </header><!-- /.tasks-header -->
                  <!-- .task-body -->
                  <div class="task-body" data-toggle="sortable" data-group="tasks" data-delay="50" data-force-fallback="true">
                    <!-- .task-issue -->
                    <div class="task-issue">
                      <!-- .card -->
                      <div class="card">
                        <!-- .card-header -->
                        <header class="card-header">
                          <div class="task-label-group">
                            <span class="task-label bg-red"></span> <span class="task-label bg-cyan"></span>
                          </div>
                          <h4 class="card-title">
                            <a href="#!" data-toggle="modal" data-target="#modalViewTask">Apply new styles</a>
                          </h4>
                          <h6 class="card-subtitle text-muted">
                            <time class="text-red">50:02</time> / <time class="text-muted">48:00</time> <span class="mx-1">·</span> <span class="due-date"><i class="far fa-fw fa-clock"></i> Tomorrow</span>
                          </h6>
                        </header><!-- /.card-header -->
                        <!-- .card-body -->
                        <div class="card-body pt-0">
                          <!-- .list-group -->
                          <div class="list-group">
                            <!-- .list-group-item -->
                            <a href="#!" class="list-group-item" data-toggle="modal" data-target="#modalViewTask">
                              <!-- .list-group-item-body -->
                              <div class="list-group-item-body">
                                <!-- members -->
                                <figure class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Johnny Day">
                                  <img src="http://uselooper.com/assets/images/avatars/uifaces2.jpg" alt="Johnny Day">
                                </figure>
                                <figure class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Sarah Bishop">
                                  <img src="http://uselooper.com/assets/images/avatars/uifaces3.jpg" alt="Sarah Bishop">
                                </figure><!-- /members -->
                              </div><!-- /.list-group-item-body -->
                            </a> <!-- /.list-group-item -->
                            <!-- .list-group-item -->
                            <a href="#!" class="list-group-item pt-0" data-toggle="modal" data-target="#modalViewTask">
                              <!-- .list-group-item-body -->
                              <div class="list-group-item-body">
                                <div class="progress progress-xs">
                                  <div class="progress-bar bg-success" role="progressbar" style="width: 90.47619047619048%;" aria-valuenow="90.47619047619048" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                              </div><!-- /.list-group-item-body -->
                              <!-- .list-group-item-figure -->
                              <div class="list-group-item-figure">
                                <span class="todos">19/21</span>
                              </div><!-- /.list-group-item-figure -->
                            </a> <!-- /.list-group-item -->
                          </div><!-- /.list-group -->
                        </div><!-- /.card-body -->
                        <!-- .card-footer -->
                        <footer class="card-footer">
                          <a href="#!" class="card-footer-item card-footer-item-bordered text-muted" data-toggle="modal" data-target="#modalViewTask"><i class="oi oi-comment-square mr-1"></i> 36</a> <a href="#!" class="card-footer-item card-footer-item-bordered text-muted"><i class="fa fa-pause"></i></a>
                        </footer><!-- /.card-footer -->
                      </div><!-- .card -->
                    </div><!-- /.task-issue -->
                  </div><!-- /.task-body -->
                </section><!-- /.tasks -->
                <!-- .tasks -->
                <section class="tasks">
                  <!-- .tasks-header -->
                  <header class="task-header">
                    <h3 class="task-title mr-auto"> In Review <span class="badge text-muted">(1)</span>
                    </h3><!-- .btn -->
                    <button class="btn btn-light btn-icon text-muted" data-toggle="modal" data-target="#modalNewTask" title="Add task"><i class="fa fa-plus-circle"></i></button> <!-- /.btn -->
                    <!-- .dropdown -->
                    <div class="dropdown">
                      <!-- .btn -->
                      <button class="btn btn-light btn-icon text-muted" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button> <!-- /.btn -->
                      <div class="dropdown-arrow"></div><!-- .dropdown-menu -->
                      <div class="dropdown-menu dropdown-menu-right">
                        <a href="#!" class="dropdown-item">Edit</a> <a href="#!" class="dropdown-item">Move</a> <a href="#!" class="dropdown-item">Duplicate</a> <a href="#!" class="dropdown-item">Subcribe</a> <a href="#!" class="dropdown-item">Trash</a>
                      </div><!-- /.dropdown-menu -->
                    </div><!-- /.dropdown -->
                  </header><!-- /.tasks-header -->
                  <!-- .task-body -->
                  <div class="task-body" data-toggle="sortable" data-group="tasks" data-delay="50" data-force-fallback="true">
                    <!-- .task-issue -->
                    <div class="task-issue">
                      <!-- .card -->
                      <div class="card">
                        <!-- .card-header -->
                        <header class="card-header">
                          <div class="task-label-group">
                            <span class="task-label bg-purple"></span>
                          </div>
                          <h4 class="card-title">
                            <a href="#!" data-toggle="modal" data-target="#modalViewTask">Write new content</a>
                          </h4>
                          <h6 class="card-subtitle text-muted">
                            <time class="text-muted">03:36</time> / <time class="text-muted">04:00</time> <span class="mx-1">·</span> <span class="due-date"><i class="far fa-fw fa-clock"></i> In 3 days</span>
                          </h6>
                        </header><!-- /.card-header -->
                        <!-- .card-body -->
                        <div class="card-body pt-0">
                          <!-- .list-group -->
                          <div class="list-group">
                            <!-- .list-group-item -->
                            <a href="#!" class="list-group-item" data-toggle="modal" data-target="#modalViewTask">
                              <!-- .list-group-item-body -->
                              <div class="list-group-item-body">
                                <!-- members -->
                                <figure class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Johnny Day">
                                  <img src="http://uselooper.com/assets/images/avatars/uifaces2.jpg" alt="Johnny Day">
                                </figure><!-- /members -->
                              </div><!-- /.list-group-item-body -->
                            </a> <!-- /.list-group-item -->
                            <!-- .list-group-item -->
                            <a href="#!" class="list-group-item pt-0" data-toggle="modal" data-target="#modalViewTask">
                              <!-- .list-group-item-body -->
                              <div class="list-group-item-body">
                                <div class="progress progress-xs">
                                  <div class="progress-bar bg-success" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                              </div><!-- /.list-group-item-body -->
                              <!-- .list-group-item-figure -->
                              <div class="list-group-item-figure">
                                <span class="todos">6/6</span>
                              </div><!-- /.list-group-item-figure -->
                            </a> <!-- /.list-group-item -->
                          </div><!-- /.list-group -->
                        </div><!-- /.card-body -->
                        <!-- .card-footer -->
                        <footer class="card-footer">
                          <a href="#!" class="card-footer-item card-footer-item-bordered text-muted"><i class="fa fa-pause"></i></a>
                        </footer><!-- /.card-footer -->
                      </div><!-- .card -->
                    </div><!-- /.task-issue -->
                  </div><!-- /.task-body -->
                </section><!-- /.tasks -->
                <!-- .tasks -->
                <section class="tasks">
                  <!-- .tasks-header -->
                  <header class="task-header">
                    <h3 class="task-title mr-auto"> Done <span class="badge text-muted">(1)</span>
                    </h3><!-- .btn -->
                    <button class="btn btn-light btn-icon text-muted" data-toggle="modal" data-target="#modalNewTask" title="Add task"><i class="fa fa-plus-circle"></i></button> <!-- /.btn -->
                    <!-- .dropdown -->
                    <div class="dropdown">
                      <!-- .btn -->
                      <button class="btn btn-light btn-icon text-muted" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button> <!-- /.btn -->
                      <div class="dropdown-arrow"></div><!-- .dropdown-menu -->
                      <div class="dropdown-menu dropdown-menu-right">
                        <a href="#!" class="dropdown-item">Edit</a> <a href="#!" class="dropdown-item">Move</a> <a href="#!" class="dropdown-item">Duplicate</a> <a href="#!" class="dropdown-item">Subcribe</a> <a href="#!" class="dropdown-item">Trash</a>
                      </div><!-- /.dropdown-menu -->
                    </div><!-- /.dropdown -->
                  </header><!-- /.tasks-header -->
                  <!-- .task-body -->
                  <div class="task-body" data-toggle="sortable" data-group="tasks" data-delay="50" data-force-fallback="true">
                    <!-- .task-issue -->
                    <div class="task-issue">
                      <!-- .card -->
                      <div class="card">
                        <!-- .card-header -->
                        <header class="card-header">
                          <div class="task-label-group">
                            <span class="task-label bg-teal"></span> <span class="task-label bg-pink"></span> <span class="task-label bg-yellow"></span>
                          </div>
                          <h4 class="card-title">
                            <a href="#!" data-toggle="modal" data-target="#modalViewTask">Redesign website</a>
                          </h4>
                          <h6 class="card-subtitle text-muted">
                            <time class="text-muted">16:36</time> / <time class="text-muted">24:00</time> <span class="mx-1">·</span> <span class="due-date"><i class="far fa-fw fa-clock"></i> Apr 02</span>
                          </h6>
                        </header><!-- /.card-header -->
                        <!-- .card-body -->
                        <div class="card-body pt-0">
                          <!-- .list-group -->
                          <div class="list-group">
                            <!-- .list-group-item -->
                            <a href="#!" class="list-group-item" data-toggle="modal" data-target="#modalViewTask">
                              <!-- .list-group-item-body -->
                              <div class="list-group-item-body">
                                <!-- members -->
                                <figure class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Johnny Day">
                                  <img src="http://uselooper.com/assets/images/avatars/uifaces2.jpg" alt="Johnny Day">
                                </figure>
                                <figure class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Sarah Bishop">
                                  <img src="http://uselooper.com/assets/images/avatars/uifaces3.jpg" alt="Sarah Bishop">
                                </figure>
                                <figure class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Craig Hansen">
                                  <img src="http://uselooper.com/assets/images/avatars/uifaces5.jpg" alt="Craig Hansen">
                                </figure><!-- /members -->
                              </div><!-- /.list-group-item-body -->
                            </a> <!-- /.list-group-item -->
                            <!-- .list-group-item -->
                            <a href="#!" class="list-group-item pt-0" data-toggle="modal" data-target="#modalViewTask">
                              <!-- .list-group-item-body -->
                              <div class="list-group-item-body">
                                <div class="progress progress-xs">
                                  <div class="progress-bar bg-success" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                              </div><!-- /.list-group-item-body -->
                              <!-- .list-group-item-figure -->
                              <div class="list-group-item-figure">
                                <span class="todos">15/15</span>
                              </div><!-- /.list-group-item-figure -->
                            </a> <!-- /.list-group-item -->
                          </div><!-- /.list-group -->
                        </div><!-- /.card-body -->
                        <!-- .card-footer -->
                        <footer class="card-footer">
                          <a href="#!" class="card-footer-item card-footer-item-bordered text-muted" data-toggle="modal" data-target="#modalViewTask"><i class="oi oi-comment-square mr-1"></i> 12</a> <a href="#!" class="card-footer-item card-footer-item-bordered text-muted"><i class="fa fa-play text-teal"></i></a>
                        </footer><!-- /.card-footer -->
                      </div><!-- .card -->
                    </div><!-- /.task-issue -->
                  </div><!-- /.task-body -->
                </section><!-- /.tasks -->
                <!-- .task-action -->
                <section class="tasks-action">
                  <!-- .publisher -->
                  <div class="publisher">
                    <!-- .publisher-input -->
                    <div class="publisher-input pr-0">
                      <input class="form-control" placeholder="+ Enter list title">
                    </div><!-- /.publisher-input -->
                    <!-- .publisher-actions -->
                    <div class="publisher-actions">
                      <!-- .publisher-tools -->
                      <div class="publisher-tools pb-0">
                        <button type="submit" class="btn btn-primary">Add List</button> <button type="submit" class="btn btn-light">Cancel</button>
                      </div><!-- /.publisher-tools -->
                    </div><!-- /.publisher-actions -->
                  </div><!-- /.publisher -->
                </section><!-- /.task-action -->
              </div><!-- /.board -->
            </div><!-- /.page-inner -->
          </div><!-- /.page -->
          <!-- Layer #1 Modal -->
          <!-- .modalBoardConfig -->
          <div class="modal modal-drawer fade" id="modalBoardConfig" tabindex="-1" role="dialog" aria-labelledby="modalBoardConfigTitle" aria-hidden="true">
            <!-- .modal-dialog -->
            <div class="modal-dialog modal-drawer-right" role="document">
              <!-- .modal-content -->
              <div id="modalContentLayer1" class="modal-content">
                <!-- .modal-header -->
                <div class="modal-header">
                  <h5 class="modal-title" id="modalBoardConfigTitle"> Menu </h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div><!-- /.modal-header -->
                <!-- .modal-body -->
                <div class="modal-body">
                  <!-- .nav -->
                  <ul class="nav flex-column">
                    <li class="nav-item">
                      <a class="nav-link" href="#modalLayer2" data-toggle="modal" data-dismiss="modal" data-content-layer="board-overview.html">Overview</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#modalLayer2" data-toggle="modal" data-dismiss="modal" data-content-layer="board-teams.html">Teams</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#modalLayer2" data-toggle="modal" data-dismiss="modal" data-content-layer="board-time-tracking.html">Time tracking</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#modalLayer2" data-toggle="modal" data-dismiss="modal" data-content-layer="board-files.html">Files</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#modalLayer2" data-toggle="modal" data-dismiss="modal" data-content-layer="board-feeds.html">Feeds</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#modalLayer2" data-toggle="modal" data-dismiss="modal" data-content-layer="board-tickets.html">Tickets</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#modalLayer2" data-toggle="modal" data-dismiss="modal" data-content-layer="board-settings.html">Settings</a>
                    </li>
                  </ul><!-- /.nav -->
                  <hr>
                  <h2 class="section-title">
                    <i class="oi oi-pulse text-muted mr-2"></i>Recent activity </h2><!-- .timeline -->
                  <ul class="timeline timeline-fluid mb-0">
                    <!-- .timeline-item -->
                    <li class="timeline-item">
                      <!-- .timeline-figure -->
                      <div class="timeline-figure">
                        <span class="tile tile-circle tile-sm"><i class="far fa-calendar-alt fa-lg"></i></span>
                      </div><!-- /.timeline-figure -->
                      <!-- .timeline-body -->
                      <div class="timeline-body">
                        <!-- .media -->
                        <div class="media">
                          <!-- .media-body -->
                          <div class="media-body">
                            <p class="mb-0">
                              <a href="#!">Jeffrey Wells</a> created a <a href="#!">schedule</a>
                            </p><span class="timeline-date">About a minute ago</span>
                          </div><!-- /.media-body -->
                        </div><!-- /.media -->
                      </div><!-- /.timeline-body -->
                    </li><!-- /.timeline-item -->
                    <!-- .timeline-item -->
                    <li class="timeline-item">
                      <!-- .timeline-figure -->
                      <div class="timeline-figure">
                        <span class="tile tile-circle tile-sm"><i class="oi oi-chat fa-lg"></i></span>
                      </div><!-- /.timeline-figure -->
                      <!-- .timeline-body -->
                      <div class="timeline-body">
                        <!-- .media -->
                        <div class="media">
                          <!-- .media-body -->
                          <div class="media-body">
                            <p class="mb-0">
                              <a href="#!">Anna Vargas</a> logged a <a href="#!">chat</a> with team </p><span class="timeline-date">3 hours ago</span>
                          </div><!-- /.media-body -->
                        </div><!-- /.media -->
                      </div><!-- /.timeline-body -->
                    </li><!-- /.timeline-item -->
                    <!-- .timeline-item -->
                    <li class="timeline-item">
                      <!-- .timeline-figure -->
                      <div class="timeline-figure">
                        <span class="tile tile-circle tile-sm"><i class="fa fa-tasks fa-lg"></i></span>
                      </div><!-- /.timeline-figure -->
                      <!-- .timeline-body -->
                      <div class="timeline-body">
                        <!-- .media -->
                        <div class="media">
                          <!-- .media-body -->
                          <div class="media-body">
                            <p class="mb-0">
                              <a href="#!">Arthur Carroll</a> created a <a href="#!">task</a>
                            </p><span class="timeline-date">8:14pm</span>
                          </div><!-- /.media-body -->
                        </div><!-- /.media -->
                      </div><!-- /.timeline-body -->
                    </li><!-- /.timeline-item -->
                    <!-- .timeline-item -->
                    <li class="timeline-item">
                      <!-- .timeline-figure -->
                      <div class="timeline-figure">
                        <span class="tile tile-circle tile-sm"><i class="fas fa-user-plus fa-lg"></i></span>
                      </div><!-- /.timeline-figure -->
                      <!-- .timeline-body -->
                      <div class="timeline-body">
                        <!-- .media -->
                        <div class="media">
                          <!-- .media-body -->
                          <div class="media-body">
                            <p class="mb-0">
                              <a href="#!">Sara Carr</a> invited to <a href="#!">Stilearn Admin</a> project </p><span class="timeline-date">7:21pm</span>
                          </div><!-- /.media-body -->
                        </div><!-- /.media -->
                      </div><!-- /.timeline-body -->
                    </li><!-- /.timeline-item -->
                    <!-- .timeline-item -->
                    <li class="timeline-item">
                      <!-- .timeline-figure -->
                      <div class="timeline-figure">
                        <span class="tile tile-circle tile-sm"><i class="fa fa-folder fa-lg"></i></span>
                      </div><!-- /.timeline-figure -->
                      <!-- .timeline-body -->
                      <div class="timeline-body">
                        <!-- .media -->
                        <div class="media">
                          <!-- .media-body -->
                          <div class="media-body">
                            <p class="mb-0">
                              <a href="#!">Angela Peterson</a> added <a href="#!">Looper Admin</a> to collection </p><span class="timeline-date">5:21pm</span>
                          </div><!-- /.media-body -->
                        </div><!-- /.media -->
                      </div><!-- /.timeline-body -->
                    </li><!-- /.timeline-item -->
                    <!-- .timeline-item -->
                    <li class="timeline-item">
                      <!-- .timeline-figure -->
                      <div class="timeline-figure">
                        <span class="tile tile-circle tile-sm"><i class="oi oi-person fa-lg"></i></span>
                      </div><!-- /.timeline-figure -->
                      <!-- .timeline-body -->
                      <div class="timeline-body">
                        <!-- .media -->
                        <div class="media">
                          <!-- .media-body -->
                          <div class="media-body">
                            <div class="avatar-group mb-2">
                              <a href="#!" class="user-avatar user-avatar-sm"><img src="http://uselooper.com/assets/images/avatars/uifaces4.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm"><img src="http://uselooper.com/assets/images/avatars/uifaces5.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm"><img src="http://uselooper.com/assets/images/avatars/uifaces6.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm"><img src="http://uselooper.com/assets/images/avatars/uifaces7.jpg" alt=""></a>
                            </div>
                            <p class="mb-0">
                              <a href="#!">Willie Dixon</a> and 3 others followed you </p><span class="timeline-date">4:32pm</span>
                          </div><!-- /.media-body -->
                        </div><!-- /.media -->
                      </div><!-- /.timeline-body -->
                    </li><!-- /.timeline-item -->
                  </ul><!-- /.timeline -->
                  <!-- .timeline -->
                  <ul class="timeline timeline-fluid mb-0">
                    <!-- .timeline-item -->
                    <li class="timeline-item">
                      <!-- .timeline-figure -->
                      <div class="timeline-figure">
                        <span class="tile tile-circle tile-sm"><i class="far fa-calendar-alt fa-lg"></i></span>
                      </div><!-- /.timeline-figure -->
                      <!-- .timeline-body -->
                      <div class="timeline-body">
                        <!-- .media -->
                        <div class="media">
                          <!-- .media-body -->
                          <div class="media-body">
                            <p class="mb-0">
                              <a href="#!">Jeffrey Wells</a> created a <a href="#!">schedule</a>
                            </p><span class="timeline-date">About a minute ago</span>
                          </div><!-- /.media-body -->
                        </div><!-- /.media -->
                      </div><!-- /.timeline-body -->
                    </li><!-- /.timeline-item -->
                    <!-- .timeline-item -->
                    <li class="timeline-item">
                      <!-- .timeline-figure -->
                      <div class="timeline-figure">
                        <span class="tile tile-circle tile-sm"><i class="oi oi-chat fa-lg"></i></span>
                      </div><!-- /.timeline-figure -->
                      <!-- .timeline-body -->
                      <div class="timeline-body">
                        <!-- .media -->
                        <div class="media">
                          <!-- .media-body -->
                          <div class="media-body">
                            <p class="mb-0">
                              <a href="#!">Anna Vargas</a> logged a <a href="#!">chat</a> with team </p><span class="timeline-date">3 hours ago</span>
                          </div><!-- /.media-body -->
                        </div><!-- /.media -->
                      </div><!-- /.timeline-body -->
                    </li><!-- /.timeline-item -->
                    <!-- .timeline-item -->
                    <li class="timeline-item">
                      <!-- .timeline-figure -->
                      <div class="timeline-figure">
                        <span class="tile tile-circle tile-sm"><i class="fa fa-tasks fa-lg"></i></span>
                      </div><!-- /.timeline-figure -->
                      <!-- .timeline-body -->
                      <div class="timeline-body">
                        <!-- .media -->
                        <div class="media">
                          <!-- .media-body -->
                          <div class="media-body">
                            <p class="mb-0">
                              <a href="#!">Arthur Carroll</a> created a <a href="#!">task</a>
                            </p><span class="timeline-date">8:14pm</span>
                          </div><!-- /.media-body -->
                        </div><!-- /.media -->
                      </div><!-- /.timeline-body -->
                    </li><!-- /.timeline-item -->
                    <!-- .timeline-item -->
                    <li class="timeline-item">
                      <!-- .timeline-figure -->
                      <div class="timeline-figure">
                        <span class="tile tile-circle tile-sm"><i class="fas fa-user-plus fa-lg"></i></span>
                      </div><!-- /.timeline-figure -->
                      <!-- .timeline-body -->
                      <div class="timeline-body">
                        <!-- .media -->
                        <div class="media">
                          <!-- .media-body -->
                          <div class="media-body">
                            <p class="mb-0">
                              <a href="#!">Sara Carr</a> invited to <a href="#!">Stilearn Admin</a> project </p><span class="timeline-date">7:21pm</span>
                          </div><!-- /.media-body -->
                        </div><!-- /.media -->
                      </div><!-- /.timeline-body -->
                    </li><!-- /.timeline-item -->
                    <!-- .timeline-item -->
                    <li class="timeline-item">
                      <!-- .timeline-figure -->
                      <div class="timeline-figure">
                        <span class="tile tile-circle tile-sm"><i class="fa fa-folder fa-lg"></i></span>
                      </div><!-- /.timeline-figure -->
                      <!-- .timeline-body -->
                      <div class="timeline-body">
                        <!-- .media -->
                        <div class="media">
                          <!-- .media-body -->
                          <div class="media-body">
                            <p class="mb-0">
                              <a href="#!">Angela Peterson</a> added <a href="#!">Looper Admin</a> to collection </p><span class="timeline-date">5:21pm</span>
                          </div><!-- /.media-body -->
                        </div><!-- /.media -->
                      </div><!-- /.timeline-body -->
                    </li><!-- /.timeline-item -->
                    <!-- .timeline-item -->
                    <li class="timeline-item">
                      <!-- .timeline-figure -->
                      <div class="timeline-figure">
                        <span class="tile tile-circle tile-sm"><i class="oi oi-person fa-lg"></i></span>
                      </div><!-- /.timeline-figure -->
                      <!-- .timeline-body -->
                      <div class="timeline-body">
                        <!-- .media -->
                        <div class="media">
                          <!-- .media-body -->
                          <div class="media-body">
                            <div class="avatar-group mb-2">
                              <a href="#!" class="user-avatar user-avatar-sm"><img src="http://uselooper.com/assets/images/avatars/uifaces4.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm"><img src="http://uselooper.com/assets/images/avatars/uifaces5.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm"><img src="http://uselooper.com/assets/images/avatars/uifaces6.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm"><img src="http://uselooper.com/assets/images/avatars/uifaces7.jpg" alt=""></a>
                            </div>
                            <p class="mb-0">
                              <a href="#!">Willie Dixon</a> and 3 others followed you </p><span class="timeline-date">4:32pm</span>
                          </div><!-- /.media-body -->
                        </div><!-- /.media -->
                      </div><!-- /.timeline-body -->
                    </li><!-- /.timeline-item -->
                  </ul><!-- /.timeline -->
                  <div class="text-center p-3">
                    <a href="#!" class="btn btn-link">View all activity ...</a>
                  </div>
                </div><!-- /.modal-body -->
              </div><!-- .modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modalBoardConfig -->
          <!-- /Layer #1 Modal -->
          <!-- Layer #2 Modal -->
          <!-- You can create modal as many as your menu or simply create
layer modal with dinamy content to handle your menus (overview, members, time tracking, and so on) -->

  <br><br>
          <!-- .modalBoardConfig -->
          <div class="modal modal-drawer fade" id="modalLayer2" tabindex="-1" role="dialog" aria-labelledby="modalLayer2Title" aria-hidden="true">
            <!-- .modal-dialog -->
            <div class="modal-dialog modal-drawer-right" role="document">
              <!-- .modal-content -->
              <div id="modalContentLayer2" class="modal-content">
                <!-- .modal-header -->
                <div class="modal-header d-block">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                      <a href="#!" data-dismiss="modal" data-toggle="modal" data-target="#modalBoardConfig"><i class="breadcrumb-icon fa fa-angle-left mr-2"></i>Back</a>
                    </li>
                  </ol>
                  <h5 class="modal-title" id="modalLayer2Title">
                    <span id="layer-title">Overview</span>
                  </h5>
                </div><!-- /.modal-header -->
                <!-- .modal-body -->
                <div class="modal-body">
                  <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, neque odio nisi. </p>
                </div><!-- /.modal-body -->
              </div><!-- .modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modalBoardConfig -->
          <!-- /Layer #2 Modal -->
          <!-- .form -->
          <form id="addNewTask" action="/add-new-task" method="post" enctype="multipart/form-data" name="addNewTask">
            <!-- .modal -->
            <div class="modal fade" id="modalNewTask" tabindex="-1" role="dialog" aria-labelledby="modalNewTaskLabel" aria-hidden="true">
              <!-- .modal-dialog -->
              <div class="modal-dialog modal-lg" role="document">
                <!-- .modal-content -->
                <div class="modal-content px-lg-4 py-lg-3">
                  <!-- .modal-header -->
                  <div class="modal-header">
                    <h6 class="modal-title"> Add new tasks </h6>
                  </div><!-- /.modal-header -->
                  <!-- .modal-body -->
                  <div class="modal-body">
                    <!-- .form-group -->
                    <div class="form-group">
                      <label for="tasksTitle">Title</label> <input type="text" name="taskTitle" id="tasksTitle" class="form-control" required="" autocomplete="off" autofocus="">
                    </div><!-- /.form-group -->
                    <!-- .form-group -->
                    <div class="form-group">
                      <div class="d-flex justify-content-between">
                        <label for="taskDescription">Description</label> <a href="#!" onclick="boardDemo.ntMarkdown.togglePreview()" tabindex="-1">Preview</a>
                      </div>
                      <textarea name="taskDescription" id="taskDescription" class="form-control" data-toggle="simplemde" data-spell-checker="false" data-status="false" data-toolbar="false" style="display: none;"></textarea><div class="CodeMirror cm-s-paper CodeMirror-wrap"><div style="overflow: hidden; position: relative; width: 3px; height: 0px;"><textarea autocorrect="off" autocapitalize="off" spellcheck="false" style="position: absolute; padding: 0px; width: 1000px; height: 1em; outline: none;" tabindex="0"></textarea></div><div class="CodeMirror-vscrollbar" cm-not-content="true"><div style="min-width: 1px;"></div></div><div class="CodeMirror-hscrollbar" cm-not-content="true"><div style="height: 100%; min-height: 1px;"></div></div><div class="CodeMirror-scrollbar-filler" cm-not-content="true"></div><div class="CodeMirror-gutter-filler" cm-not-content="true"></div><div class="CodeMirror-scroll" tabindex="-1"><div class="CodeMirror-sizer" style="margin-left: 0px;"><div style="position: relative;"><div class="CodeMirror-lines"><div style="position: relative; outline: none;"><div class="CodeMirror-measure"><pre><span>xxxxxxxxxx</span></pre></div><div class="CodeMirror-measure"></div><div style="position: relative; z-index: 1;"></div><div class="CodeMirror-cursors"></div><div class="CodeMirror-code"></div></div></div></div></div><div style="position: absolute; height: 30px; width: 1px;"></div><div class="CodeMirror-gutters" style="display: none;"></div></div></div><div class="editor-preview-side"></div>
                      <div class="d-flex justify-content-between">
                        <span class="text-muted mt-2"><a href="https://simplemde.com/markdown-guide" target="_blank">Markdown</a> is supported</span> <button class="btn btn-reset fileinput-button text-muted"><i class="fa fa-paperclip"></i> <span>Attach a file</span> <input type="file" name="taskAttachment"></button>
                      </div>
                    </div><!-- /.form-group -->
                    <!-- .form-group -->
                    <div class="form-group">
                      <div class="custom-control custom-checkbox">
                        <input type="checkbox" name="taskVisibility" class="custom-control-input" id="taskVisibility"> <label class="custom-control-label" for="taskVisibility">Should only be visible to team members.</label>
                      </div>
                    </div><!-- /.form-group -->
                    <hr>
                    <!-- .form-group -->
                    <div class="form-group form-row">
                      <!-- .col -->
                      <div class="col-md-6">
                        <!-- .form-group -->
                        <div class="form-group">
                          <label>Assignee</label>
                          <div class="dropdown">
                            <button type="button" class="btn btn-secondary btn-block d-flex justify-content-between align-items-center" id="dntAssignees" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span>Beni Arisandi + 1 more</span> <span class="fa fa-angle-down"></span></button>
                            <div class="dropdown-arrow"></div><!-- .dropdown-menu -->
                            <div class="dropdown-menu dropdown-menu-md stop-propagation" id="dntmAssignees" aria-labelledby="dropdownAssignee" style="max-height: 30rem; overflow-y: auto;">
                              <h6 class="dropdown-header"> Add assignee </h6>
                              <div class="dropdown-divider"></div>
                              <div class="form-group px-3 py-2 mb-0">
                                <input type="text" class="form-control" data-filter="#dntmAssignees .filterable" placeholder="Search members" autofocus="">
                              </div><label class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input task-unassignees" name="taskAssignees[]" value="0" data-label="Unassigned"> <span class="custom-control-label">Unassigned</span></label>
                              <div class="dropdown-divider"></div>
                              <h6 class="dropdown-header"> Assignee(s) </h6><label class="custom-control custom-checkbox filterable" data-sort="Beni Arisandi"><input type="checkbox" class="custom-control-input task-assignees" name="taskAssignees[]" value="4" data-label="Beni Arisandi" checked=""> <span class="custom-control-label custom-control-label-media"><span class="media"><span class="mr-2"><span class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces1.jpg" alt=""></span></span> <span class="media-body">Beni Arisandi<br>
                                      <span class="text-muted">@benz</span></span></span></span></label> <label class="custom-control custom-checkbox filterable" data-sort="Melissa Day"><input type="checkbox" class="custom-control-input task-assignees" name="taskAssignees[]" value="2" data-label="Melissa Day" checked=""> <span class="custom-control-label custom-control-label-media"><span class="media"><span class="mr-2"><span class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces4.jpg" alt=""></span></span> <span class="media-body">Melissa Day<br>
                                      <span class="text-muted">@melday</span></span></span></span></label>
                              <div id="dntDivider" class="dropdown-divider"></div><label class="custom-control custom-checkbox filterable" data-sort="Adam Medina"><input type="checkbox" class="custom-control-input task-assignees" name="taskAssignees[]" value="3" data-label="Adam Medina"> <span class="custom-control-label custom-control-label-media"><span class="media"><span class="mr-2"><span class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces15.jpg" alt=""></span></span> <span class="media-body">Adam Medina<br>
                                      <span class="text-muted">@amed</span></span></span></span></label> <label class="custom-control custom-checkbox filterable" data-sort="Diana Miller"><input type="checkbox" class="custom-control-input task-assignees" name="taskAssignees[]" value="1" data-label="Diana Miller"> <span class="custom-control-label custom-control-label-media"><span class="media"><span class="mr-2"><span class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces7.jpg" alt=""></span></span> <span class="media-body">Diana Miller<br>
                                      <span class="text-muted">@dmiller</span></span></span></span></label> <label class="custom-control custom-checkbox filterable" data-sort="Douglas Lucas"><input type="checkbox" class="custom-control-input task-assignees" name="taskAssignees[]" value="5" data-label="Douglas Lucas"> <span class="custom-control-label custom-control-label-media"><span class="media"><span class="mr-2"><span class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces5.jpg" alt=""></span></span> <span class="media-body">Douglas Lucas<br>
                                      <span class="text-muted">@lucaldoug</span></span></span></span></label>
                            </div><!-- /.dropdown-menu -->
                          </div><!-- /.dropdown -->
                        </div><!-- /.form-group -->
                        <!-- .form-group -->
                        <div class="form-group">
                          <label>Labels</label> <!-- .dropdown -->
                          <div class="dropdown">
                            <button type="button" class="btn btn-secondary btn-block d-flex justify-content-between align-items-center" id="dntLabels" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span>No label</span> <span class="fa fa-angle-down"></span></button>
                            <div class="dropdown-arrow"></div><!-- .dropdown-menu -->
                            <div class="dropdown-menu dropdown-menu-md stop-propagation" id="dntmLabels" aria-labelledby="dropdownTaskLabels">
                              <h6 class="dropdown-header"> Select labels </h6>
                              <div class="dropdown-divider"></div>
                              <div class="form-group px-3 py-2 mb-0">
                                <input type="text" class="form-control" data-filter="#dntmLabels .filterable" placeholder="Search" autofocus="">
                              </div><label class="custom-control custom-checkbox filterable"><input type="checkbox" class="custom-control-input task-nolabel" name="taskLabels[]" value="0" data-label="No Label" checked=""> <span class="custom-control-label">No Label</span></label>
                              <div class="dropdown-divider"></div>
                              <div class="dropdown-scroll">
                                <label class="custom-control custom-checkbox filterable"><input type="checkbox" class="custom-control-input task-label" name="taskLabels[]" value="1" data-label="http://uselooper.com/assets"> <span class="custom-control-label"><i class="fa fa-square fa-lg text-blue mr-1"></i> http://uselooper.com/assets</span></label> <label class="custom-control custom-checkbox filterable"><input type="checkbox" class="custom-control-input task-label" name="taskLabels[]" value="2" data-label="Build Tools"> <span class="custom-control-label"><i class="fa fa-square fa-lg text-indigo mr-1"></i> Build Tools</span></label> <label class="custom-control custom-checkbox filterable"><input type="checkbox" class="custom-control-input task-label" name="taskLabels[]" value="3" data-label="Components"> <span class="custom-control-label"><i class="fa fa-square fa-lg text-purple mr-1"></i> Components</span></label> <label class="custom-control custom-checkbox filterable"><input type="checkbox" class="custom-control-input task-label" name="taskLabels[]" value="4" data-label="Dependencies"> <span class="custom-control-label"><i class="fa fa-square fa-lg text-pink mr-1"></i> Dependencies</span></label> <label class="custom-control custom-checkbox filterable"><input type="checkbox" class="custom-control-input task-label" name="taskLabels[]" value="5" data-label="Design"> <span class="custom-control-label"><i class="fa fa-square fa-lg text-red mr-1"></i> Design</span></label> <label class="custom-control custom-checkbox filterable"><input type="checkbox" class="custom-control-input task-label" name="taskLabels[]" value="6" data-label="Documentation"> <span class="custom-control-label"><i class="fa fa-square fa-lg text-orange mr-1"></i> Documentation</span></label> <label class="custom-control custom-checkbox filterable"><input type="checkbox" class="custom-control-input task-label" name="taskLabels[]" value="7" data-label="Doing"> <span class="custom-control-label"><i class="fa fa-square fa-lg text-yellow mr-1"></i> Doing</span></label> <label class="custom-control custom-checkbox filterable"><input type="checkbox" class="custom-control-input task-label" name="taskLabels[]" value="8" data-label="JS"> <span class="custom-control-label"><i class="fa fa-square fa-lg text-green mr-1"></i> JS</span></label> <label class="custom-control custom-checkbox filterable"><input type="checkbox" class="custom-control-input task-label" name="taskLabels[]" value="9" data-label="SCSS"> <span class="custom-control-label"><i class="fa fa-square fa-lg text-teal mr-1"></i> SCSS</span></label> <label class="custom-control custom-checkbox filterable"><input type="checkbox" class="custom-control-input task-label" name="taskLabels[]" value="10" data-label="To Do"> <span class="custom-control-label"><i class="fa fa-square fa-lg text-cyan mr-1"></i> To Do</span></label>
                              </div>
                              <div class="dropdown-divider"></div><a href="#!" class="dropdown-item">Create new label</a> <a href="#!" class="dropdown-item">Manage labels</a>
                            </div><!-- /.dropdown-menu -->
                          </div><!-- /.dropdown -->
                        </div><!-- /.form-group -->
                      </div><!-- /.col -->
                      <!-- .col -->
                      <div class="col-md-6">
                        <!-- .form-group -->
                        <div class="form-group">
                          <label class="control-label" for="taskDueDate">Due date</label>
                          <div class="input-group input-group-alt flatpickr" data-toggle="flatpickr" data-wrap="true" data-min-date="today">
                            <input name="taskDueDate" id="taskDueDate" type="text" class="form-control flatpickr-input" data-input="" readonly="readonly">
                            <div class="input-group-append">
                              <button type="button" class="btn btn-secondary" data-toggle=""><i class="far fa-calendar"></i></button> <button type="button" class="btn btn-secondary" data-clear=""><i class="fa fa-times"></i></button>
                            </div>
                          </div>
                        </div><!-- /.form-group -->
                      </div><!-- /.col -->
                    </div><!-- /.form-group -->
                    <hr>
                    <!-- .form-group -->
                    <div class="form-group">
                      <div class="d-flex justify-content-between">
                        <label>Todos</label> <span id="ntTodosMeter" class="text-muted">(0/1)</span>
                      </div><!-- .progress -->
                      <div class="progress progress-sm">
                        <div class="progress-bar bg-primary" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                          <span class="sr-only">0% Complete</span>
                        </div>
                      </div><!-- /.progress -->
                    </div><!-- /.form-group -->
                    <!-- .form-group -->
                    <div class="form-group">
                      <!-- save task todos to this input hidden -->
                      <input type="hidden" name="ntTodos" value="[{id:0, desc:Eat corn on the cob, checked: false}]"> <!-- .todo-list -->
                      <div id="ntTodos" class="todo-list">
                        <!-- .todo -->
                        <div class="todo">
                          <!-- .custom-control -->
                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="todo0" value="0"> <label class="custom-control-label" for="todo0">Eat corn on the cob</label>
                          </div><!-- /.custom-control -->
                          <!-- .todo-actions -->
                          <div class="todo-actions pr-1">
                            <button type="button" class="btn btn-sm btn-light" onclick="boardDemo.removeNtTodo(0)">Delete</button>
                          </div><!-- /.todo-actions -->
                        </div><!-- /.todo -->
                      </div><!-- /.todo-list -->
                      <!-- .publisher -->
                      <div class="publisher">
                        <!-- .publisher-input -->
                        <div class="publisher-input pr-0">
                          <input id="ntTodosInpt" class="form-control form-control-reflow" placeholder="Add a todo" autocomplete="off">
                        </div><!-- /.publisher-input -->
                        <!-- .publisher-actions -->
                        <div class="publisher-actions">
                          <!-- .publisher-tools -->
                          <div class="publisher-tools pb-0">
                            <button id="ntTodosAdd" type="button" class="btn btn-secondary">Add</button> <button id="ntTodosClear" type="button" class="btn btn-light"><i class="fa fa-times"></i></button>
                          </div><!-- /.publisher-tools -->
                        </div><!-- /.publisher-actions -->
                      </div><!-- /.publisher -->
                    </div><!-- /.form-group -->
                  </div><!-- /.modal-body -->
                  <hr>
                  <!-- .modal-footer -->
                  <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save</button> <button type="reset" class="btn btn-light" data-dismiss="modal">Cancel</button>
                  </div><!-- /.modal-footer -->
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </form><!-- /.form -->
          <!-- .modal -->
          <div class="modal modal-drawer fade" id="modalViewTask" tabindex="-1" role="dialog" aria-labelledby="modalViewTaskLabel" aria-hidden="true">
            <!-- .modal-dialog -->
            <div class="modal-dialog modal-lg modal-drawer-right" role="document">
              <!-- .modal-content -->
              <div class="modal-content">
                <!-- .modal-body -->
                <div class="modal-body p-3 p-lg-4">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                      <a href="#!" data-dismiss="modal"><i class="breadcrumb-icon fa fa-angle-left mr-2"></i>In Progress</a>
                    </li>
                  </ol>
                  <h5 class="modal-title" id="modalLayer2Title"> Ride a Roller Coaster </h5>
                  <hr>
                  <p class="text-muted"> Opened one week ago by <a href="#!" class="link-text" data-toggle="tooltip" title="" data-original-title="@jgrif"><span class="user-avatar user-avatar-xs"><img src="http://uselooper.com/assets/images/avatars/uifaces6.jpg" alt=""></span> Jacob Griffin</a>
                  </p>
                  <hr>
                  <div class="task-description">
                    <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus perferendis magnam fugit veritatis numquam perspiciatis. Mollitia optio vero eum velit, recusandae! Amet in consectetur expedita repellat, obcaecati non deserunt molestias. </p>
                  </div><!-- grid row -->
                  <div class="row my-3">
                    <!-- grid column -->
                    <div class="col-6 mb-3">
                      <!-- .time-tracking -->
                      <div class="time-tracking">
                        <h6> Time tracking </h6><button class="btn btn-subtle-success btn-icon"><i class="fa fa-play"></i></button> <span class="counter ml-1"><span class="hours">00</span> <span class="separtor">:</span> <span class="minutes">00</span> <span class="separtor">:</span> <span class="second">00</span></span> / <span class="estimate">8h</span>
                      </div><!-- /.time-tracking -->
                    </div><!-- /grid column -->
                    <!-- grid column -->
                    <div class="col-6 mb-3">
                      <h6> Due date </h6>
                      <div class="inline-editable pt-1">
                        <input type="text" class="form-control" value="No due date">
                      </div>
                    </div><!-- /grid column -->
                    <!-- grid column -->
                    <div class="col-6 mb-3">
                      <h6> Labels </h6><span class="tile bg-green"></span> <span class="tile bg-pink"></span> <span class="tile bg-yellow"></span>
                      <div class="dropdown d-inline-block">
                        <a href="#!" class="tile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-plus"></i></a>
                        <div class="dropdown-arrow"></div><!-- .dropdown-menu -->
                        <div class="dropdown-menu dropdown-menu-md stop-propagation" id="dntmLabels" aria-labelledby="dropdownTaskLabels">
                          <h6 class="dropdown-header"> Select labels </h6>
                          <div class="dropdown-divider"></div>
                          <div class="form-group px-3 py-2 mb-0">
                            <input type="text" class="form-control" data-filter="#dntmLabels .filterable" placeholder="Search" autofocus="">
                          </div><label class="custom-control custom-checkbox filterable"><input type="checkbox" class="custom-control-input task-nolabel" name="taskLabels[]" value="0" data-label="No Label" checked=""> <span class="custom-control-label">No Label</span></label>
                          <div class="dropdown-divider"></div>
                          <div class="dropdown-scroll">
                            <label class="custom-control custom-checkbox filterable"><input type="checkbox" class="custom-control-input task-label" name="taskLabels[]" value="1" data-label="http://uselooper.com/assets"> <span class="custom-control-label"><i class="fa fa-square fa-lg text-blue mr-1"></i> http://uselooper.com/assets</span></label> <label class="custom-control custom-checkbox filterable"><input type="checkbox" class="custom-control-input task-label" name="taskLabels[]" value="2" data-label="Build Tools"> <span class="custom-control-label"><i class="fa fa-square fa-lg text-indigo mr-1"></i> Build Tools</span></label> <label class="custom-control custom-checkbox filterable"><input type="checkbox" class="custom-control-input task-label" name="taskLabels[]" value="3" data-label="Components"> <span class="custom-control-label"><i class="fa fa-square fa-lg text-purple mr-1"></i> Components</span></label> <label class="custom-control custom-checkbox filterable"><input type="checkbox" class="custom-control-input task-label" name="taskLabels[]" value="4" data-label="Dependencies"> <span class="custom-control-label"><i class="fa fa-square fa-lg text-pink mr-1"></i> Dependencies</span></label> <label class="custom-control custom-checkbox filterable"><input type="checkbox" class="custom-control-input task-label" name="taskLabels[]" value="5" data-label="Design"> <span class="custom-control-label"><i class="fa fa-square fa-lg text-red mr-1"></i> Design</span></label> <label class="custom-control custom-checkbox filterable"><input type="checkbox" class="custom-control-input task-label" name="taskLabels[]" value="6" data-label="Documentation"> <span class="custom-control-label"><i class="fa fa-square fa-lg text-orange mr-1"></i> Documentation</span></label> <label class="custom-control custom-checkbox filterable"><input type="checkbox" class="custom-control-input task-label" name="taskLabels[]" value="7" data-label="Doing"> <span class="custom-control-label"><i class="fa fa-square fa-lg text-yellow mr-1"></i> Doing</span></label> <label class="custom-control custom-checkbox filterable"><input type="checkbox" class="custom-control-input task-label" name="taskLabels[]" value="8" data-label="JS"> <span class="custom-control-label"><i class="fa fa-square fa-lg text-green mr-1"></i> JS</span></label> <label class="custom-control custom-checkbox filterable"><input type="checkbox" class="custom-control-input task-label" name="taskLabels[]" value="9" data-label="SCSS"> <span class="custom-control-label"><i class="fa fa-square fa-lg text-teal mr-1"></i> SCSS</span></label> <label class="custom-control custom-checkbox filterable"><input type="checkbox" class="custom-control-input task-label" name="taskLabels[]" value="10" data-label="To Do"> <span class="custom-control-label"><i class="fa fa-square fa-lg text-cyan mr-1"></i> To Do</span></label>
                          </div>
                          <div class="dropdown-divider"></div><a href="#!" class="dropdown-item">Create new label</a> <a href="#!" class="dropdown-item">Manage labels</a>
                        </div><!-- /.dropdown-menu -->
                      </div>
                    </div><!-- /grid column -->
                    <!-- grid column -->
                    <div class="col-6 mb-3">
                      <!-- .assignee -->
                      <div class="assignee">
                        <h6> Assignee </h6>
                        <div class="avatar-group">
                          <a href="#!" class="user-avatar" data-toggle="tooltip" title="" data-original-title="You"><img src="http://uselooper.com/assets/images/avatars/uifaces2.jpg" alt="You"></a> <a href="#!" class="user-avatar" data-toggle="tooltip" title="" data-original-title="Mellisa Day"><img src="http://uselooper.com/assets/images/avatars/uifaces4.jpg" alt="Mellisa Day"></a>
                          <div class="dropdown d-inline-block">
                            <a href="#!" class="tile tile-circle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-plus"></i></a>
                            <div class="dropdown-arrow"></div><!-- .dropdown-menu -->
                            <div class="dropdown-menu dropdown-menu-md stop-propagation" aria-labelledby="dropdownAssignee" style="max-height: 30rem; overflow-y: auto;">
                              <h6 class="dropdown-header"> Add assignee </h6>
                              <div class="dropdown-divider"></div>
                              <div class="form-group px-3 py-2 mb-0">
                                <input type="text" class="form-control" placeholder="Search members" autofocus="">
                              </div><label class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input task-unassignees" name="taskAssignees[]" value="0" data-label="Unassigned"> <span class="custom-control-label">Unassigned</span></label>
                              <div class="dropdown-divider"></div>
                              <h6 class="dropdown-header"> Assignee(s) </h6><label class="custom-control custom-checkbox filterable" data-sort="Beni Arisandi"><input type="checkbox" class="custom-control-input task-assignees" name="taskAssignees[]" value="4" data-label="Beni Arisandi" checked=""> <span class="custom-control-label custom-control-label-media"><span class="media"><span class="mr-2"><span class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces2.jpg" alt=""></span></span> <span class="media-body">Beni Arisandi<br>
                                      <span class="text-muted">@benz</span></span></span></span></label> <label class="custom-control custom-checkbox filterable" data-sort="Melissa Day"><input type="checkbox" class="custom-control-input task-assignees" name="taskAssignees[]" value="2" data-label="Melissa Day" checked=""> <span class="custom-control-label custom-control-label-media"><span class="media"><span class="mr-2"><span class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces4.jpg" alt=""></span></span> <span class="media-body">Melissa Day<br>
                                      <span class="text-muted">@melday</span></span></span></span></label>
                              <div id="dntDivider" class="dropdown-divider"></div><label class="custom-control custom-checkbox filterable" data-sort="Adam Medina"><input type="checkbox" class="custom-control-input task-assignees" name="taskAssignees[]" value="3" data-label="Adam Medina"> <span class="custom-control-label custom-control-label-media"><span class="media"><span class="mr-2"><span class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces15.jpg" alt=""></span></span> <span class="media-body">Adam Medina<br>
                                      <span class="text-muted">@amed</span></span></span></span></label> <label class="custom-control custom-checkbox filterable" data-sort="Diana Miller"><input type="checkbox" class="custom-control-input task-assignees" name="taskAssignees[]" value="1" data-label="Diana Miller"> <span class="custom-control-label custom-control-label-media"><span class="media"><span class="mr-2"><span class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces9.jpg" alt=""></span></span> <span class="media-body">Diana Miller<br>
                                      <span class="text-muted">@dmiller</span></span></span></span></label> <label class="custom-control custom-checkbox filterable" data-sort="Douglas Lucas"><input type="checkbox" class="custom-control-input task-assignees" name="taskAssignees[]" value="5" data-label="Douglas Lucas"> <span class="custom-control-label custom-control-label-media"><span class="media"><span class="mr-2"><span class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces5.jpg" alt=""></span></span> <span class="media-body">Douglas Lucas<br>
                                      <span class="text-muted">@lucaldoug</span></span></span></span></label>
                            </div><!-- /.dropdown-menu -->
                          </div>
                        </div>
                      </div><!-- /.assignee -->
                    </div><!-- /grid column -->
                  </div><!-- /grid row -->
                  <!-- .form-group -->
                  <div class="form-group">
                    <div class="d-flex justify-content-between">
                      <label>Todos</label> <span class="text-muted">(2/3)</span>
                    </div><!-- .progress -->
                    <div class="progress progress-sm">
                      <div class="progress-bar bg-primary" role="progressbar" aria-valuenow="66.67" aria-valuemin="0" aria-valuemax="100" style="width: 66.67%">
                        <span class="sr-only">66.67% Complete</span>
                      </div>
                    </div><!-- /.progress -->
                  </div><!-- /.form-group -->
                  <!-- .form-group -->
                  <div class="form-group">
                    <!-- save task todos to this input hidden -->
                    <input type="hidden" name="vtTodos"> <!-- .todo-list -->
                    <div id="vtTodos" class="todo-list">
                      <!-- .todo -->
                      <div class="todo">
                        <!-- .custom-control -->
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="vtodo0" value="0" checked=""> <label class="custom-control-label" for="vtodo0">Eat corn on the cob</label>
                        </div><!-- /.custom-control -->
                        <!-- .todo-actions -->
                        <div class="todo-actions pr-1">
                          <button type="button" class="btn btn-sm btn-light">Delete</button>
                        </div><!-- /.todo-actions -->
                      </div><!-- /.todo -->
                      <!-- .todo -->
                      <div class="todo">
                        <!-- .custom-control -->
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="vtodo1" value="1"> <label class="custom-control-label" for="vtodo1">Mix up a pitcher of sangria</label>
                        </div><!-- /.custom-control -->
                        <!-- .todo-actions -->
                        <div class="todo-actions pr-1">
                          <button type="button" class="btn btn-sm btn-light">Delete</button>
                        </div><!-- /.todo-actions -->
                      </div><!-- /.todo -->
                      <!-- .todo -->
                      <div class="todo">
                        <!-- .custom-control -->
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="vtodo2" value="2" checked=""> <label class="custom-control-label" for="vtodo2">Have a barbecue</label>
                        </div><!-- /.custom-control -->
                        <!-- .todo-actions -->
                        <div class="todo-actions pr-1">
                          <button type="button" class="btn btn-sm btn-light">Delete</button>
                        </div><!-- /.todo-actions -->
                      </div><!-- /.todo -->
                    </div><!-- /.todo-list -->
                    <!-- .publisher -->
                    <div class="publisher">
                      <!-- .publisher-input -->
                      <div class="publisher-input pr-0">
                        <input class="form-control form-control-reflow" placeholder="Add a todo" autocomplete="off">
                      </div><!-- /.publisher-input -->
                      <!-- .publisher-actions -->
                      <div class="publisher-actions">
                        <!-- .publisher-tools -->
                        <div class="publisher-tools pb-0">
                          <button type="button" class="btn btn-secondary">Add</button> <button type="button" class="btn btn-light"><i class="fa fa-times"></i></button>
                        </div><!-- /.publisher-tools -->
                      </div><!-- /.publisher-actions -->
                    </div><!-- /.publisher -->
                  </div><!-- /.form-group -->
                  <hr>
                  <!-- .media -->
                  <div class="media">
                    <figure class="user-avatar user-avatar-md mr-2">
                      <img src="http://uselooper.com/assets/images/avatars/profile.jpg" alt="">
                    </figure><!-- .media-body -->
                    <div class="media-body">
                      <!-- .publisher -->
                      <div class="publisher keep-focus focus">
                        <label for="publisherInput7" class="publisher-label">Add comment</label> <!-- .publisher-input -->
                        <div class="publisher-input">
                          <textarea id="publisherInput7" class="form-control" placeholder="Write a comment"></textarea>
                        </div><!-- /.publisher-input -->
                        <!-- .publisher-actions -->
                        <div class="publisher-actions">
                          <!-- .publisher-tools -->
                          <div class="publisher-tools mr-auto">
                            <button type="button" class="btn btn-light btn-icon fileinput-button"><i class="fa fa-paperclip"></i> <input type="file" id="message-attachment" name="attachment[]" multiple=""></button> <button type="button" class="btn btn-light btn-icon"><i class="far fa-smile"></i></button>
                          </div><!-- /.publisher-tools -->
                          <button type="submit" class="btn btn-primary">Publish</button>
                        </div><!-- /.publisher-actions -->
                      </div><!-- /.publisher -->
                    </div><!-- /.media-body -->

                  </div><!-- /.media -->
                </div><!-- /.modal-body -->
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->
        </div>
@endsection
