@extends('admin.master.master')


@section('content')
<div class="wrapper">
          <!-- .page -->
          <div class="page">
            <!-- .page-cover -->
            <header class="page-cover">
              <div class="text-center">
                <a href="{{route('admin.profile')}}" class="user-avatar user-avatar-xl"><img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt=""></a>
                <h2 class="h4 mt-2 mb-0"> {{Auth::user()->firstname}} {{Auth::user()->lastname}}</h2>
                <div class="my-1">
                  @if(Auth::user()->username == 'McDarsenek')
                    <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i>
                  @else
                  <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i> <i class="fa fa-star-o text-yellow"></i> <i class="fa fa-star-o text-yellow"></i> <i class="fa fa-star-o text-yellow"></i>
                  @endif
                </div>
                <p class="text-muted"> Administrator </p>
                <p>  </p>
              </div><!-- .cover-controls --><!-- /.cover-controls -->
              <div class="cover-controls cover-controls-bottom">
                <a href="#!" class="btn btn-light" data-toggle="modal" data-target="#RequestModal"><span class="fa fa-fw fa-bell pulse"></span> 2,159 Notifications</a>
                 <a href="#!" class="btn btn-light" data-toggle="modal" data-target="#MessagesModal"><span class="fa fa-fw fa-envelope pulse"></span>136 Messages</a>
              </div><!-- /.cover-controls -->
            </header><!-- /.page-cover -->
            <!-- Followers Modal -->
            <!-- .modal -->
            <div class="modal fade " id="RequestModal" tabindex="-1" role="dialog" aria-labelledby="RequestModalLabel" aria-hidden="true">
              <!-- .modal-dialog -->
              <div class="modal-dialog modal-dialog-vert" role="document">
                <!-- .modal-content -->
                <div class="modal-content">
                  <!-- .modal-header -->
                  <div class="modal-header">
                    <h6 class="modal-title"> Followers </h6>
                  </div><!-- /.modal-header -->
                  <!-- .modal-body -->
                  <div class="modal-body px-0">
                    <!-- .list-group -->
                    <div class="list-group list-group-flush list-group-divider border">

                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces7.jpg" alt="Julia Silva"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Julia Silva</a>
                          </h4>
                          <p class="list-group-item-text"> Photographer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button type="button" class="btn btn-sm btn-primary">Follow</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces1.jpg" alt="Joe Hanson"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Joe Hanson</a>
                          </h4>
                          <p class="list-group-item-text"> Logistician </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button type="button" class="btn btn-sm btn-secondary">Follow</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces11.jpg" alt="Brenda Griffin"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Brenda Griffin</a>
                          </h4>
                          <p class="list-group-item-text"> Medical Assistant </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button type="button" class="btn btn-sm btn-primary">Follow</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces12.jpg" alt="Ryan Jimenez"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Ryan Jimenez</a>
                          </h4>
                          <p class="list-group-item-text"> Photographer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button type="button" class="btn btn-sm btn-primary">Follow</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces13.jpg" alt="Bryan Hayes"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Bryan Hayes</a>
                          </h4>
                          <p class="list-group-item-text"> Computer Systems Analyst </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button type="button" class="btn btn-sm btn-primary">Follow</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces14.jpg" alt="Cynthia Clark"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Cynthia Clark</a>
                          </h4>
                          <p class="list-group-item-text"> Web Developer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button type="button" class="btn btn-sm btn-primary">Follow</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->


                    </div><!-- /.list-group -->
                    <!-- .loading -->
                    <div class="loading border-bottom">
                      <div class="loader loader-sm fa-spin"></div>
                      <div class="sr-only"> Loading more content </div>
                    </div><!-- /.loading -->
                  </div><!-- /.modal-body -->
                  <!-- .modal-footer -->
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger btn-sm" data-dismiss="modal">Close</button>
                  </div><!-- /.modal-footer -->
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <!-- /Followers Modal -->
            <!-- .modal -->
            <div class="modal fade " id="MessagesModal" tabindex="-1" role="dialog" aria-labelledby="MessagesModalLabel" aria-hidden="true">
              <!-- .modal-dialog -->
              <div class="modal-dialog modal-dialog-vert" role="document">
                <!-- .modal-content -->
                <div class="modal-content">
                  <!-- .modal-header -->
                  <div class="modal-header">
                    <h6 class="modal-title"> Messages </h6>
                  </div><!-- /.modal-header -->
                  <!-- .modal-body -->
                  <div class="modal-body px-0">
                    <!-- .list-group -->
                    <div class="list-group list-group-flush list-group-divider border">
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces2.jpg" alt="Johnny Day"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Johnny Day</a>
                          </h4>
                          <p class="list-group-item-text"> Computer Hardware Engineer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button class="btn btn-sm btn-secondary">Following</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces3.jpg" alt="Sarah Bishop"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Sarah Bishop</a>
                          </h4>
                          <p class="list-group-item-text"> Designer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button class="btn btn-sm btn-secondary">Following</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces13.jpg" alt="Bryan Hayes"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Bryan Hayes</a>
                          </h4>
                          <p class="list-group-item-text"> Computer Systems Analyst </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button class="btn btn-sm btn-secondary">Following</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces14.jpg" alt="Cynthia Clark"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Cynthia Clark</a>
                          </h4>
                          <p class="list-group-item-text"> Web Developer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button class="btn btn-sm btn-secondary">Following</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces6.jpg" alt="Martha Myers"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Martha Myers</a>
                          </h4>
                          <p class="list-group-item-text"> Writer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button class="btn btn-sm btn-secondary">Following</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces5.jpg" alt="Tammy Beck"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Tammy Beck</a>
                          </h4>
                          <p class="list-group-item-text"> Designer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button class="btn btn-sm btn-secondary">Following</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces7.jpg" alt="Susan Kelley"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Susan Kelley</a>
                          </h4>
                          <p class="list-group-item-text"> Web Developer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button class="btn btn-sm btn-secondary">Following</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces1.jpg" alt="Albert Newman"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Albert Newman</a>
                          </h4>
                          <p class="list-group-item-text"> Web Developer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button class="btn btn-sm btn-secondary">Following</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces9.jpg" alt="Kyle Grant"></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Kyle Grant</a>
                          </h4>
                          <p class="list-group-item-text"> Designer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure">
                          <button class="btn btn-sm btn-secondary">Following</button>
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                    </div><!-- /.list-group -->
                    <!-- .loading -->
                    <div class="loading border-bottom">
                      <div class="loader loader-sm"></div>
                      <div class="sr-only"> Loading more content </div>
                    </div><!-- /.loading -->
                  </div><!-- /.modal-body -->
                  <!-- .modal-footer -->
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger btn-sm" data-dismiss="modal">Close</button>
                  </div><!-- /.modal-footer -->
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <!-- /Following Modal -->
            <nav class="page-navs">
              <!-- .nav-scroller -->
              <div class="nav-scroller">
                <!-- .nav -->
                <div class="nav nav-center nav-tabs">
                  <a class="nav-link" href="{{route('admin.profile')}}">Profile Overview</a>
                  <a class="nav-link active" href="javascript: window.location.reload();">Request <span class="badge">16</span></a>
                   <a class="nav-link" href="{{route('partner-list')}}">Partners</a>
                   <a class="nav-link" href="{{route('users')}}">Users</a>
                   <a class="nav-link" href="{{route('notifications')}}">Notifications</a>
                   <a class="nav-link " href="{{route('account')}}">Settings</a>
                </div><!-- /.nav -->
              </div><!-- /.nav-scroller -->
            </nav><!-- /.page-navs -->
            <!-- .page-inner -->
            <div class="page-inner">
              <!-- .page-title-bar -->
              <header class="page-title-bar">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                      <a href="user-profile.html"><i class="breadcrumb-icon fa fa-angle-left mr-2"></i>Overview</a>
                    </li>
                  </ol>
                </nav>
              </header><!-- /.page-title-bar -->
              <!-- .page-section -->
              <div class="page-section">
                <!-- .section-block -->
                <div class="section-block">
                  <!-- .board -->
                  <div class="board board-list">
                    <!-- .tasks -->
                    <section class="tasks">
                      <!-- .task-header -->
                      <header class="task-header">
                        <h1 class="task-title mr-auto"> On Going Tasks<small class="badge">(6)</small>
                        </h1><select class="custom-select" style="width:160px">
                          <option value=""> Filter project </option>
                          <option value="1"> CFM Admin  </option>
                          <option value="2"> Smart Paper </option>
                          <option value="3"> Booking Up </option>
                          <option value="4"> Online Store </option>
                        </select>
                      </header><!-- /.task-header -->
                      <!-- .task-body -->
                      <div class="task-body">
                        <!-- .task-issue -->
                        <div class="task-issue">
                          <!-- .card -->
                          <div class="card">
                            <!-- .card-header -->
                            <header class="card-header">
                              <h4 class="card-title">
                                <a href="#!">Make lemonade from scratch</a>
                              </h4><br>
                              <h6 class="card-subtitle text-muted">
                                <time class="text-muted">16:36</time> / <time class="text-muted">24:00</time> <span class="mx-1">·</span> <span class="due-date"><i class="far fa-fw fa-clock"></i> Apr 02</span>
                              </h6>
                            </header><!-- /.card-header -->
                            <!-- .card-body -->
                            <div class="card-body">
                              <!-- .list-group -->
                              <div class="list-group">
                                <!-- .list-group-item -->
                                <div class="list-group-item">
                                  <!-- .list-group-item-body -->
                                  <div class="list-group-item-body py-1 text-truncate">
                                    <a href="#!" class="tile tile-sm bg-indigo text-white" data-toggle="tooltip" title="" data-original-title="Smart Paper Project">SP</a> <span class="text-muted mx-2"><i class="fas fa-long-arrow-alt-right"></i></span> <!-- members -->
                                    <div class="avatar-group">
                                      <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Johnny Day"><img src="https://uselooper.com/assets/images/avatars/uifaces2.jpg" alt="Johnny Day"></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Sarah Bishop"><img src="https://uselooper.com/assets/images/avatars/uifaces3.jpg" alt="Sarah Bishop"></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Craig Hansen"><img src="https://uselooper.com/assets/images/avatars/uifaces5.jpg" alt="Craig Hansen"></a>
                                    </div><!-- /members -->
                                  </div><!-- /.list-group-item-body -->
                                </div><!-- /.list-group-item -->
                                <!-- .list-group-item -->
                                <a href="#!" class="list-group-item pt-0">
                                  <!-- .list-group-item-body -->
                                  <div class="list-group-item-body">
                                    <div class="progress progress-xs">
                                      <div class="progress-bar bg-success" role="progressbar" style="width: 66.66666666666667%;" aria-valuenow="66.66666666666667" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                  </div><!-- /.list-group-item-body -->
                                  <!-- .list-group-item-figure -->
                                  <div class="list-group-item-figure">
                                    <span class="todos">4/6</span>
                                  </div><!-- /.list-group-item-figure -->
                                </a> <!-- /.list-group-item -->
                              </div><!-- /.list-group -->
                            </div><!-- /.card-body -->
                            <!-- .card-footer -->
                            <footer class="card-footer">
                              <a href="#!" class="card-footer-item card-footer-item-bordered text-muted"><i class="oi oi-comment-square mr-1"></i> 12</a> <a href="#!" class="card-footer-item card-footer-item-bordered text-muted"><i class="fa fa-pause"></i></a>
                            </footer><!-- /.card-footer -->
                          </div><!-- .card -->
                        </div><!-- /.task-issue -->
                        <!-- .task-issue -->
                        <div class="task-issue">
                          <!-- .card -->
                          <div class="card">
                            <!-- .card-header -->
                            <header class="card-header">
                              <h4 class="card-title">
                                <a href="#!">Mix up a pitcher of sangria</a>
                              </h4><br>
                              <h6 class="card-subtitle text-muted">
                                <time class="text-muted">03:36</time> / <time class="text-muted">04:00</time> <span class="mx-1">·</span> <span class="due-date"><i class="far fa-fw fa-clock"></i> In 3 days</span>
                              </h6>
                            </header><!-- /.card-header -->
                            <!-- .card-body -->
                            <div class="card-body">
                              <!-- .list-group -->
                              <div class="list-group">
                                <!-- .list-group-item -->
                                <div class="list-group-item">
                                  <!-- .list-group-item-body -->
                                  <div class="list-group-item-body py-1 text-truncate">
                                    <a href="#!" class="tile tile-sm bg-blue text-white" data-toggle="tooltip" title="" data-original-title="Looper Admin Theme Project">LT</a> <span class="text-muted mx-2"><i class="fas fa-long-arrow-alt-right"></i></span> <!-- members -->
                                    <div class="avatar-group">
                                      <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Johnny Day"><img src="https://uselooper.com/assets/images/avatars/uifaces2.jpg" alt="Johnny Day"></a>
                                    </div><!-- /members -->
                                  </div><!-- /.list-group-item-body -->
                                </div><!-- /.list-group-item -->
                              </div><!-- /.list-group -->
                            </div><!-- /.card-body -->
                            <!-- .card-footer -->
                            <footer class="card-footer">
                              <a href="#!" class="card-footer-item card-footer-item-bordered text-muted"><i class="fa fa-pause"></i></a>
                            </footer><!-- /.card-footer -->
                          </div><!-- .card -->
                        </div><!-- /.task-issue -->
                        <!-- .task-issue -->
                        <div class="task-issue">
                          <!-- .card -->
                          <div class="card">
                            <!-- .card-header -->
                            <header class="card-header">
                              <h4 class="card-title">
                                <a href="#!">Ride a roller coaster</a>
                              </h4><br>
                              <h6 class="card-subtitle text-muted">
                                <time class="text-red">50:02</time> / <time class="text-muted">48:00</time> <span class="mx-1">·</span> <span class="due-date"><i class="far fa-fw fa-clock"></i> Tomorrow</span>
                              </h6>
                            </header><!-- /.card-header -->
                            <!-- .card-body -->
                            <div class="card-body">
                              <!-- .list-group -->
                              <div class="list-group">
                                <!-- .list-group-item -->
                                <div class="list-group-item">
                                  <!-- .list-group-item-body -->
                                  <div class="list-group-item-body py-1 text-truncate">
                                    <a href="#!" class="tile tile-sm bg-yellow text-white" data-toggle="tooltip" title="" data-original-title="Online Store Project">OS</a> <span class="text-muted mx-2"><i class="fas fa-long-arrow-alt-right"></i></span> <!-- members -->
                                    <div class="avatar-group">
                                      <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Johnny Day"><img src="https://uselooper.com/assets/images/avatars/uifaces2.jpg" alt="Johnny Day"></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Sarah Bishop"><img src="https://uselooper.com/assets/images/avatars/uifaces3.jpg" alt="Sarah Bishop"></a>
                                    </div><!-- /members -->
                                  </div><!-- /.list-group-item-body -->
                                </div><!-- /.list-group-item -->
                                <!-- .list-group-item -->
                                <a href="#!" class="list-group-item pt-0">
                                  <!-- .list-group-item-body -->
                                  <div class="list-group-item-body">
                                    <div class="progress progress-xs">
                                      <div class="progress-bar bg-success" role="progressbar" style="width: 90.47619047619048%;" aria-valuenow="90.47619047619048" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                  </div><!-- /.list-group-item-body -->
                                  <!-- .list-group-item-figure -->
                                  <div class="list-group-item-figure">
                                    <span class="todos">19/21</span>
                                  </div><!-- /.list-group-item-figure -->
                                </a> <!-- /.list-group-item -->
                              </div><!-- /.list-group -->
                            </div><!-- /.card-body -->
                            <!-- .card-footer -->
                            <footer class="card-footer">
                              <a href="#!" class="card-footer-item card-footer-item-bordered text-muted"><i class="oi oi-comment-square mr-1"></i> 36</a> <a href="#!" class="card-footer-item card-footer-item-bordered text-muted"><i class="fa fa-pause"></i></a>
                            </footer><!-- /.card-footer -->
                          </div><!-- .card -->
                        </div><!-- /.task-issue -->
                        <!-- .task-issue -->
                        <div class="task-issue">
                          <!-- .card -->
                          <div class="card">
                            <!-- .card-header -->
                            <header class="card-header">
                              <h4 class="card-title">
                                <a href="#!">Dangle your feet off a dock</a>
                              </h4><br>
                              <h6 class="card-subtitle text-muted">
                                <time class="text-muted">03:11</time> / <time class="text-muted">6:00</time> <span class="mx-1">·</span> <span class="due-date"><i class="far fa-fw fa-clock"></i> Apr 07</span>
                              </h6>
                            </header><!-- /.card-header -->
                            <!-- .card-body -->
                            <div class="card-body">
                              <!-- .list-group -->
                              <div class="list-group">
                                <!-- .list-group-item -->
                                <div class="list-group-item">
                                  <!-- .list-group-item-body -->
                                  <div class="list-group-item-body py-1 text-truncate">
                                    <a href="#!" class="tile tile-sm bg-pink text-white" data-toggle="tooltip" title="" data-original-title="Syrena Project Project">SP</a> <span class="text-muted mx-2"><i class="fas fa-long-arrow-alt-right"></i></span> <!-- members -->
                                    <div class="avatar-group">
                                      <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Johnny Day"><img src="https://uselooper.com/assets/images/avatars/uifaces2.jpg" alt="Johnny Day"></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Sarah Bishop"><img src="https://uselooper.com/assets/images/avatars/uifaces3.jpg" alt="Sarah Bishop"></a>
                                    </div><!-- /members -->
                                  </div><!-- /.list-group-item-body -->
                                </div><!-- /.list-group-item -->
                              </div><!-- /.list-group -->
                            </div><!-- /.card-body -->
                            <!-- .card-footer -->
                            <footer class="card-footer">
                              <a href="#!" class="card-footer-item card-footer-item-bordered text-muted"><i class="fa fa-pause"></i></a>
                            </footer><!-- /.card-footer -->
                          </div><!-- .card -->
                        </div><!-- /.task-issue -->
                        <!-- .task-issue -->
                        <div class="task-issue">
                          <!-- .card -->
                          <div class="card">
                            <!-- .card-header -->
                            <header class="card-header">
                              <h4 class="card-title">
                                <a href="#!">Have a picnic in the park</a>
                              </h4><br>
                              <h6 class="card-subtitle text-muted">
                                <time class="text-muted">08:36</time> / <time class="text-muted">12:00</time> <span class="mx-1">·</span> <span class="due-date"><i class="far fa-fw fa-clock"></i> In 2 days</span>
                              </h6>
                            </header><!-- /.card-header -->
                            <!-- .card-body -->
                            <div class="card-body">
                              <!-- .list-group -->
                              <div class="list-group">
                                <!-- .list-group-item -->
                                <div class="list-group-item">
                                  <!-- .list-group-item-body -->
                                  <div class="list-group-item-body py-1 text-truncate">
                                    <a href="#!" class="tile tile-sm bg-yellow text-white" data-toggle="tooltip" title="" data-original-title="Online Store Project">OS</a> <span class="text-muted mx-2"><i class="fas fa-long-arrow-alt-right"></i></span> <!-- members -->
                                    <div class="avatar-group">
                                      <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Johnny Day"><img src="https://uselooper.com/assets/images/avatars/uifaces2.jpg" alt="Johnny Day"></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Sarah Bishop"><img src="https://uselooper.com/assets/images/avatars/uifaces3.jpg" alt="Sarah Bishop"></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Craig Hansen"><img src="https://uselooper.com/assets/images/avatars/uifaces5.jpg" alt="Craig Hansen"></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Jane Barnes"><img src="https://uselooper.com/assets/images/avatars/uifaces9.jpg" alt="Jane Barnes"></a>
                                    </div><!-- /members -->
                                  </div><!-- /.list-group-item-body -->
                                </div><!-- /.list-group-item -->
                                <!-- .list-group-item -->
                                <a href="#!" class="list-group-item pt-0">
                                  <!-- .list-group-item-body -->
                                  <div class="list-group-item-body">
                                    <div class="progress progress-xs">
                                      <div class="progress-bar bg-success" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                  </div><!-- /.list-group-item-body -->
                                  <!-- .list-group-item-figure -->
                                  <div class="list-group-item-figure">
                                    <span class="todos">15/15</span>
                                  </div><!-- /.list-group-item-figure -->
                                </a> <!-- /.list-group-item -->
                              </div><!-- /.list-group -->
                            </div><!-- /.card-body -->
                            <!-- .card-footer -->
                            <footer class="card-footer">
                              <a href="#!" class="card-footer-item card-footer-item-bordered text-muted"><i class="oi oi-comment-square mr-1"></i> 8</a> <a href="#!" class="card-footer-item card-footer-item-bordered text-muted"><i class="fa fa-pause"></i></a>
                            </footer><!-- /.card-footer -->
                          </div><!-- .card -->
                        </div><!-- /.task-issue -->
                        <!-- .task-issue -->
                        <div class="task-issue">
                          <!-- .card -->
                          <div class="card">
                            <!-- .card-header -->
                            <header class="card-header">
                              <h4 class="card-title">
                                <a href="#!">Swim in a lake</a>
                              </h4>
                              <h6 class="card-subtitle text-muted">
                                <time class="text-muted">11:36</time> / <time class="text-muted">12:00</time> <span class="mx-1">·</span> <span class="due-date"><i class="far fa-fw fa-clock"></i> Mar 23</span>
                              </h6>
                            </header><!-- /.card-header -->
                            <!-- .card-body -->
                            <div class="card-body">
                              <!-- .list-group -->
                              <div class="list-group">
                                <!-- .list-group-item -->
                                <div class="list-group-item">
                                  <!-- .list-group-item-body -->
                                  <div class="list-group-item-body py-1 text-truncate">
                                    <a href="#!" class="tile tile-sm bg-blue text-white" data-toggle="tooltip" title="" data-original-title="Looper Admin Theme Project">LT</a> <span class="text-muted mx-2"><i class="fas fa-long-arrow-alt-right"></i></span> <!-- members -->
                                    <div class="avatar-group">
                                      <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Johnny Day"><img src="https://uselooper.com/assets/images/avatars/uifaces2.jpg" alt="Johnny Day"></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Sarah Bishop"><img src="https://uselooper.com/assets/images/avatars/uifaces3.jpg" alt="Sarah Bishop"></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Craig Hansen"><img src="https://uselooper.com/assets/images/avatars/uifaces5.jpg" alt="Craig Hansen"></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Jane Barnes"><img src="https://uselooper.com/assets/images/avatars/uifaces9.jpg" alt="Jane Barnes"></a>
                                    </div><!-- /members -->
                                  </div><!-- /.list-group-item-body -->
                                </div><!-- /.list-group-item -->
                                <!-- .list-group-item -->
                                <a href="#!" class="list-group-item pt-0">
                                  <!-- .list-group-item-body -->
                                  <div class="list-group-item-body">
                                    <div class="progress progress-xs">
                                      <div class="progress-bar bg-success" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                  </div><!-- /.list-group-item-body -->
                                  <!-- .list-group-item-figure -->
                                  <div class="list-group-item-figure">
                                    <span class="todos">6/6</span>
                                  </div><!-- /.list-group-item-figure -->
                                </a> <!-- /.list-group-item -->
                              </div><!-- /.list-group -->
                            </div><!-- /.card-body -->
                            <!-- .card-footer -->
                            <footer class="card-footer">
                              <a href="#!" class="card-footer-item card-footer-item-bordered text-muted"><i class="oi oi-comment-square mr-1"></i> 24</a> <a href="#!" class="card-footer-item card-footer-item-bordered text-muted"><i class="fa fa-pause"></i></a>
                            </footer><!-- /.card-footer -->
                          </div><!-- .card -->
                        </div><!-- /.task-issue -->
                      </div><!-- /.task-body -->
                    </section><!-- /.tasks -->
                  </div><!-- /.board -->
                </div><!-- /.section-block -->
              </div><!-- /.page-section -->
            </div><!-- /.page-inner -->
          </div><!-- /.page -->
        </div>
@endsection
