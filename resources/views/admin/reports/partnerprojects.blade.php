@extends('admin.master.master')


@section('content')
<div class="wrapper">
          <!-- .page -->
        @foreach($partners as $partner)
          <div class="page has-sidebar has-sidebar-expand-xl"><div class="sidebar-backdrop"></div>
            <!-- .page-inner -->
            <div class="page-inner">
              <!-- .page-title-bar -->
              <header class="page-title-bar">
                <!-- .d-flex -->
                <div class="d-flex justify-content-between align-items-center">
                  <!-- .breadcrumb -->
                  <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item active">
                        <a href="javascript:window.location.reload();"><i class="breadcrumb-icon fa fa-angle-left mr-2"></i>Projects</a>
                      </li>
                    </ol>
                  </nav><!-- /.breadcrumb -->
                  <button type="button" class="btn btn-light btn-icon d-xl-none" data-toggle="sidebar"><i class="fa fa-angle-double-left"></i></button>
                </div><!-- /.d-flex -->
                <!-- grid row -->
                <div class="row text-center text-sm-left">
                  <!-- grid column -->
                  <div class="col-sm-auto col-12 mb-2">
                    <!-- .has-badge -->
                    <div class="has-badge has-badge-bottom">
                      <a href="#!" class="user-avatar user-avatar-xl"><img src="https://uselooper.com/assets/images/avatars/team4.jpg" alt=""></a> <span class="tile tile-circle tile-xs" data-toggle="tooltip" title="" data-original-title="Public"><i class="fa fa-globe"></i></span>
                    </div><!-- /.has-badge -->
                  </div><!-- /grid column -->
                  <!-- grid column -->
                  <div class="col">
                    <h1 class="page-title text-capitalize">{{$partner->name}}  </h1>
                    <p class="text-muted">All the project under this partner  </p>
                  </div><!-- /grid column -->
                </div><!-- /grid row -->
                <!-- .nav-scroller -->
                <div class="nav-scroller border-bottom">
                  <!-- .nav -->
                  <div class="nav nav-tabs">
                    <a class="nav-link" data-toggle="tab" href="#partner-overview">Overview</a>
                    <a class="nav-link" data-toggle="tab" href="#partner-feeds" >Feeds</a>
                    <a class="nav-link active" data-toggle="tab" href="#partner-projects" >Projects</a>
                    <a class="nav-link" data-toggle="tab" href="#partner-member" >Members</a>
                    <a class="nav-link" href="#!">Settings</a>
                  </div><!-- /.nav -->
                </div><!-- /.nav-scroller -->
              </header><!-- /.page-title-bar -->

              <!-- .tab-content -->
              <div class="tab-content pt-4" id="partnerDetailsTabs">
                <!-- .tab-pane -->
                <div class="tab-pane fade show " id="partner-overview" role="tabpanel" aria-labelledby="partner-overview-tab">
                    <!-- overview -->
                    <div class="page-section">
                      <!-- .section-block -->
                      <div class="section-block">
                        <!-- .metric-row -->
                        <div class="metric-row metric-flush">
                          <!-- metric column -->
                          <div class="col">
                            <!-- .metric -->
                            <a href="page-team-members.html" class="metric metric-bordered align-items-center">
                              <h2 class="metric-label"> Members </h2>
                              <p class="metric-value h3">
                                <sub><i class="oi oi-people"></i></sub> <span class="value">12</span>
                              </p>
                            </a> <!-- /.metric -->
                          </div><!-- /metric column -->
                          <!-- metric column -->
                          <div class="col">
                            <!-- .metric -->
                            <a href="page-team-projects.html" class="metric metric-bordered align-items-center">
                              <h2 class="metric-label"> Projects </h2>
                              <p class="metric-value h3">
                                <sub><i class="oi oi-fork"></i></sub> <span class="value">26</span>
                              </p>
                            </a> <!-- /.metric -->
                          </div><!-- /metric column -->
                          <!-- metric column -->
                          <div class="col">
                            <!-- .metric -->
                            <a href="page-team-projects.html" class="metric metric-bordered align-items-center">
                              <h2 class="metric-label"> Active Projects </h2>
                              <p class="metric-value h3">
                                <sub><i class="oi oi-timer fa-lg"></i></sub> <span class="value">5</span>
                              </p>
                            </a> <!-- /.metric -->
                          </div><!-- /metric column -->
                        </div><!-- /.metric-row -->
                      </div><!-- /.section-block -->
                      <!-- .section-block -->
                      <div class="section-block d-flex justify-content-between align-items-center my-3">
                        <h1 class="section-title mb-0"> Achievement </h1><!-- .dropdown -->
                        <div class="dropdown">
                          <button class="btn btn-secondary" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span>This Month</span> <i class="fa fa-fw fa-caret-down"></i></button>
                          <div class="dropdown-arrow dropdown-arrow-right"></div><!-- .dropdown-menu -->
                          <div class="dropdown-menu dropdown-menu-right dropdown-menu-md stop-propagation">
                            <!-- .custom-control -->
                            <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" id="dpToday" name="dpFilter" value="0"> <label class="custom-control-label d-flex justify-content-between" for="dpToday"><span>Today</span> <span class="text-muted">Mar 27</span></label>
                            </div><!-- /.custom-control -->
                            <!-- .custom-control -->
                            <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" id="dpYesterday" name="dpFilter" value="1"> <label class="custom-control-label d-flex justify-content-between" for="dpYesterday"><span>Yesterday</span> <span class="text-muted">Mar 26</span></label>
                            </div><!-- /.custom-control -->
                            <!-- .custom-control -->
                            <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" id="dpWeek" name="dpFilter" value="2"> <label class="custom-control-label d-flex justify-content-between" for="dpWeek"><span>This Week</span> <span class="text-muted">Mar 21-27</span></label>
                            </div><!-- /.custom-control -->
                            <!-- .custom-control -->
                            <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" id="dpMonth" name="dpFilter" value="3" checked=""> <label class="custom-control-label d-flex justify-content-between" for="dpMonth"><span>This Month</span> <span class="text-muted">Mar 1-31</span></label>
                            </div><!-- /.custom-control -->
                            <!-- .custom-control -->
                            <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" id="dpYear" name="dpFilter" value="4"> <label class="custom-control-label d-flex justify-content-between" for="dpYear"><span>This Year</span> <span class="text-muted">2018</span></label>
                            </div><!-- /.custom-control -->
                            <!-- .custom-control -->
                            <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" id="dpCustom" name="dpFilter" value="5"> <label class="custom-control-label" for="dpCustom">Custom</label>
                              <div class="custom-control-hint my-1" for="dpCustom">
                                <!-- datepicker:range -->
                                <input type="text" class="form-control flatpickr-input" data-toggle="flatpickr" data-mode="range" data-date-format="Y-m-d" readonly="readonly"> <!-- /datepicker:range -->
                              </div>
                            </div><!-- /.custom-control -->
                          </div><!-- /.dropdown-menu -->
                        </div><!-- /.dropdown -->
                      </div><!-- /.section-block -->
                      <!-- .card -->
                      <div class="card card-body">
                        <!-- legend -->
                        <ul class="list-inline small">
                          <li class="list-inline-item">
                            <i class="fa fa-fw fa-circle text-teal"></i> Projects </li>
                          <li class="list-inline-item">
                            <i class="fa fa-fw fa-circle text-purple"></i> Completed </li>
                        </ul><!-- /legend -->
                        <div class="chartjs"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                          <canvas id="canvas-achievement" width="826" height="312" class="chartjs-render-monitor" style="display: block; height: 250px; width: 661px;"></canvas>
                        </div>
                      </div><!-- /.card -->
                      <!-- .section-deck -->
                      <div class="section-deck">
                        <!-- .card -->
                        <section class="card">
                          <!-- .card-body -->
                          <div class="card-body">
                            <h3 class="card-title"> Leads Funnel </h3>
                            <h6 class="h2 mb-3"> 2,630 </h6><!-- .progress -->
                            <div class="progress progress-animated rounded-0">
                              <div class="progress-bar bg-purple" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                              <div class="progress-bar bg-pink" role="progressbar" style="width: 20.5%" aria-valuenow="20.5" aria-valuemin="0" aria-valuemax="100"></div>
                              <div class="progress-bar bg-yellow" role="progressbar" style="width: 13%" aria-valuenow="13" aria-valuemin="0" aria-valuemax="100"></div>
                              <div class="progress-bar bg-green" role="progressbar" style="width: 8.75%" aria-valuenow="8.75" aria-valuemin="0" aria-valuemax="100"></div>
                              <div class="progress-bar bg-teal" role="progressbar" style="width: 1.5%" aria-valuenow="1.5" aria-valuemin="0" aria-valuemax="100"></div>
                            </div><!-- /.progress -->
                          </div><!-- /.card-body -->
                          <!-- .list-group -->
                          <div class="list-group list-group-bordered list-group-reflow">
                            <div class="list-group-item justify-content-between align-items-center">
                              <span><i class="fa fa-square text-purple mr-2"></i> New</span> <span class="text-muted">30%</span>
                            </div>
                            <div class="list-group-item justify-content-between align-items-center">
                              <span><i class="fa fa-square text-pink mr-2"></i> Initial Contact</span> <span class="text-muted">20.5%</span>
                            </div>
                            <div class="list-group-item justify-content-between align-items-center">
                              <span><i class="fa fa-square text-yellow mr-2"></i> Qualified</span> <span class="text-muted">13%</span>
                            </div>
                            <div class="list-group-item justify-content-between align-items-center">
                              <span><i class="fa fa-square text-green mr-2"></i> Proposal</span> <span class="text-muted">9.75%</span>
                            </div>
                            <div class="list-group-item justify-content-between align-items-center">
                              <span><i class="fa fa-square text-teal mr-2"></i> Conversion to clients</span> <span class="text-muted">1.5%</span>
                            </div>
                          </div><!-- /.list-group -->
                        </section><!-- /.card -->
                        <!-- .card -->
                        <section class="card">
                          <!-- .card-body -->
                          <div class="card-body pb-0">
                            <h3 class="card-title"> Leaderboards </h3><!-- legend -->
                            <ul class="list-inline small">
                              <li class="list-inline-item">
                                <i class="fa fa-fw fa-square text-teal"></i> Mailchimp </li>
                              <li class="list-inline-item">
                                <i class="fa fa-fw fa-square text-indigo"></i> Facebook </li>
                              <li class="list-inline-item">
                                <i class="fa fa-fw fa-square text-pink"></i> Google </li>
                              <li class="list-inline-item">
                                <i class="fa fa-fw fa-square text-purple"></i> Linkedin </li>
                            </ul><!-- /legend -->
                          </div><!-- /.card-body -->
                          <!-- .list-group -->
                          <div class="list-group list-group-flush">
                            <!-- .list-group-item -->
                            <div class="list-group-item">
                              <!-- .list-group-item-figure -->
                              <div class="list-group-item-figure">
                                <a href="user-profile.html" class="user-avatar" data-toggle="tooltip" title="" data-original-title="Martha Myers"><img src="https://uselooper.com/assets/images/avatars/uifaces19.jpg" alt=""></a>
                              </div><!-- /.list-group-item-figure -->
                              <!-- .list-group-item-body -->
                              <div class="list-group-item-body">
                                <!-- .progress -->
                                <div class="progress progress-animated rounded-0" data-toggle="tooltip" data-html="true" title="" data-original-title="<div class=&quot;text-left small&quot;><i class=&quot;fa fa-fw fa-square text-teal&quot;></i> 275<br><i class=&quot;fa fa-fw fa-square text-indigo&quot;></i> 614<br><i class=&quot;fa fa-fw fa-square text-pink&quot;></i> 534<br><i class=&quot;fa fa-fw fa-square text-purple&quot;></i> 388</div>">
                                  <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="15.184980673660961" aria-valuemin="0" aria-valuemax="100" style="width: 15.184980673660961%">
                                    <span class="sr-only">15.184980673660961% Complete</span>
                                  </div>
                                  <div class="progress-bar bg-indigo" role="progressbar" aria-valuenow="33.90392048591938" aria-valuemin="0" aria-valuemax="100" style="width: 33.90392048591938%">
                                    <span class="sr-only">33.90392048591938% Complete</span>
                                  </div>
                                  <div class="progress-bar bg-pink" role="progressbar" aria-valuenow="29.486471562672556" aria-valuemin="0" aria-valuemax="100" style="width: 29.486471562672556%">
                                    <span class="sr-only">29.486471562672556% Complete</span>
                                  </div>
                                  <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="21.424627277747103" aria-valuemin="0" aria-valuemax="100" style="width: 21.424627277747103%">
                                    <span class="sr-only">21.424627277747103% Complete</span>
                                  </div>
                                </div><!-- /.progress -->
                              </div><!-- /.list-group-item-body -->
                            </div><!-- /.list-group-item -->
                            <!-- .list-group-item -->
                            <div class="list-group-item">
                              <!-- .list-group-item-figure -->
                              <div class="list-group-item-figure">
                                <a href="user-profile.html" class="user-avatar" data-toggle="tooltip" title="" data-original-title="Tammy Beck"><img src="https://uselooper.com/assets/images/avatars/uifaces15.jpg" alt=""></a>
                              </div><!-- /.list-group-item-figure -->
                              <!-- .list-group-item-body -->
                              <div class="list-group-item-body">
                                <!-- .progress -->
                                <div class="progress progress-animated rounded-0" data-toggle="tooltip" data-html="true" title="" data-original-title="<div class=&quot;text-left small&quot;><i class=&quot;fa fa-fw fa-square text-teal&quot;></i> 556<br><i class=&quot;fa fa-fw fa-square text-indigo&quot;></i> 406<br><i class=&quot;fa fa-fw fa-square text-pink&quot;></i> 432<br><i class=&quot;fa fa-fw fa-square text-purple&quot;></i> 249</div>">
                                  <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="33.84053560559951" aria-valuemin="0" aria-valuemax="100" style="width: 33.84053560559951%">
                                    <span class="sr-only">33.84053560559951% Complete</span>
                                  </div>
                                  <div class="progress-bar bg-indigo" role="progressbar" aria-valuenow="24.71089470480828" aria-valuemin="0" aria-valuemax="100" style="width: 24.71089470480828%">
                                    <span class="sr-only">24.71089470480828% Complete</span>
                                  </div>
                                  <div class="progress-bar bg-pink" role="progressbar" aria-valuenow="26.29336579427876" aria-valuemin="0" aria-valuemax="100" style="width: 26.29336579427876%">
                                    <span class="sr-only">26.29336579427876% Complete</span>
                                  </div>
                                  <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="15.15520389531345" aria-valuemin="0" aria-valuemax="100" style="width: 15.15520389531345%">
                                    <span class="sr-only">15.15520389531345% Complete</span>
                                  </div>
                                </div><!-- /.progress -->
                              </div><!-- /.list-group-item-body -->
                            </div><!-- /.list-group-item -->
                            <!-- .list-group-item -->
                            <div class="list-group-item">
                              <!-- .list-group-item-figure -->
                              <div class="list-group-item-figure">
                                <a href="user-profile.html" class="user-avatar" data-toggle="tooltip" title="" data-original-title="Susan Kelley"><img src="https://uselooper.com/assets/images/avatars/uifaces17.jpg" alt=""></a>
                              </div><!-- /.list-group-item-figure -->
                              <!-- .list-group-item-body -->
                              <div class="list-group-item-body">
                                <!-- .progress -->
                                <div class="progress progress-animated rounded-0" data-toggle="tooltip" data-html="true" title="" data-original-title="<div class=&quot;text-left small&quot;><i class=&quot;fa fa-fw fa-square text-teal&quot;></i> 98<br><i class=&quot;fa fa-fw fa-square text-indigo&quot;></i> 587<br><i class=&quot;fa fa-fw fa-square text-pink&quot;></i> 271<br><i class=&quot;fa fa-fw fa-square text-purple&quot;></i> 482</div>">
                                  <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="6.815020862308762" aria-valuemin="0" aria-valuemax="100" style="width: 6.815020862308762%">
                                    <span class="sr-only">6.815020862308762% Complete</span>
                                  </div>
                                  <div class="progress-bar bg-indigo" role="progressbar" aria-valuenow="40.82058414464534" aria-valuemin="0" aria-valuemax="100" style="width: 40.82058414464534%">
                                    <span class="sr-only">40.82058414464534% Complete</span>
                                  </div>
                                  <div class="progress-bar bg-pink" role="progressbar" aria-valuenow="18.845618915159946" aria-valuemin="0" aria-valuemax="100" style="width: 18.845618915159946%">
                                    <span class="sr-only">18.845618915159946% Complete</span>
                                  </div>
                                  <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="33.51877607788595" aria-valuemin="0" aria-valuemax="100" style="width: 33.51877607788595%">
                                    <span class="sr-only">33.51877607788595% Complete</span>
                                  </div>
                                </div><!-- /.progress -->
                              </div><!-- /.list-group-item-body -->
                            </div><!-- /.list-group-item -->
                            <!-- .list-group-item -->
                            <div class="list-group-item">
                              <!-- .list-group-item-figure -->
                              <div class="list-group-item-figure">
                                <a href="user-profile.html" class="user-avatar" data-toggle="tooltip" title="" data-original-title="Albert Newman"><img src="https://uselooper.com/assets/images/avatars/uifaces18.jpg" alt=""></a>
                              </div><!-- /.list-group-item-figure -->
                              <!-- .list-group-item-body -->
                              <div class="list-group-item-body">
                                <!-- .progress -->
                                <div class="progress progress-animated rounded-0" data-toggle="tooltip" data-html="true" title="" data-original-title="<div class=&quot;text-left small&quot;><i class=&quot;fa fa-fw fa-square text-teal&quot;></i> 248<br><i class=&quot;fa fa-fw fa-square text-indigo&quot;></i> 205<br><i class=&quot;fa fa-fw fa-square text-pink&quot;></i> 427<br><i class=&quot;fa fa-fw fa-square text-purple&quot;></i> 151</div>">
                                  <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="24.05431619786615" aria-valuemin="0" aria-valuemax="100" style="width: 24.05431619786615%">
                                    <span class="sr-only">24.05431619786615% Complete</span>
                                  </div>
                                  <div class="progress-bar bg-indigo" role="progressbar" aria-valuenow="19.88360814742968" aria-valuemin="0" aria-valuemax="100" style="width: 19.88360814742968%">
                                    <span class="sr-only">19.88360814742968% Complete</span>
                                  </div>
                                  <div class="progress-bar bg-pink" role="progressbar" aria-valuenow="41.41610087293889" aria-valuemin="0" aria-valuemax="100" style="width: 41.41610087293889%">
                                    <span class="sr-only">41.41610087293889% Complete</span>
                                  </div>
                                  <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="14.645974781765277" aria-valuemin="0" aria-valuemax="100" style="width: 14.645974781765277%">
                                    <span class="sr-only">14.645974781765277% Complete</span>
                                  </div>
                                </div><!-- /.progress -->
                              </div><!-- /.list-group-item-body -->
                            </div><!-- /.list-group-item -->
                            <!-- .list-group-item -->
                            <div class="list-group-item">
                              <!-- .list-group-item-figure -->
                              <div class="list-group-item-figure">
                                <a href="user-profile.html" class="user-avatar" data-toggle="tooltip" title="" data-original-title="Kyle Grant"><img src="https://uselooper.com/assets/images/avatars/uifaces16.jpg" alt=""></a>
                              </div><!-- /.list-group-item-figure -->
                              <!-- .list-group-item-body -->
                              <div class="list-group-item-body">
                                <!-- .progress -->
                                <div class="progress progress-animated rounded-0" data-toggle="tooltip" data-html="true" title="" data-original-title="<div class=&quot;text-left small&quot;><i class=&quot;fa fa-fw fa-square text-teal&quot;></i> 108<br><i class=&quot;fa fa-fw fa-square text-indigo&quot;></i> 265<br><i class=&quot;fa fa-fw fa-square text-pink&quot;></i> 443<br><i class=&quot;fa fa-fw fa-square text-purple&quot;></i> 127</div>">
                                  <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="11.452810180275716" aria-valuemin="0" aria-valuemax="100" style="width: 11.452810180275716%">
                                    <span class="sr-only">11.452810180275716% Complete</span>
                                  </div>
                                  <div class="progress-bar bg-indigo" role="progressbar" aria-valuenow="28.101802757158005" aria-valuemin="0" aria-valuemax="100" style="width: 28.101802757158005%">
                                    <span class="sr-only">28.101802757158005% Complete</span>
                                  </div>
                                  <div class="progress-bar bg-pink" role="progressbar" aria-valuenow="46.977730646871684" aria-valuemin="0" aria-valuemax="100" style="width: 46.977730646871684%">
                                    <span class="sr-only">46.977730646871684% Complete</span>
                                  </div>
                                  <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="13.467656415694591" aria-valuemin="0" aria-valuemax="100" style="width: 13.467656415694591%">
                                    <span class="sr-only">13.467656415694591% Complete</span>
                                  </div>
                                </div><!-- /.progress -->
                              </div><!-- /.list-group-item-body -->
                            </div><!-- /.list-group-item -->
                          </div><!-- /.list-group -->
                        </section><!-- /.card -->
                      </div><!-- /.section-deck -->
                    </div>
                </div>
                <div class="tab-pane fade show active" id="partner-projects" role="tabpanel" aria-labelledby="partner-projects-tab">
                    <!-- .page-section -->
                    <div class="page-section">
                <!-- .section-block -->
                <div class="section-block">
                  <!-- grid row -->
                  <div class="row mb-4">
                    <!-- .col -->
                    <div class="col">
                      <!-- .has-clearable -->
                      <div class="has-clearable">
                        <button type="button" class="close" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times-circle"></i></span></button> <input type="text" class="form-control" placeholder="Search">
                      </div><!-- /.has-clearable -->
                    </div><!-- /.col -->
                    <!-- .col-auto -->
                    <div class="col-auto">
                      <button class="btn btn-primary">Add project</button>
                    </div><!-- /.col-auto -->
                  </div><!-- /grid row -->
                  <!-- grid row -->
                  <div class="row">
                    <!-- grid column -->
                    <div class="col-lg-6">
                      <!-- .card -->
                      <div class="card card-fluid">
                        <!-- .card-header -->
                        <header class="card-header border-0">
                          <div class="d-flex justify-content-between align-items-center">
                            <span class="badge bg-muted" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Deadline"><span class="sr-only">Deadline</span> <i class="fa fa-calendar-alt text-muted mr-1"></i> 07 Aug 2018</span>
                            <div class="dropdown">
                              <button type="button" class="btn btn-icon btn-light" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                              <div class="dropdown-arrow"></div>
                              <div class="dropdown-menu dropdown-menu-right">
                                <a href="#!" class="dropdown-item">View Project</a> <a href="#!" class="dropdown-item">Add Member</a> <a href="#!" class="dropdown-item">Edit</a> <a href="#!" class="dropdown-item">Remove</a>
                              </div>
                            </div>
                          </div>
                        </header><!-- /.card-header -->
                        <!-- .card-body -->
                        <div class="card-body text-center">
                          <!-- avatars -->
                          <a href="page-project.html" class="tile tile-lg bg-purple mb-2">LT</a> <!-- /avatars -->
                          <!-- /.media -->
                          <h5 class="card-title">
                            <a href="page-project.html">Looper Admin Theme</a>
                          </h5>
                          <p class="card-subtitle text-muted"> Progress in 74% - Last update 1d </p><!-- .my-3 -->
                          <div class="my-3">
                            <!-- team members -->
                            <div class="avatar-group">
                              <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Andrew Kim"><img src="https://uselooper.com/assets/images/avatars/uifaces6.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Bryan Morgan"><img src="https://uselooper.com/assets/images/avatars/uifaces8.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Larry Robinson"><img src="https://uselooper.com/assets/images/avatars/uifaces7.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Beverly Simpson"><img src="https://uselooper.com/assets/images/avatars/uifaces9.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Robinson"><img src="https://uselooper.com/assets/images/avatars/uifaces10.jpg" alt=""></a> <a href="#!" class="tile tile-sm tile-circle" data-toggle="modal" data-target="#membersModal">+21</a>
                            </div><!-- /team members -->
                          </div><!-- /.my-3 -->
                          <!-- grid row -->
                          <div class="row">
                            <!-- grid column -->
                            <div class="col">
                              <strong>Budget</strong> <span class="d-block">40,000$</span>
                            </div><!-- /grid column -->
                            <!-- grid column -->
                            <div class="col">
                              <strong>Tasks</strong> <span class="d-block">775</span>
                            </div><!-- /grid column -->
                          </div><!-- /grid row -->
                        </div><!-- /.card-body -->
                        <!-- .progress -->
                        <div class="progress progress-xs" data-toggle="tooltip" title="" data-original-title="74%">
                          <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="2181" aria-valuemin="0" aria-valuemax="100" style="width: 74%">
                            <span class="sr-only">74% Complete</span>
                          </div>
                        </div><!-- /.progress -->
                      </div><!-- /.card -->
                    </div><!-- /grid column -->
                    <!-- grid column -->
                    <div class="col-lg-6">
                      <!-- .card -->
                      <div class="card card-fluid">
                        <!-- .card-header -->
                        <header class="card-header border-0">
                          <div class="d-flex justify-content-between align-items-center">
                            <span class="badge bg-muted" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Deadline"><span class="sr-only">Deadline</span> <i class="fa fa-calendar-alt text-muted mr-1"></i> 15 Sep 2018</span>
                            <div class="dropdown">
                              <button type="button" class="btn btn-icon btn-light" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                              <div class="dropdown-arrow"></div>
                              <div class="dropdown-menu dropdown-menu-right">
                                <a href="#!" class="dropdown-item">View Project</a> <a href="#!" class="dropdown-item">Add Member</a> <a href="#!" class="dropdown-item">Edit</a> <a href="#!" class="dropdown-item">Remove</a>
                              </div>
                            </div>
                          </div>
                        </header><!-- /.card-header -->
                        <!-- .card-body -->
                        <div class="card-body text-center">
                          <!-- avatars -->
                          <a href="page-project.html" class="tile tile-lg bg-indigo mb-2">SP</a> <!-- /avatars -->
                          <!-- /.media -->
                          <h5 class="card-title">
                            <a href="page-project.html">Smart Paper</a>
                          </h5>
                          <p class="card-subtitle text-muted"> Progress in 22% - Last update 2h </p><!-- .my-3 -->
                          <div class="my-3">
                            <!-- team members -->
                            <div class="avatar-group">
                              <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Andrew Kim"><img src="https://uselooper.com/assets/images/avatars/uifaces6.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Bryan Morgan"><img src="https://uselooper.com/assets/images/avatars/uifaces8.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Larry Robinson"><img src="https://uselooper.com/assets/images/avatars/uifaces7.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Beverly Simpson"><img src="https://uselooper.com/assets/images/avatars/uifaces9.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Robinson"><img src="https://uselooper.com/assets/images/avatars/uifaces10.jpg" alt=""></a> <a href="#!" class="tile tile-sm tile-circle" data-toggle="modal" data-target="#membersModal">+6</a>
                            </div><!-- /team members -->
                          </div><!-- /.my-3 -->
                          <!-- grid row -->
                          <div class="row">
                            <!-- grid column -->
                            <div class="col">
                              <strong>Budget</strong> <span class="d-block">10,000$</span>
                            </div><!-- /grid column -->
                            <!-- grid column -->
                            <div class="col">
                              <strong>Tasks</strong> <span class="d-block">3090</span>
                            </div><!-- /grid column -->
                          </div><!-- /grid row -->
                        </div><!-- /.card-body -->
                        <!-- .progress -->
                        <div class="progress progress-xs" data-toggle="tooltip" title="" data-original-title="22%">
                          <div class="progress-bar bg-indigo" role="progressbar" aria-valuenow="867" aria-valuemin="0" aria-valuemax="100" style="width: 22%">
                            <span class="sr-only">22% Complete</span>
                          </div>
                        </div><!-- /.progress -->
                      </div><!-- /.card -->
                    </div><!-- /grid column -->
                    <!-- grid column -->
                    <div class="col-lg-6">
                      <!-- .card -->
                      <div class="card card-fluid">
                        <!-- .card-header -->
                        <header class="card-header border-0">
                          <div class="d-flex justify-content-between align-items-center">
                            <span class="badge bg-muted" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Deadline"><span class="sr-only">Deadline</span> <i class="fa fa-calendar-alt text-muted mr-1"></i> 15 Jul 2018</span>
                            <div class="dropdown">
                              <button type="button" class="btn btn-icon btn-light" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                              <div class="dropdown-arrow"></div>
                              <div class="dropdown-menu dropdown-menu-right">
                                <a href="#!" class="dropdown-item">View Project</a> <a href="#!" class="dropdown-item">Add Member</a> <a href="#!" class="dropdown-item">Edit</a> <a href="#!" class="dropdown-item">Remove</a>
                              </div>
                            </div>
                          </div>
                        </header><!-- /.card-header -->
                        <!-- .card-body -->
                        <div class="card-body text-center">
                          <!-- avatars -->
                          <a href="page-project.html" class="tile tile-lg bg-yellow mb-2">OS</a> <!-- /avatars -->
                          <!-- /.media -->
                          <h5 class="card-title">
                            <a href="page-project.html">Online Store</a>
                          </h5>
                          <p class="card-subtitle text-muted"> Progress in 99% - Last update 2d </p><!-- .my-3 -->
                          <div class="my-3">
                            <!-- team members -->
                            <div class="avatar-group">
                              <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Andrew Kim"><img src="https://uselooper.com/assets/images/avatars/uifaces6.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Bryan Morgan"><img src="https://uselooper.com/assets/images/avatars/uifaces8.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Larry Robinson"><img src="https://uselooper.com/assets/images/avatars/uifaces7.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Beverly Simpson"><img src="https://uselooper.com/assets/images/avatars/uifaces9.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Robinson"><img src="https://uselooper.com/assets/images/avatars/uifaces10.jpg" alt=""></a> <a href="#!" class="tile tile-sm tile-circle" data-toggle="modal" data-target="#membersModal">+42</a>
                            </div><!-- /team members -->
                          </div><!-- /.my-3 -->
                          <!-- grid row -->
                          <div class="row">
                            <!-- grid column -->
                            <div class="col">
                              <strong>Budget</strong> <span class="d-block">100,000$</span>
                            </div><!-- /grid column -->
                            <!-- grid column -->
                            <div class="col">
                              <strong>Tasks</strong> <span class="d-block">100</span>
                            </div><!-- /grid column -->
                          </div><!-- /grid row -->
                        </div><!-- /.card-body -->
                        <!-- .progress -->
                        <div class="progress progress-xs" data-toggle="tooltip" title="" data-original-title="99%">
                          <div class="progress-bar bg-yellow" role="progressbar" aria-valuenow="6683" aria-valuemin="0" aria-valuemax="100" style="width: 99%">
                            <span class="sr-only">99% Complete</span>
                          </div>
                        </div><!-- /.progress -->
                      </div><!-- /.card -->
                    </div><!-- /grid column -->
                    <!-- grid column -->
                    <div class="col-lg-6">
                      <!-- .card -->
                      <div class="card card-fluid">
                        <!-- .card-header -->
                        <header class="card-header border-0">
                          <div class="d-flex justify-content-between align-items-center">
                            <span class="badge bg-muted" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Deadline"><span class="sr-only">Deadline</span> <i class="fa fa-calendar-alt text-muted mr-1"></i> 20 Sep 2018</span>
                            <div class="dropdown">
                              <button type="button" class="btn btn-icon btn-light" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                              <div class="dropdown-arrow"></div>
                              <div class="dropdown-menu dropdown-menu-right">
                                <a href="#!" class="dropdown-item">View Project</a> <a href="#!" class="dropdown-item">Add Member</a> <a href="#!" class="dropdown-item">Edit</a> <a href="#!" class="dropdown-item">Remove</a>
                              </div>
                            </div>
                          </div>
                        </header><!-- /.card-header -->
                        <!-- .card-body -->
                        <div class="card-body text-center">
                          <!-- avatars -->
                          <a href="page-project.html" class="tile tile-lg bg-blue mb-2">BA</a> <!-- /avatars -->
                          <!-- /.media -->
                          <h5 class="card-title">
                            <a href="page-project.html">Booking App</a>
                          </h5>
                          <p class="card-subtitle text-muted"> Progress in 35% - Last update 4h </p><!-- .my-3 -->
                          <div class="my-3">
                            <!-- team members -->
                            <div class="avatar-group">
                              <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Andrew Kim"><img src="https://uselooper.com/assets/images/avatars/uifaces6.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Bryan Morgan"><img src="https://uselooper.com/assets/images/avatars/uifaces8.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Larry Robinson"><img src="https://uselooper.com/assets/images/avatars/uifaces7.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Beverly Simpson"><img src="https://uselooper.com/assets/images/avatars/uifaces9.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Robinson"><img src="https://uselooper.com/assets/images/avatars/uifaces10.jpg" alt=""></a> <a href="#!" class="tile tile-sm tile-circle" data-toggle="modal" data-target="#membersModal">+8</a>
                            </div><!-- /team members -->
                          </div><!-- /.my-3 -->
                          <!-- grid row -->
                          <div class="row">
                            <!-- grid column -->
                            <div class="col">
                              <strong>Budget</strong> <span class="d-block">5,000$</span>
                            </div><!-- /grid column -->
                            <!-- grid column -->
                            <div class="col">
                              <strong>Tasks</strong> <span class="d-block">209</span>
                            </div><!-- /grid column -->
                          </div><!-- /grid row -->
                        </div><!-- /.card-body -->
                        <!-- .progress -->
                        <div class="progress progress-xs" data-toggle="tooltip" title="" data-original-title="35%">
                          <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="112" aria-valuemin="0" aria-valuemax="100" style="width: 35%">
                            <span class="sr-only">35% Complete</span>
                          </div>
                        </div><!-- /.progress -->
                      </div><!-- /.card -->
                    </div><!-- /grid column -->
                    <!-- grid column -->
                    <div class="col-lg-6">
                      <!-- .card -->
                      <div class="card card-fluid">
                        <!-- .card-header -->
                        <header class="card-header border-0">
                          <div class="d-flex justify-content-between align-items-center">
                            <span class="badge bg-muted" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Deadline"><span class="sr-only">Deadline</span> <i class="fa fa-calendar-alt text-muted mr-1"></i> 15 Nov 2018</span>
                            <div class="dropdown">
                              <button type="button" class="btn btn-icon btn-light" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                              <div class="dropdown-arrow"></div>
                              <div class="dropdown-menu dropdown-menu-right">
                                <a href="#!" class="dropdown-item">View Project</a> <a href="#!" class="dropdown-item">Add Member</a> <a href="#!" class="dropdown-item">Edit</a> <a href="#!" class="dropdown-item">Remove</a>
                              </div>
                            </div>
                          </div>
                        </header><!-- /.card-header -->
                        <!-- .card-body -->
                        <div class="card-body text-center">
                          <!-- avatars -->
                          <a href="page-project.html" class="tile tile-lg bg-teal mb-2">SB</a> <!-- /avatars -->
                          <!-- /.media -->
                          <h5 class="card-title">
                            <a href="page-project.html">SVG Icon Bundle</a>
                          </h5>
                          <p class="card-subtitle text-muted"> Progress in 32% - Last update 1d </p><!-- .my-3 -->
                          <div class="my-3">
                            <!-- team members -->
                            <div class="avatar-group">
                              <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Andrew Kim"><img src="https://uselooper.com/assets/images/avatars/uifaces6.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Bryan Morgan"><img src="https://uselooper.com/assets/images/avatars/uifaces8.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Larry Robinson"><img src="https://uselooper.com/assets/images/avatars/uifaces7.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Beverly Simpson"><img src="https://uselooper.com/assets/images/avatars/uifaces9.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Robinson"><img src="https://uselooper.com/assets/images/avatars/uifaces10.jpg" alt=""></a> <a href="#!" class="tile tile-sm tile-circle" data-toggle="modal" data-target="#membersModal">+8</a>
                            </div><!-- /team members -->
                          </div><!-- /.my-3 -->
                          <!-- grid row -->
                          <div class="row">
                            <!-- grid column -->
                            <div class="col">
                              <strong>Budget</strong> <span class="d-block">6,000$</span>
                            </div><!-- /grid column -->
                            <!-- grid column -->
                            <div class="col">
                              <strong>Tasks</strong> <span class="d-block">958</span>
                            </div><!-- /grid column -->
                          </div><!-- /grid row -->
                        </div><!-- /.card-body -->
                        <!-- .progress -->
                        <div class="progress progress-xs" data-toggle="tooltip" title="" data-original-title="32%">
                          <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="461" aria-valuemin="0" aria-valuemax="100" style="width: 32%">
                            <span class="sr-only">32% Complete</span>
                          </div>
                        </div><!-- /.progress -->
                      </div><!-- /.card -->
                    </div><!-- /grid column -->
                    <!-- grid column -->
                    <div class="col-lg-6">
                      <!-- .card -->
                      <div class="card card-fluid">
                        <!-- .card-header -->
                        <header class="card-header border-0">
                          <div class="d-flex justify-content-between align-items-center">
                            <span class="badge bg-muted" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Deadline"><span class="sr-only">Deadline</span> <i class="fa fa-calendar-alt text-muted mr-1"></i> 18 Aug 2018</span>
                            <div class="dropdown">
                              <button type="button" class="btn btn-icon btn-light" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                              <div class="dropdown-arrow"></div>
                              <div class="dropdown-menu dropdown-menu-right">
                                <a href="#!" class="dropdown-item">View Project</a> <a href="#!" class="dropdown-item">Add Member</a> <a href="#!" class="dropdown-item">Edit</a> <a href="#!" class="dropdown-item">Remove</a>
                              </div>
                            </div>
                          </div>
                        </header><!-- /.card-header -->
                        <!-- .card-body -->
                        <div class="card-body text-center">
                          <!-- avatars -->
                          <a href="page-project.html" class="tile tile-lg bg-pink mb-2">SP</a> <!-- /avatars -->
                          <!-- /.media -->
                          <h5 class="card-title">
                            <a href="page-project.html">Syrena Project</a>
                          </h5>
                          <p class="card-subtitle text-muted"> Progress in 93% - Last update 8h </p><!-- .my-3 -->
                          <div class="my-3">
                            <!-- team members -->
                            <div class="avatar-group">
                              <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Andrew Kim"><img src="https://uselooper.com/assets/images/avatars/uifaces6.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Bryan Morgan"><img src="https://uselooper.com/assets/images/avatars/uifaces8.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Larry Robinson"><img src="https://uselooper.com/assets/images/avatars/uifaces7.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Beverly Simpson"><img src="https://uselooper.com/assets/images/avatars/uifaces9.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Robinson"><img src="https://uselooper.com/assets/images/avatars/uifaces10.jpg" alt=""></a> <a href="#!" class="tile tile-sm tile-circle" data-toggle="modal" data-target="#membersModal">+32</a>
                            </div><!-- /team members -->
                          </div><!-- /.my-3 -->
                          <!-- grid row -->
                          <div class="row">
                            <!-- grid column -->
                            <div class="col">
                              <strong>Budget</strong> <span class="d-block">50,000$</span>
                            </div><!-- /grid column -->
                            <!-- grid column -->
                            <div class="col">
                              <strong>Tasks</strong> <span class="d-block">314</span>
                            </div><!-- /grid column -->
                          </div><!-- /grid row -->
                        </div><!-- /.card-body -->
                        <!-- .progress -->
                        <div class="progress progress-xs" data-toggle="tooltip" title="" data-original-title="93%">
                          <div class="progress-bar bg-pink" role="progressbar" aria-valuenow="3981" aria-valuemin="0" aria-valuemax="100" style="width: 93%">
                            <span class="sr-only">93% Complete</span>
                          </div>
                        </div><!-- /.progress -->
                      </div><!-- /.card -->
                    </div><!-- /grid column -->
                    <!-- grid column -->
                    <div class="col-lg-6">
                      <!-- .card -->
                      <div class="card card-fluid">
                        <!-- .card-header -->
                        <header class="card-header border-0">
                          <div class="d-flex justify-content-between align-items-center">
                            <span class="badge bg-muted" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Finished"><span class="sr-only">Finished</span> <i class="fa fa-fw fa-check-circle text-teal"></i></span>
                            <div class="dropdown">
                              <button type="button" class="btn btn-icon btn-light" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                              <div class="dropdown-arrow"></div>
                              <div class="dropdown-menu dropdown-menu-right">
                                <a href="#!" class="dropdown-item">View Project</a> <a href="#!" class="dropdown-item">Add Member</a> <a href="#!" class="dropdown-item">Edit</a> <a href="#!" class="dropdown-item">Remove</a>
                              </div>
                            </div>
                          </div>
                        </header><!-- /.card-header -->
                        <!-- .card-body -->
                        <div class="card-body text-center">
                          <!-- avatars -->
                          <a href="page-project.html" class="tile tile-lg bg-green mb-2">MG</a> <!-- /avatars -->
                          <!-- /.media -->
                          <h5 class="card-title">
                            <a href="page-project.html">Mobile App Gex</a>
                          </h5>
                          <p class="card-subtitle text-muted"> Progress in 100% - Last update 1m </p><!-- .my-3 -->
                          <div class="my-3">
                            <!-- team members -->
                            <div class="avatar-group">
                              <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Andrew Kim"><img src="https://uselooper.com/assets/images/avatars/uifaces6.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Bryan Morgan"><img src="https://uselooper.com/assets/images/avatars/uifaces8.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Larry Robinson"><img src="https://uselooper.com/assets/images/avatars/uifaces7.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Beverly Simpson"><img src="https://uselooper.com/assets/images/avatars/uifaces9.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Robinson"><img src="https://uselooper.com/assets/images/avatars/uifaces10.jpg" alt=""></a> <a href="#!" class="tile tile-sm tile-circle" data-toggle="modal" data-target="#membersModal">+26</a>
                            </div><!-- /team members -->
                          </div><!-- /.my-3 -->
                          <!-- grid row -->
                          <div class="row">
                            <!-- grid column -->
                            <div class="col">
                              <strong>Budget</strong> <span class="d-block">40,000$</span>
                            </div><!-- /grid column -->
                            <!-- grid column -->
                            <div class="col">
                              <strong>Tasks</strong> <span class="d-block">0</span>
                            </div><!-- /grid column -->
                          </div><!-- /grid row -->
                        </div><!-- /.card-body -->
                      </div><!-- /.card -->
                    </div><!-- /grid column -->
                    <!-- grid column -->
                    <div class="col-lg-6">
                      <!-- .card -->
                      <div class="card card-fluid">
                        <!-- .card-header -->
                        <header class="card-header border-0">
                          <div class="d-flex justify-content-between align-items-center">
                            <span class="badge bg-muted" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Finished"><span class="sr-only">Finished</span> <i class="fa fa-fw fa-check-circle text-teal"></i></span>
                            <div class="dropdown">
                              <button type="button" class="btn btn-icon btn-light" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                              <div class="dropdown-arrow"></div>
                              <div class="dropdown-menu dropdown-menu-right">
                                <a href="#!" class="dropdown-item">View Project</a> <a href="#!" class="dropdown-item">Add Member</a> <a href="#!" class="dropdown-item">Edit</a> <a href="#!" class="dropdown-item">Remove</a>
                              </div>
                            </div>
                          </div>
                        </header><!-- /.card-header -->
                        <!-- .card-body -->
                        <div class="card-body text-center">
                          <!-- avatars -->
                          <a href="page-project.html" class="tile tile-lg bg-red mb-2">LB</a> <!-- /avatars -->
                          <!-- /.media -->
                          <h5 class="card-title">
                            <a href="page-project.html">Landing Page Booster</a>
                          </h5>
                          <p class="card-subtitle text-muted"> Progress in 100% - Last update 1w </p><!-- .my-3 -->
                          <div class="my-3">
                            <!-- team members -->
                            <div class="avatar-group">
                              <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Andrew Kim"><img src="https://uselooper.com/assets/images/avatars/uifaces6.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Bryan Morgan"><img src="https://uselooper.com/assets/images/avatars/uifaces8.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Larry Robinson"><img src="https://uselooper.com/assets/images/avatars/uifaces7.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Beverly Simpson"><img src="https://uselooper.com/assets/images/avatars/uifaces9.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Robinson"><img src="https://uselooper.com/assets/images/avatars/uifaces10.jpg" alt=""></a> <a href="#!" class="tile tile-sm tile-circle" data-toggle="modal" data-target="#membersModal">+2</a>
                            </div><!-- /team members -->
                          </div><!-- /.my-3 -->
                          <!-- grid row -->
                          <div class="row">
                            <!-- grid column -->
                            <div class="col">
                              <strong>Budget</strong> <span class="d-block">1,000$</span>
                            </div><!-- /grid column -->
                            <!-- grid column -->
                            <div class="col">
                              <strong>Tasks</strong> <span class="d-block">0</span>
                            </div><!-- /grid column -->
                          </div><!-- /grid row -->
                        </div><!-- /.card-body -->
                      </div><!-- /.card -->
                    </div><!-- /grid column -->
                    <!-- grid column -->
                    <div class="col-lg-6">
                      <!-- .card -->
                      <div class="card card-fluid">
                        <!-- .card-header -->
                        <header class="card-header border-0">
                          <div class="d-flex justify-content-between align-items-center">
                            <span class="badge bg-muted" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Finished"><span class="sr-only">Finished</span> <i class="fa fa-fw fa-check-circle text-teal"></i></span>
                            <div class="dropdown">
                              <button type="button" class="btn btn-icon btn-light" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                              <div class="dropdown-arrow"></div>
                              <div class="dropdown-menu dropdown-menu-right">
                                <a href="#!" class="dropdown-item">View Project</a> <a href="#!" class="dropdown-item">Add Member</a> <a href="#!" class="dropdown-item">Edit</a> <a href="#!" class="dropdown-item">Remove</a>
                              </div>
                            </div>
                          </div>
                        </header><!-- /.card-header -->
                        <!-- .card-body -->
                        <div class="card-body text-center">
                          <!-- avatars -->
                          <a href="page-project.html" class="tile tile-lg bg-orange mb-2">SB</a> <!-- /avatars -->
                          <!-- /.media -->
                          <h5 class="card-title">
                            <a href="page-project.html">Stilearn Branding</a>
                          </h5>
                          <p class="card-subtitle text-muted"> Progress in 100% - Last update 2m </p><!-- .my-3 -->
                          <div class="my-3">
                            <!-- team members -->
                            <div class="avatar-group">
                              <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Andrew Kim"><img src="https://uselooper.com/assets/images/avatars/uifaces6.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Bryan Morgan"><img src="https://uselooper.com/assets/images/avatars/uifaces8.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Larry Robinson"><img src="https://uselooper.com/assets/images/avatars/uifaces7.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Beverly Simpson"><img src="https://uselooper.com/assets/images/avatars/uifaces9.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="Robinson"><img src="https://uselooper.com/assets/images/avatars/uifaces10.jpg" alt=""></a> <a href="#!" class="tile tile-sm tile-circle" data-toggle="modal" data-target="#membersModal">+12</a>
                            </div><!-- /team members -->
                          </div><!-- /.my-3 -->
                          <!-- grid row -->
                          <div class="row">
                            <!-- grid column -->
                            <div class="col">
                              <strong>Budget</strong> <span class="d-block">20,000$</span>
                            </div><!-- /grid column -->
                            <!-- grid column -->
                            <div class="col">
                              <strong>Tasks</strong> <span class="d-block">0</span>
                            </div><!-- /grid column -->
                          </div><!-- /grid row -->
                        </div><!-- /.card-body -->
                      </div><!-- /.card -->
                    </div><!-- /grid column -->
                  </div><!-- /grid row -->
                </div><!-- /.section-block -->
                <!-- Modal Members List -->
                <!-- .modal -->
                <div class="modal fade" id="membersModal" tabindex="-1" role="dialog" aria-labelledby="membersModalLabel" aria-hidden="true">
                  <!-- .modal-dialog -->
                  <div class="modal-dialog modal-dialog-overflow" role="document">
                    <!-- .modal-content -->
                    <div class="modal-content">
                      <!-- .modal-header -->
                      <div class="modal-header">
                        <!-- .input-group -->
                        <div class="input-group has-clearable">
                          <button type="button" class="close" aria-label="Close"><i class="fa fa-times-circle"></i></button>
                          <div class="input-group-prepend">
                            <span class="input-group-text"><span class="oi oi-magnifying-glass"></span></span>
                          </div><input type="text" class="form-control" placeholder="Search Members">
                        </div><!-- /.input-group -->
                      </div><!-- /.modal-header -->
                      <!-- .modal-body -->
                      <div class="modal-body px-0">
                        <!-- .list-group -->
                        <div class="list-group list-group-flush">
                          <!-- .list-group-item -->
                          <div class="list-group-item">
                            <!-- .list-group-item-figure -->
                            <div class="list-group-item-figure">
                              <a href="#!" class="user-avatar"><img src="https://uselooper.com/assets/images/avatars/uifaces5.jpg" alt="Craig Hansen"></a>
                            </div><!-- /.list-group-item-figure -->
                            <!-- .list-group-item-body -->
                            <div class="list-group-item-body">
                              <h4 class="list-group-item-title">
                                <a href="#!">Craig Hansen</a>
                              </h4>
                              <p class="list-group-item-text"> Software Developer </p>
                            </div><!-- /.list-group-item-body -->
                            <!-- .list-group-item-figure -->
                            <div class="list-group-item-figure dropdown">
                              <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                              <div class="dropdown-arrow dropdown-arrow-left"></div>
                              <div class="dropdown-menu dropdown-menu-right">
                                <button type="button" class="dropdown-item">Message</button> <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                                <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                              </div>
                            </div><!-- /.list-group-item-figure -->
                          </div><!-- /.list-group-item -->
                          <!-- .list-group-item -->
                          <div class="list-group-item">
                            <!-- .list-group-item-figure -->
                            <div class="list-group-item-figure">
                              <a href="#!" class="user-avatar"><img src="https://uselooper.com/assets/images/avatars/uifaces9.jpg" alt="Jane Barnes"></a>
                            </div><!-- /.list-group-item-figure -->
                            <!-- .list-group-item-body -->
                            <div class="list-group-item-body">
                              <h4 class="list-group-item-title">
                                <a href="#!">Jane Barnes</a>
                              </h4>
                              <p class="list-group-item-text"> Social Worker </p>
                            </div><!-- /.list-group-item-body -->
                            <!-- .list-group-item-figure -->
                            <div class="list-group-item-figure dropdown">
                              <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                              <div class="dropdown-arrow dropdown-arrow-left"></div>
                              <div class="dropdown-menu dropdown-menu-right">
                                <button type="button" class="dropdown-item">Message</button> <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                                <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                              </div>
                            </div><!-- /.list-group-item-figure -->
                          </div><!-- /.list-group-item -->
                          <!-- .list-group-item -->
                          <div class="list-group-item">
                            <!-- .list-group-item-figure -->
                            <div class="list-group-item-figure">
                              <a href="#!" class="user-avatar"><img src="https://uselooper.com/assets/images/avatars/uifaces4.jpg" alt="Nicole Barnett"></a>
                            </div><!-- /.list-group-item-figure -->
                            <!-- .list-group-item-body -->
                            <div class="list-group-item-body">
                              <h4 class="list-group-item-title">
                                <a href="#!">Nicole Barnett</a>
                              </h4>
                              <p class="list-group-item-text"> Marketing Manager </p>
                            </div><!-- /.list-group-item-body -->
                            <!-- .list-group-item-figure -->
                            <div class="list-group-item-figure dropdown">
                              <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                              <div class="dropdown-arrow dropdown-arrow-left"></div>
                              <div class="dropdown-menu dropdown-menu-right">
                                <button type="button" class="dropdown-item">Message</button> <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                                <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                              </div>
                            </div><!-- /.list-group-item-figure -->
                          </div><!-- /.list-group-item -->
                          <!-- .list-group-item -->
                          <div class="list-group-item">
                            <!-- .list-group-item-figure -->
                            <div class="list-group-item-figure">
                              <a href="#!" class="user-avatar"><img src="https://uselooper.com/assets/images/avatars/uifaces6.jpg" alt="Michael Ward"></a>
                            </div><!-- /.list-group-item-figure -->
                            <!-- .list-group-item-body -->
                            <div class="list-group-item-body">
                              <h4 class="list-group-item-title">
                                <a href="#!">Michael Ward</a>
                              </h4>
                              <p class="list-group-item-text"> Lawyer </p>
                            </div><!-- /.list-group-item-body -->
                            <!-- .list-group-item-figure -->
                            <div class="list-group-item-figure dropdown">
                              <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                              <div class="dropdown-arrow dropdown-arrow-left"></div>
                              <div class="dropdown-menu dropdown-menu-right">
                                <button type="button" class="dropdown-item">Message</button> <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                                <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                              </div>
                            </div><!-- /.list-group-item-figure -->
                          </div><!-- /.list-group-item -->
                          <!-- .list-group-item -->
                          <div class="list-group-item">
                            <!-- .list-group-item-figure -->
                            <div class="list-group-item-figure">
                              <a href="#!" class="user-avatar"><img src="https://uselooper.com/assets/images/avatars/uifaces8.jpg" alt="Juan Fuller"></a>
                            </div><!-- /.list-group-item-figure -->
                            <!-- .list-group-item-body -->
                            <div class="list-group-item-body">
                              <h4 class="list-group-item-title">
                                <a href="#!">Juan Fuller</a>
                              </h4>
                              <p class="list-group-item-text"> Budget analyst </p>
                            </div><!-- /.list-group-item-body -->
                            <!-- .list-group-item-figure -->
                            <div class="list-group-item-figure dropdown">
                              <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                              <div class="dropdown-arrow dropdown-arrow-left"></div>
                              <div class="dropdown-menu dropdown-menu-right">
                                <button type="button" class="dropdown-item">Message</button> <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                                <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                              </div>
                            </div><!-- /.list-group-item-figure -->
                          </div><!-- /.list-group-item -->
                          <!-- .list-group-item -->
                          <div class="list-group-item">
                            <!-- .list-group-item-figure -->
                            <div class="list-group-item-figure">
                              <a href="#!" class="user-avatar"><img src="https://uselooper.com/assets/images/avatars/uifaces7.jpg" alt="Julia Silva"></a>
                            </div><!-- /.list-group-item-figure -->
                            <!-- .list-group-item-body -->
                            <div class="list-group-item-body">
                              <h4 class="list-group-item-title">
                                <a href="#!">Julia Silva</a>
                              </h4>
                              <p class="list-group-item-text"> Photographer </p>
                            </div><!-- /.list-group-item-body -->
                            <!-- .list-group-item-figure -->
                            <div class="list-group-item-figure dropdown">
                              <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                              <div class="dropdown-arrow dropdown-arrow-left"></div>
                              <div class="dropdown-menu dropdown-menu-right">
                                <button type="button" class="dropdown-item">Message</button> <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                                <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                              </div>
                            </div><!-- /.list-group-item-figure -->
                          </div><!-- /.list-group-item -->
                          <!-- .list-group-item -->
                          <div class="list-group-item">
                            <!-- .list-group-item-figure -->
                            <div class="list-group-item-figure">
                              <a href="#!" class="user-avatar"><img src="https://uselooper.com/assets/images/avatars/uifaces10.jpg" alt="Joe Hanson"></a>
                            </div><!-- /.list-group-item-figure -->
                            <!-- .list-group-item-body -->
                            <div class="list-group-item-body">
                              <h4 class="list-group-item-title">
                                <a href="#!">Joe Hanson</a>
                              </h4>
                              <p class="list-group-item-text"> Logistician </p>
                            </div><!-- /.list-group-item-body -->
                            <!-- .list-group-item-figure -->
                            <div class="list-group-item-figure dropdown">
                              <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                              <div class="dropdown-arrow dropdown-arrow-left"></div>
                              <div class="dropdown-menu dropdown-menu-right">
                                <button type="button" class="dropdown-item">Message</button> <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                                <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                              </div>
                            </div><!-- /.list-group-item-figure -->
                          </div><!-- /.list-group-item -->
                          <!-- .list-group-item -->
                          <div class="list-group-item">
                            <!-- .list-group-item-figure -->
                            <div class="list-group-item-figure">
                              <a href="#!" class="user-avatar"><img src="https://uselooper.com/assets/images/avatars/uifaces11.jpg" alt="Brenda Griffin"></a>
                            </div><!-- /.list-group-item-figure -->
                            <!-- .list-group-item-body -->
                            <div class="list-group-item-body">
                              <h4 class="list-group-item-title">
                                <a href="#!">Brenda Griffin</a>
                              </h4>
                              <p class="list-group-item-text"> Medical Assistant </p>
                            </div><!-- /.list-group-item-body -->
                            <!-- .list-group-item-figure -->
                            <div class="list-group-item-figure dropdown">
                              <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                              <div class="dropdown-arrow dropdown-arrow-left"></div>
                              <div class="dropdown-menu dropdown-menu-right">
                                <button type="button" class="dropdown-item">Message</button> <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                                <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                              </div>
                            </div><!-- /.list-group-item-figure -->
                          </div><!-- /.list-group-item -->
                          <!-- .list-group-item -->
                          <div class="list-group-item">
                            <!-- .list-group-item-figure -->
                            <div class="list-group-item-figure">
                              <a href="#!" class="user-avatar"><img src="https://uselooper.com/assets/images/avatars/uifaces12.jpg" alt="Ryan Jimenez"></a>
                            </div><!-- /.list-group-item-figure -->
                            <!-- .list-group-item-body -->
                            <div class="list-group-item-body">
                              <h4 class="list-group-item-title">
                                <a href="#!">Ryan Jimenez</a>
                              </h4>
                              <p class="list-group-item-text"> Photographer </p>
                            </div><!-- /.list-group-item-body -->
                            <!-- .list-group-item-figure -->
                            <div class="list-group-item-figure dropdown">
                              <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                              <div class="dropdown-arrow dropdown-arrow-left"></div>
                              <div class="dropdown-menu dropdown-menu-right">
                                <button type="button" class="dropdown-item">Message</button> <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                                <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                              </div>
                            </div><!-- /.list-group-item-figure -->
                          </div><!-- /.list-group-item -->
                          <!-- .list-group-item -->
                          <div class="list-group-item">
                            <!-- .list-group-item-figure -->
                            <div class="list-group-item-figure">
                              <a href="#!" class="user-avatar"><img src="https://uselooper.com/assets/images/avatars/uifaces13.jpg" alt="Bryan Hayes"></a>
                            </div><!-- /.list-group-item-figure -->
                            <!-- .list-group-item-body -->
                            <div class="list-group-item-body">
                              <h4 class="list-group-item-title">
                                <a href="#!">Bryan Hayes</a>
                              </h4>
                              <p class="list-group-item-text"> Computer Systems Analyst </p>
                            </div><!-- /.list-group-item-body -->
                            <!-- .list-group-item-figure -->
                            <div class="list-group-item-figure dropdown">
                              <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                              <div class="dropdown-arrow dropdown-arrow-left"></div>
                              <div class="dropdown-menu dropdown-menu-right">
                                <button type="button" class="dropdown-item">Message</button> <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                                <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                              </div>
                            </div><!-- /.list-group-item-figure -->
                          </div><!-- /.list-group-item -->
                        </div><!-- /.list-group -->
                      </div><!-- /.modal-body -->
                      <!-- .modal-footer -->
                      <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                      </div><!-- /.modal-footer -->
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- /Modal Members List -->
              </div><!-- /.page-section -->
                </div>
                <div class="tab-pane fade show " id="partner-feeds" role="tabpanel" aria-labelledby="partner-feeds-tab">
                  <div class="page-section">
                    <!-- .section-block -->
                    <div class="section-block">
                      <!-- .feed-publisher -->
                      <div class="feed-publisher">
                        <!-- .media -->
                        <div class="media">
                          <figure class="user-avatar user-avatar-md">
                            <img src="https://uselooper.com/assets/images/avatars/profile.jpg" alt="">
                          </figure><!-- .media-body -->
                          <div class="media-body">
                            <!-- .publisher -->
                            <div class="publisher">
                              <!-- .publisher-input -->
                              <div class="publisher-input">
                                <textarea id="publisherInput1" class="form-control" placeholder="What happening?"></textarea>
                              </div><!-- /.publisher-input -->
                              <!-- .publisher-actions -->
                              <div class="publisher-actions">
                                <!-- .publisher-tools -->
                                <div class="publisher-tools mr-auto">
                                  <button type="button" class="btn btn-light btn-icon fileinput-button"><i class="fa fa-paperclip"></i> <input type="file" id="message-attachment" name="attachment[]" multiple=""></button> <button type="button" class="btn btn-light btn-icon"><i class="fa fa-smile"></i></button>
                                </div><!-- /.publisher-tools -->
                                <button type="submit" class="btn btn-primary">Publish</button>
                              </div><!-- /.publisher-actions -->
                            </div><!-- /.publisher -->
                          </div><!-- /.media-body -->
                        </div><!-- /.media -->
                      </div><!-- /.feed-publisher -->
                      <!-- .feed -->
                      <article class="feed">
                        <!-- .feed-post -->
                        <section class="feed-post card">
                          <!-- .card-header -->
                          <header class="card-header card-header-fluid">
                            <a href="#!" class="btn-account" role="button">
                              <div class="user-avatar user-avatar-lg">
                                <img src="https://uselooper.com/assets/images/avatars/profile.jpg" alt="">
                              </div>
                              <div class="account-summary">
                                <p class="account-name"> Beni arisandi </p>
                                <p class="account-description"> 3 Hours ago </p>
                              </div>
                            </a> <!-- .dropdown -->
                            <div class="dropdown align-self-start ml-auto">
                              <button class="btn btn-icon btn-light text-muted" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                              <div class="dropdown-arrow"></div>
                              <div class="dropdown-menu dropdown-menu-right">
                                <a href="#!" class="dropdown-item">Get notified</a> <a href="#!" class="dropdown-item">Mute notified</a> <a href="#!" class="dropdown-item">Permalink</a> <a href="#!" class="dropdown-item">Block this user</a> <a href="#!" class="dropdown-item">Remove</a>
                              </div>
                            </div><!-- /.dropdown -->
                          </header><!-- /.card-header -->
                          <!-- .card-body -->
                          <div class="card-body">
                            <p> Hey there! this is a post with engagement, <a href="#!" class="mention">@stilearning</a> ratione doloremque eos <a href="#!" class="hashtag">#looper</a> aliquid ipsam, molestias cumque asperiores mollitia nam corrupti dolore recusandae nihil? </p><!-- .feed-summary -->
                            <div class="feed-summary">
                              <a href="#!" class="feed-summary-item mr-auto">
                                <div class="avatar-group mr-2">
                                  <figure class="user-avatar user-avatar-xs">
                                    <img src="https://uselooper.com/assets/images/avatars/uifaces5.jpg" alt="">
                                  </figure>
                                  <figure class="user-avatar user-avatar-xs">
                                    <img src="https://uselooper.com/assets/images/avatars/uifaces6.jpg" alt="">
                                  </figure>
                                  <figure class="user-avatar user-avatar-xs">
                                    <img src="https://uselooper.com/assets/images/avatars/uifaces7.jpg" alt="">
                                  </figure>
                                </div>6.3K likes
                              </a> <a href="#!" class="feed-summary-item">826 comments</a> <a href="#!" class="feed-summary-item">·</a> <a href="#!" class="feed-summary-item">2.9K shares</a>
                            </div><!-- .feed-summary -->
                          </div><!-- /.card-body -->
                          <!-- .card-footer -->
                          <footer class="card-footer">
                            <div class="card-footer-item">
                              <button type="button" class="btn btn-reset text-muted"><i class="fa fa-fw fa-heart"></i> Like</button>
                            </div>
                            <div class="card-footer-item">
                              <button type="button" class="btn btn-reset text-muted"><i class="fa fa-fw fa-comment"></i> Comment</button>
                            </div>
                            <div class="card-footer-item">
                              <button type="button" class="btn btn-reset text-muted"><i class="fa fa-fw fa-share"></i> Share</button>
                            </div>
                          </footer><!-- /.card-footer -->
                        </section><!-- /.feed-post -->
                        <!-- .feed-comments -->
                        <section class="feed-comments card">
                          <!-- .card-header -->
                          <header class="card-header d-flex justify-content-between">
                            <a href="#!">View more comments</a> <span class="text-muted">72 of 826</span>
                          </header><!-- /.card-header -->
                          <!-- .conversations -->
                          <section role="log" class="conversations">
                            <!-- .conversation-list -->
                            <ul class="conversation-list">
                              <!-- .conversation-inbound -->
                              <li class="conversation-inbound">
                                <!-- .conversation-avatar -->
                                <div class="conversation-avatar">
                                  <a href="#!" class="user-avatar"><img src="https://uselooper.com/assets/images/avatars/profile.jpg" alt=""></a>
                                </div><!-- /.conversation-avatar -->
                                <!-- .conversation-message -->
                                <div class="conversation-message">
                                  <div class="conversation-meta">
                                    <a href="#!"><strong>Beni arisandi</strong></a> <span class="mention ml-1">Author</span> <small class="time ml-1">1hr</small>
                                  </div>
                                  <div class="conversation-message-text"> Quaerat eum quia ad, obcaecati ex placeat autem, molestiae iusto ab ipsum eius dicta dolores corporis debitis quasi! Neque, modi impedit iusto! </div>
                                  <div class="conversation-meta">
                                    <a href="#!">Like</a> · <a href="#!">Reply</a> · <a href="#!">Edit</a> · <a href="#!">Delete</a>
                                  </div>
                                </div><!-- /.conversation-message -->
                              </li><!-- /.conversation-inbound -->
                              <!-- .conversation-inbound -->
                              <li class="conversation-inbound">
                                <!-- .conversation-avatar -->
                                <div class="conversation-avatar">
                                  <a href="#!" class="tile tile-circle bg-muted"><i class="oi oi-person"></i></a>
                                </div><!-- /.conversation-avatar -->
                                <!-- .conversation-message -->
                                <div class="conversation-message">
                                  <div class="conversation-meta">
                                    <a href="#!"><strong>Diane Peters</strong></a> <small class="time ml-1">32m</small>
                                  </div>
                                  <div class="conversation-message-text"> Consectetur quis veritatis aut maiores omnis, expedita officiis delectus perspiciatis a dolores. </div>
                                  <div class="conversation-meta">
                                    <a href="#!">Like</a> · <a href="#!">Reply</a> · <a href="#!">Edit</a> · <a href="#!">Delete</a>
                                  </div><!-- Nested .conversation-list -->
                                  <ul class="conversation-list">
                                    <!-- .conversation-inbound -->
                                    <li class="conversation-inbound">
                                      <div class="conversation-message">
                                        <div class="conversation-meta">
                                          <a href="#!"><i class="fa fa-reply fa-rotate-180"></i></a> <a href="#!" class="user-avatar user-avatar-xs mx-1"><img src="https://uselooper.com/assets/images/avatars/uifaces16.jpg" alt=""></a> <a href="#!">Betty Simmons · 5 Replies</a> · 12m </div>
                                      </div>
                                    </li><!-- /.conversation-inbound -->
                                  </ul><!-- /Nested .conversation-list -->
                                </div><!-- /.conversation-message -->
                              </li><!-- /.conversation-inbound -->
                              <!-- .conversation-inbound -->
                              <li class="conversation-inbound">
                                <!-- .conversation-avatar -->
                                <div class="conversation-avatar">
                                  <a href="#!" class="user-avatar"><img src="https://uselooper.com/assets/images/avatars/uifaces11.jpg" alt=""></a>
                                </div><!-- /.conversation-avatar -->
                                <!-- .conversation-message -->
                                <div class="conversation-message">
                                  <div class="conversation-meta">
                                    <a href="#!"><strong>Jennifer Gray</strong></a> <small class="time ml-1">Edited 14m</small>
                                  </div>
                                  <div class="conversation-message-text"> Officiis numquam, repellat nam tempore sit aliquid nostrum autem excepturi quis nihil. </div>
                                  <div class="conversation-meta">
                                    <a href="#!">Like</a> · <a href="#!">Reply</a> · <a href="#!">Edit</a> · <a href="#!">Delete</a>
                                  </div><!-- Nested .conversation-list -->
                                  <ul class="conversation-list">
                                    <!-- .conversation-inbound -->
                                    <li class="conversation-inbound">
                                      <!-- .conversation-avatar -->
                                      <div class="conversation-avatar">
                                        <a href="#!" class="user-avatar user-avatar-sm"><img src="https://uselooper.com/assets/images/avatars/uifaces15.jpg" alt=""></a>
                                      </div><!-- /.conversation-avatar -->
                                      <!-- .conversation-message -->
                                      <div class="conversation-message">
                                        <div class="conversation-meta">
                                          <a href="#!"><strong>Russell Gilbert</strong></a> <small class="time ml-1">4m</small>
                                        </div>
                                        <div class="conversation-message-text"> Enim laborum, architecto molestias velit quod tempora! </div>
                                        <div class="conversation-meta">
                                          <a href="#!">Like</a> · <a href="#!">Reply</a> · <a href="#!">Edit</a> · <a href="#!">Delete</a>
                                        </div>
                                      </div><!-- /.conversation-message -->
                                    </li><!-- /.conversation-inbound -->
                                    <!-- .conversation-inbound -->
                                    <li class="conversation-inbound">
                                      <!-- .conversation-avatar -->
                                      <div class="conversation-avatar">
                                        <a href="#!" class="user-avatar user-avatar-sm"><img src="https://uselooper.com/assets/images/avatars/profile.jpg" alt=""></a>
                                      </div><!-- /.conversation-avatar -->
                                      <!-- .conversation-message -->
                                      <div class="conversation-message">
                                        <div class="conversation-meta">
                                          <a href="#!"><strong>Beni Arisandi</strong></a> <span class="mention ml-1">Author</span> <small class="time ml-1">Just now</small>
                                        </div>
                                        <div class="conversation-message-text">
                                          <a href="#!"><strong></strong></a> Adipisicing elit. </div>
                                        <div class="conversation-meta">
                                          <a href="#!">Like</a> · <a href="#!">Reply</a> · <a href="#!">Edit</a> · <a href="#!">Delete</a>
                                        </div>
                                      </div><!-- /.conversation-message -->
                                    </li><!-- /.conversation-inbound -->
                                    <!-- .conversation-action -->
                                    <li class="conversation-action mt-3">
                                      <!-- .media -->
                                      <div class="media">
                                        <figure class="user-avatar user-avatar-sm mt-1 mr-2">
                                          <img src="https://uselooper.com/assets/images/avatars/profile.jpg" alt="">
                                        </figure><!-- .media-body -->
                                        <div class="media-body">
                                          <!-- .composer -->
                                          <div class="publisher publisher-alt">
                                            <!-- .publisher-input -->
                                            <div class="publisher-input">
                                              <textarea id="publisherInput1" class="form-control" placeholder="Write a comment"></textarea>
                                            </div><!-- /.publisher-input -->
                                            <!-- .publisher-actions -->
                                            <div class="publisher-actions">
                                              <!-- .publisher-tools -->
                                              <div class="publisher-tools mr-auto">
                                                <button type="button" class="btn btn-light btn-icon fileinput-button"><i class="fa fa-paperclip"></i> <input type="file" id="message-attachment" name="attachment[]" multiple=""></button> <button type="button" class="btn btn-light btn-icon"><i class="fa fa-smile"></i></button>
                                              </div><!-- /.publisher-tools -->
                                              <button type="submit" class="btn btn-primary">Publish</button>
                                            </div><!-- /.publisher-actions -->
                                          </div><!-- /.publisher -->
                                        </div><!-- /.media-body -->
                                      </div><!-- /.media -->
                                    </li><!-- /.conversation-action -->
                                  </ul><!-- /Nested .conversation-list -->
                                </div><!-- /.conversation-message -->
                              </li><!-- /.conversation-inbound -->
                              <!-- .conversation-inbound -->
                              <li class="conversation-inbound">
                                <!-- .conversation-avatar -->
                                <div class="conversation-avatar">
                                  <a href="#!" class="user-avatar"><img src="https://uselooper.com/assets/images/avatars/uifaces12.jpg" alt=""></a>
                                </div><!-- /.conversation-avatar -->
                                <!-- .conversation-message -->
                                <div class="conversation-message">
                                  <div class="conversation-meta">
                                    <a href="#!"><strong>Zachary Fowler</strong></a> <small class="time ml-1">5m</small>
                                  </div>
                                  <div class="conversation-message-text"> Ad earum dolore excepturi itaque officia. </div>
                                  <div class="conversation-meta">
                                    <a href="#!">Like</a> · <a href="#!">Reply</a> · <a href="#!">Edit</a> · <a href="#!">Delete</a>
                                  </div><!-- Nested .conversation-list -->
                                  <ul class="conversation-list">
                                    <!-- .conversation-inbound -->
                                    <li class="conversation-inbound">
                                      <div class="conversation-message">
                                        <div class="conversation-meta">
                                          <a href="#!"><i class="fa fa-reply fa-rotate-180"></i></a> <a href="#!">View 8 Replies</a>
                                        </div>
                                      </div>
                                    </li><!-- /.conversation-inbound -->
                                  </ul><!-- /Nested .conversation-list -->
                                </div><!-- /.conversation-message -->
                              </li><!-- /.conversation-inbound -->
                              <!-- .conversation-action -->
                              <li class="conversation-action mt-3">
                                <!-- .media -->
                                <div class="media">
                                  <figure class="user-avatar mr-2">
                                    <img src="https://uselooper.com/assets/images/avatars/profile.jpg" alt="">
                                  </figure><!-- .media-body -->
                                  <div class="media-body">
                                    <!-- .composer -->
                                    <div class="publisher publisher-alt">
                                      <!-- .publisher-input -->
                                      <div class="publisher-input">
                                        <textarea id="publisherInput1" class="form-control" placeholder="Write a comment"></textarea>
                                      </div><!-- /.publisher-input -->
                                      <!-- .publisher-actions -->
                                      <div class="publisher-actions">
                                        <!-- .publisher-tools -->
                                        <div class="publisher-tools mr-auto">
                                          <button type="button" class="btn btn-light btn-icon fileinput-button"><i class="fa fa-paperclip"></i> <input type="file" id="message-attachment" name="attachment[]" multiple=""></button> <button type="button" class="btn btn-light btn-icon"><i class="fa fa-smile"></i></button>
                                        </div><!-- /.publisher-tools -->
                                        <button type="submit" class="btn btn-primary">Publish</button>
                                      </div><!-- /.publisher-actions -->
                                    </div><!-- /.publisher -->
                                  </div><!-- /.media-body -->
                                </div><!-- /.media -->
                              </li><!-- /.conversation-action -->
                            </ul><!-- /.conversation-list -->
                          </section><!-- /.conversations -->
                        </section><!-- /.feed-comments -->
                      </article><!-- /.feed -->
                      <!-- .feed -->
                      <article class="feed">
                        <!-- .feed-post -->
                        <section class="feed-post card">
                          <!-- .card-header -->
                          <header class="card-header card-header-fluid">
                            <a href="#!" class="btn-account" role="button">
                              <div class="user-avatar user-avatar-lg">
                                <img src="https://uselooper.com/assets/images/avatars/uifaces7.jpg" alt="">
                              </div>
                              <div class="account-summary">
                                <p class="account-name"> Christina Gray </p>
                                <p class="account-description"> New member · Yesterday at 5:48 PM </p>
                              </div>
                            </a> <!-- .dropdown -->
                            <div class="dropdown align-self-start ml-auto">
                              <button class="btn btn-icon btn-light text-muted" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                              <div class="dropdown-arrow"></div>
                              <div class="dropdown-menu dropdown-menu-right">
                                <a href="#!" class="dropdown-item">Get notified</a> <a href="#!" class="dropdown-item">Mute notified</a> <a href="#!" class="dropdown-item">Permalink</a> <a href="#!" class="dropdown-item">Block this user</a> <a href="#!" class="dropdown-item">Remove</a>
                              </div>
                            </div><!-- /.dropdown -->
                          </header><!-- /.card-header -->
                          <!-- .card-body -->
                          <div class="card-body">
                            <p class="mb-2"> Hey there! this is a post with Attachments, <a href="#!" class="mention">@stilearning</a> ratione doloremque eos <a href="#!" class="hashtag">#looper</a> aliquid ipsam, molestias cumque asperiores mollitia nam corrupti dolore recusandae nihil? </p><!-- .feed-attachments -->
                            <div class="pswp-gallery" data-pswp-uid="1">
                              <div class="feed-attachments">
                                <div class="attachment-item">
                                  <!-- .card-figure -->
                                  <section class="card-figure">
                                    <!-- .card-figure -->
                                    <figure class="figure">
                                      <!-- .figure-img -->
                                      <div class="figure-img">
                                        <img class="img-fluid" src="https://uselooper.com/assets/images/dummy/img-1.jpg" alt="Card image cap"> <a href="https://uselooper.com/assets/images/dummy/img-1.jpg" class="img-link" data-size="600x450"><span class="tile tile-circle bg-danger"><span class="oi oi-eye"></span></span> <span class="img-caption d-none">Card image cap</span></a>
                                      </div><!-- /.figure-img -->
                                    </figure><!-- /.card-figure -->
                                  </section><!-- /.card-figure -->
                                </div>
                                <div class="attachment-item">
                                  <!-- .card-figure -->
                                  <section class="card-figure">
                                    <!-- .card-figure -->
                                    <figure class="figure">
                                      <!-- .figure-img -->
                                      <div class="figure-img">
                                        <img class="img-fluid" src="https://uselooper.com/assets/images/dummy/img-4.jpg" alt="Card image cap"> <a href="https://uselooper.com/assets/images/dummy/img-4.jpg" class="img-link" data-size="600x450"><span class="tile tile-circle bg-danger"><span class="oi oi-eye"></span></span> <span class="img-caption d-none">Card image cap</span></a>
                                      </div><!-- /.figure-img -->
                                    </figure><!-- /.card-figure -->
                                  </section><!-- /.card-figure -->
                                </div>
                              </div>
                            </div><!-- /.feed-attachments -->
                            <!-- .feed-summary -->
                            <div class="feed-summary">
                              <a href="#!" class="feed-summary-item mr-auto">6.3K likes</a> <a href="#!" class="feed-summary-item">826 comments</a> <a href="#!" class="feed-summary-item">·</a> <a href="#!" class="feed-summary-item">2.9K shares</a>
                            </div><!-- .feed-summary -->
                          </div><!-- /.card-body -->
                          <!-- .card-footer -->
                          <footer class="card-footer">
                            <div class="card-footer-item">
                              <button type="button" class="btn btn-reset text-muted"><i class="fa fa-fw fa-heart"></i> Like</button>
                            </div>
                            <div class="card-footer-item">
                              <button type="button" class="btn btn-reset text-muted"><i class="fa fa-fw fa-comment"></i> Comment</button>
                            </div>
                            <div class="card-footer-item">
                              <button type="button" class="btn btn-reset text-muted"><i class="fa fa-fw fa-share"></i> Share</button>
                            </div>
                          </footer><!-- /.card-footer -->
                        </section><!-- /.feed-post -->
                      </article><!-- /.feed -->
                      <!-- .feed -->
                      <article class="feed">
                        <!-- .feed-post -->
                        <section class="feed-post card">
                          <!-- .card-header -->
                          <header class="card-header card-header-fluid">
                            <a href="#!" class="btn-account" role="button">
                              <div class="user-avatar user-avatar-lg">
                                <img src="https://uselooper.com/assets/images/avatars/uifaces18.jpg" alt="">
                              </div>
                              <div class="account-summary">
                                <p class="account-name"> Ethan Marshall </p>
                                <p class="account-description"> New member · Yesterday at 5:48 PM </p>
                              </div>
                            </a> <!-- .dropdown -->
                            <div class="dropdown align-self-start ml-auto">
                              <button class="btn btn-icon btn-light text-muted" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                              <div class="dropdown-arrow"></div>
                              <div class="dropdown-menu dropdown-menu-right">
                                <a href="#!" class="dropdown-item">Get notified</a> <a href="#!" class="dropdown-item">Mute notified</a> <a href="#!" class="dropdown-item">Permalink</a> <a href="#!" class="dropdown-item">Block this user</a> <a href="#!" class="dropdown-item">Remove</a>
                              </div>
                            </div><!-- /.dropdown -->
                          </header><!-- /.card-header -->
                          <!-- .card-body -->
                          <div class="card-body">
                            <p class="mb-2"> Hey there! this is a post with outbound link, <a href="#!" class="mention">@stilearning</a> ratione doloremque eos <a href="#!" class="hashtag">#looper</a> aliquid ipsam, molestias cumque asperiores mollitia nam corrupti dolore recusandae nihil? </p><!-- .outbound-link -->
                            <a href="#!" class="outbound-link">
                              <h5 class="outbound-title"> Expanding Your Home Network’s Reach </h5>
                              <p class="outbound-text"> Atque natus, quod quas commodi beatae inventore eligendi recusandae necessitatibus eum distinctio. </p>
                            </a> <!-- /.outbound-link -->
                            <!-- .feed-summary -->
                            <div class="feed-summary">
                              <a href="#!" class="feed-summary-item mr-auto">6.3K likes</a> <a href="#!" class="feed-summary-item">826 comments</a> <a href="#!" class="feed-summary-item">·</a> <a href="#!" class="feed-summary-item">2.9K shares</a>
                            </div><!-- .feed-summary -->
                          </div><!-- /.card-body -->
                          <!-- .card-footer -->
                          <footer class="card-footer">
                            <div class="card-footer-item">
                              <button type="button" class="btn btn-reset text-muted"><i class="fa fa-fw fa-heart"></i> Like</button>
                            </div>
                            <div class="card-footer-item">
                              <button type="button" class="btn btn-reset text-muted"><i class="fa fa-fw fa-comment"></i> Comment</button>
                            </div>
                            <div class="card-footer-item">
                              <button type="button" class="btn btn-reset text-muted"><i class="fa fa-fw fa-share"></i> Share</button>
                            </div>
                          </footer><!-- /.card-footer -->
                        </section><!-- /.feed-post -->
                      </article><!-- /.feed -->
                      <!-- PhotoSwipe (.pswp) element -->
                      <div id="gallery" class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
                        <!-- .pswp__bg -->
                        <div class="pswp__bg"></div><!-- .pswp__scroll-wrap -->
                        <div class="pswp__scroll-wrap">
                          <!-- .pswp__container -->
                          <div class="pswp__container">
                            <div class="pswp__item"></div>
                            <div class="pswp__item"></div>
                            <div class="pswp__item"></div>
                          </div><!-- /.pswp__container -->
                          <!-- .pswp__ui pswp__ui--hidden -->
                          <div class="pswp__ui pswp__ui--hidden">
                            <!-- .pswp__top-bar -->
                            <div class="pswp__top-bar">
                              <div class="pswp__counter"></div><button class="pswp__button pswp__button--close" title="Close (Esc)"></button> <button class="pswp__button pswp__button--share" title="Share"></button> <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button> <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                              <div class="pswp__preloader">
                                <div class="pswp__preloader__icn">
                                  <div class="pswp__preloader__cut">
                                    <div class="pswp__preloader__donut"></div>
                                  </div>
                                </div>
                              </div>
                            </div><!-- /.pswp__top-bar -->
                            <!-- .pswp__share-modal -->
                            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                              <div class="pswp__share-tooltip"></div>
                            </div><!-- /.pswp__share-modal -->
                            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button> <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
                            <div class="pswp__caption">
                              <div class="pswp__caption__center"></div>
                            </div>
                          </div><!-- /.pswp__ui pswp__ui--hidden -->
                        </div><!-- /.pswp__scroll-wrap -->
                      </div><!-- /PhotoSwipe (.pswp) element -->
                    </div><!-- /.section-block -->
                  </div>
                </div>
                <div class="tab-pane fade show " id="partner-member" role="tabpanel" aria-labelledby="partner-member-tab">
                  <div class="page-section">
                      <!-- .section-block -->
                      <div class="section-block">
                        <!-- grid row -->
                        <div class="row mb-4">
                          <!-- .col -->
                          <div class="col">
                            <!-- .has-clearable -->
                            <div class="has-clearable">
                              <button type="button" class="close" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times-circle"></i></span></button> <input type="text" class="form-control" placeholder="Search">
                            </div><!-- /.has-clearable -->
                          </div><!-- /.col -->
                          <!-- .col-auto -->
                          <div class="col-auto">
                            <!-- invite members -->
                            <div class="dropdown">
                              <button class="btn btn-primary" data-toggle="dropdown" data-display="static" aria-haspoppup="true" aria-expanded="false"><i class="fa fa-user-plus mr-1"></i> Invite</button>
                              <div class="dropdown-arrow"></div><!-- .dropdown-menu -->
                              <div class="dropdown-menu dropdown-menu-right dropdown-menu-rich stop-propagation">
                                <div class="dropdown-header"> Add members </div>
                                <div class="form-group px-3 py-2 m-0">
                                  <input type="text" class="form-control" placeholder="e.g. @bent10" data-toggle="tribute" data-remote="https://uselooper.com/assets/data/tribute.json" data-menu-container="#people-list" data-item-template="true" autofocus="" data-tribute="true"> <small class="form-text text-muted">Search people by username or email address to invite them.</small>
                                </div>
                                <div id="people-list" class="tribute-inline stop-propagation"></div><a href="#!" class="dropdown-footer">Invite member by link <i class="fa fa-clone"></i></a>
                              </div><!-- /.dropdown-menu -->
                            </div><!-- /invite members -->
                          </div><!-- /.col-auto -->
                        </div><!-- /grid row -->
                        <!-- .card -->
                        <section class="card card-fluid mb-2">
                          <!-- .card-body -->
                          <div class="card-body">
                            <!-- grid row -->
                            <div class="row align-items-center">
                              <!-- grid column -->
                              <div class="col-auto">
                                <a href="user-profile.html" class="user-avatar user-avatar-lg"><img src="https://uselooper.com/assets/images/avatars/uifaces5.jpg" alt=""> <span class="avatar-badge online" title="online"></span></a>
                              </div><!-- /grid column -->
                              <!-- grid column -->
                              <div class="col">
                                <h3 class="card-title">
                                  <a href="user-profile.html">Craig Hansen</a> <small class="text-muted">@chansen</small>
                                </h3>
                                <h6 class="card-subtitle text-muted"> Software Developer </h6>
                              </div><!-- /grid column -->
                              <!-- /grid column -->
                              <div class="col-auto">
                                <button type="button" class="btn btn-icon btn-secondary mr-1" data-toggle="tooltip" title="" data-original-title="Private message"><i class="fa fa-comment-alt"></i></button> <!-- .dropdown -->
                                <div class="dropdown d-inline-block">
                                  <button class="btn btn-icon btn-secondary" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-h"></i></button>
                                  <div class="dropdown-arrow"></div>
                                  <div class="dropdown-menu dropdown-menu-right">
                                    <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                                    <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                                  </div>
                                </div><!-- /.dropdown -->
                              </div><!-- /grid column -->
                            </div><!-- /grid row -->
                          </div><!-- /.card-body -->
                        </section><!-- /.card -->
                        <!-- .card -->
                        <section class="card card-fluid mb-2">
                          <!-- .card-body -->
                          <div class="card-body">
                            <!-- grid row -->
                            <div class="row align-items-center">
                              <!-- grid column -->
                              <div class="col-auto">
                                <a href="user-profile.html" class="user-avatar user-avatar-lg"><img src="https://uselooper.com/assets/images/avatars/uifaces9.jpg" alt=""> <span class="avatar-badge idle" title="idle"></span></a>
                              </div><!-- /grid column -->
                              <!-- grid column -->
                              <div class="col">
                                <h3 class="card-title">
                                  <a href="user-profile.html">Jane Barnes</a> <small class="text-muted">@jbarnes</small>
                                </h3>
                                <h6 class="card-subtitle text-muted"> Social Worker </h6>
                              </div><!-- /grid column -->
                              <!-- /grid column -->
                              <div class="col-auto">
                                <button type="button" class="btn btn-icon btn-secondary mr-1" data-toggle="tooltip" title="" data-original-title="Private message"><i class="fa fa-comment-alt"></i></button> <!-- .dropdown -->
                                <div class="dropdown d-inline-block">
                                  <button class="btn btn-icon btn-secondary" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-h"></i></button>
                                  <div class="dropdown-arrow"></div>
                                  <div class="dropdown-menu dropdown-menu-right">
                                    <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                                    <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                                  </div>
                                </div><!-- /.dropdown -->
                              </div><!-- /grid column -->
                            </div><!-- /grid row -->
                          </div><!-- /.card-body -->
                        </section><!-- /.card -->
                        <!-- .card -->
                        <section class="card card-fluid mb-2">
                          <!-- .card-body -->
                          <div class="card-body">
                            <!-- grid row -->
                            <div class="row align-items-center">
                              <!-- grid column -->
                              <div class="col-auto">
                                <a href="user-profile.html" class="user-avatar user-avatar-lg"><img src="https://uselooper.com/assets/images/avatars/uifaces4.jpg" alt=""> <span class="avatar-badge online" title="online"></span></a>
                              </div><!-- /grid column -->
                              <!-- grid column -->
                              <div class="col">
                                <h3 class="card-title">
                                  <a href="user-profile.html">Nicole Barnett</a> <small class="text-muted">@nbarnett</small>
                                </h3>
                                <h6 class="card-subtitle text-muted"> Marketing Manager </h6>
                              </div><!-- /grid column -->
                              <!-- /grid column -->
                              <div class="col-auto">
                                <button type="button" class="btn btn-icon btn-secondary mr-1" data-toggle="tooltip" title="" data-original-title="Private message"><i class="fa fa-comment-alt"></i></button> <!-- .dropdown -->
                                <div class="dropdown d-inline-block">
                                  <button class="btn btn-icon btn-secondary" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-h"></i></button>
                                  <div class="dropdown-arrow"></div>
                                  <div class="dropdown-menu dropdown-menu-right">
                                    <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                                    <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                                  </div>
                                </div><!-- /.dropdown -->
                              </div><!-- /grid column -->
                            </div><!-- /grid row -->
                          </div><!-- /.card-body -->
                        </section><!-- /.card -->
                        <!-- .card -->
                        <section class="card card-fluid mb-2">
                          <!-- .card-body -->
                          <div class="card-body">
                            <!-- grid row -->
                            <div class="row align-items-center">
                              <!-- grid column -->
                              <div class="col-auto">
                                <a href="user-profile.html" class="user-avatar user-avatar-lg"><img src="https://uselooper.com/assets/images/avatars/uifaces6.jpg" alt=""> <span class="avatar-badge busy" title="busy"></span></a>
                              </div><!-- /grid column -->
                              <!-- grid column -->
                              <div class="col">
                                <h3 class="card-title">
                                  <a href="user-profile.html">Michael Ward</a> <small class="text-muted">@mward</small>
                                </h3>
                                <h6 class="card-subtitle text-muted"> Lawyer </h6>
                              </div><!-- /grid column -->
                              <!-- /grid column -->
                              <div class="col-auto">
                                <button type="button" class="btn btn-icon btn-secondary mr-1" data-toggle="tooltip" title="" data-original-title="Private message"><i class="fa fa-comment-alt"></i></button> <!-- .dropdown -->
                                <div class="dropdown d-inline-block">
                                  <button class="btn btn-icon btn-secondary" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-h"></i></button>
                                  <div class="dropdown-arrow"></div>
                                  <div class="dropdown-menu dropdown-menu-right">
                                    <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                                    <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                                  </div>
                                </div><!-- /.dropdown -->
                              </div><!-- /grid column -->
                            </div><!-- /grid row -->
                          </div><!-- /.card-body -->
                        </section><!-- /.card -->
                        <!-- .card -->
                        <section class="card card-fluid mb-2">
                          <!-- .card-body -->
                          <div class="card-body">
                            <!-- grid row -->
                            <div class="row align-items-center">
                              <!-- grid column -->
                              <div class="col-auto">
                                <a href="user-profile.html" class="user-avatar user-avatar-lg"><img src="https://uselooper.com/assets/images/avatars/uifaces8.jpg" alt=""> <span class="avatar-badge idle" title="idle"></span></a>
                              </div><!-- /grid column -->
                              <!-- grid column -->
                              <div class="col">
                                <h3 class="card-title">
                                  <a href="user-profile.html">Juan Fuller</a> <small class="text-muted">@jfuller</small>
                                </h3>
                                <h6 class="card-subtitle text-muted"> Budget analyst </h6>
                              </div><!-- /grid column -->
                              <!-- /grid column -->
                              <div class="col-auto">
                                <button type="button" class="btn btn-icon btn-secondary mr-1" data-toggle="tooltip" title="" data-original-title="Private message"><i class="fa fa-comment-alt"></i></button> <!-- .dropdown -->
                                <div class="dropdown d-inline-block">
                                  <button class="btn btn-icon btn-secondary" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-h"></i></button>
                                  <div class="dropdown-arrow"></div>
                                  <div class="dropdown-menu dropdown-menu-right">
                                    <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                                    <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                                  </div>
                                </div><!-- /.dropdown -->
                              </div><!-- /grid column -->
                            </div><!-- /grid row -->
                          </div><!-- /.card-body -->
                        </section><!-- /.card -->
                        <!-- .card -->
                        <section class="card card-fluid mb-2">
                          <!-- .card-body -->
                          <div class="card-body">
                            <!-- grid row -->
                            <div class="row align-items-center">
                              <!-- grid column -->
                              <div class="col-auto">
                                <a href="user-profile.html" class="user-avatar user-avatar-lg"><img src="https://uselooper.com/assets/images/avatars/uifaces7.jpg" alt=""> <span class="avatar-badge online" title="online"></span></a>
                              </div><!-- /grid column -->
                              <!-- grid column -->
                              <div class="col">
                                <h3 class="card-title">
                                  <a href="user-profile.html">Julia Silva</a> <small class="text-muted">@jsilva</small>
                                </h3>
                                <h6 class="card-subtitle text-muted"> Photographer </h6>
                              </div><!-- /grid column -->
                              <!-- /grid column -->
                              <div class="col-auto">
                                <button type="button" class="btn btn-icon btn-secondary mr-1" data-toggle="tooltip" title="" data-original-title="Private message"><i class="fa fa-comment-alt"></i></button> <!-- .dropdown -->
                                <div class="dropdown d-inline-block">
                                  <button class="btn btn-icon btn-secondary" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-h"></i></button>
                                  <div class="dropdown-arrow"></div>
                                  <div class="dropdown-menu dropdown-menu-right">
                                    <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                                    <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                                  </div>
                                </div><!-- /.dropdown -->
                              </div><!-- /grid column -->
                            </div><!-- /grid row -->
                          </div><!-- /.card-body -->
                        </section><!-- /.card -->
                        <!-- .card -->
                        <section class="card card-fluid mb-2">
                          <!-- .card-body -->
                          <div class="card-body">
                            <!-- grid row -->
                            <div class="row align-items-center">
                              <!-- grid column -->
                              <div class="col-auto">
                                <a href="user-profile.html" class="user-avatar user-avatar-lg"><img src="https://uselooper.com/assets/images/avatars/uifaces10.jpg" alt=""> <span class="avatar-badge offline" title="offline"></span></a>
                              </div><!-- /grid column -->
                              <!-- grid column -->
                              <div class="col">
                                <h3 class="card-title">
                                  <a href="user-profile.html">Joe Hanson</a> <small class="text-muted">@jhanson</small>
                                </h3>
                                <h6 class="card-subtitle text-muted"> Logistician </h6>
                              </div><!-- /grid column -->
                              <!-- /grid column -->
                              <div class="col-auto">
                                <button type="button" class="btn btn-icon btn-secondary mr-1" data-toggle="tooltip" title="" data-original-title="Private message"><i class="fa fa-comment-alt"></i></button> <!-- .dropdown -->
                                <div class="dropdown d-inline-block">
                                  <button class="btn btn-icon btn-secondary" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-h"></i></button>
                                  <div class="dropdown-arrow"></div>
                                  <div class="dropdown-menu dropdown-menu-right">
                                    <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                                    <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                                  </div>
                                </div><!-- /.dropdown -->
                              </div><!-- /grid column -->
                            </div><!-- /grid row -->
                          </div><!-- /.card-body -->
                        </section><!-- /.card -->
                        <!-- .card -->
                        <section class="card card-fluid mb-2">
                          <!-- .card-body -->
                          <div class="card-body">
                            <!-- grid row -->
                            <div class="row align-items-center">
                              <!-- grid column -->
                              <div class="col-auto">
                                <a href="user-profile.html" class="user-avatar user-avatar-lg"><img src="https://uselooper.com/assets/images/avatars/uifaces11.jpg" alt=""> <span class="avatar-badge online" title="online"></span></a>
                              </div><!-- /grid column -->
                              <!-- grid column -->
                              <div class="col">
                                <h3 class="card-title">
                                  <a href="user-profile.html">Brenda Griffin</a> <small class="text-muted">@bgriffin</small>
                                </h3>
                                <h6 class="card-subtitle text-muted"> Medical Assistant </h6>
                              </div><!-- /grid column -->
                              <!-- /grid column -->
                              <div class="col-auto">
                                <button type="button" class="btn btn-icon btn-secondary mr-1" data-toggle="tooltip" title="" data-original-title="Private message"><i class="fa fa-comment-alt"></i></button> <!-- .dropdown -->
                                <div class="dropdown d-inline-block">
                                  <button class="btn btn-icon btn-secondary" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-h"></i></button>
                                  <div class="dropdown-arrow"></div>
                                  <div class="dropdown-menu dropdown-menu-right">
                                    <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                                    <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                                  </div>
                                </div><!-- /.dropdown -->
                              </div><!-- /grid column -->
                            </div><!-- /grid row -->
                          </div><!-- /.card-body -->
                        </section><!-- /.card -->
                        <!-- .card -->
                        <section class="card card-fluid mb-2">
                          <!-- .card-body -->
                          <div class="card-body">
                            <!-- grid row -->
                            <div class="row align-items-center">
                              <!-- grid column -->
                              <div class="col-auto">
                                <a href="user-profile.html" class="user-avatar user-avatar-lg"><img src="https://uselooper.com/assets/images/avatars/uifaces12.jpg" alt=""> <span class="avatar-badge online" title="online"></span></a>
                              </div><!-- /grid column -->
                              <!-- grid column -->
                              <div class="col">
                                <h3 class="card-title">
                                  <a href="user-profile.html">Ryan Jimenez</a> <small class="text-muted">@rjimenez</small>
                                </h3>
                                <h6 class="card-subtitle text-muted"> Photographer </h6>
                              </div><!-- /grid column -->
                              <!-- /grid column -->
                              <div class="col-auto">
                                <button type="button" class="btn btn-icon btn-secondary mr-1" data-toggle="tooltip" title="" data-original-title="Private message"><i class="fa fa-comment-alt"></i></button> <!-- .dropdown -->
                                <div class="dropdown d-inline-block">
                                  <button class="btn btn-icon btn-secondary" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-h"></i></button>
                                  <div class="dropdown-arrow"></div>
                                  <div class="dropdown-menu dropdown-menu-right">
                                    <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                                    <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                                  </div>
                                </div><!-- /.dropdown -->
                              </div><!-- /grid column -->
                            </div><!-- /grid row -->
                          </div><!-- /.card-body -->
                        </section><!-- /.card -->
                        <!-- .card -->
                        <section class="card card-fluid mb-2">
                          <!-- .card-body -->
                          <div class="card-body">
                            <!-- grid row -->
                            <div class="row align-items-center">
                              <!-- grid column -->
                              <div class="col-auto">
                                <a href="user-profile.html" class="user-avatar user-avatar-lg"><img src="https://uselooper.com/assets/images/avatars/uifaces13.jpg" alt=""> <span class="avatar-badge busy" title="busy"></span></a>
                              </div><!-- /grid column -->
                              <!-- grid column -->
                              <div class="col">
                                <h3 class="card-title">
                                  <a href="user-profile.html">Bryan Hayes</a> <small class="text-muted">@bhayes</small>
                                </h3>
                                <h6 class="card-subtitle text-muted"> Computer Systems Analyst </h6>
                              </div><!-- /grid column -->
                              <!-- /grid column -->
                              <div class="col-auto">
                                <button type="button" class="btn btn-icon btn-secondary mr-1" data-toggle="tooltip" title="" data-original-title="Private message"><i class="fa fa-comment-alt"></i></button> <!-- .dropdown -->
                                <div class="dropdown d-inline-block">
                                  <button class="btn btn-icon btn-secondary" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-h"></i></button>
                                  <div class="dropdown-arrow"></div>
                                  <div class="dropdown-menu dropdown-menu-right">
                                    <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                                    <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                                  </div>
                                </div><!-- /.dropdown -->
                              </div><!-- /grid column -->
                            </div><!-- /grid row -->
                          </div><!-- /.card-body -->
                        </section><!-- /.card -->
                      </div><!-- /.section-block -->
                    </div>
                </div>
              </div>
            </div><!-- /.page-inner -->
            <!-- .page-sidebar -->
            <div class="page-sidebar">
              <!-- .sidebar-header -->
              <header class="sidebar-header d-sm-none">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                      <a href="#!" onclick="Looper.toggleSidebar()"><i class="breadcrumb-icon fa fa-angle-left mr-2"></i>Back</a>
                    </li>
                  </ol>
                </nav>
              </header><!-- /.sidebar-header -->
              <!-- .sidebar-section-fill -->
              <div class="sidebar-section-fill">
                <!-- .card -->
                <div class="card card-reflow">
                  <!-- .card-body -->
                  <div class="card-body">
                    <h4 class="card-title"> Summary </h4><!-- grid row -->
                    <div class="row">
                      <!-- grid column -->
                      <div class="col-6">
                        <!-- .metric -->
                        <div class="metric">
                          <h6 class="metric-value"> $83,743 </h6><br>
                          <p class="metric-label mt-1"> Incomes </p>
                        </div><!-- /.metric -->
                      </div><!-- /grid column -->
                      <!-- grid column -->
                      <div class="col-6">
                        <!-- .metric -->
                        <div class="metric">
                          <h6 class="metric-value"> $18,821 </h6><br>
                          <p class="metric-label mt-1"> Expenses </p>
                        </div><!-- /.metric -->
                      </div><!-- /grid column -->
                      <!-- grid column -->
                      <div class="col-6">
                        <!-- .metric -->
                        <div class="metric">
                          <h6 class="metric-value"> 2,630 </h6><br>
                          <p class="metric-label mt-1"> Leads </p>
                        </div><!-- /.metric -->
                      </div><!-- /grid column -->
                      <!-- grid column -->
                      <div class="col-6">
                        <!-- .metric -->
                        <div class="metric">
                          <h6 class="metric-value"> 40 </h6><br>
                          <p class="metric-label mt-1"> Clients </p>
                        </div><!-- /.metric -->
                      </div><!-- /grid column -->
                    </div><!-- /grid row -->
                  </div><!-- /.card-body -->
                  <!-- .card-body -->
                  <div class="card-body border-top pb-1">
                    <h4 class="card-title"> Leads source </h4><!-- .progress -->
                    <div class="progress mb-2">
                      <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="33.84" aria-valuemin="0" aria-valuemax="100" style="width: 33.84%">
                        <span class="sr-only">33.84% Complete</span>
                      </div>
                      <div class="progress-bar bg-indigo" role="progressbar" aria-valuenow="24.71" aria-valuemin="0" aria-valuemax="100" style="width: 24.71%">
                        <span class="sr-only">24.71% Complete</span>
                      </div>
                      <div class="progress-bar bg-pink" role="progressbar" aria-valuenow="26.29" aria-valuemin="0" aria-valuemax="100" style="width: 26.29%">
                        <span class="sr-only">26.29% Complete</span>
                      </div>
                      <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="15.15" aria-valuemin="0" aria-valuemax="100" style="width: 15.15%">
                        <span class="sr-only">15.15% Complete</span>
                      </div>
                    </div><!-- /.progress -->
                  </div><!-- /.card -->
                  <!-- .list-group -->
                  <div class="list-group list-group-bordered list-group-reflow">
                    <!-- .list-group-item -->
                    <div class="list-group-item justify-content-between align-items-center">
                      <span><i class="fa fa-square text-teal mr-2"></i> Mailchimp</span> <span class="text-muted">890 result</span>
                    </div><!-- /.list-group-item -->
                    <!-- .list-group-item -->
                    <div class="list-group-item justify-content-between align-items-center">
                      <span><i class="fa fa-square text-indigo mr-2"></i> Facebook</span> <span class="text-muted">650 result</span>
                    </div><!-- /.list-group-item -->
                    <!-- .list-group-item -->
                    <div class="list-group-item justify-content-between align-items-center">
                      <span><i class="fa fa-square text-pink mr-2"></i> Google</span> <span class="text-muted">692 result</span>
                    </div><!-- /.list-group-item -->
                    <!-- .list-group-item -->
                    <div class="list-group-item justify-content-between align-items-center">
                      <span><i class="fa fa-square text-purple mr-2"></i> Linkedin</span> <span class="text-muted">398 result</span>
                    </div><!-- /.list-group-item -->
                  </div><!-- /.list-group -->
                  <!-- .card-body -->
                  <div class="card-body border-top">
                    <div class="d-flex justify-content-between my-3">
                      <h4 class="card-title"> Recent activity </h4><a href="#!">View all</a>
                    </div><!-- .timeline -->
                    <ul class="timeline timeline-fluid mb-0">
                      <!-- .timeline-item -->
                      <li class="timeline-item">
                        <!-- .timeline-figure -->
                        <div class="timeline-figure">
                          <span class="tile tile-circle tile-sm"><i class="fa fa-calendar-alt fa-lg"></i></span>
                        </div><!-- /.timeline-figure -->
                        <!-- .timeline-body -->
                        <div class="timeline-body">
                          <!-- .media -->
                          <div class="media">
                            <!-- .media-body -->
                            <div class="media-body">
                              <p class="mb-0">
                                <a href="#!">Jeffrey Wells</a> created a <a href="#!">schedule</a>
                              </p><span class="timeline-date">About a minute ago</span>
                            </div><!-- /.media-body -->
                          </div><!-- /.media -->
                        </div><!-- /.timeline-body -->
                      </li><!-- /.timeline-item -->
                      <!-- .timeline-item -->
                      <li class="timeline-item">
                        <!-- .timeline-figure -->
                        <div class="timeline-figure">
                          <span class="tile tile-circle tile-sm"><i class="oi oi-chat fa-lg"></i></span>
                        </div><!-- /.timeline-figure -->
                        <!-- .timeline-body -->
                        <div class="timeline-body">
                          <!-- .media -->
                          <div class="media">
                            <!-- .media-body -->
                            <div class="media-body">
                              <p class="mb-0">
                                <a href="#!">Anna Vargas</a> logged a <a href="#!">chat</a> with team </p><span class="timeline-date">3 hours ago</span>
                            </div><!-- /.media-body -->
                          </div><!-- /.media -->
                        </div><!-- /.timeline-body -->
                      </li><!-- /.timeline-item -->
                      <!-- .timeline-item -->
                      <li class="timeline-item">
                        <!-- .timeline-figure -->
                        <div class="timeline-figure">
                          <span class="tile tile-circle tile-sm"><i class="fa fa-tasks fa-lg"></i></span>
                        </div><!-- /.timeline-figure -->
                        <!-- .timeline-body -->
                        <div class="timeline-body">
                          <!-- .media -->
                          <div class="media">
                            <!-- .media-body -->
                            <div class="media-body">
                              <p class="mb-0">
                                <a href="#!">Arthur Carroll</a> created a <a href="#!">task</a>
                              </p><span class="timeline-date">8:14pm</span>
                            </div><!-- /.media-body -->
                          </div><!-- /.media -->
                        </div><!-- /.timeline-body -->
                      </li><!-- /.timeline-item -->
                      <!-- .timeline-item -->
                      <li class="timeline-item">
                        <!-- .timeline-figure -->
                        <div class="timeline-figure">
                          <span class="tile tile-circle tile-sm"><i class="fa fa-user-plus fa-lg"></i></span>
                        </div><!-- /.timeline-figure -->
                        <!-- .timeline-body -->
                        <div class="timeline-body">
                          <!-- .media -->
                          <div class="media">
                            <!-- .media-body -->
                            <div class="media-body">
                              <p class="mb-0">
                                <a href="#!">Sara Carr</a> invited to <a href="#!">Stilearn Admin</a> project </p><span class="timeline-date">7:21pm</span>
                            </div><!-- /.media-body -->
                          </div><!-- /.media -->
                        </div><!-- /.timeline-body -->
                      </li><!-- /.timeline-item -->
                      <!-- .timeline-item -->
                      <li class="timeline-item">
                        <!-- .timeline-figure -->
                        <div class="timeline-figure">
                          <span class="tile tile-circle tile-sm"><i class="fa fa-folder fa-lg"></i></span>
                        </div><!-- /.timeline-figure -->
                        <!-- .timeline-body -->
                        <div class="timeline-body">
                          <!-- .media -->
                          <div class="media">
                            <!-- .media-body -->
                            <div class="media-body">
                              <p class="mb-0">
                                <a href="#!">Angela Peterson</a> added <a href="#!">Looper Admin</a> to collection </p><span class="timeline-date">5:21pm</span>
                            </div><!-- /.media-body -->
                          </div><!-- /.media -->
                        </div><!-- /.timeline-body -->
                      </li><!-- /.timeline-item -->
                      <!-- .timeline-item -->
                      <li class="timeline-item">
                        <!-- .timeline-figure -->
                        <div class="timeline-figure">
                          <span class="tile tile-circle tile-sm"><i class="oi oi-person fa-lg"></i></span>
                        </div><!-- /.timeline-figure -->
                        <!-- .timeline-body -->
                        <div class="timeline-body">
                          <!-- .media -->
                          <div class="media">
                            <!-- .media-body -->
                            <div class="media-body">
                              <div class="avatar-group mb-2">
                                <a href="#!" class="user-avatar user-avatar-sm"><img src="https://uselooper.com/assets/images/avatars/uifaces4.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm"><img src="https://uselooper.com/assets/images/avatars/uifaces5.jpg" alt=""></a> <a href="#!" class="user-avatar user-avatar-sm"><img src="https://uselooper.com/assets/images/avatars/uifaces6.jpg" alt=""></a>
                                 <a href="#!" class="user-avatar user-avatar-sm"><img src="https://uselooper.com/assets/images/avatars/uifaces7.jpg" alt=""></a>
                              </div>
                              <p class="mb-0">
                                <a href="#!">Willie Dixon</a> and 3 others followed you </p><span class="timeline-date">4:32pm</span>
                            </div><!-- /.media-body -->
                          </div><!-- /.media -->
                        </div><!-- /.timeline-body -->
                      </li><!-- /.timeline-item -->
                    </ul><!-- /.timeline -->
                  </div><!-- /.card-body -->
                </div><!-- /.card -->
              </div><!-- /.sidebar-section-fill -->
            </div><!-- /.page-sidebar -->
          </div><!-- /.page -->
          @endforeach
          <br><br>
        </div>
@endsection
