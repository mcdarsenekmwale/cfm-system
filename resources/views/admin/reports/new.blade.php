@extends('admin.master.master')


@section('content')

<div class="wrapper">
  <!-- .page -->
  <div class="page">
    <!-- .page-inner -->
    <div class="page-inner">
      <!-- .page-title-bar -->
      <header class="page-title-bar">
        <!-- .breadcrumb -->

        <div class="d-md-flex align-items-md-start  ">
          <div class="small text-muted text-center page-title mr-sm-auto">
          </div>
          <div class="btn-toolbar">
            <button type="button" class="btn btn-light" id="export"><i class="fa fa-file-excel-o"></i> <span class="ml-1">Export</span>
            </button> <button type="button" class="btn btn-light"><i class="fa fa-upload"></i> <span class="ml-1">Import</span></button>
            <div class="dropdown">
              <button type="button" class="btn btn-light" data-toggle="dropdown"><span>More</span> <span class="fa fa-caret-down"></span></button>
              <div class="dropdown-arrow dropdown-arrow-right"></div>
              <div class="dropdown-menu dropdown-menu-right">
                <a href="{{route('newuser')}}" class="dropdown-item">Add Picture</a>
                <a href="#!" class="dropdown-item">Check Cases</a>
                <div class="dropdown-divider"></div><a href="#!" class="dropdown-item">Share</a> <a href="#!" class="dropdown-item">Archive</a> <a href="#!" class="dropdown-item">Remove</a>
              </div>
            </div>
          </div><!-- /.btn-toolbar -->
        </div><!-- /title and toolbar -->
        <!-- floating action -->
        <button type="file" class="btn btn-success btn-floated" id="file-upload"><span title="upload a file" class="fa fa-paperclip"></span></button> <!-- /floating action -->
        <!-- title and toolbar -->
        <input type="file"  class="k-hide" name="upload" id="my-file">
      </header><!-- /.page-title-bar -->
      <!-- .page-section -->
      <div class="page-section">
        <!-- .card -->
        <div class="col-xl-12">
          <!-- .card -->
          <section class="card card-fluid">
            <!-- .card-header -->
            <header class="card-header border-0">
              <div class="d-flex align-items-center">
                <span class="mr-auto">Counsellors</span> <button type="button" class="btn btn-icon btn-light " ><i class="fa fa-copy" title="copy"></i></button> <button type="button" class="btn btn-icon btn-light"><i class="fa fa-download" title="download"></i></button>
              </div>
            </header><!-- /.card-header -->

            <!--  -->

            <div class="card card-table">
                            <div class="card-header">Export Functions
                              <div class="tools dropdown"><span class="icon mdi mdi-download"></span><a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown"><span class="icon mdi mdi-more-vert"></span></a>
                                <div class="dropdown-menu" role="menu"><a class="dropdown-item" href="#">Action</a><a class="dropdown-item" href="#">Another action</a><a class="dropdown-item" href="#">Something else here</a>
                                  <div class="dropdown-divider"></div><a class="dropdown-item" href="#">Separated link</a>
                                </div>
                              </div>
                            </div>
                            <div class="card-body">
                              <div id="table3_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                <div class="row be-datatable-header">
                                  <div class="col-sm-6">
                                    <table class="table table-striped table-hover table-fw-widget dataTable no-footer" id="table3" role="grid" aria-describedby="table3_info">
                                <thead>
                                  <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="table3" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 209.2px;">Rendering engine</th><th class="sorting" tabindex="0" aria-controls="table3" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 267.2px;">Browser</th><th class="sorting" tabindex="0" aria-controls="table3" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 244px;">Platform(s)</th><th class="sorting" tabindex="0" aria-controls="table3" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 175.2px;">Engine version</th><th class="sorting" tabindex="0" aria-controls="table3" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 133.6px;">CSS grade</th></tr>
                                </thead>
                                <tr class="gradeA odd" role="row">
                                    <td class="sorting_1">Gecko</td>
                                    <td>Firefox 1.0</td>
                                    <td>Win 98+ / OSX.2+</td>
                                    <td class="center">1.7</td>
                                    <td class="center">A</td>
                                  </tr><tr class="gradeA even" role="row">
                                    <td class="sorting_1">Gecko</td>
                                    <td>Firefox 1.5</td>
                                    <td>Win 98+ / OSX.2+</td>
                                    <td class="center">1.8</td>
                                    <td class="center">A</td>
                                  </tr><tr class="gradeA odd" role="row">
                                    <td class="sorting_1">Gecko</td>
                                    <td>Firefox 2.0</td>
                                    <td>Win 98+ / OSX.2+</td>
                                    <td class="center">1.8</td>
                                    <td class="center">A</td>
                                  </tr><tr class="gradeA even" role="row">
                                    <td class="sorting_1">Gecko</td>
                                    <td>Firefox 3.0</td>
                                    <td>Win 2k+ / OSX.3+</td>
                                    <td class="center">1.9</td>
                                    <td class="center">A</td>
                                  </tr><tr class="gradeA odd" role="row">
                                    <td class="sorting_1">Gecko</td>
                                    <td>Camino 1.0</td>
                                    <td>OSX.2+</td>
                                    <td class="center">1.8</td>
                                    <td class="center">A</td>
                                  </tr><tr class="gradeA even" role="row">
                                    <td class="sorting_1">Gecko</td>
                                    <td>Camino 1.5</td>
                                    <td>OSX.3+</td>
                                    <td class="center">1.8</td>
                                    <td class="center">A</td>
                                  </tr></tbody>
                              </table>
                                </div>
                              </div>
                            </div>
                          </div>

            <!--  -->
          </section><!-- /.card -->
        </div><!-- /grid column -->
      </div><!-- /.page-section -->
      <br><br><br>
    </div><!-- /.page-inner -->
  </div><!-- /.page -->
</div>



@endsection
