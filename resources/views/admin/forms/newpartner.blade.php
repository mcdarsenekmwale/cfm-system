@extends('admin.master.master')

@section('content')
<div class="wrapper">
  <!-- .page -->
  <div class="page">
    <!-- .page-inner -->
    <div class="page-inner">
      <!-- .page-title-bar -->
      <header class="page-title-bar">
        <div class="small text-muted"> <b>NEW PARTNER
          <span class="fa fa-angle-right fa-lg" style="position: relative; right: -10px;"></span><span class="fa fa-angle-right fa-lg " style="position: relative; right: -10px;"></span> </b>
          <span class="" style="position:absolute; right: 12em;">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">
                <a href="{{route('newcall')}}" class="small text-muted active"> <b>New call </b></a>
              </li>
              <li class="breadcrumb-item">
                <a href="{{route('newcase')}}" class="small text-muted"> <b>New Case </b></a>
              </li>
              <li class="breadcrumb-item">
                <a href="#" class="small text-muted"> <b>New Partner </b></a>
              </li>
              <li class="breadcrumb-item">
                <a href="{{route('newuser')}}" class="small text-muted"> <b>New User </b></a>
              </li>
            </ol>
          </span>
        </div>
      </header><!-- /.page-title-bar -->
      <!-- .page-section -->
      <div class="page-section py-3">

        <form method="post">

            <!-- grid row -->
            <div class="row">
              <!-- grid column -->
              <div class="col-lg-4">
                <!-- .card -->
                <div class="card card-fluid">
                  <h6 class="card-header text-white text-uppercase" style="background-color:#343a40;"> Partner Details </h6><!-- .nav -->
                  <nav class="nav nav-tabs flex-column border-0" id="myTab" role="tablist">
                    <a class="nav-link active"  id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true">Partner Profile</a>
                    <a  class="nav-link "  id="account-tab" data-toggle="tab" href="#account" role="tab" aria-controls="account" aria-selected="true">Partner Account</a>
                    <a  class="nav-link"  id="issues-tab" data-toggle="tab" href="#issues" role="tab" aria-controls="issues" aria-selected="true">Partner Issues</a>
                    <a class="nav-link"  id="others-tab" data-toggle="tab" href="#others" role="tab" aria-controls="others" aria-selected="true">Partner Others</a>
                  </nav><!-- /.nav -->
                </div><!-- /.card -->
              </div><!-- /grid column -->
              <!-- grid column -->
              <div class="col-lg-8">

                <div class="tab-content my--2" id="nav-tabContent">
                  <div class="tab-content" id="myTabContent">

                    <!-- profile -->
                    <div class="tab-pane fade show  active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                      <!-- .card -->
                      <div class="card card-fluid">
                      <h6 class="card-header text-white text-uppercase" style="background-color:#343a40;">Partner Profile : </h6><!-- .card-body -->
                      <div class="card-body">
                        <!-- .media -->
                        <div class="media mb-3">
                          <!-- avatar -->
                          <div class="user-avatar user-avatar-xl fileinput-button">
                            <div class="fileinput-button-label"> Change logo </div><img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt=""> <input id="fileupload-avatar" type="file" name="avatar">
                          </div><!-- /avatar -->
                          <!-- .media-body -->
                          <div class="media-body pl-3">
                            <h3 class="card-title"> Partner logo </h3>
                            <h6 class="card-subtitle text-muted"> Click to change your profile logo. </h6>

                            <div id="progress-avatar" class="progress progress-xs fade">
                              <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                            </div><!-- /avatar upload progress bar -->
                          </div><!-- /.media-body -->
                        </div><!-- /.media -->

                          <!-- form row -->
                          <div class="form-row">
                            <!-- form column -->
                            <label for="input01" class="col-md-3 form-control-label text-muted" >*Cover image :</label> <!-- /form column -->
                            <!-- form column -->
                            <div class="col-md-9 mb-3">
                              <div class="custom-file">
                                <input type="file" class="custom-file-input" id="input01" multiple=""> <label class="custom-file-label" for="input01">Choose cover</label>
                              </div><small class="text-muted">Upload a new cover image, JPG 1200x300</small>
                            </div><!-- /form column -->
                          </div><!-- /form row -->
                          <!-- form row -->
                          <div class="form-row">
                            <!-- form column -->
                            <label for="input02" class="col-md-3 form-control-label text-muted">*Institution :</label> <!-- /form column -->
                            <!-- form column -->
                            <div class="col-md-9 mb-3">
                              <input type="text" class="form-control"  value="">
                            </div><!-- /form column -->
                          </div><!-- /form row -->
                          <!-- form row -->
                          <div class="form-row">
                            <!-- form column -->
                            <label for="input03" class="col-md-3 form-control-label text-muted">*Logo Vision :</label> <!-- /form column -->
                            <!-- form column -->
                            <div class="col-md-9 mb-3">
                              <textarea type="text" class="form-control" ></textarea>
                              <small class="text-muted">Appears on your profile page, 300 chars max.</small>
                            </div><!-- /form column -->
                          </div><!-- /form row -->
                          <!-- form row -->
                          <div class="form-row">
                            <!-- form column -->
                            <label for="input04" class="col-md-3 form-control-label text-muted">* User Type:</label> <!-- /form column -->
                            <!-- form column -->
                            <div class="col-md-9 mb-3">
                              <label class="k-checkbox ">
                                <input type="checkbox" name="user_type" value="partner" checked> Partner
                                <span></span>
                              </label>
                            </div><!-- /form column -->
                          </div><!-- /form row -->
                          <hr>
                          <!-- .form-actions -->
                          <div class="form-actions">
                            <button type="button" class="btn btn-primary ml-auto" id="account-tab" data-toggle="tab" href="#account" role="tab" aria-controls="account" aria-selected="true">Save and Continue</button>
                          </div><!-- /.form-actions -->

                      </div><!-- /.card-body -->
                    </div>
                    </div>

                    <!-- account -->
                    <div class="tab-pane fade show  " id="account" role="tabpanel" aria-labelledby="account-tab">
                      <!-- .card -->
                      <div class="card card-fluid">
                        <h6 class="card-header text-white text-uppercase" style="background-color:#343a40;">Partner Account </h6><!-- .card-body -->
                        <div class="card-body">

                            <!-- .form-group -->
                            <div class="form-row">
                              <!-- form column -->
                              <div class="col-md-6 mb-3">
                                <label class="form-control-label text-muted">*First Name :</label> <input type="text" class="form-control" name="firstname" value="{{old('firstname')}}" required="">
                              </div><!-- /form column -->
                              <!-- form column -->
                              <div class="col-md-6 mb-3">
                                <label class="form-control-label text-muted">*Last Name :</label> <input type="text" class="form-control" name="lastname" value="{{old('lastname')}}" required="">
                              </div><!-- /form column -->
                            </div><!-- /form row -->
                            <div class="form-group">
                              <label class="form-control-label text-muted">*Email :</label> <input type="email" class="form-control" name="email" value="{{old('email')}}" required="">
                            </div><!-- /.form-group -->
                            <!-- .form-group -->
                            <div class="form-group">
                              <label class="form-control-label text-muted">*Username :</label> <input type="text" class="form-control"name="username" value="{{old('username')}}" required=""> 
                            </div><!-- /.form-group -->
                            <hr>
                            <!-- .form-group -->
                            <div class="form-group">
                              <label class="form-control-label text-muted">*New Password :</label> <input type="password" class="form-control" name="password"  value="{{old('password')}}" required="">
                            </div><!-- /.form-group -->
                            <!-- .form-actions -->
                            <div class="form-actions">
                              <button type="button" class="btn btn-primary ml-auto" id="notifications-tab" data-toggle="tab" href="#notifications" role="tab" aria-controls="account" aria-selected="true">Save and Continue</button>
                            </div><!-- /.form-actions -->

                        </div><!-- /.card-body -->
                      </div><!-- /.card -->
                    </div>

                    <!-- issues -->
                    <div class="tab-pane fade show  " id="issues" role="tabpanel" aria-labelledby="issues-tab">
                      <!-- .card -->
                      <div class="card card-fluid">
                        <h6 class="card-header">Partner Issues </h6><!-- .card-body -->
                        <div class="card-body">
                          <!-- form -->

                        </div><!-- /.card-body -->
                      </div><!-- /.card -->
                    </div>

                    <!-- others -->
                    <div class="tab-pane fade show  " id="others" role="tabpanel" aria-labelledby="others-tab">
                      <!-- .card -->
                      <div class="card card-fluid">
                        <h6 class="card-header text-white text-uppercase" style="background-color:#343a40;"> Additional Details </h6><!-- .card-body -->
                        <div class="card-body">

                        </div><!-- /.card-body -->
                      </div><!-- /.card -->
                    </div>

                  </div>
                </div>
              </div><!-- /grid column -->
            </div><!-- /grid row -->
        </form>
      </div><!-- /.page-section -->
    </div><!-- /.page-inner -->
  </div><!-- /.page -->
</div>
@endsection
