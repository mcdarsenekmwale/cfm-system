<!-- .app-aside -->
<aside class="app-aside app-aside-expand-md app-aside-light py-4 card-color">
  <!-- .aside-content -->
  <div class="aside-content">
    <!-- .aside-header -->
    <header class="aside-header d-block d-md-none">
      <!-- .btn-account -->
      <button class="btn-account" type="button" data-toggle="collapse" data-target="#dropdown-aside"><span class="user-avatar user-avatar-lg"><img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt=""></span> <span class="account-icon"><span class="fa fa-caret-down fa-lg"></span></span> <span class="account-summary"><span class="account-name">{{Auth::user()->firstname}} {{Auth::user()->lastname}}</span>
        <span class="account-description">{{Auth::user()->type}}</span></span></button>
      <!-- .dropdown-aside -->
      <div id="dropdown-aside" class="dropdown-aside collapse">
        <!-- dropdown-items -->
        <div class="pb-3">
          <a class="dropdown-item" href="{{route('profile')}}"><span class="dropdown-icon fa fa-user-circle"></span> Profile</a> <a class="dropdown-item" href="{{ route('logout') }}"><span class="dropdown-icon fa fa-sign-out"></span> Logout</a>
          <div class="dropdown-divider"></div><a class="dropdown-item" href="#!">Help Center</a> <a class="dropdown-item" href="#!">Ask Forum</a> <a class="dropdown-item" href="#!">Keyboard Shortcuts</a>
        </div><!-- /dropdown-items -->
      </div><!-- /.dropdown-aside -->
    </header><!-- /.aside-header -->
    <!-- .aside-menu -->
    <section class="aside-menu overflow-hidden">
      <!-- .stacked-menu -->
      <nav id="stacked-menu" class="stacked-menu stacked-menu-has-collapsible">
        <!-- .menu -->
        <ul class="menu">
          <!-- .menu-item -->
          <li class="menu-item ">
            <a href="{{url('/')}} " class="menu-link menu-a-link  {{'active'}}"><span class="menu-icon fa fa-home"></span> <span class="menu-text">Dashboard</span></a>
          </li><!-- /.menu-item -->
          <!-- .menu-item -->
          <li class="menu-item has-child {{'active'}}">
            <a href="#" class="menu-link menu-a-link"><span class="menu-icon fa fa-clone"></span> <span class="menu-text">Reports</span> <span class="badge badge-warning "> <span class="pulse">12</span></span></a> <!-- child menu -->
            <ul class="menu"><li class="menu-subhead">Reports</li>
              <li class="menu-item">
                <a href="{{route('newreport')}}" class="menu-link menu-a-link" tabindex="-1"> New Report<span class="badge badge-success float-right "> <span class="pulse">new</span></a>
              </li>
              <li class="menu-item">
                <a href="#" class="menu-link menu-a-link" tabindex="-1"> Counsellor</a>
              </li>
              <li class="menu-item">
                <a href="#" class="menu-link menu-a-link" tabindex="-1">Supervisor</a>
              </li>
              <li class="menu-item">
                <a href="{{route('partner-list')}}" class="menu-link menu-a-link" tabindex="-1"> Partners</a>
              </li>

              <li class="menu-item">
                <a href="{{route('request')}}" class="menu-link menu-a-link" tabindex="-1">Requests<span class="badge badge-subtle badge-success"><i class=" px-2 fa fa-fw fa-circle pulse mr-1"></i> </span></a>
              </li>
              <li class="menu-item">
                <a href="{{route('calendar')}}" class="menu-link menu-a-link" tabindex="-1">Calendar</a>
              </li>
            </ul><!-- /child menu -->
          </li><!-- /.menu-item -->
          <!-- .menu-item -->
          <li class="menu-item has-child {{'active'}}">
            <a   class="menu-link menu-a-link"><span class="menu-icon fa fa-wrench"></span> <span class="menu-text">Authentication</span></a> <!-- child menu -->
            <ul class="menu " ><li class="menu-subhead">Authentication</li>
              <li class="menu-item">
                <a href="#" class="menu-link menu-a-link " tabindex="-1">Log</a>
              </li>
              <li class="menu-item">
                <a href="#" class="menu-link menu-a-link" tabindex="-1">Error Pages</a>
              </li>

              <li class="menu-item">
                <a href="#" class="menu-link menu-a-link" tabindex="-1">Maintenance Plans</a>
              </li>
              <li class="menu-item">
                <a href="#" class="menu-link menu-a-link" tabindex="-1">Recovery Password</a>
              </li>
              <li class="menu-item">
                <a href="#" class="menu-link menu-a-link" tabindex="-1">Cases Locked</a>
              </li>
            </ul><!-- /child menu -->
          </li><!-- /.menu-item -->
          <!-- .menu-item -->
          <li class="menu-item  has-child {{'active'}}">
            <a href="#" class="menu-link menu-a-link"><span class="menu-icon fa fa-user-circle"></span> <span class="menu-text">Users and Settings</span></a> <!-- child menu -->
            <ul class="menu"><li class="menu-subhead">Users and Settings</li>
              <li class="menu-item">
                <a href="{{route('admin.profile')}}" class="menu-link menu-a-link" tabindex="-1"><i class="fa fa-user-circle"></i> Profile</a>
              </li>
              <li class="menu-item">
                <a href="{{route('users')}}" class="menu-link menu-a-link" tabindex="-1"><i class="fa fa-users"></i> Users</a>
              </li>
              <li class="menu-item has-child">
                <a href="#" class="menu-link menu-a-link" tabindex="-1"><i class="fa fa-building"></i> Partners</a> <!-- grand child menu -->
                <ul class="menu">
                  <li class="menu-item">
                    <a href="{{route('partner-list')}}" class="menu-link menu-a-link" tabindex="-1"> Partners List</a>
                  </li>
                  <li class="menu-item">
                    <a href="{{route('boardPartner')}}" class="menu-link menu-a-link" tabindex="-1">Partners Board</a>
                  </li>
                  <li class="menu-item">
                    <a href="{{route('partnerprojects')}}" class="menu-link menu-a-link" tabindex="-1">Partners Projects</a>
                  </li>
                  <li class="menu-item">
                    <a href="{{route('partnergrantt')}}" class="menu-link menu-a-link" tabindex="-1">Partners Gantt Chart</a>
                  </li>
                </ul><!-- /grand child menu -->
              </li>
              <li class="menu-item">
                <a href="{{route('admin.profile')}}" class="menu-link menu-a-link" tabindex="-1"><i class="fa fa-vcard"></i> Profile Settings</a>
              </li>
              <li class="menu-item">
                <a href="{{route('account')}}" class="menu-link menu-a-link" tabindex="-1"><i class="fa fa-wrench"></i> Account Settings</a>
              </li>
              <li class="menu-item">
                <a href="{{route('notifications')}}" class="menu-link menu-a-link" tabindex="-1"><i class="fa fa-universal-access"></i> Notification Settings</a>
              </li>
            </ul><!-- /child menu -->
          </li><!-- /.menu-item -->
          <!-- .menu-item -->
          <li class="menu-item has-child">
            <a href="#" class="menu-link menu-a-link"><span class="menu-icon fa fa-book"></span> <span class="menu-text">System Layouts</span> <span class="badge badge-subtle badge-success">+4</span></a> <!-- child menu -->
            <ul class="menu"><li class="menu-subhead">System Layouts</li>
              <li class="menu-item">
                <a href="#" class="menu-link menu-a-link" tabindex="-1">Search</a>
              </li>
              <li class="menu-item">
                <a href="#" class="menu-link menu-a-link" tabindex="-1">Rendering</a>
              </li>
              <li class="menu-item">
                <a href="#" class="menu-link menu-a-link" tabindex="-1"> Cover</a>
              </li>
              <li class="menu-item">
                <a href="#" class="menu-link menu-a-link" tabindex="-1">Logos</a>
              </li>
              <li class="menu-item">
                <a href="#" class="menu-link menu-a-link" tabindex="-1">Sidebar</a>
              </li>
              <li class="menu-item">
                <a href="#" class="menu-link menu-a-link" tabindex="-1">Custom</a>
              </li>
            </ul><!-- /child menu -->
          </li><!-- /.menu-item -->
          <!-- .menu-header -->
          <li class="menu-header">General Tasks </li><!-- /.menu-header -->
          <!-- .menu-item -->
          <li class="menu-item has-child">
            <a href="#" class="menu-link menu-a-link"><span class="menu-icon fa fa-pencil-square-o"></span> <span class="menu-text">Forms</span></a> <!-- child menu -->
            <ul class="menu"><li class="menu-subhead">Forms</li>
              <li class="menu-item">
                <a href="{{route('newcall')}}" class="menu-link menu-a-link" tabindex="-1"><i class="fa fa-phone"></i> New Call</a>
              </li>
              <li class="menu-item">
                <a href="{{route('newcase')}}" class="menu-link menu-a-link" tabindex="-1"><i class="fa fa-folder"></i>  New Case</a>
              </li>
              <li class="menu-item">
                <a href="{{route('newuser')}}" class="menu-link menu-a-link" tabindex="-1"><i class="fa fa-user-circle-o"></i> New User</a>
              </li>
              <li class="menu-item">
                <a href="{{route('newpartner')}}" class="menu-link menu-a-link" tabindex="-1"><i class="fa fa-building"></i> New Partner</a>
              </li>
            </ul><!-- /child menu -->
          </li><!-- /.menu-item -->
          <!-- .menu-item -->
          <li class="menu-item has-child">
            <a href="#" class="menu-link menu-a-link"><span class="menu-icon fa fa-table"></span> <span class="menu-text">Tables</span></a> <!-- child menu -->
            <ul class="menu"><li class="menu-subhead">Analytic Tables</li>
              <li class="menu-item">
                <a href="{{route('partners')}}" class="menu-link menu-a-link" tabindex="-1">Partners Table</a>
              </li>
              <li class="menu-item">
                <a href="{{route('counsellors')}}" class="menu-link menu-a-link" tabindex="-1">Counsellors Table</a>
              </li>
              <li class="menu-item">
                <a href="{{route('supervisors')}}" class="menu-link menu-a-link" tabindex="-1">Supervisors Table</a>
              </li>
              <li class="menu-item">
                <a href="{{route('cases')}}" class="menu-link menu-a-link" tabindex="-1">Cases Table</a>
              </li>
              <li class="menu-item">
                <a href="{{route('calls')}}" class="menu-link menu-a-link" tabindex="-1">Calls Table</a>
              </li>
            </ul><!-- /child menu -->
          </li><!-- /.menu-item -->
          <!-- .menu-item -->
          <li class="menu-item has-child">
            <a href="#" class="menu-link menu-a-link"><span class="menu-icon fa fa-bar-chart"></span> <span class="menu-text">Analytics</span></a> <!-- child menu -->
            <ul class="menu"><li class="menu-subhead">Analytics</li>
              <li class="menu-item">
                <a href="{{route('charts')}}" class="menu-link menu-a-link" tabindex="-1"><i class="fa fa-pie-chart"></i> Analytics</a>
              </li>
              <li class="menu-item">
                <a href="{{route('maps')}}" class="menu-link menu-a-link" tabindex="-1"><i class="fa fa-globe"></i> Map</a>
              </li>
            </ul><!-- /child menu -->
          </li><!-- /.menu-item -->
        </ul><!-- /.menu -->
      </nav><!-- /.stacked-menu -->
    </section><!-- /.aside-menu -->
    <!-- /Skin changer -->
  </div><!-- /.aside-content -->
</aside><!-- /.app-aside -->
