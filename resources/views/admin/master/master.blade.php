<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>CFM System</title>


  <!-- Fonts -->
  <link rel="dns-prefetch" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CRaleway:300,400,500,600,700" media="all">

  <!-- Fonts -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="dns-prefetch" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.css" integrity="sha256-/WnCqTIGflaXyhZUIQe/O4gwaMRNtN39rHMKIeKbN3c=" crossorigin="anonymous" >
  <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" />
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" type="text/css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/TableExport/5.1.0/css/tableexport.css" />

  <!--  dataTables-->
  <link rel="stylesheet" href="https://cdn.datatables.net/autofill/2.3.2/css/autoFill.bootstrap4.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.4/css/buttons.bootstrap4.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.bootstrap4.min.css">

  <!--  -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.print.css">
  <!-- Styles -->
<!--  -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tributejs/3.4.0/tribute.css" />
<!--  -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.2/photoswipe.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.2/default-skin/default-skin.css" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/typeahead.js-bootstrap4-css@1.0.0/typeaheadjs.css">
<!-- grant -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/frappe-gantt/0.3.0/frappe-gantt.css" />
<link rel="" href="https://uselooper.com/assets/stylesheets/theme-dark.min.css" data-skin="dark" disabled="true">
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('css/style.bundle.css') }}">
  <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
  <link rel="stylesheet" href="{{ asset('css/master.css') }}">
  <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">

</head>
<body style=" background-color: #E6E6E6;" class="  pace-done">
  <div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
  <div class="pace-progress-inner"></div>
</div>
<div class="pace-activity"></div></div>
  <div class="app" id="header">
        <!-- .app-header -->
        <header class="app-header app-header-dark " style="height:4.5em;">
          <!-- .top-bar -->
          <div class="top-bar" style="position:relative; top:2px;">
            <!-- .top-bar-brand -->
            <div class="top-bar-brand" style="background-color:#343a40;">
              <a href="https://www.yoneco.org " class="zoom px-4">  <img src="{{url('/')}}/images/logo.jpg" alt="" class="rounded-circle bounceIn img-fluid " style="height: 58px;width: 58px; "></a>
              <a class=" " href="{{ url('/') }}" title="Conflict Feedback Mechanism System">
                <b> <span class="h6 " style="font-size:16.7px">CFM SYSTEM</span></b>
              </a>
            </div><!-- /.top-bar-brand -->
            <!-- .top-bar-list -->
            <div class="top-bar-list">
              <!-- .top-bar-item -->
              <div class="top-bar-item px-2 d-md-none d-lg-none d-xl-none">
                <!-- toggle menu -->
                <button class="hamburger hamburger-squeeze" type="button" data-toggle="aside" aria-label="toggle menu"><span class="hamburger-box"><span class="hamburger-inner"></span></span></button> <!-- /toggle menu -->
              </div><!-- /.top-bar-item -->
              <!-- .top-bar-item -->
              <div class="top-bar-item top-bar-item-full">
                <!-- .top-bar-search -->
                <div class="top-bar-search">
                  <div class="input-group input-group-search">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><span class="fa fa-search fa-lg"></span></span>
                    </div><input type="search" class="form-control typeahead" aria-label="Search" placeholder="Search" keyup.Enter="" id="searching">

                  </div>
                </div><!-- /.top-bar-search -->
              </div><!-- /.top-bar-item -->
              <!-- .top-bar-item -->
              <div class="top-bar-item top-bar-item-right px-0 d-none d-sm-flex">
                <!-- .nav -->
                <ul class="header-nav nav">
                  <!-- .nav-item -->
                  <li class="nav-item dropdown header-nav-dropdown ">
                    <a class="nav-link" href="#!" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-bell fa-2x"></span></a>
                    <div class="dropdown-arrow"></div><!-- .dropdown-menu -->
                    <div class="dropdown-menu dropdown-menu-rich dropdown-menu-right dropdown-menu-card ">
                      <h6 class="dropdown-header stop-propagation">
                        <span>Activities <span class="badge">(2)</span></span>
                      </h6><!-- .dropdown-scroll -->
                      <div class="dropdown-scroll perfect-scrollbar">
                        <!-- .dropdown-item -->
                        <a href="#!" class="dropdown-item unread">
                          <div class="user-avatar">
                            <img src="http://uselooper.com/assets/images/avatars/uifaces15.jpg" alt="">
                          </div>
                          <div class="dropdown-item-body">
                            <p class="text"> Jeffrey Wells created a schedule </p><span class="date">Just now</span>
                          </div>
                        </a> <!-- /.dropdown-item -->
                        <!-- .dropdown-item -->
                        <a href="#!" class="dropdown-item unread">
                          <div class="user-avatar">
                            <img src="http://uselooper.com/assets/images/avatars/uifaces16.jpg" alt="">
                          </div>
                          <div class="dropdown-item-body">
                            <p class="text"> Anna Vargas logged a chat </p><span class="date">3 hours ago</span>
                          </div>
                        </a> <!-- /.dropdown-item -->
                        <!-- .dropdown-item -->
                        <a href="#!" class="dropdown-item">
                          <div class="user-avatar">
                            <img src="http://uselooper.com/assets/images/avatars/uifaces17.jpg" alt="">
                          </div>
                          <div class="dropdown-item-body">
                            <p class="text"> Sara Carr invited to Stilearn Admin </p><span class="date">5 hours ago</span>
                          </div>
                        </a> <!-- /.dropdown-item -->
                        <!-- .dropdown-item -->
                        <a href="#!" class="dropdown-item">
                          <div class="user-avatar">
                            <img src="http://uselooper.com/assets/images/avatars/uifaces18.jpg" alt="">
                          </div>
                          <div class="dropdown-item-body">
                            <p class="text"> Arthur Carroll updated a project </p><span class="date">1 day ago</span>
                          </div>
                        </a> <!-- /.dropdown-item -->
                        <!-- .dropdown-item -->
                        <a href="#!" class="dropdown-item">
                          <div class="user-avatar">
                            <img src="http://uselooper.com/assets/images/avatars/uifaces19.jpg" alt="">
                          </div>
                          <div class="dropdown-item-body">
                            <p class="text"> Hannah Romero created a task </p><span class="date">1 day ago</span>
                          </div>
                        </a> <!-- /.dropdown-item -->
                        <!-- .dropdown-item -->
                        <a href="#!" class="dropdown-item">
                          <div class="user-avatar">
                            <img src="http://uselooper.com/assets/images/avatars/uifaces20.jpg" alt="">
                          </div>
                          <div class="dropdown-item-body">
                            <p class="text"> Angela Peterson assign a task to you </p><span class="date">2 days ago</span>
                          </div>
                        </a> <!-- /.dropdown-item -->
                        <!-- .dropdown-item -->
                        <a href="#!" class="dropdown-item">
                          <div class="user-avatar">
                            <img src="http://uselooper.com/assets/images/avatars/uifaces21.jpg" alt="">
                          </div>
                          <div class="dropdown-item-body">
                            <p class="text"> Shirley Mason and 3 others followed you </p><span class="date">2 days ago</span>
                          </div>
                        </a> <!-- /.dropdown-item -->
                      </div><!-- /.dropdown-scroll -->
                      <a href="http://uselooper.com/user-activities.html" class="dropdown-footer text-muted">All activities <i class="fas fa-fw fa-long-arrow-alt-right"></i></a>
                    </div><!-- /.dropdown-menu -->
                  </li><!-- /.nav-item -->
                  <!-- .nav-item -->
                  <li class="nav-item dropdown header-nav-dropdown ">
                    <a class="nav-link" href="#!" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-envelope fa-2x "></span></a>
                    <div class="dropdown-arrow"></div><!-- .dropdown-menu -->
                    <div class="dropdown-menu dropdown-menu-rich dropdown-menu-right dropdown-menu-card ">
                      <h6 class="dropdown-header stop-propagation">
                        <span>Messages</span> <a href="#!">Mark all as read</a>
                      </h6><!-- .dropdown-scroll -->
                      <div class="dropdown-scroll perfect-scrollbar">
                        <!-- .dropdown-item -->
                        <a href="#!" class="dropdown-item unread">
                          <div class="user-avatar">
                            <img src="http://uselooper.com/assets/images/avatars/team1.jpg" alt="">
                          </div>
                          <div class="dropdown-item-body">
                            <p class="subject"> Stilearning </p>
                            <p class="text text-truncate"> Invitation: Joe's Dinner @ Fri Aug 22 </p><span class="date">2 hours ago</span>
                          </div>
                        </a> <!-- /.dropdown-item -->
                        <!-- .dropdown-item -->
                        <a href="#!" class="dropdown-item">
                          <div class="user-avatar">
                            <img src="http://uselooper.com/assets/images/avatars/team3.png" alt="">
                          </div>
                          <div class="dropdown-item-body">
                            <p class="subject"> Openlane </p>
                            <p class="text text-truncate"> Final reminder: Upgrade to Pro </p><span class="date">23 hours ago</span>
                          </div>
                        </a> <!-- /.dropdown-item -->
                        <!-- .dropdown-item -->
                        <a href="#!" class="dropdown-item">
                          <div class="tile tile-circle bg-green"> GZ </div>
                          <div class="dropdown-item-body">
                            <p class="subject"> Gogo Zoom </p>
                            <p class="text text-truncate"> Live healthy with this wireless sensor. </p><span class="date">1 day ago</span>
                          </div>
                        </a> <!-- /.dropdown-item -->
                        <!-- .dropdown-item -->
                        <a href="#!" class="dropdown-item">
                          <div class="tile tile-circle bg-teal"> GD </div>
                          <div class="dropdown-item-body">
                            <p class="subject"> Gold Dex </p>
                            <p class="text text-truncate"> Invitation: Design Review @ Mon Jul 7 </p><span class="date">1 day ago</span>
                          </div>
                        </a> <!-- /.dropdown-item -->
                        <!-- .dropdown-item -->
                        <a href="#!" class="dropdown-item">
                          <div class="user-avatar">
                            <img src="http://uselooper.com/assets/images/avatars/team2.png" alt="">
                          </div>
                          <div class="dropdown-item-body">
                            <p class="subject"> Creative Division </p>
                            <p class="text text-truncate"> Need some feedback on this please </p><span class="date">2 days ago</span>
                          </div>
                        </a> <!-- /.dropdown-item -->
                        <!-- .dropdown-item -->
                        <a href="#!" class="dropdown-item">
                          <div class="tile tile-circle bg-pink"> LD </div>
                          <div class="dropdown-item-body">
                            <p class="subject"> Lab Drill </p>
                            <p class="text text-truncate"> Our UX exercise is ready </p><span class="date">6 days ago</span>
                          </div>
                        </a> <!-- /.dropdown-item -->
                      </div><!-- /.dropdown-scroll -->
                      <a href="http://uselooper.com/page-messages.html" class="dropdown-footer">All messages <i class="fas fa-fw fa-long-arrow-alt-right"></i></a>
                    </div><!-- /.dropdown-menu -->
                  </li><!-- /.nav-item -->
                  <!-- .nav-item -->
                  <li class="nav-item dropdown header-nav-dropdown">
                    <a class="nav-link" href="#!" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-cog fa-2x"></span></a>
                    <div class="dropdown-arrow"></div><!-- .dropdown-menu -->
                    <div class="dropdown-menu dropdown-menu-rich dropdown-menu-right dropdown-menu-card">
                      <!-- .dropdown-sheets -->
                      <div class="dropdown-sheets">
                        <!-- .dropdown-sheet-item -->
                        <div class="dropdown-sheet-item">
                          <a href="#!" class="tile-wrapper"><span class="tile tile-lg bg-indigo"><i class="fa fa-users"></i></span> <span class="tile-peek">Users</span></a>
                        </div><!-- /.dropdown-sheet-item -->
                        <!-- .dropdown-sheet-item -->
                        <div class="dropdown-sheet-item">
                          <a href="#!" class="tile-wrapper"><span class="tile tile-lg bg-teal"><i class="fa fa-cork-fork"></i></span> <span class="tile-peek">Reports</span></a>
                        </div><!-- /.dropdown-sheet-item -->
                        <!-- .dropdown-sheet-item -->
                        <div class="dropdown-sheet-item">
                          <a href="#!" class="tile-wrapper"><span class="tile tile-lg bg-pink"><i class="fa fa-tasks"></i></span> <span class="tile-peek">Cases</span></a>
                        </div><!-- /.dropdown-sheet-item -->
                        <!-- .dropdown-sheet-item -->
                        <div class="dropdown-sheet-item">
                          <a href="#!" class="tile-wrapper"><span class="tile tile-lg bg-yellow"><i class=" text-white fa fa-phone"></i></span> <span class="tile-peek">Calls</span></a>
                        </div><!-- /.dropdown-sheet-item -->
                        <!-- .dropdown-sheet-item -->
                        <div class="dropdown-sheet-item">
                          <a href="#!" class="tile-wrapper"><span class="tile tile-lg bg-cyan"><i class="fa fa-file"></i></span> <span class="tile-peek">Files</span></a>
                        </div><!-- /.dropdown-sheet-item -->
                      </div><!-- .dropdown-sheets -->
                    </div><!-- .dropdown-menu -->
                  </li><!-- /.nav-item -->
                </ul><!-- /.nav -->
                <!-- .btn-account -->
                <div class="dropdown">
                  <button class="btn-account d-none d-md-flex" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="user-avatar user-avatar-md">
                    <img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt="">
                  </span>
                  <span class="account-summary pr-lg-4 d-none d-lg-block"><span class="account-name">{{Auth::user()->firstname}} {{Auth::user()->lastname}}</span> <span class="account-description"> {{Auth::user()->type}} </span></span></button>
                  <div class="dropdown-arrow dropdown-arrow-left"></div><!-- .dropdown-menu -->
                  <div class="dropdown-menu">
                    <h6 class="dropdown-header d-none d-md-block d-lg-none"> {{Auth::user()->firstname}} {{Auth::user()->lastname}} </h6>
                    <a class="dropdown-item" href="{{route('admin.profile')}}"><span class="dropdown-icon fa fa-user-circle-o"></span> Profile</a>
                     <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
                     <span class="dropdown-icon fa fa-sign-out"></span> Logout</a>
                   <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                     @csrf
                   </form>
                    <div class="dropdown-divider"></div><a class="dropdown-item" href="#!">Help Center</a> <a class="dropdown-item" href="#!">Ask Team</a>
                  </div><!-- /.dropdown-menu -->
                </div><!-- /.btn-account -->
              </div><!-- /.top-bar-item -->
            </div><!-- /.top-bar-list -->
          </div><!-- /.top-bar -->
        </header><!-- /.app-header -->
        @include('admin.master.sidebar')
        <!-- .app-main -->
    <main class="app-main" >
        <div class="" id="display">
          @yield('content')
        </div>
        <div class="container px-5" id="displayResults">
          <div class="container-fluid">
            <div class="row">
                <div class="" id="result">

                </div>
            </div>

          </div>
        </div>
        <footer class="footer white-text text-center border-top">
          <div class="container">
            <span class="text-dark"><?php date_default_timezone_set('Africa/Blantyre'); echo date("Y")?> <i class="fa fa-copyright"></i> YONECO CFM</span>
            <small class="text-muted">YONECO ICT DEPARTMENT</small> <br>
              <small class="text-muted"><ul class="list-inline">
                <li class="list-inline-item">
                  <a class="text-muted" href="#!">Support</a>
                </li>
                <li class="list-inline-item">
                  <a class="text-muted" href="#!">Help Center</a>
                </li>
                <li class="list-inline-item">
                  <a class="text-muted" href="#!">Privacy</a>
                </li>
                <li class="list-inline-item">
                  <a class="text-muted" href="#!">Terms of Service</a>
                </li>
              </ul></small>

          </div>
        </footer>
      </main><!-- /.app-main -->
      <div class="aside-backdrop"></div>
    </div>

<!-- jQuery --><script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.2.1/typeahead.jquery.js"></script> -->
<script src="https://cdn.bootcss.com/typeahead.js/0.11.1/bloodhound.js" charset="utf-8"></script>
<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.js" integrity="sha256-zTde1SYEpUiY54BwIFLX07JyfYU46JlHZvyTiCmg6ig=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"><\/script>')
</script><script src="https://fastcdn.org/FileSaver.js/1.1.20151003/FileSaver.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/TableExport/5.1.0/js/tableexport.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<!-- Google Analytics -->
<script>
  window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
  ga('create', 'UA-XXXXX-Y', 'auto');
  ga('send', 'pageview');
</script>
<script async src='https://www.google-analytics.com/analytics.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
<script>
WebFont.load({
  google: {"families":["Poppins:300,400,500,600,700","Raleway:300,400,500,600,700"]},
  active: function() {
    sessionStorage.fonts = true;
  }
});
</script>
<script src="https://cdn.freecodecamp.org/testable-projects-fcc/v1/bundle.js"></script>


<!--grant charts  -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/frappe-gantt/0.3.0/frappe-gantt.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="{{ asset('js/grantCharts.js') }}" defer></script>
<script src="{{ asset('js/grantDraw.js') }}" defer></script>
<script src="{{ asset('js/svggrantt.js') }}" defer></script>
<script src="{{ asset('js/board.js') }}" defer></script>

<!-- Scripts -->
<script src="{{ asset('js/style.bundle.js') }}" defer></script>
<script src="https://unpkg.com/moment" ></script>
<script src="https://unpkg.com/vue-tribute"></script>
<script src="{{ asset('js/moment.min.js') }}" defer></script>
<script src="{{ asset('js/perfect-scrollbar.min.js') }}" defer></script>
<script src="{{ asset('js/stacked-menu.min.js') }}" defer></script>
<script src="{{ asset('js/scrollpy.js') }}" defer></script>
<script src="{{ asset('js/pie-chart.js') }}" defer></script>
<script src="{{ asset('js/bootstrap.js') }}" defer></script>
<script src="{{ asset('js/master.js') }}" defer></script>
<script src="{{ asset('js/pace.js') }}" defer></script>
<script src="{{ asset('js/Admindashboard.js') }}" defer></script>
<script src="{{ asset('js/dashboard.js') }}" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js" defer></script>
<script src="{{ asset('js/calendar.js') }}" defer></script>
<!-- map js -->

<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/maps.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/dataviz.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/material.js"></script>
<script src="https://www.amcharts.com/lib/4/lang/de_DE.js"></script>
<script src="https://www.amcharts.com/lib/4/geodata/worldLow.js"></script>
<script src="https://www.amcharts.com/lib/4/geodata/usaLow.js"></script>
<script src="https://www.amcharts.com/lib/4/geodata/region/world/africaLow.js" charset="utf-8"></script>s
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
<script src="{{ asset('js/adminpie.js') }}" defer></script>
<script src="{{ asset('js/adminpie_users.js') }}" defer></script>
<script src="{{ asset('js/maps.js') }}" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>

<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-116692175-1"></script>

<script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
<script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/maps/fusioncharts.malawi.js"></script>
<script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.world.js"></script>
<script src="https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js" charset="utf-8"></script>
<script src="{{asset('js/fusionchart.js')}}" charset="utf-8"></script>
<script src="{{asset('js/export.js')}}" charset="utf-8"></script>

<!--  -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/tributejs/3.4.0/tribute.js" charset="utf-8"></script>
<script src="{{asset('js/chart.partner.js')}}" charset="utf-8"></script>

<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js" charset="utf-8"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js" charset="utf-8"></script>
<script src="https://cdn.datatables.net/autofill/2.3.2/js/dataTables.autoFill.min.js" charset="utf-8"></script>
<script src="https://cdn.datatables.net/autofill/2.3.2/js/autoFill.bootstrap4.min.js" charset="utf-8"></script>
<script src="https://cdn.datatables.net/buttons/1.5.4/js/dataTables.buttons.min.js" charset="utf-8"></script>
<script src="https://cdn.datatables.net/buttons/1.5.4/js/buttons.bootstrap4.min.js" charset="utf-8"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js" charset="utf-8"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js" charset="utf-8"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js" charset="utf-8"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js" charset="utf-8"></script>
<script src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js" charset="utf-8"></script>
<script src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap4.min.js" charset="utf-8"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.2/photoswipe-ui-default.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.2/photoswipe.js"></script>
<!--  -->
<script src="{{asset('js/photoswipe.js')}}" charset="utf-8"></script>
<script src="{{asset('js/overview.js')}}" charset="utf-8"></script>
<script src="{{asset('js/search.js')}}" charset="utf-8"></script>
<script type="text/javascript">
// data Tables
$('#dvData').DataTable( {
    colReorder: true,
   autoFill: true,
    responsive: true,
    select: true,
    buttons: [
         'excel','copy', 'pdf'
    ],
    dom: 'Bfrtip',
} );
</script>
</body>
</html>
