@extends('layouts.master')

@section('activeB','Close Case')

@section('mainContent')
	<div class="page-content">
		<div class="ace-settings-container" id="ace-settings-container">
			<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
				<i class="ace-icon fa fa-cog bigger-130"></i>
			</div>
			<div class="ace-settings-box clearfix" id="ace-settings-box">
				<div class="pull-left width-50">
				</div><!-- /.pull-left -->
				<div class="pull-left width-50">
				</div><!-- /.pull-left -->
			</div><!-- /.ace-settings-box -->
		</div><!-- /.ace-settings-container -->
		<div class="page-header">
			<h1>Cases<small><i class="ace-icon fa fa-angle-double-right"></i></small></h1>
		</div><!-- /.page-header -->
		<!-- the form -->
		<div class="row">
			<!-- PAGE CONTENT BEGINS -->
			@include('notifications')
			<form class="form-horizontal" role="form" action="/caseClose?id={{$callDetails[0]->id}}" method="POST">
				{!! csrf_field() !!}
				<div class="form-group row">
					<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1">Client Name:</label>
					<div class="col-sm-3">
						<input type="text" id="form-field-1-1" placeholder="Username" value="{{$callDetails[0]->client->name}}" disabled />
					</div>
				</div>

				<div class="form-group row">
					<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1">Age:</label>

					<div class="col-sm-3">
						<input type="text" id="form-field-1-1" placeholder="Text Field" value="{{$callDetails[0]->client->age}}"  class="form-control" disabled />
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1">District:</label>

					<div class="col-sm-3">
						<input type="t" id="form-field-1-1" name="partner" value="{{$callDetails[0]->district->name }}"  class="form-control" disabled/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1">Date case captured:</label>

					<div class="col-sm-3">
						<input type="t" name="case_date" value="{{$callDetails[0]->date }}" class="form-control" disabled/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1">Partner:</label>

					<div class="col-sm-3">
						<input type="t" id="form-field-1-1" name="partner" value="{{$callDetails[0]->partner->name }}"  class="form-control" disabled/>
					</div>
				</div>
				@if($callDetails[0]->partner_id === 0)
					<div class="form-group row">
						<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1" data-toggle="tooltip" title=" make sure you select a partner here">Select Partner</label>

						<div class="col-sm-6">

							@foreach($partners as $partner)
								@if($partner->id !=0)
									<input type="radio"  name="partner" value="{{$partner->id}}"  style="color:#000;font-size:15px;" /> <b> {{$partner->name}}</b><br>
								@else

								@endif
							@endforeach
						</div>
					</div>
				@endif
				<div class="row">
					<div class="col-xs-12 col-sm-4">
						<div class="widget-box">
							<div class="widget-header">
								<h4 class="widget-title" style="color:#000; font-family: times new romans; font-size:16px">Issues</h4>
								<div class="widget-toolbar">
									<a href="#" data-action="collapse">
										<i class="ace-icon fa fa-chevron-up"></i>
									</a>
								</div>
							</div>
							<div class="widget-body">
								<div class="widget-main">
									<div>
										@foreach($callDetails[0]->subissues as $subissue)

											<div class="ace-settings-item">
												<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" checked  disabled />
												<label class="lbl" for="ace-settings-compact" style="color:#000; font-family: times new romans; font-size:16px">{{$subissue->issue->name}} - {{$subissue->name}}</label>
											</div>
										@endforeach
									</div>
								</div>
							</div>
						</div>
					</div><!-- /.span -->

					<div class="col-xs-12 col-sm-4">
						<div class="widget-box">
							<div class="widget-header">
								<h4 class="widget-title" style="color:#000; font-family: times new romans; font-size:16px">Intervention</h4>

								<span class="widget-toolbar">
									<a href="#" data-action="settings">

										<a href="#" data-action="collapse">
											<i class="ace-icon fa fa-chevron-up"></i>
										</a>
									</span>
								</div>

								<div class="widget-body">
									<div class="widget-main">
										<div>
											@foreach($callDetails[0]->interventions as $intervention)
												<div class="ace-settings-item">
													<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" checked  disabled />
													<label class="lbl" for="ace-settings-compact" style="color:#000; font-family: times new romans; font-size:16px"> {{$intervention->name}} </label>
												</div>
											@endforeach
										</div>
									</div>
								</div>
							</div>
						</div><!-- /.span -->

						<div class="col-xs-12 col-sm-4">
							<div class="widget-box">
								<div class="widget-header">
									<h4 class="widget-title" style="color:#000; font-family: times new romans; font-size:16px">Refferal</h4>

									<span class="widget-toolbar">

										<a href="#" data-action="collapse">
											<i class="ace-icon fa fa-chevron-up"></i>
										</a>
									</span>
								</div>

								<div class="widget-body">
									<div class="widget-main">
										@foreach($callDetails[0]->referrals as $referral)
											<div class="ace-settings-item">
												<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" checked  disabled />
												<label class="lbl" for="ace-settings-compact" style="color:#000; font-family: times new romans; font-size:16px"> {{$referral->name}} </label>
											</div>
										@endforeach
									</div>
								</div>
							</div>
						</div><!-- /.span -->
					</div><!-- /.row -->
					<div class="space-24"></div>
					<hr />
					<div class="row">
						<div class="col-sm-9 col-sm-offset-1" >
							<div class="widget-box">
								<div class="widget-header">
									<h4 class="widget-title">Case Summary</h4>
									<div class="widget-toolbar">
										<a href="#" data-action="collapse">
											<i class="ace-icon fa fa-chevron-up"></i>
										</a>
									</div>
								</div>
								<div class="widget-body">
									<div class="widget-main">
										<div class="form-group">
											<div class="col-xs-12">
												<input type="hidden" name="case_comments" value="{{$callDetails[0]->comments}}">
												<textarea name="closure_comments" class="col-sm-12" style="color:#000; font-family: times new romans; font-size:16px;height:150px"> {{$callDetails[0]->comments}}</textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr />
						<div class="row">
							<div class="col-sm-9 col-sm-offset-1">
								<div class="widget-box">
									<div class="widget-header">
										<h4 class="widget-title"><b >Action Taken <span style="color:red">**</span></b> </h4>
										<span class="widget-toolbar">
											<a href="#" data-action="collapse">
												<i class="ace-icon fa fa-chevron-up"></i>
											</a>
										</span>
									</div>

									<div class="widget-body">
										<div class="widget-main">
											<div class="row">
												<div class="">
													<div class="col-xs-12">
														<textarea class="col-sm-12" style="color:#000; font-family: times new romans; font-size:16px;height:150px" id="" name="action_taken" type="text"></textarea>
													</div>
												</div>
											</div>

											<hr />
										</div>
									</div>
									<div class="space space-8">  </div>
								</div>
								<div class="widget-box">
									<div class="widget-header">
										<h4 class="widget-title"><b >Outcome <span style="color:red">**</span></b></h4>

										<span class="widget-toolbar">
											<a href="#" data-action="collapse">
												<i class="ace-icon fa fa-chevron-up"></i>
											</a>
										</span>
									</div>
									<div class="widget-body">
										<div class="widget-main">
											<div class="row">
												<div class="">
													<div class="col-xs-12">
														<textarea class="col-sm-12" style="color:#000; font-family: times new romans; font-size:16px; height:150px" id="" name="outcome" type="text" ></textarea>
													</div>
												</div>
											</div>
											<hr />
										</div>
									</div>
									<div class="space space-8">  </div>
								</div>
								<input name="status" value="{{$callDetails[0]->status}}" type="hidden">
								<button type="submit" class="btn btn-primary">Close case</button>
						</div>

						<div class="hr hr-18 dotted hr-double"></div>
					</div>
				</div>
			</div><!-- PAGE CONTENT ENDS -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->
@endsection
