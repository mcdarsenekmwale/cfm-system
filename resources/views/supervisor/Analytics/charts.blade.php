@extends('supervisor.master.master')

@section('content')
<div class="wrapper">
          <!-- .page -->
          <div class="page">
            <!-- .page-inner -->
            <div class="page-inner">
              <!-- .page-title-bar -->
              <header class="page-title-bar">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                      <a href="#!"><i class="breadcrumb-icon fa fa-angle-left mr-2"></i>Collections</a>
                    </li>
                  </ol>
                </nav>
                <h1 class="page-title mr-sm-auto"> Analytics </h1>
                <p class="text-muted"> </p>
              </header><!-- /.page-title-bar -->
              <!-- .page-section -->
              <div class="page-section">
                <!-- grid row -->
                <div class="row">

                  <div class="col-lg-8">
                    <!-- .card -->
                    <section class="card card-fluid">
                      <!-- .card-body -->
                      <div class="card-body">
                        <h3 class="card-title"> User Distribution </h3>
                      </div><!-- /.card-body -->
                      <div class="" id="userchart" style="width: 100%;height: 400px;"></div>
                    </section><!-- /.card -->
                  </div><!-- /grid column -->
                  <div class="col-lg-4">
                    <!-- .card -->
                    <section class="card card-fluid">
                      <!-- .card-body -->
                      <div class="card-body">
                        <h3 class="card-title"> User Distribution Stats </h3>
                      </div><!-- /.card-body -->
                      <div class="table-responsive mb-0" data-toggle="lists" style="height: 400px;" data-lists-values="[&quot;goal-project&quot;, &quot;goal-status&quot;, &quot;goal-progress&quot;, &quot;goal-date&quot;]">
                        <table class="table table-sm table-nowrap card-table table-hover" >
                          <thead class="thead">
                            <tr>
                              <th>
                                <a href="#" class="text-muted sort" data-sort="goal-project">
                                  User Group
                                </a>
                              </th>
                              <th>
                                <a href="#" class="text-muted sort" data-sort="goal-date">
                                  Last Update
                                </a>
                              </th>
                              <th class="text-right ">
                                Value
                              </th>

                            </tr>
                          </thead>
                          <tbody class="lists">
                            @foreach($userdistribution as $userdistr)
                            @if($userdistr->user_count > 0)

                            <tr class="hoverable">
                              <td class="goal-project text-left">
                                <script type="">
                                var string = "{{$userdistr->type}}";
                                var res;
                                if (string == 'common') {
                                   res =  'Counsellor';
                                }
                                else {
                                   res = string;
                                }
                                document.write('<b class="text-capitalize">' + res +'</b>');
                                </script>

                              </td>

                              <td class="goal-date text-left">
                                <time datetime="2018-10-24">{{$userdistr->date}}</time>
                              </td>
                              <td class="text-center">
                                <b>{{$userdistr->user_count}}</b>
                              </td>

                            </tr>
                            @endif
                            @endforeach

                          </tbody>
                        </table>
                      </div>

                    </section><!-- /.card -->
                  </div><!-- /grid column -->
                  <!-- grid column -->
                  <div class="col-lg-6">
                    <!-- .card -->
                    <section class="card card-fluid">
                      <!-- .card-body -->
                      <div class="card-body">
                        <h3 class="card-title text-center"> Line Chart </h3>
                        <div id="flot-line" class="flot" style="padding: 0px; position: relative;"><canvas class="flot-base" width="826" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 660.8px; height: 200px;"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px;"><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 82px; top: 183px; left: 22px; text-align: center;">0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 82px; top: 183px; left: 115px; text-align: center;">2</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 82px; top: 183px; left: 208px; text-align: center;">4</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 82px; top: 183px; left: 301px; text-align: center;">6</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 82px; top: 183px; left: 394px; text-align: center;">8</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 82px; top: 183px; left: 484px; text-align: center;">10</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 82px; top: 183px; left: 578px; text-align: center;">12</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px;"><div class="flot-tick-label tickLabel" style="position: absolute; top: 156px; left: 2px; text-align: right;">-1.0</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 120px; left: 0px; text-align: right;">-0.5</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 85px; left: 4px; text-align: right;">0.0</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 50px; left: 5px; text-align: right;">0.5</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 15px; left: 6px; text-align: right;">1.0</div></div></div><canvas class="flot-overlay" width="826" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 660.8px; height: 200px;"></canvas><div class="legend"><div style="position: absolute; width: 61.6px; height: 36.8px; bottom: 27px; right: 13px; background-color: rgb(255, 255, 255); opacity: 0.85;"> </div><table style="position:absolute;bottom:27px;right:13px;;font-size:smaller;color:#888c9b"><tbody><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgb(95,75,139);overflow:hidden"></div></div></td><td class="legendLabel">sin(x)</td></tr><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgb(0,162,138);overflow:hidden"></div></div></td><td class="legendLabel">cos(x)</td></tr></tbody></table></div></div>
                      </div><!-- /.card-body -->
                    </section><!-- /.card -->
                  </div><!-- /grid column -->
                  <!-- grid column -->
                  <div class="col-lg-6">
                    <!-- .card -->
                    <section class="card card-fluid">
                      <!-- .card-body -->
                      <div class="card-body">
                        <h3 class="card-title text-center"> Bar Chart </h3>
                        <div id="flot-bar" class="flot" style="padding: 0px; position: relative;"><canvas class="flot-base" width="826" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 660.8px; height: 200px;"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px;"><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 110px; top: 183px; left: 32px; text-align: center;">January</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 110px; top: 183px; left: 140px; text-align: center;">February</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 110px; top: 183px; left: 259px; text-align: center;">March</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 110px; top: 183px; left: 374px; text-align: center;">April</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 110px; top: 183px; left: 487px; text-align: center;">May</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 110px; top: 183px; left: 597px; text-align: center;">June</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px;"><div class="flot-tick-label tickLabel" style="position: absolute; top: 170px; left: 6px; text-align: right;">0</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 127px; left: 6px; text-align: right;">5</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 85px; left: 1px; text-align: right;">10</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 43px; left: 1px; text-align: right;">15</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 1px; left: 0px; text-align: right;">20</div></div></div><canvas class="flot-overlay" width="826" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 660.8px; height: 200px;"></canvas></div>
                      </div><!-- /.card-body -->
                    </section><!-- /.card -->
                  </div><!-- /grid column -->
                  <!-- grid column -->
                  <div class="col-lg-6">
                    <!-- .card -->
                    <section class="card card-fluid">
                      <!-- .card-body -->
                      <div class="card-body">
                        <h3 class="card-title text-center"> Area Chart </h3>
                        <div id="flot-area" class="flot" style="padding: 0px; position: relative;"><canvas class="flot-base" width="826" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 660.8px; height: 200px;"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px;"><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 55px; top: 161px; left: 103px; text-align: center;">1820</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 55px; top: 161px; left: 159px; text-align: center;">1840</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 55px; top: 161px; left: 215px; text-align: center;">1860</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 55px; top: 161px; left: 271px; text-align: center;">1880</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 55px; top: 161px; left: 327px; text-align: center;">1900</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 55px; top: 161px; left: 384px; text-align: center;">1920</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 55px; top: 161px; left: 439px; text-align: center;">1940</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 55px; top: 161px; left: 495px; text-align: center;">1960</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 55px; top: 161px; left: 551px; text-align: center;">1980</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 55px; top: 161px; left: 607px; text-align: center;">2000</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px;"><div class="flot-tick-label tickLabel" style="position: absolute; top: 148px; left: 47px; text-align: right;">0</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 118px; left: 16px; text-align: right;">100000</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 89px; left: 15px; text-align: right;">200000</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 59px; left: 15px; text-align: right;">300000</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 30px; left: 15px; text-align: right;">400000</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 1px; left: 15px; text-align: right;">500000</div></div></div><canvas class="flot-overlay" width="826" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 660.8px; height: 200px;"></canvas><div class="legend"><div style="position: absolute; width: 225.6px; height: 18.4px; top: 14px; left: 64px; background-color: rgb(255, 255, 255); opacity: 0.85;"> </div><table style="position:absolute;top:14px;left:64px;;font-size:smaller;color:#888c9b"><tbody><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid #5F4B8B;overflow:hidden"></div></div></td><td class="legendLabel">Asia</td><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid #00A28A;overflow:hidden"></div></div></td><td class="legendLabel">Europe</td><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid #346CB0;overflow:hidden"></div></div></td><td class="legendLabel">North America</td></tr></tbody></table></div></div>
                      </div><!-- /.card-body -->
                    </section><!-- /.card -->
                  </div><!-- /grid column -->
                  <!-- grid column -->
                  <div class="col-lg-6">
                    <!-- .card -->
                    <section class="card card-fluid">
                      <!-- .card-body -->
                      <div class="card-body">
                        <h3 class="card-title text-center"> Horizontal Bar Chart </h3>
                        <div id="flot-barhor" class="flot" style="padding: 0px; position: relative;"><canvas class="flot-base" width="826" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 660.8px; height: 200px;"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px;"><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 73px; top: 161px; left: 76px; text-align: center;">0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 73px; top: 161px; left: 141px; text-align: center;">250</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 73px; top: 161px; left: 212px; text-align: center;">500</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 73px; top: 161px; left: 284px; text-align: center;">750</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 73px; top: 161px; left: 351px; text-align: center;">1000</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 73px; top: 161px; left: 423px; text-align: center;">1250</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 73px; top: 161px; left: 494px; text-align: center;">1500</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 73px; top: 161px; left: 565px; text-align: center;">1750</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 73px; top: 161px; left: 635px; text-align: center;">2000</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px;"><div class="flot-tick-label tickLabel" style="position: absolute; top: 139px; left: 49px; text-align: right;">Gold</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 118px; left: 44px; text-align: right;">Silver</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 96px; left: 26px; text-align: right;">Platinum</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 74px; left: 27px; text-align: right;">Palldium</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 52px; left: 25px; text-align: right;">Rhodium</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 31px; left: 15px; text-align: right;">Ruthenium</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 9px; left: 35px; text-align: right;">Iridium</div></div></div><canvas class="flot-overlay" width="826" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 660.8px; height: 200px;"></canvas><div class="legend"><div style="position: absolute; width: 136.8px; height: 18.4px; top: 14px; right: 18px; background-color: rgb(255, 255, 255); opacity: 0.85;"> </div><table style="position:absolute;top:14px;right:18px;;font-size:smaller;color:#888c9b"><tbody><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid #00A28A;overflow:hidden"></div></div></td><td class="legendLabel">Precious Metal Price</td></tr></tbody></table></div></div>
                      </div><!-- /.card-body -->
                    </section><!-- /.card -->
                  </div><!-- /grid column -->
                  <!-- grid column -->
                  <div class="col-lg-12">
                    <!-- .card -->
                    <section class="card card-fluid">
                      <!-- .card-body -->
                      <div class="card-body">
                        <h3 class="card-title text-center"> FUN TEMPERATURE CHART </h3>
                            <div id="chartdiv" style="width: 100%;height: 700px;"></div>
                      </div><!-- /.card-body -->
                    </section><!-- /.card -->
                  </div><!-- /grid column -->
                  <!-- grid column -->
                </div><!-- /grid row -->
              </div><!-- /.page-section -->
            </div><!-- /.page-inner -->
            <br><br>
          </div><!-- /.page -->
        </div>
@endsection
