@extends('supervisor.master.master')

@section('content')

<!-- .wrapper -->
<div class="wrapper">
  <!-- .page -->
  <div class="page">
    <!-- .page-inner -->
    <div class="page-inner">
      <!-- .page-title-bar -->
      @include('notifications')
      <header class="page-title-bar">
        <p class="lead">
          <span class="font-weight-bold " style="font-size:15px;">Welcome <span class="text-capitalize">{{Auth::user()->firstname}} {{Auth::user()->lastname}}.</span> </span> <span class="float-right small d-block text-muted">If you need help feel free to contact McDarsene.</span>
        </p>
      </header><!-- /.page-title-bar -->
      <!-- .page-section -->
      <div class="page-section">
        <!-- .section-block -->
        <div class="section-block" >
          <!-- metric row -->
          <div class="col-12">
            <div class="row">
                  <div class="col-12 col-lg-6 col-xl">
                    <!-- Card -->
                    <div class="card hoverable" onclick="javascript: window.location.href='{{route('supervisor.allcases')}}'">
                      <div class="card-body">
                        <div class="row align-items-center">
                          <div class="col">
                            <!-- Title -->
                            <h6 class="card-title text-uppercase text-muted mb-2">
                              Total cases
                            </h6>
                            <!-- Heading -->
                            <span class="h2 mb-0">
                              {{$totalCases}}
                            </span>

                            <!-- Badge -->
                            <span class="badge badge-soft-success mt--1">
                              +3.5%
                            </span>

                          </div>
                          <div class="col-auto">

                            <!-- Icon -->
                            <span class="h2 fe fe-dollar-sign text-muted mb-0"></span>

                          </div>
                        </div> <!-- / .row -->

                      </div>
                    </div>

                  </div>
                  <div class="col-12 col-lg-6 col-xl">

                    <!-- Card -->
                    <div class="card hoverable" onclick="javascript: window.location.href='{{route('supervisor.allcalls')}}'">
                      <div class="card-body">
                        <div class="row align-items-center" >
                          <div class="col">

                            <!-- Title -->
                            <h6 class="card-title text-uppercase text-muted mb-2">
                              Total Calls
                            </h6>

                            <!-- Heading -->
                            <span class="h2 mb-0">
                              {{$totalCalls}}
                            </span>

                          </div>
                          <div class="col-auto">

                            <!-- Icon -->
                            <span class="h2 fe fe-briefcase text-muted mb-0"></span>

                          </div>
                        </div> <!-- / .row -->

                      </div>
                    </div>

                  </div>
                  <div class="col-12 col-lg-6 col-xl">

                    <!-- Card -->
                    <div class="card hoverable" onclick="javascript: window.location.href='/register/case'">
                      <div class="card-body">
                        <div class="row align-items-center">
                          <div class="col">

                            <!-- Title -->
                            <h6 class="card-title text-uppercase text-muted mb-2">
                              REGISTER
                            </h6>

                            <!-- Heading -->
                            <span class="h2 mb-0">
                              NEW CASE
                            </span>

                          </div>
                          <div class="col-auto  text-success">
                            <!-- Icon -->

                            <span class="fa-2x fa fa-folder   mb-1"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-lg-6 col-xl">

                    <!-- Card -->
                    <div class="card hoverable" onclick="javascript: window.location.href='/register/call'">
                      <div class="card-body">
                        <div class="row align-items-center">
                          <div class="col">

                            <!-- Title -->
                            <h6 class="card-title text-uppercase text-muted mb-2">
                              REGISTER
                            </h6>

                            <!-- Heading -->
                            <span class="h2 mb-0">
                              NEW CALL
                            </span>

                          </div>
                          <div class="col-auto  text-success">
                            <!-- Icon -->
                            <span class="fa-2x fa fa-phone    mb-1"></span>
                            <span class="fa-2x fa fa-phone   mb-1"></span>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>

            </div>
          </div>
        </div><!-- /.section-block -->
        <!-- grid row -->
        <div class="row">
          <!-- grid column -->
          <div class="col-12 col-lg-12 col-xl-4">
            <!-- .card -->
            <section class="card card-fluid">
              <!-- .card-body -->
              <div class="card-body">
                <!-- .d-flex -->
                <div class="d-flex align-items-center mb-4">
                  <h3 class="card-title mb-0 text-uppercase"> Case Trending </h3><!-- .card-title-control -->
                  <div class="card-title-control ml-auto">
                    <!-- .dropdown -->
                    <div class="dropdown">
                      <button class="btn btn-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="navDropdown"><span>This Week</span> <i class="fa fa-fw fa-caret-down"></i></button>

                      <div class="dropdown-arrow dropdown-arrow-right"></div><!-- .dropdown-menu -->
                      <div class="dropdown-menu dropdown-menu-right dropdown-menu-md stop-propagation"  aria-labelledby="navDropdown" style="position: absolute; will-change: top, left; top: 36px; left: 110px;">
                        <!-- .custom-control -->
                        <div class="custom-control custom-radio">
                          <input type="radio" class="custom-control-input" id="dpToday" name="dpFilter" value="0"> <label class="custom-control-label d-flex justify-content-between" for="dpToday"><span>Today</span> <span class="text-muted" id="today"></span></label>
                        </div><!-- /.custom-control -->
                        <!-- .custom-control -->
                        <div class="custom-control custom-radio">
                          <input type="radio" class="custom-control-input" id="dpYesterday" name="dpFilter" value="1"> <label class="custom-control-label d-flex justify-content-between" for="dpYesterday"><span>Yesterday</span> <span class="text-muted" id="yesterday"></span></label>
                        </div><!-- /.custom-control -->
                        <!-- .custom-control -->
                        <div class="custom-control custom-radio">
                          <input type="radio" class="custom-control-input" id="dpWeek" name="dpFilter" value="2" checked=""> <label class="custom-control-label d-flex justify-content-between" for="dpWeek"><span>This Week</span> <span class="text-muted" id="week"></span></label>
                        </div><!-- /.custom-control -->
                        <!-- .custom-control -->
                        <div class="custom-control custom-radio">
                          <input type="radio" class="custom-control-input" id="dpMonth" name="dpFilter" value="3"> <label class="custom-control-label d-flex justify-content-between" for="dpMonth"><span>This Month</span> <span class="text-muted" id="month"></span></label>
                        </div><!-- /.custom-control -->
                        <!-- .custom-control -->
                        <div class="custom-control custom-radio">
                          <input type="radio" class="custom-control-input" id="dpYear" name="dpFilter" value="4"> <label class="custom-control-label d-flex justify-content-between" for="dpYear"><span>This Year</span> <span class="text-muted"><?php date_default_timezone_set('Africa/Blantyre'); echo date("Y")?></span></label>
                        </div><!-- /.custom-control -->
                        <!-- .custom-control -->
                        <div class="custom-control custom-radio">
                          <input type="radio" class="custom-control-input" id="dpCustom" name="dpFilter" value="5"> <label class="custom-control-label" for="dpCustom">Custom</label>
                          <div class="custom-control-hint my-1" for="dpCustom">
                            <!-- datepicker:range -->
                            <input type="text" class="form-control flatpickr-input" data-toggle="flatpickr" data-mode="range" data-disable-mobile="true" data-date-format="Y-m-d" readonly="readonly"> <!-- /datepicker:range -->
                          </div>
                        </div><!-- /.custom-control -->
                      </div><!-- /.dropdown-menu -->
                    </div><!-- /.dropdown -->
                  </div><!-- /.card-title-control -->
                </div><!-- /.d-flex -->
                <div class="chartjs" style="height: 291px"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                  <canvas id="completion-tasks" width="826" height="363" class="chartjs-render-monitor" style="display: block; height: 291px; width: 661px;"></canvas>
                </div>
              </div><!-- /.card-body -->
            </section><!-- /.card -->
          </div><!-- /grid column -->
          <!-- grid column -->
          <div class="col-12 col-lg-6 col-xl-4">
            <!-- .card -->
            <section class="card card-fluid">
              <!-- .card-body -->
              <div class="card-body">
                <h3 class="card-title text-uppercase"> Tasks Performance </h3><!-- easy-pie-chart -->
                <div class="text-center pt-4" >
                  <div class="chart chart-inline-group" style="height:214px; " >
                    <div class="easypiechart" data-toggle="easypiechart" data-percent="60" data-size="214" data-bar-color="#346CB0" data-track-color="false"
                    data-scale-color="false" data-rotate="225" style="position:relative; ">
                    </div>
                  </div>
                </div><!-- /easy-pie-chart -->
              </div><!-- /.card-body -->
              <!-- .card-footer -->
              <div class="card-footer">
                <div class="card-footer-item">
                  <i class="fa fa-fw fa-circle text-indigo"></i> 100% <div class="text-muted small">  Pending</div>
                </div>
                <div class="card-footer-item">
                  <i class="fa fa-fw fa-circle text-purple"></i> 75% <div class="text-muted small"> Unresolved </div>
                </div>
                <div class="card-footer-item">
                  <i class="fa fa-fw fa-circle text-teal"></i> 60% <div class="text-muted small"> Resolved </div>
                </div>
              </div><!-- /.card-footer -->
            </section><!-- /.card -->
          </div><!-- /grid column -->
          <!-- grid column -->
          <div class="col-12 col-lg-6 col-xl-4">
            <!-- .card -->
            <section class="card card-fluid">
              <!-- .card-body -->
              <div class="card-body pb-0">
                <h3 class="card-title text-uppercase">Counsellor Leaderboard </h3><!-- legend -->
                <ul class="list-inline small">
                  <li class="list-inline-item">
                    <i class="fa fa-fw fa-phone text-success"></i><b>Calls</b>  </li>
                  <li class="list-inline-item">
                      <i class="fa fa-fw fa-circle pulse text-red"></i> Incomplete </li>
                  <li class="list-inline-item">
                    <i class="fa fa-fw fa-circle pulse  text-teal"></i> Completed </li>


                </ul><!-- /legend -->
              </div><!-- /.card-body -->
              <!-- .list-group -->
              <div class="list-group list-group-flush">
                <!-- .list-group-item -->
                <div class="list-group-item">
                  <!-- .list-group-item-figure -->
                  <div class="list-group-item-figure">
                    <a href="http://uselooper.com/user-profile.html" class="user-avatar" data-toggle="tooltip" title="" data-original-title="Martha Myers"><img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt=""></a>
                  </div><!-- /.list-group-item-figure -->
                  <!-- .list-group-item-body -->
                  <div class="list-group-item-body">
                    <!-- .progress -->
                    <div class="progress progress-animated bg-transparent rounded-0" data-toggle="tooltip" data-html="true" title=""
                    data-original-title="<div class=&quot;text-left small&quot;>
                      <i class=&quot;fa fa-fw fa-circle text-teal&quot;>
                      </i> 231<br><i class=&quot;fa fa-fw fa-circle text-red&quot;></i> 54
                    </div>">
                      <div class="progress-bar bg-red" role="progressbar" aria-valuenow="73.46140163642832" aria-valuemin="0" aria-valuemax="100" style="width: 73.46140163642832%">
                        <span class="sr-only">73.46140163642832% Complete</span>
                      </div>
                      <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="8.217716115261473" aria-valuemin="0" aria-valuemax="100" style="width: 8.217716115261473%">
                        <span class="sr-only">10.217716115261473% Complete</span>
                      </div>
                    </div><!-- /.progress -->
                  </div><!-- /.list-group-item-body -->
                </div><!-- /.list-group-item -->
                <!-- .list-group-item -->
                <div class="list-group-item">
                  <!-- .list-group-item-figure -->
                  <div class="list-group-item-figure">
                    <a href="http://uselooper.com/user-profile.html" class="user-avatar" data-toggle="tooltip" title="" data-original-title="Tammy Beck"><img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt=""></a>
                  </div><!-- /.list-group-item-figure -->
                  <!-- .list-group-item-body -->
                  <div class="list-group-item-body">
                    <!-- .progress -->
                    <div class="progress progress-animated bg-transparent rounded-0" data-toggle="tooltip" data-html="true" title="" data-original-title="<div class=&quot;text-left small&quot;><i class=&quot;fa fa-fw fa-circle text-teal&quot;></i>
                       406<br><i class=&quot;fa fa-fw fa-circle text-red&quot;></i> 49</div>">
                      <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="54.180855088914115" aria-valuemin="0" aria-valuemax="100" style="width: 54.180855088914115%">
                        <span class="sr-only">54.180855088914115% Complete</span>
                      </div>
                      <div class="progress-bar bg-red" role="progressbar" aria-valuenow="17.361331819901627" aria-valuemin="0" aria-valuemax="100" style="width: 15.361331819901627%">
                        <span class="sr-only">17.361331819901627% Complete</span>
                      </div>

                    </div><!-- /.progress -->
                  </div><!-- /.list-group-item-body -->
                </div><!-- /.list-group-item -->
                <!-- .list-group-item -->
                <div class="list-group-item">
                  <!-- .list-group-item-figure -->
                  <div class="list-group-item-figure">
                    <a href="http://uselooper.com/user-profile.html" class="user-avatar" data-toggle="tooltip" title="" data-original-title="Susan Kelley"><img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt=""></a>
                  </div><!-- /.list-group-item-figure -->
                  <!-- .list-group-item-body -->
                  <div class="list-group-item-body">
                    <!-- .progress -->
                    <div class="progress progress-animated bg-transparent rounded-0" data-toggle="tooltip" data-html="true" title="" data-original-title="<div class=&quot;text-left small&quot;><i class=&quot;fa fa-fw fa-circle text-teal&quot;></i> 1271<br><i class=&quot;fa fa-fw fa-circle text-red&quot;></i> 163</div>">
                      <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="52.13289581624282" aria-valuemin="0" aria-valuemax="100" style="width: 52.13289581624282%">
                        <span class="sr-only">52.13289581624282% Complete</span>
                      </div>
                      <div class="progress-bar bg-red" role="progressbar" aria-valuenow="7.568498769483183" aria-valuemin="0" aria-valuemax="100" style="width: 3.568498769483183%">
                        <span class="sr-only">7.568498769483183% Complete</span>
                      </div>

                    </div><!-- /.progress -->
                  </div><!-- /.list-group-item-body -->
                </div><!-- /.list-group-item -->
                <!-- .list-group-item -->
                <div class="list-group-item">
                  <!-- .list-group-item-figure -->
                  <div class="list-group-item-figure">
                    <a href="http://uselooper.com/user-profile.html" class="user-avatar" data-toggle="tooltip" title="" data-original-title="Albert Newman"><img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt=""></a>
                  </div><!-- /.list-group-item-figure -->
                  <!-- .list-group-item-body -->
                  <div class="list-group-item-body">
                    <!-- .progress -->
                    <div class="progress progress-animated bg-transparent rounded-0" data-toggle="tooltip" data-html="true" title="" data-original-title="<div class=&quot;text-left small&quot;><i class=&quot;fa fa-fw fa-circle text-teal&quot;></i> 1527<br><i class=&quot;fa fa-fw fa-circle text-red&quot;></i> 356</div>">
                      <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="75.18463810930577" aria-valuemin="0" aria-valuemax="100" style="width: 75.18463810930577%">
                        <span class="sr-only">75.18463810930577% Complete</span>
                      </div>
                      <div class="progress-bar bg-red" role="progressbar" aria-valuenow="17.593549975381585" aria-valuemin="0" aria-valuemax="100" style="width: 10.093549975381585%">
                        <span class="sr-only">17.593549975381585% Complete</span>
                      </div>
                    </div><!-- /.progress -->
                  </div><!-- /.list-group-item-body -->
                </div><!-- /.list-group-item -->
                <!-- .list-group-item -->
                <div class="list-group-item">
                  <!-- .list-group-item-figure -->
                  <div class="list-group-item-figure">
                    <a href="http://uselooper.com/user-profile.html" class="user-avatar" data-toggle="tooltip" title="" data-original-title="Kyle Grant"><img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt=""></a>
                  </div><!-- /.list-group-item-figure -->
                  <!-- .list-group-item-body -->
                  <div class="list-group-item-body">
                    <!-- .progress -->
                    <div class="progress progress-animated bg-transparent rounded-0" data-toggle="tooltip" data-html="true" title="" data-original-title="<div class=&quot;text-left small&quot;><i class=&quot;fa fa-fw fa-circle text-teal&quot;></i> 643<br><i class=&quot;fa fa-fw fa-circle text-red&quot;></i> 347</div>">
                      <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="36.89041881812966" aria-valuemin="0" aria-valuemax="100" style="width: 36.89041881812966%">
                        <span class="sr-only">36.89041881812966% Complete</span>
                      </div>
                      <div class="progress-bar bg-red" role="progressbar" aria-valuenow="22.203671830177854" aria-valuemin="0" aria-valuemax="100" style="width: 15.203671830177854%">
                        <span class="sr-only">22.503671830177854% Complete</span>
                      </div>
                    </div><!-- /.progress -->
                  </div><!-- /.list-group-item-body -->
                </div><!-- /.list-group-item -->
              </div><!-- /.list-group -->
            </section><!-- /.card -->
          </div><!-- /grid column -->

        </div><!-- /grid row -->
        <!-- section-deck -->
        <div class="section-deck">
          <!-- .card -->
          <div class="card card-fluid pb-3">
            <header class="card-header text-uppercase text-muted"> Partner Case Distribution</header><!-- .lits-group -->
            <div class="lits-group list-group-flush">

              <div class="table-responsive mb-0" data-toggle="lists" data-lists-values="[&quot;goal-project&quot;, &quot;goal-status&quot;, &quot;goal-progress&quot;, &quot;goal-date&quot;]">
  							<table class="table table-sm table-nowrap card-table table-hover" >
  								<thead class="thead">
  									<tr>
                      <th >
                        <a href="#" class="text-muted sort smaller" data-sort="goal-partner">
                        PARTNER
                      </a>
                    </th>
                      <th class="text-center">
                        <a href="#" class="text-muted sort smaller" data-sort="goal-project">
                          Unreferred
                        </a>
                      </th>
                      <th class="text-center">
                        <a href="#" class="text-muted sort smaller" data-sort="goal-status">
                          Referred
                        </a>
                      </th>

                      <th class="text-center">
                        <a href="#" class="text-muted sort smaller" data-sort="goal-date">
                          Resolved
                        </a>
                      </th>
  									</tr>
  								</thead>
  								<tbody class="lists">
                    @if($data['issues']->total() == 0)
                    <tr class="card-body" style="height:10rem; width:56rem;"></tr>
                    <tr class="alert  align-items-center" role="alert " >
                      <td class="alert-text text-center " colspan="3" >
                        No recent partner with a case data yet
                        <b>{{Auth::user()->username}}  </b>
                      </td>
                    </tr>
                    <tr class="card-body" style="height:10.7rem; width:56rem;"></tr>
                    @else
                    @foreach($issues as $issue)
                    <tr class="hoverable" >
                        <td class="align-middle smaller"> <b>{{$issue->name}}</b> </td>
                        <td class="align-middle text-center"> {{$issue->unref}} </td>
                        <td class="align-middle text-center"> {{$issue->ref}} </td>
                        <td class="align-middle text-center"> {{$issue->res}} </td>
                    </tr>
                    @endforeach

                    @endif

  								</tbody>
  							</table>
  						</div>
                <div class="card-body">
                  <div class="row">
                    <b class="col-10"></b>
                    <b class="col-2 float-right" >
                      <a class="text-muted " href="{!! $data['issues']->url($data['issues']->currentPage()-1)!!}" aria-label="Previous">
                        <span class="fa fa-chevron-circle-left fa-lg"></span>
                        <span class="sr-only">Previous</span>
                      </a>

                      <a class="text-muted " href="{!! $data['issues']->url($data['issues']->currentPage()+1)!!}" aria-label="Next">
                        <span class="fa fa-chevron-circle-right fa-lg"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </b>
                  </div>

                </div>
                <footer class="card-footer">
                  <a href="javascript: viewAll();" id="viewAll"  class="card-footer-item text-muted"><b>View all</b> <i class="fa fa-fw fa-angle-right"></i></a>
                </footer><!-- /.card-footer -->
              </section><!-- /.card -->

            </div><!-- /.lits-group -->
            <!-- .card-footer -->

          </div><!-- /.card -->
          <!-- .card -->
          <div class="card card-fluid">
            <header class="card-header text-uppercase text-muted"> Partner Issues </header><!-- .card-body -->
              <!-- .todo-list -->
              <div class="todo-list">
                <div class="table-responsive mb-0" data-toggle="lists" data-lists-values="[&quot;goal-project&quot;, &quot;goal-status&quot;, &quot;goal-progress&quot;, &quot;goal-date&quot;]">
                  <table class="table table-sm table-nowrap card-table table-hover" >
                    <thead class="thead">
                      <tr>
                        <th >
                          <a href="#" class="text-muted sort smaller" data-sort="goal-partner">
                          Issue Name
                        </a>
                      </th>

                        <th class="text-center">
                          <a href="#" class="text-muted sort smaller" data-sort="goal-status">
                            Value
                          </a>
                        </th>

                      </tr>
                    </thead>
                    <tbody class="lists">
                      @if($datapa['partnerIssues']->total() == 0)
                      <tr class="card-body" style="height:10rem; width:56rem;"></tr>
                      <tr class="alert  align-items-center" role="alert " >
                        <td class="alert-text text-center " colspan="3" >
                          No recent partner with a case data yet
                          <b>{{Auth::user()->username}}  </b>
                        </td>
                      </tr>
                      <tr class="card-body" style="height:10.7rem; width:56rem;"></tr>
                      @else
                      @foreach($partnerIssues as $partnerIssue)
                      <tr class="hoverable" >
                          <td class="align-middle text-left smaller">
                            <b>{{$partnerIssue->name}}</b>
                           </td>
                          <td class="align-middle text-center"> {{$partnerIssue->value}} </td>

                      </tr>
                      @endforeach

                      @endif

                    </tbody>
                  </table>
                </div>
                  <div class="card-body">
                    <div class="row">
                      <b class="col-10"></b>
                      <b class="col-2 float-right" >
                        <a class="text-muted " href="{!! $datapa['partnerIssues']->url($datapa['partnerIssues']->currentPage()-1)!!}" aria-label="Previous">
                          <span class="fa fa-chevron-circle-left fa-lg"></span>
                          <span class="sr-only">Previous</span>
                        </a>

                        <a class="text-muted " href="{!! $datapa['partnerIssues']->url($datapa['partnerIssues']->currentPage()+1)!!}" aria-label="Next">
                          <span class="fa fa-chevron-circle-right fa-lg"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </b>
                    </div>

                  </div>
              </div>

            <!-- .card-footer -->
            <footer class="card-footer">
              <a href="#!" class="card-footer-item text-muted"> <b>View all</b> <i class="fa fa-fw fa-angle-right"></i></a>
            </footer><!-- /.card-footer -->
          </div><!-- /.card -->
        </div><!-- /section-deck -->
      </div><!-- /.page-section -->
      <br><br>
    </div><!-- /.page-inner -->
  </div><!-- /.page -->
</div><!-- .app-footer -->

<!-- /.wrapper -->

@endsection
