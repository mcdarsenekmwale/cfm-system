@extends('supervisor.master.master')


@section('content')
<div class="wrapper py-5" >
  <div class="page">
    <div class="page-inner">
        <div class="" id="result">

          <div class="container py-5">
              @if(isset($users))
              <h2>Sample User details</h2>
                  <table class="table table-striped">
                      <thead>
                          <tr>
                              <th>Name</th>
                              <th>Email</th>
                          </tr>
                      </thead>
                      <tbody>
                          @foreach($users as $dummy)
                          <tr>
                              <td>{{$dummy->firstname}} {{$dummy->lastname}}</td>
                              <td>{{$dummy->email}}</td>
                          </tr>
                          @endforeach

                      </tbody>
                  </table>
                  {!! $users->render() !!}@endif
              </div>

          <div class="container">
                  <form action="/searching" method="POST" role="search">
                      {{ csrf_field() }}
                      <div class="input-group">
                          <input type="text" class="form-control" name="search"
                              placeholder="Search users"> <span class="input-group-btn">
                              <button type="submit" class="btn btn-default">
                                  <span class="fa fa-search"></span>
                              </button>
                          </span>
                      </div>
                  </form>
          </div>

        </div>
    </div>

  </div>
</div>
@endsection
