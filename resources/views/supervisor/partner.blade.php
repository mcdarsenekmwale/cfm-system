@extends('supervisor.master.master')

@section('content')

<div class="wrapper">
          <!-- .page -->
          <div class="page has-sidebar has-sidebar-fluid has-sidebar-expand-xl">
            <!-- .page-inner -->
            <div class="page-inner page-inner-fill position-relative">
              <header class="page-navs bg-light shadow-sm">
                <!-- .input-group -->
                <div class="input-group has-clearable">
                  <button type="button" class="close" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times-circle"></i></span></button> <label class="input-group-prepend" for="searchPartners"><span class="input-group-text">
                    <span class="fa fa-search"></span></span></label> <input type="text" class="form-control" id="searchPartners" data-filter=".board .list-group-item" placeholder="Find Partners">
                </div><!-- /.input-group -->
              </header>
              <button type="button" class="btn btn-primary btn-floated position-absolute" onclick="javascript:window.location.href='/forms/newpartner';" title="Add new partner"><i class="fa fa-plus"></i></button> <!-- board -->
              <div class="board p-0 perfect-scrollbar">
                <!-- .list-group -->
                <div class="list-group list-group-flush list-group-divider border-top" data-toggle="radiolist">
                  <!-- .list-group-item -->
                  <?php
                      $colors = ['blue', 'indigo', 'purple', 'pink', 'orange','red','green','yellow', 'teal'];
                      $i = 0;
                   ?>
                  @foreach($partners as $partner )
                  <?php $index = rand(0, 8); ?>
                  <a href="#!" class="list-group-item " data-toggle="sidebar" data-sidebar="show">
                    <!-- .list-group-item-figure -->
                    <div class="list-group-item-figure">
                      <div class=" text-uppercase tile tile-circle bg-{{$colors[$index]}}"> {{ substr(trim($partner->firstname) , 0, 1)}} </div>
                    </div><!-- /.list-group-item-figure -->
                    <!-- .list-group-item-body -->
                    <div class="list-group-item-body">
                      <h4 class="list-group-item-title"> {{$partner->username}} </h4>
                      <p class="list-group-item-text"> {{$partner->name}} </p>
                    </div><!-- /.list-group-item-body -->
                  </a> <!-- /.list-group-item -->
                  <?php $index = $index+1; ?>
                  @endforeach

                </div><!-- /.list-group -->
                <!-- .loading -->
                <div class="card rounded-0 mb-0 shadow-0">
                  <div class="loading">
                    <div class="loader loader-sm"></div>
                  </div>
                </div><!-- /.loading -->
              </div><!-- /board -->
            </div><!-- /.page-inner -->
            <!-- .page-sidebar -->
            <div class="page-sidebar bg-light">
              <!-- .sidebar-section -->
              <div class="sidebar-section">
                <nav class="d-xl-none" aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                      <a href="#!" data-toggle="sidebar"><i class="breadcrumb-icon fa fa-angle-left mr-2"></i>Partners</a>
                    </li>
                  </ol>
                </nav>
                <h1 class="page-title">
                  <i class="fa fa-building text-muted mr-2"></i> UNITED PURPOSE. </h1>
                <p class="text-muted"> Balaka, Malawi </p><!-- .nav-scroller -->
                <div class="nav-scroller border-bottom">
                  <!-- .nav-tabs -->
                  <ul class="nav nav-tabs">
                    <li class="nav-item">
                      <a class="nav-link active show" data-toggle="tab" href="#partner-billing-contact">Partner Details &amp; Contacts</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#partner-tasks">To Do</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#partner-projects">Projects</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#partner-invoices">Invoices</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#partner-expenses">Expenses</a>
                    </li>
                  </ul><!-- /.nav-tabs -->
                </div><!-- /.nav-scroller -->
                <!-- .tab-content -->
                <div class="tab-content pt-4" id="partnerDetailsTabs">
                  <!-- .tab-pane -->
                  <div class="tab-pane fade show active" id="partner-billing-contact" role="tabpanel" aria-labelledby="partner-billing-contact-tab">
                    <!-- .card -->
                    <section class="card">
                      <!-- .card-body -->
                      <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center">
                          <h2 class="card-title"> Partner Address </h2><button type="button" class="btn btn-link" data-toggle="modal" data-target="#partnerBillingEditModal">Edit</button>
                        </div>
                        <address> 280 Suzanne Throughway, Breannabury<br> San Francisco, 45801<br> United States </address>
                      </div><!-- /.card-body -->
                    </section><!-- /.card -->
                    <!-- .card -->
                    <section class="card mt-4">
                      <!-- .card-body -->
                      <div class="card-body">
                        <h2 class="card-title"> Manager Contacts </h2><!-- .table-responsive -->
                        <div class="table-responsive">
                          <table class="table table-hover" style="min-width: 678px">
                            <thead>
                                <tr>
                                  <th> Name </th>
                                  <th> Email </th>
                                  <th> Phone </th>
                                  <th></th>
                                </tr>

                            </thead>
                            <tbody>
                              @foreach($partners as $partner)
                                <tr>
                                  <td class="align-middle"> {{$partner->firstname}} {{$partner->lastname}}</td>
                                  <td class="align-middle"> {{$partner->email}} </td>
                                  <td class="align-middle"> {{$partner->phonenumber}} </td>
                                  <td class="align-middle text-right">
                                    <button type="button" class="btn btn-sm btn-icon btn-secondary" data-toggle="modal" data-target="#partnerContactEditModal"><i class="fa fa-pencil"></i> <span class="sr-only">Edit</span></button> <button type="button" class="btn btn-sm btn-icon btn-secondary"><i class="fa fa-trash"></i> <span class="sr-only">Remove</span></button>
                                  </td>
                                </tr>
                            @endforeach
                            </tbody>
                          </table>
                        </div><!-- /.table-responsive -->
                      </div><!-- /.card-body -->
                      <!-- .card-footer -->
                      <div class="card-footer">
                        <a href="#partnerContactNewModal" class="card-footer-item" data-toggle="modal"><i class="fa fa-plus-circle mr-1"></i> Add contact</a>
                      </div><!-- /.card-footer -->
                    </section><!-- /.card -->
                  </div><!-- /.tab-pane -->
                  <!-- .tab-pane -->
                  <div class="tab-pane fade" id="partner-tasks" role="tabpanel" aria-labelledby="partner-tasks-tab">
                    <!-- .card -->
                    <section class="card">
                      <!-- .card-body -->
                      <div class="card-body">
                        <div class="d-flex justify-content-between">
                          <h3 class="card-title"> To do </h3>
                          <div class="card-title-control">
                            <div class="dropdown">
                              <button type="button" class="btn btn-icon btn-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                              <div class="dropdown-arrow"></div>
                              <div class="dropdown-menu dropdown-menu-right">
                                <h6 class="dropdown-header"> Sort by </h6><label class="custom-control custom-radio stop-propagation"><input type="radio" class="custom-control-input" name="todoSorting" value="0" checked=""> <span class="custom-control-label">My order</span></label> <label class="custom-control custom-radio stop-propagation"><input type="radio" class="custom-control-input" name="todoSorting" value="1"> <span class="custom-control-label">Due date</span></label>
                                <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Rename list</button> <button type="button" class="dropdown-item">Delete completed todos</button>
                              </div>
                            </div>
                          </div>
                        </div><!-- .todo-list -->
                        <div class="todo-list todo-list-bordered">
                          <!-- .todo -->
                          <div class="todo">
                            <!-- .custom-control -->
                            <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="todo1"> <label class="custom-control-label" for="todo1"><span>Add payment method to user-billing page</span></label>
                            </div><!-- /.custom-control -->
                            <!-- .todo-actions -->
                            <div class="todo-actions d-block">
                              <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="1"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="1"><i class="fa fa-pencil-alt"></i></button>
                            </div><!-- /.todo-actions -->
                          </div><!-- /.todo -->
                          <!-- .todo -->
                          <div class="todo">
                            <!-- .custom-control -->
                            <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="todo3"> <label class="custom-control-label" for="todo3"><span>Add view / edit task modal</span></label>
                            </div><!-- /.custom-control -->
                            <!-- .todo-actions -->
                            <div class="todo-actions d-block">
                              <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="3"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="3"><i class="fa fa-pencil-alt"></i></button>
                            </div><!-- /.todo-actions -->
                          </div><!-- /.todo -->
                          <!-- .todo -->
                          <div class="todo">
                            <!-- .custom-control -->
                            <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="todo4"> <label class="custom-control-label" for="todo4"><span>Increase app-aside zindex on screen md &amp; up</span></label>
                            </div><!-- /.custom-control -->
                            <!-- .todo-actions -->
                            <div class="todo-actions d-block">
                              <small class="text-muted border-bottom border-warning pb-1" data-toggle="tooltip" title="22 Sep">Today</small> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="4"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="4"><i class="fa fa-pencil-alt"></i></button>
                            </div><!-- /.todo-actions -->
                          </div><!-- /.todo -->
                          <!-- .todo -->
                          <div class="todo">
                            <!-- .custom-control -->
                            <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="todo9"> <label class="custom-control-label" for="todo9"><span>Fixed overlap flatpickr zindex in modal</span></label>
                            </div><!-- /.custom-control -->
                            <!-- .todo-actions -->
                            <div class="todo-actions d-block">
                              <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="9"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="9"><i class="fa fa-pencil-alt"></i></button>
                            </div><!-- /.todo-actions -->
                          </div><!-- /.todo -->
                          <!-- .todo -->
                          <div class="todo">
                            <!-- .custom-control -->
                            <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="todo11"> <label class="custom-control-label" for="todo11"><span>Add filterlist fn to main script</span></label>
                            </div><!-- /.custom-control -->
                            <!-- .todo-actions -->
                            <div class="todo-actions d-block">
                              <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="11"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="11"><i class="fa fa-pencil-alt"></i></button>
                            </div><!-- /.todo-actions -->
                          </div><!-- /.todo -->
                          <!-- .todo -->
                          <div class="todo">
                            <!-- .custom-control -->
                            <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="todo14"> <label class="custom-control-label" for="todo14"><span>Reinvent apple fonts system</span></label>
                            </div><!-- /.custom-control -->
                            <!-- .todo-actions -->
                            <div class="todo-actions d-block">
                              <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="14"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="14"><i class="fa fa-pencil-alt"></i></button>
                            </div><!-- /.todo-actions -->
                          </div><!-- /.todo -->
                        </div><!-- /.todo-list -->
                      </div><!-- /.card-body -->
                      <!-- .card-footer -->
                      <div class="card-footer">
                        <a href="#!" class="card-footer-item"><i class="fa fa-plus-circle mr-1"></i> Add todo</a>
                      </div><!-- /.card-footer -->
                    </section><!-- /.card -->
                    <!-- .card -->
                    <section class="card">
                      <header class="card-header border-0" id="headingCompletedTodo">
                        <button class="btn btn-block btn-reset d-flex justify-content-between collapsed" data-toggle="collapse" data-target="#completedTodo" aria-expanded="false" aria-controls="completedTodo"><span><i class="fa fa-history text-muted mr-2"></i> Completed (27)</span> <span class="collapse-indicator"><i class="fa fa-fw fa-chevron-down"></i></span></button>
                      </header>
                      <div id="completedTodo" class="collapse" aria-labelledby="headingCompletedTodo">
                        <div class="card-body pt-0">
                          <!-- .todo-list -->
                          <div class="todo-list todo-list-bordered">
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo2" checked=""> <label class="custom-control-label" for="todo2"><span>Sidebar enhancement: adding fluid width &amp; expand classes like bootstrap navbar</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="2"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="2"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo5" checked=""> <label class="custom-control-label" for="todo5"><span>Change stacked-menu bg to transparent</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="5"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="5"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo6" checked=""> <label class="custom-control-label" for="todo6"><span>Fix color-picker</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="6"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="6"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo7" checked=""> <label class="custom-control-label" for="todo7"><span>Add create new task modal</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="7"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="7"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo8" checked=""> <label class="custom-control-label" for="todo8"><span>Add flagging class for macos</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="8"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="8"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo10" checked=""> <label class="custom-control-label" for="todo10"><span>Mark toastr as a deprecated dependencies</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="10"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="10"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo12" checked=""> <label class="custom-control-label" for="todo12"><span>Fix or remove .custom-control-nolabel</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="12"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="12"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo13" checked=""> <label class="custom-control-label" for="todo13"><span>Change datatables sort icons</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="13"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="13"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo15" checked=""> <label class="custom-control-label" for="todo15"><span>Improve jqvmap zoom buttons</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="15"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="15"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo16" checked=""> <label class="custom-control-label" for="todo16"><span>Improve pagination active bg</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="16"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="16"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo17" checked=""> <label class="custom-control-label" for="todo17"><span>Fix knobify footer item</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="17"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="17"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo18" checked=""> <label class="custom-control-label" for="todo18"><span>Handle autofocus input when modal/dropdown shown</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="18"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="18"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo19" checked=""> <label class="custom-control-label" for="todo19"><span>Add new tasks list input</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="19"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="19"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo20" checked=""> <label class="custom-control-label" for="todo20"><span>Add page fullscreen behavior</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="20"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="20"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo21" checked=""> <label class="custom-control-label" for="todo21"><span>Make content layout fluid</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="21"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="21"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo22" checked=""> <label class="custom-control-label" for="todo22"><span>Different render line-height between OS due to different font-family</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="22"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="22"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo23" checked=""> <label class="custom-control-label" for="todo23"><span>Add scrollable content to tasks component</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="23"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="23"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo24" checked=""> <label class="custom-control-label" for="todo24"><span>Fixed .message-title line-height</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="24"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="24"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo25" checked=""> <label class="custom-control-label" for="todo25"><span>Re-order navigations</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="25"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="25"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo26" checked=""> <label class="custom-control-label" for="todo26"><span>Add modal right &amp; left</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="26"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="26"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo27" checked=""> <label class="custom-control-label" for="todo27"><span>Clean up line-height comments</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="27"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="27"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo28" checked=""> <label class="custom-control-label" for="todo28"><span>Improved grays color scheme</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="28"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="28"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo29" checked=""> <label class="custom-control-label" for="todo29"><span>Prevent form-control height since v4.1.3</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="29"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="29"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo30" checked=""> <label class="custom-control-label" for="todo30"><span>Fixed item align on dropdown-sheet</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="30"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="30"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo31" checked=""> <label class="custom-control-label" for="todo31"><span>Re-store pooper.js due to lack of dropdown position on bootstrap-bundle</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="31"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="31"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo32" checked=""> <label class="custom-control-label" for="todo32"><span>Update Bootstrap to v4.1.3</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="32"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="32"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo33" checked=""> <label class="custom-control-label" for="todo33"><span>Add animation class to .progress</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="33"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="33"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                            <!-- .todo -->
                            <div class="todo">
                              <!-- .custom-control -->
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="todo34" checked=""> <label class="custom-control-label" for="todo34"><span>Remove popper.js from package.json - use bootstrap-bundle instead</span></label>
                              </div><!-- /.custom-control -->
                              <!-- .todo-actions -->
                              <div class="todo-actions d-block">
                                <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="34"><i class="fa fa-arrows-alt"></i></button> <button type="button" class="btn btn-sm btn-icon btn-light" data-todoid="34"><i class="fa fa-trash-alt"></i></button>
                              </div><!-- /.todo-actions -->
                            </div><!-- /.todo -->
                          </div><!-- /.todo-list -->
                        </div>
                      </div>
                    </section><!-- /.card -->
                  </div><!-- /.tab-pane -->
                  <!-- .tab-pane -->
                  <div class="tab-pane fade" id="partner-projects" role="tabpanel" aria-labelledby="partner-projects-tab">
                    <!-- .card -->
                    <section class="card">
                      <!-- .card-header -->
                      <header class="card-header d-flex">
                        <!-- .dropdown -->
                        <div class="dropdown">
                          <button type="button" class="btn btn-secondary" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-filter mr-1"></i> All (3) <i class="fa fa-caret-down"></i></button>
                          <div class="dropdown-arrow"></div><!-- .dropdown-menu -->
                          <div class="dropdown-menu stop-propagation">
                            <h6 class="dropdown-header"> Projects </h6><label class="custom-control custom-radio"><input type="radio" class="custom-control-input" name="partnerProjectFilter" value="0" checked="">
                              <span class="custom-control-label">All (3)</span></label>
                              <label class="custom-control custom-radio"><input type="radio" class="custom-control-input" name="partnerProjectFilter" value="1">
                                <span class="custom-control-label">On Going (1)</span>
                              </label>
                              <label class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" name="partnerProjectFilter" value="2">
                                <span class="custom-control-label">Completed (2)</span>
                              </label>
                              <label class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" name="partnerProjectFilter" value="3">
                                <span class="custom-control-label">Archived (0)</span>
                              </label>
                          </div><!-- /.dropdown-menu -->
                        </div><!-- /.dropdown -->
                        <button type="button" class="btn btn-primary ml-auto">Add project</button>
                      </header><!-- /.card-header -->
                      <!-- .table-responsive -->
                      <div class="table-responsive">
                        <!-- .table -->
                        <table class="table">
                          <!-- thead -->
                          <thead>
                            <tr>
                              <th style="min-width:260px"> Project </th>
                              <th> Start </th>
                              <th> Due </th>
                              <th> Status </th>
                              <th></th>
                            </tr>
                          </thead><!-- /thead -->
                          <!-- tbody -->
                          <tbody>
                            <!-- tr -->
                            <tr>
                              <td class="align-middle text-truncate">
                                <a href="#!" class="tile bg-pink text-white mr-2">SP</a> <a href="#!">Syrena Project</a>
                              </td>
                              <td class="align-middle"> 04/10/2018 </td>
                              <td class="align-middle"> 08/18/2018 </td>
                              <td class="align-middle">
                                <span class="badge badge-warning">On Going</span>
                              </td>
                              <td class="align-middle text-right">
                                <div class="dropdown">
                                  <button type="button" class="btn btn-sm btn-icon btn-secondary" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true"><i class="fa fa-ellipsis-h"></i> <span class="sr-only">Actions</span></button>
                                  <div class="dropdown-arrow"></div>
                                  <div class="dropdown-menu dropdown-menu-right">
                                    <button class="dropdown-item" type="button">Edit</button> <button class="dropdown-item" type="button">Delete</button>
                                  </div>
                                </div>
                              </td>
                            </tr><!-- /tr -->
                            <!-- tr -->
                            <tr>
                              <td class="align-middle text-truncate">
                                <a href="#!" class="tile bg-green text-white mr-2">MG</a> <a href="#!">Mobile App Gex</a>
                              </td>
                              <td class="align-middle"> 06/12/2018 </td>
                              <td class="align-middle"> 07/03/2018 </td>
                              <td class="align-middle">
                                <span class="badge badge-success">Completed</span>
                              </td>
                              <td class="align-middle text-right">
                                <div class="dropdown">
                                  <button type="button" class="btn btn-sm btn-icon btn-secondary" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true"><i class="fa fa-ellipsis-h"></i> <span class="sr-only">Actions</span></button>
                                  <div class="dropdown-arrow"></div>
                                  <div class="dropdown-menu dropdown-menu-right">
                                    <button class="dropdown-item" type="button">Edit</button> <button class="dropdown-item" type="button">Delete</button>
                                  </div>
                                </div>
                              </td>
                            </tr><!-- /tr -->
                            <!-- tr -->
                            <tr>
                              <td class="align-middle text-truncate">
                                <a href="#!" class="tile bg-red text-white mr-2">LB</a> <a href="#!">Landing Page Booster</a>
                              </td>
                              <td class="align-middle"> 09/29/2018 </td>
                              <td class="align-middle"> 11/24/2018 </td>
                              <td class="align-middle">
                                <span class="badge badge-success">Completed</span>
                              </td>
                              <td class="align-middle text-right">
                                <div class="dropdown">
                                  <button type="button" class="btn btn-sm btn-icon btn-secondary" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true"><i class="fa fa-ellipsis-h"></i> <span class="sr-only">Actions</span></button>
                                  <div class="dropdown-arrow"></div>
                                  <div class="dropdown-menu dropdown-menu-right">
                                    <button class="dropdown-item" type="button">Edit</button> <button class="dropdown-item" type="button">Delete</button>
                                  </div>
                                </div>
                              </td>
                            </tr><!-- /tr -->
                          </tbody><!-- /tbody -->
                        </table><!-- /.table -->
                      </div><!-- /.table-responsive -->
                    </section><!-- /.card -->
                  </div><!-- /.tab-pane -->
                  <!-- .tab-pane -->
                  <div class="tab-pane fade" id="partner-invoices" role="tabpanel" aria-labelledby="partner-invoices-tab">
                    <!-- .card -->
                    <section class="card">
                      <!-- .card-header -->
                      <header class="card-header d-flex">
                        <!-- .dropdown -->
                        <div class="dropdown">
                          <button type="button" class="btn btn-secondary" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-filter mr-1"></i> All (5) <i class="fa fa-caret-down"></i></button>
                          <div class="dropdown-arrow"></div><!-- .dropdown-menu -->
                          <div class="dropdown-menu stop-propagation">
                            <h6 class="dropdown-header"> Invoices </h6><label class="custom-control custom-radio"><input type="radio" class="custom-control-input" name="partnerInvoiceFilter" value="0" checked=""> <span class="custom-control-label">All (5)</span></label> <label class="custom-control custom-radio"><input type="radio" class="custom-control-input" name="partnerInvoiceFilter" value="1"> <span class="custom-control-label">Draft (1)</span></label> <label class="custom-control custom-radio"><input type="radio" class="custom-control-input" name="partnerInvoiceFilter" value="2"> <span class="custom-control-label">Send (2)</span></label> <label class="custom-control custom-radio"><input type="radio" class="custom-control-input" name="partnerInvoiceFilter" value="3"> <span class="custom-control-label">Paid (0)</span></label>
                          </div><!-- /.dropdown-menu -->
                        </div><!-- /.dropdown -->
                        <button type="button" class="btn btn-primary ml-auto">Add invoice</button>
                      </header><!-- /.card-header -->
                      <!-- .table-responsive -->
                      <div class="table-responsive">
                        <!-- .table -->
                        <table class="table">
                          <!-- thead -->
                          <thead>
                            <tr>
                              <th style="min-width:256px"> Project </th>
                              <th></th>
                              <th> Due </th>
                              <th> Amount </th>
                              <th> Received </th>
                              <th> Status </th>
                              <th></th>
                            </tr>
                          </thead><!-- /thead -->
                          <!-- tbody -->
                          <tbody>
                            <!-- tr -->
                            <tr>
                              <td class="align-middle text-truncate">
                                <div class="media align-items-center">
                                  <a href="#!" class="tile bg-pink text-white mr-2">SP</a>
                                  <div class="media-body">
                                    <a href="#!">Syrena Project</a> <small class="d-block text-muted">Invoice #9463</small>
                                  </div>
                                </div>
                              </td>
                              <td class="align-middle"></td>
                              <td class="align-middle"> 09/16/2018 </td>
                              <td class="align-middle"> $1,500 </td>
                              <td class="align-middle"> $1,500 </td>
                              <td class="align-middle">
                                <span class="badge badge-success">Paid</span>
                              </td>
                              <td class="align-middle text-right">
                                <div class="dropdown">
                                  <button type="button" class="btn btn-sm btn-icon btn-secondary" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true"><i class="fa fa-ellipsis-h"></i> <span class="sr-only">Actions</span></button>
                                  <div class="dropdown-arrow"></div>
                                  <div class="dropdown-menu dropdown-menu-right">
                                    <button class="dropdown-item" type="button">Edit</button> <button class="dropdown-item" type="button">Delete</button>
                                  </div>
                                </div>
                              </td>
                            </tr><!-- /tr -->
                            <!-- tr -->
                            <tr>
                              <td class="align-middle text-truncate">
                                <div class="media align-items-center">
                                  <a href="#!" class="tile bg-purple text-white mr-2">BA</a>
                                  <div class="media-body">
                                    <a href="#!">Mobile App Gex</a> <small class="d-block text-muted">Invoice #0754</small>
                                  </div>
                                </div>
                              </td>
                              <td class="align-middle"></td>
                              <td class="align-middle"> 12/13/2018 </td>
                              <td class="align-middle"> $1,000 </td>
                              <td class="align-middle"> - </td>
                              <td class="align-middle">
                                <span class="badge badge-warning">Outstanding</span>
                              </td>
                              <td class="align-middle text-right">
                                <div class="dropdown">
                                  <button type="button" class="btn btn-sm btn-icon btn-secondary" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true"><i class="fa fa-ellipsis-h"></i> <span class="sr-only">Actions</span></button>
                                  <div class="dropdown-arrow"></div>
                                  <div class="dropdown-menu dropdown-menu-right">
                                    <button class="dropdown-item" type="button">Edit</button> <button class="dropdown-item" type="button">Delete</button>
                                  </div>
                                </div>
                              </td>
                            </tr><!-- /tr -->
                            <!-- tr -->
                            <tr>
                              <td class="align-middle text-truncate">
                                <div class="media align-items-center">
                                  <a href="#!" class="tile bg-teal text-white mr-2">SB</a>
                                  <div class="media-body">
                                    <a href="#!">SVG Icon Bundle</a> <small class="d-block text-muted">Invoice #8613</small>
                                  </div>
                                </div>
                              </td>
                              <td class="align-middle">
                                <i class="fa fa-paperclip text-muted"></i>
                              </td>
                              <td class="align-middle"> 02/09/2019 </td>
                              <td class="align-middle"> $3,000 </td>
                              <td class="align-middle"> $500 </td>
                              <td class="align-middle">
                                <span class="badge badge-warning">Outstanding</span>
                              </td>
                              <td class="align-middle text-right">
                                <div class="dropdown">
                                  <button type="button" class="btn btn-sm btn-icon btn-secondary" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true"><i class="fa fa-ellipsis-h"></i> <span class="sr-only">Actions</span></button>
                                  <div class="dropdown-arrow"></div>
                                  <div class="dropdown-menu dropdown-menu-right">
                                    <button class="dropdown-item" type="button">Edit</button> <button class="dropdown-item" type="button">Delete</button>
                                  </div>
                                </div>
                              </td>
                            </tr><!-- /tr -->
                            <!-- tr -->
                            <tr>
                              <td class="align-middle text-truncate">
                                <div class="media align-items-center">
                                  <a href="#!" class="tile bg-red text-white mr-2">LB</a>
                                  <div class="media-body">
                                    <a href="#!">Landing Page Booster</a> <small class="d-block text-muted">Invoice #9458</small>
                                  </div>
                                </div>
                              </td>
                              <td class="align-middle"></td>
                              <td class="align-middle"> 05/30/2018 </td>
                              <td class="align-middle"> $499 </td>
                              <td class="align-middle"> - </td>
                              <td class="align-middle">
                                <span class="badge badge-danger">Past Due</span>
                              </td>
                              <td class="align-middle text-right">
                                <div class="dropdown">
                                  <button type="button" class="btn btn-sm btn-icon btn-secondary" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true"><i class="fa fa-ellipsis-h"></i> <span class="sr-only">Actions</span></button>
                                  <div class="dropdown-arrow"></div>
                                  <div class="dropdown-menu dropdown-menu-right">
                                    <button class="dropdown-item" type="button">Edit</button> <button class="dropdown-item" type="button">Delete</button>
                                  </div>
                                </div>
                              </td>
                            </tr><!-- /tr -->
                          </tbody><!-- /tbody -->
                        </table><!-- /.table -->
                      </div><!-- /.table-responsive -->
                    </section><!-- /.card -->
                  </div><!-- /.tab-pane -->
                  <!-- .tab-pane -->
                  <div class="tab-pane fade" id="partner-expenses" role="tabpanel" aria-labelledby="partner-expenses-tab">
                    <!-- .card -->
                    <section class="card">
                      <!-- .card-header -->
                      <header class="card-header d-flex">
                        <!-- .dropdown -->
                        <div class="dropdown">
                          <button class="btn btn-secondary" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span>This Year</span> <i class="fa fa-fw fa-caret-down"></i></button>
                          <div class="dropdown-arrow"></div><!-- .dropdown-menu -->
                          <div class="dropdown-menu dropdown-menu-md stop-propagation">
                            <!-- .custom-control -->
                            <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" id="partnerExpensesDateFilter0" name="partnerExpensesDateFilter" value="0"> <label class="custom-control-label" for="partnerExpensesDateFilter0">Last 7 days</label>
                            </div><!-- /.custom-control -->
                            <!-- .custom-control -->
                            <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" id="partnerExpensesDateFilter1" name="partnerExpensesDateFilter" value="1"> <label class="custom-control-label" for="partnerExpensesDateFilter1">Last 3 days</label>
                            </div><!-- /.custom-control -->
                            <!-- .custom-control -->
                            <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" id="partnerExpensesDateFilter2" name="partnerExpensesDateFilter" value="2"> <label class="custom-control-label" for="partnerExpensesDateFilter2">This month</label>
                            </div><!-- /.custom-control -->
                            <!-- .custom-control -->
                            <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" id="partnerExpensesDateFilter3" name="partnerExpensesDateFilter" value="3"> <label class="custom-control-label" for="partnerExpensesDateFilter3">Last month</label>
                            </div><!-- /.custom-control -->
                            <!-- .custom-control -->
                            <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" id="partnerExpensesDateFilter4" name="partnerExpensesDateFilter" value="4" checked=""> <label class="custom-control-label" for="partnerExpensesDateFilter4">This year</label>
                            </div><!-- /.custom-control -->
                            <!-- .custom-control -->
                            <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" id="partnerExpensesDateFilter5" name="partnerExpensesDateFilter" value="5"> <label class="custom-control-label" for="partnerExpensesDateFilter5">Custom date range</label>
                              <div class="custom-control-hint my-1">
                                <!-- datepicker:range -->
                                <input type="date" name="partnerExpensesDateFilterCustom" class="form-control" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}"> <!-- /datepicker:range -->
                              </div>
                            </div><!-- /.custom-control -->
                          </div><!-- /.dropdown-menu -->
                        </div><!-- /.dropdown -->
                        <button type="button" class="btn btn-primary ml-auto">Add expense</button>
                      </header><!-- /.card-header -->
                      <!-- .table-responsive -->
                      <div class="table-responsive">
                        <!-- .table -->
                        <table class="table">
                          <!-- thead -->
                          <thead>
                            <tr>
                              <th> Date </th>
                              <th> Amount </th>
                              <th style="min-width:200px"> Vendor </th>
                              <th></th>
                              <th> Category </th>
                              <th></th>
                            </tr>
                          </thead><!-- /thead -->
                          <!-- tbody -->
                          <tbody>
                            <!-- tr -->
                            <tr>
                              <td class="align-middle"> 04/11/2018 </td>
                              <td class="align-middle"> $360.00 </td>
                              <td class="align-middle"> Facebook, Inc. </td>
                              <td class="align-middle">
                                <i class="fa fa-paperclip text-muted"></i>
                              </td>
                              <td class="align-middle">
                                <span class="badge text-white bg-purple">Campaign</span>
                              </td>
                              <td class="align-middle text-right">
                                <div class="dropdown">
                                  <button type="button" class="btn btn-sm btn-icon btn-secondary" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true"><i class="fa fa-ellipsis-h"></i> <span class="sr-only">Actions</span></button>
                                  <div class="dropdown-arrow"></div>
                                  <div class="dropdown-menu dropdown-menu-right">
                                    <button class="dropdown-item" type="button">Edit</button> <button class="dropdown-item" type="button">Delete</button>
                                  </div>
                                </div>
                              </td>
                            </tr><!-- /tr -->
                            <!-- tr -->
                            <tr>
                              <td class="align-middle"> 09/15/2018 </td>
                              <td class="align-middle"> $49.00 </td>
                              <td class="align-middle"> Adobe Systems </td>
                              <td class="align-middle">
                                <i class="fa fa-paperclip text-muted"></i>
                              </td>
                              <td class="align-middle">
                                <span class="badge text-white bg-orange">Other</span>
                              </td>
                              <td class="align-middle text-right">
                                <div class="dropdown">
                                  <button type="button" class="btn btn-sm btn-icon btn-secondary" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true"><i class="fa fa-ellipsis-h"></i> <span class="sr-only">Actions</span></button>
                                  <div class="dropdown-arrow"></div>
                                  <div class="dropdown-menu dropdown-menu-right">
                                    <button class="dropdown-item" type="button">Edit</button> <button class="dropdown-item" type="button">Delete</button>
                                  </div>
                                </div>
                              </td>
                            </tr><!-- /tr -->
                          </tbody><!-- /tbody -->
                        </table><!-- /.table -->
                      </div><!-- /.table-responsive -->
                    </section><!-- /.card -->
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div><!-- /.sidebar-section -->
            </div><!-- /.page-sidebar -->
            <!-- Keep in mind that modals should be placed outsite of page sidebar -->
            <!-- .modal -->
            <form id="partnerBillingEditForm" name="partnerBillingEditForm">
              <div class="modal fade" id="partnerBillingEditModal" tabindex="-1" role="dialog" aria-labelledby="partnerBillingEditModalLabel" aria-hidden="true">
                <!-- .modal-dialog -->
                <div class="modal-dialog" role="document">
                  <!-- .modal-content -->
                  <div class="modal-content">
                    <!-- .modal-header -->
                    <div class="modal-header">
                      <h6 class="modal-title inline-editable">
                        <input type="text" class="form-control form-control-lg" value="Zathunicon, Inc." placeholder="E.g. Stilearning, Inc." required="">
                      </h6>
                    </div><!-- /.modal-header -->
                    <!-- .modal-body -->
                    <div class="modal-body">
                      <!-- .form-row -->
                      <div class="form-row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="ceStreet">Street</label> <input type="text" id="ceStreet" class="form-control" value="280 Suzanne Throughway">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="ceSuite">Suite</label> <input type="text" id="ceSuite" class="form-control" value="Breannabury">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="ceZip">Zip</label> <input type="text" id="ceZip" class="form-control" value="45801">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="ceCountry">Country</label> <select id="ceCountry" class="custom-select d-block w-100">
                              <option value=""> Choose... </option>
                              <option selected=""> United States </option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="ceCity">City</label> <select id="ceCity" class="custom-select d-block w-100">
                              <option value=""> Choose... </option>
                              <option selected=""> San Francisco </option>
                            </select>
                          </div>
                        </div>
                      </div><!-- /.form-row -->
                    </div><!-- /.modal-body -->
                    <!-- .modal-footer -->
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary">Save</button> <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                    </div><!-- /.modal-footer -->
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div>
            </form><!-- /.modal -->
            <!-- .modal -->
            <form id="partnerContactNewForm fixed-center" name="partnerContactNewForm">
              <div class="modal fade" id="partnerContactNewModal" tabindex="-1" role="dialog" aria-labelledby="partnerContactNewModalLabel" aria-hidden="true">
                <!-- .modal-dialog -->
                <div class="modal-dialog" role="document">
                  <!-- .modal-content -->
                  <div class="modal-content">
                    <!-- .modal-header -->
                    <div class="modal-header">
                      <h6 class="modal-title inline-editable">
                        <input type="text" class="form-control form-control-lg" placeholder="Name (e.g. John Doe)" required="">
                      </h6>
                    </div><!-- /.modal-header -->
                    <!-- .modal-body -->
                    <div class="modal-body">
                      <!-- .form-group -->
                      <div class="form-group">
                        <div class="form-label-group">
                          <input type="email" id="cnEmail" class="form-control" placeholder="Email" required=""> <label for="cnEmail">Email</label>
                        </div>
                      </div><!-- /.form-group -->
                      <!-- .form-group -->
                      <div class="form-group">
                        <div class="form-label-group">
                          <input type="tel" id="cnPhone" class="form-control" placeholder="Phone"> <label for="cnPhone">Phone</label>
                        </div>
                      </div><!-- /.form-group -->
                    </div><!-- /.modal-body -->
                    <!-- .modal-footer -->
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary">Save</button> <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                    </div><!-- /.modal-footer -->
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div>
            </form><!-- /.modal -->
            <!-- .modal -->
            <form id="partnerContactEditForm" name="partnerContactEditForm">
              <div class="modal fade" id="partnerContactEditModal" tabindex="-1" role="dialog" aria-labelledby="partnerContactEditModalLabel" aria-hidden="true">
                <!-- .modal-dialog -->
                <div class="modal-dialog" role="document">
                  <!-- .modal-content -->
                  <div class="modal-content">
                    <!-- .modal-header -->
                    <div class="modal-header">
                      <h6 class="modal-title inline-editable">
                        <input type="text" class="form-control form-control-lg" value="Alexane Collins" placeholder="Name (e.g. John Doe)" required="">
                      </h6>
                    </div><!-- /.modal-header -->
                    <!-- .modal-body -->
                    <div class="modal-body">
                      <!-- .form-group -->
                      <div class="form-group">
                        <div class="form-label-group">
                          <input type="email" id="ceEmail" class="form-control" value="fhauck@gmail.com" placeholder="Email" required=""> <label for="ceEmail">Email</label>
                        </div>
                      </div><!-- /.form-group -->
                      <!-- .form-group -->
                      <div class="form-group">
                        <div class="form-label-group">
                          <input type="tel" id="cePhone" class="form-control" value="0621099222" placeholder="Phone"> <label for="cePhone">Phone</label>
                        </div>
                      </div><!-- /.form-group -->
                    </div><!-- /.modal-body -->
                    <!-- .modal-footer -->
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary">Save</button> <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                    </div><!-- /.modal-footer -->
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div>
            </form><!-- /.modal -->
          </div><!-- /.page -->
          <br><br><br>
        </div>
@endsection
