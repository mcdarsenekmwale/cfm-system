@extends('supervisor.master.master')

@section('content')

<div class="wrapper">
          <!-- .page -->
          <div class="page">
            <!-- .page-inner -->
            <div class="page-inner">
              <!-- .page-title-bar -->
              <header class="page-title-bar">
                <!-- .breadcrumb -->

                <div class="d-md-flex align-items-md-start  ">
                  <div class="small text-muted text-center page-title mr-sm-auto">
                  </div>
                  <div class="btn-toolbar">
                    <button type="button" class="btn btn-light" id="export"><i class="fa fa-file-excel-o"></i> <span class="ml-1">Export</span></button> <button type="button" class="btn btn-light"><i class="fa fa-upload"></i> <span class="ml-1">Import</span></button>
                    <div class="dropdown">
                      <button type="button" class="btn btn-light" data-toggle="dropdown"><span>More</span> <span class="fa fa-caret-down"></span></button>
                      <div class="dropdown-arrow dropdown-arrow-right"></div>
                      <div class="dropdown-menu dropdown-menu-right">
                        <a href="{{route('newuser')}}" class="dropdown-item">Add Counsellors</a>
                         <a href="#!" class="dropdown-item">Check Cases</a>
                        <div class="dropdown-divider"></div><a href="#!" class="dropdown-item">Share</a> <a href="#!" class="dropdown-item">Archive</a> <a href="#!" class="dropdown-item">Remove</a>
                      </div>
                    </div>
                  </div><!-- /.btn-toolbar -->
                </div><!-- /title and toolbar -->
                <!-- floating action -->
                <!-- title and toolbar -->

              </header><!-- /.page-title-bar -->
              <!-- .page-section -->
              <div class="page-section">
                <!-- .card -->
                <div class="col-xl-12">
                  <!-- .card -->
                  <section class="card card-fluid">
                    <!-- .card-header -->
                    <header class="card-header border-0">
                      <div class="d-flex align-items-center">
                        <span class="mr-auto">Counsellors</span> <button type="button" class="btn btn-icon btn-light"><i class="fa fa-copy" title="copy"></i></button> <button type="button" class="btn btn-icon btn-light"><i class="fa fa-download" title="download"></i></button>
                      </div>
                    </header><!-- /.card-header -->
                    <!-- .table-responsive -->
                    <!-- <div class="text-muted px-3"> Showing 1 to {!!$data['counsellor']->count()!!} of {!!$data['counsellor']->total()!!} counsellors </div> -->

                    <div  class="dataTables_wrapper dt-bootstrap4 no-footer">

                        <div class="table-responsive be-datatable-header"  >
                        <!-- .table -->
                        <table  class="table table-sm mb-0 table-fw-widget ">
                          <!-- thead -->
                          <thead class="thead-light">
                            <tr>
                              <th >
                                <a href="#" class="text-muted sort small" data-sort="goal-name">
                                <b>Counsellor Name</b>
                                </a>
                              </th>
                              <th class="text-center">
                                <a href="#" class="text-muted sort small " data-sort="goal-email">
                                <b>Email Address</b>
                                </a>
                               </th>
                              <th>
                                <a href="#" class="text-muted sort small" data-sort="goal-calls">
                                 Number of Calls
                               </a>
                               </th>
                               <th>
                                 <a href="#" class="text-muted sort small" data-sort="goal-cases">
                                  Status
                                </a>
                                </th>
                               <th style="width:150px; min-width:100px;"> &nbsp; </th>
                            </tr>
                          </thead><!-- /thead -->
                          <!-- tbody -->
                          <tbody>
                            <!-- tr -->
                            @if($data['counsellor']->total() == 0)
                            <tr class="card-body" style="height:10rem; width:56rem;"></tr>
                            <tr class="alert  align-items-center" role="alert " >
                              <td class="alert-text text-center " colspan="3" >
                                No recent partner with a case data yet
                                <b>{{Auth::user()->username}}  </b>
                              </td>
                            </tr>
                            <tr class="card-body" style="height:10.7rem; width:56rem;"></tr>
                            @else
                            @foreach($counsellors as $counsellor)
                              <tr class="hoverable ">
                                <td class="text-uppercase"><b> {{$counsellor->firstname}} {{$counsellor->lastname}}</b> </td>
                                <td> {{$counsellor->email}} </td>
                                <td>  </td>
                                <td>
                                  @if($counsellor->online== 'online')
                                    <span class="fa fa-circle fa-fw pulse text-success"></span>
                                      <b>online</b>
                                  @elseif($counsellor->status== '1')
                                    <span class="fa fa-circle fa-fw  text-success"></span>
                                      <b>active</b>
                                  @endif
                                </td>

                                <td class="align-middle text-right">
                                  <a href="{{route('counsellorStats')}}?id={{$counsellor->id}}" data-num1="{{$counsellor->id}}" class="btn btn-sm btn-icon btn-success"><i class="fa fa-line-chart" title="edit"></i> <span class="sr-only">Edit</span></a>
                                  <a href="{{route('counsellorDetails')}}?id={{$counsellor->id}}" data-num="{{$counsellor->id}}" class="btn btn-sm btn-icon btn-info"><i class="fa fa-info-circle" title="delete"></i> <span class="sr-only">Remove</span></a>
                                </td>
                              </tr>
                            @endforeach

                            @endif
                          </tbody><!-- /tbody -->
                        </table><!-- /.table -->
                      </div><!-- /.table-responsive -->
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <b class="col-11"></b>
                        <b class="col-1 float-right" >
                          <a class="text-muted " href="{!! $data['counsellor']->url($data['counsellor']->currentPage()-1)!!}" aria-label="Previous">
                            <span class="fa fa-chevron-circle-left fa-lg"></span>
                            <span class="sr-only">Previous</span>
                          </a>

                          <a class="text-muted " href="{!! $data['counsellor']->url($data['counsellor']->currentPage()+1)!!}" aria-label="Next">
                            <span class="fa fa-chevron-circle-right fa-lg"></span>
                            <span class="sr-only">Next</span>
                          </a>
                        </b>
                      </div>

                    </div>
                  </section><!-- /.card -->
                </div><!-- /grid column -->
              </div><!-- /.page-section -->
              <br><br><br>
            </div><!-- /.page-inner -->
          </div><!-- /.page -->
        </div>
@endsection
