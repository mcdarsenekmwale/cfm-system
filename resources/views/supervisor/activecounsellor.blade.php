@extends('supervisor.master.master')
@section('content')
<div class="wrapper">
          <!-- .page -->

@foreach($counsellorStats as $counsellorStat)
          <div class="page has-sidebar">
            <div class="sidebar-backdrop"></div>
            <!-- .page-navs -->
            <header class="page-navs shadow-sm pr-3">
              <!-- btn-account -->
              <a href="#" class="btn-account">
                <div class="user-avatar mr-2">
                  <img src="http://uselooper.com/assets/images/avatars/team4.jpg" alt="">
                </div>
                <div class="account-summary">
                  <h1 class="card-title">{{Auth::user()->firstname}} {{Auth::user()->lastname}}</h1>
                  <h6 class="card-subtitle text-muted">{{Auth::user()->type}} </h6>
                </div>
              </a> <!-- /btn-account -->


              <!-- right actions -->
              <div class="ml-auto">
                <!-- invite members -->
                <div class="dropdown d-inline-block ">
                  <button type="button" class="btn btn-light btn-icon " title="Choose Councellors" data-toggle="dropdown" data-display="static" aria-haspopup="true" aria-expanded="false"
                  aria-expanded="false"><i class="fa fa-user"></i></button>
                  <div class="dropdown-arrow"></div><!-- .dropdown-menu -->
                  <div class="  dropdown-menu dropdown-menu-rich dropdown-menu-right dropdown-menu-card" >
                    <div class="dropdown-header stop-propagation"> Choose Councellor </div>
                    <div class="form-group px-3 py-2 m-0">
                      <input type="text" class="form-control" placeholder="username e.g @mcdarsene" data-toggle="tribute"
                       data-remote="{{route('allusers')}}"
                       data-menu-container="#people-list" data-item-template="true" autofocus="" data-tribute="true">
                       <small class="form-text text-muted">Search people by username or email address to invite them.</small>
                    </div>
                    <div id="people-list" class="tribute-inline stop-propagation"></div><a href="#!" class="dropdown-footer">View Councellor <i class="fa fa-clone"></i></a>
                  </div><!-- /.dropdown-menu -->
                </div><!-- /invite members -->
                <button type="button" class="btn btn-light btn-icon" data-toggle="sidebar"><i class="fa fa-angle-double-left"></i></button>
              </div><!-- /right actions -->
            </header><!-- /.page-navs -->
            <!-- .page-inner -->

            <div class="page-inner">
              <!-- .page-title-bar -->
                <span id="load" ></span>
              <header class="page-title-bar">
                <!-- grid row -->
                <div class="row text-center text-sm-left">
                  <!-- grid column -->
                  <div class="col-sm-auto col-12 mb-2">
                    <span class="tile tile-xl bg-blue text">
                    </span>
                  </div><!-- /grid column -->
                  <!-- grid column -->
                  <div class="col">
                    <h1 class="page-title"> {{Auth::user()->firstname}} {{Auth::user()->lastname}}</h1>
                    <p class="text-link disabled">{{Auth::user()->type}} </p>
                  </div><!-- /grid column -->
                </div><!-- /grid row -->
                <!-- .nav-scroller -->
                <div class="nav-scroller border-bottom">
                  <!-- .nav -->
                  <div class="nav nav-tabs text-uppercase">
                    @if($pageDescription == 'counsellorStats')
                      <a class="nav-link active" id="Statistics-tab" data-toggle="tab" href="#Statistics" role="tab" aria-controls="Statistics" aria-selected="true">Statistics</a>
                      <a class="nav-link " id="details-tab" data-toggle="tab" href="#details" role="tab" aria-controls="details" aria-selected="true">Details</a>
                    @elseif($pageDescription == 'counsellorDetails')
                      <a class="nav-link " id="Statistics-tab" data-toggle="tab" href="#Statistics" role="tab" aria-controls="Statistics" aria-selected="true">Statistics</a>
                      <a class="nav-link active" id="details-tab" data-toggle="tab" href="#details" role="tab" aria-controls="details" aria-selected="true">Details</a>
                    @endif
                  </div><!-- /.nav -->
                </div><!-- /.nav-scroller -->
              </header><!-- /.page-title-bar -->
              <!-- .page-section -->

              <div class="tab-content my--2" id="nav-tabContent">
                <div class="tab-content" id="myTabContent">
                  <!-- profile -->

                    <div class="tab-pane fade show  active" id="Statistics" role="tabpanel" aria-labelledby="Statistics-tab">
                      <div class="page-section">
                      <!-- .section-block -->
                      <div class="section-block">
                        <!-- metric row -->

                        <div class="metric-row">
                          <!-- metric column -->
                          <div class="col">
                            <!-- .metric -->
                            <div class="metric metric-bordered card card-fluid hoverable">
                              <h2 class="metric-label text-uppercase"><b>Total Calls</b>  </h2>
                              <p class="metric-value h1 ">
                                <sup><i class="fa fa-phone"></i></sup> <span class="value">{{$counsellorStat->total}}</span>
                              </p>
                            </div><!-- /.metric -->
                          </div><!-- /metric column -->
                          <!-- metric column -->
                          <div class="col">
                            <!-- .metric -->
                            <div class="metric metric-bordered card card-fluid hoverable">
                              <h2 class="metric-label text-uppercase"> <b>Complete Calls</b>  </h2>
                              <p class="metric-value h1">
                                <sup><i class="fa fa-phone"></i></sup> <span class="value">{{$counsellorStat->com}}</span>
                              </p>
                            </div><!-- /.metric -->
                          </div><!-- /metric column -->
                          <!-- metric column -->
                          <div class="col">
                            <!-- .metric -->
                            <div class="metric metric-bordered card card-fluid hoverable">
                              <h2 class="metric-label text-uppercase"><b> Incomplete Calls</b>  </h2>
                              <p class="metric-value h1">
                                <sup><i class="fa fa-phone"></i> </sup> <span class="value">{{$counsellorStat->incomp}}</span>
                              </p>
                            </div><!-- /.metric -->
                          </div><!-- /metric column -->
                          <!-- metric column -->
                          <div class="col">
                            <!-- .metric -->
                            <div class="metric metric-bordered card card-fluid hoverable">
                              <h2 class="metric-label text-uppercase"><b> Progress </b> </h2>
                              @if($counsellorStat->total != 0)
                              <p class="metric-value h1">
                                <?php
                                  $percentage = ($counsellorStat->com/$counsellorStat->total)*100;
                                 ?>
                                <span class="value">{{substr($percentage,0,4)}}</span> <sup>%</sup>
                              </p>
                              @else
                              <p class="metric-value h1">
                                <span class="value">0</span> <sup>%</sup>
                              </p>
                              @endif
                            </div><!-- /.metric -->
                          </div><!-- /metric column -->
                        </div><!-- /metric row -->

                      </div><!-- /.section-block -->
                      <!-- .section-deck -->
                      <div class="section-deck">
                        <!-- .card -->
                        <section class="card card-fluid" >
                          <!-- .card-body -->
                          <div class="card-body">
                            <h3 class="card-title"> Activity by day </h3>
                            <div class="chartjs">
                              <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                                <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                  <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0">
                                  </div>
                                </div>
                                <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                  <div style="position:absolute;width:200%;height:200%;left:0; top:0">
                                </div>
                              </div>
                            </div>
                              <canvas id="counsellor-achievement" d width="826" height="312" class="chartjs-render-monitor" style="display: block; height: 250px; width: 661px;"></canvas>
                            </div>
                          </div><!-- /.card-body -->
                        </section><!-- /.card -->
                        <!-- .card -->
                        <section class="card card-fluid">
                          <!-- .card-body -->
                          <div class="card-body">
                            <h3 class="card-title"> counsellor Calls Status Distribution </h3>
                            <div class="row align-items-center">
                              <div class="col-xs-8">
                                <div class="chartjs">
                                  <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                                    <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                      <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0">
                                      </div>
                                    </div>
                                    <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                      <div style="position:absolute;width:200%;height:200%;left:0; top:0">
                                      </div>
                                    </div>
                                  </div>
                                  <canvas id="canvas-counsellor"  width="375" height="312" class="chartjs-render-monitor" style="display: block; height: 250px; width: 300px;"></canvas>
                                </div>
                              </div>
                            </div>
                          </div><!-- /.card-body -->
                        </section><!-- /.card -->
                      </div><!-- /.section-deck -->
                      <!-- .card -->
                      <section class="card">
                        <!-- .card-header -->
                        <header class="card-header border-0">Recent Cases </header><!-- /.card-header -->
                        <!-- .table-responsive -->
                        <div class="table-responsive">
                          <!-- .table -->
                          <table class="table">
                            <!-- thead -->
                            <thead>
                              <tr>
                                <th style="min-width:350px"> Comment </th>
                                <th> Date </th>
                                <th> Time </th>
                                <th> District </th>
                                <th> Counsellor </th>
                                <th style="width:50px"></th>
                              </tr>
                            </thead><!-- /thead -->
                            <!-- tbody -->
                            <tbody>
                              <!-- tr -->
                              @if($data['issues']->total() == 0)
                              <tr class="card-body" style="height:10rem; width:56rem;"></tr>
                              <tr class="alert  align-items-center" role="alert " >
                                <td class="alert-text text-center " colspan="3" >
                                  No recent partner with a case data yet
                                  <b>{{Auth::user()->username}}  </b>
                                </td>
                              </tr>
                              <tr class="card-body" style="height:10.7rem; width:56rem;"></tr>
                              @else
                              @foreach($Issues as $issue)
                                  <tr class="hoverable">
                                    <td class="align-middle text-truncate">
                                      <a href="#tracking" class="text-muted">{{substr($issue->comments,0,105)}}</a>
                                    </td>
                                    <td class="align-middle">
                                      {{date("d/m/Y", strtotime($issue->date))}}
                                   </td>
                                    <td class="align-middle"> {{date('G:ia', strtotime($issue->starttime))}} </td>
                                    <td class="align-middle"> {{$issue->ta->district->name}}</td>
                                    <td class="align-middle"> {{$issue->counsellor_username}}</td>
                                    <td class="align-middle text-right">
                                      <!-- .dropdown -->
                                      <div class="dropdown">
                                        <button type="button" class="btn btn-sm btn-icon btn-secondary" data-toggle="dropdown" aria-haspoppup="true" aria-expanded="false"><i class="fa fa-fw fa-ellipsis-h"></i></button>
                                        <div class="dropdown-arrow"></div>
                                        <div class="dropdown-menu dropdown-menu-right">
                                          <a href="#!" class="dropdown-item">Edit</a> <a href="#!" class="dropdown-item">Delete</a>
                                        </div>
                                      </div><!-- /.dropdown -->
                                    </td>
                                  </tr><!-- /tr -->
                              @endforeach
                              @endif
                              <!-- tr -->
                            </tbody><!-- /tbody -->
                          </table><!-- /.table -->
                        </div><!-- /.table-responsive -->
                        <!-- .card-footer -->
                        <footer class="card-footer">
                          <a href="#!" class="card-footer-item text-muted"><b>View All</b> <i class="fa fa-fw fa-angle-right"></i></a>

                            <b class="card-footer-item float-right" >
                              <a class="text-muted " href="{!! $data['issues']->url($data['issues']->currentPage()-1)!!}" aria-label="Previous">
                                <span class="fa fa-chevron-circle-left fa-lg"></span>
                                <span class="sr-only">Previous</span>
                              </a>

                              <a class="text-muted " href="{!! $data['issues']->url($data['issues']->currentPage()+1)!!}" aria-label="Next">
                                <span class="fa fa-chevron-circle-right fa-lg"></span>
                                <span class="sr-only">Next</span>
                              </a>
                            </b>
                        </footer><!-- /.card-footer -->
                      </section><!-- /.card -->
                    </div><!-- /.page-section -->
                    </div>

                    <div class="tab-pane fade show  " id="details" role="tabpanel" aria-labelledby="details-tab">
                    <header class="page-cover">
                      <div class="text-center">
                        <a href="{{route('supervisor.profile')}}" class="user-avatar user-avatar-xl"><img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt=""></a>
                        <h2 class="h4 mt-2 mb-0"> {{$counsellors->firstname}} {{$counsellors->lastname}}</h2>
                        <div class="my-1">
                          <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i> <i class="fa fa-star-o text-yellow"></i> <i class="fa fa-star-o text-yellow"></i> <i class="fa fa-star-o text-yellow"></i>
                        </div>
                        <p class="text-muted text-link"> <span class="fa fa-fw fa-envelope pulse"></span><b>Email :</b>@<a href="#!" class="text-link disabled">{{$counsellors->email}}</a> </p>
                        <p>  </p>
                      </div><!-- .cover-controls --><!-- /.cover-controls -->
                    </header><!-- /.page-cover -->

                </div>

                </div>
              </div>

            </div><!-- /.page-inner -->


            <!-- .page-sidebar -->
            <div class="page-sidebar">
              <!-- .sidebar-header -->
              <header class="sidebar-header d-sm-none">
                <ol class="breadcrumb mb-0">
                  <li class="breadcrumb-item">
                    <a class="prevent-default" href="#!" onclick="Looper.toggleSidebar()"><i class="breadcrumb-icon fa fa-angle-left mr-2"></i>Back</a>
                  </li>
                </ol>
              </header><!-- /.sidebar-header -->
              <!-- .nav-tabs -->
              <ul class="nav nav-tabs nav-fill">
                <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#team-profile" role="tab" aria-controls="team-profile" aria-selected="true">Contributors</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#message-files" role="tab" aria-controls="message-files" aria-selected="false">Files</a>
                </li>
              </ul><!-- /.nav-tabs -->
              <!-- .sidebar-section-fill -->
              <div class="sidebar-section-fill">
                <!-- Tab panes -->
                <div class="tab-content">
                  <!-- #team-profile -->
                  <div id="team-profile" class="tab-pane fade show active" role="tabpanel" aria-labelledby="team-profile">
                    <!-- .list-group -->
                    <div class="list-group list-group-reflow list-group-flush list-group-divider">
                      <!-- .list-group-header -->
                      <div class="list-group-header"> 8 Members </div><!-- /.list-group-header -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure align-items-start">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces11.jpg" alt="Brenda Griffin"> <span class="avatar-badge online" title="online"></span></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Brenda Griffin</a>
                          </h4>
                          <p class="list-group-item-text"> Medical Assistant </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure align-items-start">
                          <!-- .dropdown -->
                          <div class="dropdown">
                            <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                            <div class="dropdown-arrow"></div>
                            <div class="dropdown-menu dropdown-menu-right">
                              <button type="button" class="dropdown-item">Message</button> <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                              <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                            </div>
                          </div><!-- /.dropdown -->
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure align-items-start">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces12.jpg" alt="Ryan Jimenez"> <span class="avatar-badge online" title="online"></span></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Ryan Jimenez</a>
                          </h4>
                          <p class="list-group-item-text"> Photographer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure align-items-start">
                          <!-- .dropdown -->
                          <div class="dropdown">
                            <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                            <div class="dropdown-arrow"></div>
                            <div class="dropdown-menu dropdown-menu-right">
                              <button type="button" class="dropdown-item">Message</button> <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                              <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                            </div>
                          </div><!-- /.dropdown -->
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure align-items-start">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces13.jpg" alt="Bryan Hayes"> <span class="avatar-badge busy" title="busy"></span></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Bryan Hayes</a>
                          </h4>
                          <p class="list-group-item-text"> Computer Systems Analyst </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure align-items-start">
                          <!-- .dropdown -->
                          <div class="dropdown">
                            <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                            <div class="dropdown-arrow"></div>
                            <div class="dropdown-menu dropdown-menu-right">
                              <button type="button" class="dropdown-item">Message</button> <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                              <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                            </div>
                          </div><!-- /.dropdown -->
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure align-items-start">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces14.jpg" alt="Cynthia Clark"> <span class="avatar-badge idle" title="idle"></span></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Cynthia Clark</a>
                          </h4>
                          <p class="list-group-item-text"> Web Developer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure align-items-start">
                          <!-- .dropdown -->
                          <div class="dropdown">
                            <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                            <div class="dropdown-arrow"></div>
                            <div class="dropdown-menu dropdown-menu-right">
                              <button type="button" class="dropdown-item">Message</button> <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                              <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                            </div>
                          </div><!-- /.dropdown -->
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure align-items-start">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces16.jpg" alt="Martha Myers"> <span class="avatar-badge online" title="online"></span></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Martha Myers</a>
                          </h4>
                          <p class="list-group-item-text"> Writer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure align-items-start">
                          <!-- .dropdown -->
                          <div class="dropdown">
                            <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                            <div class="dropdown-arrow"></div>
                            <div class="dropdown-menu dropdown-menu-right">
                              <button type="button" class="dropdown-item">Message</button> <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                              <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                            </div>
                          </div><!-- /.dropdown -->
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure align-items-start">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces15.jpg" alt="Tammy Beck"> <span class="avatar-badge online" title="online"></span></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Tammy Beck</a>
                          </h4>
                          <p class="list-group-item-text"> Designer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure align-items-start">
                          <!-- .dropdown -->
                          <div class="dropdown">
                            <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                            <div class="dropdown-arrow"></div>
                            <div class="dropdown-menu dropdown-menu-right">
                              <button type="button" class="dropdown-item">Message</button> <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                              <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                            </div>
                          </div><!-- /.dropdown -->
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure align-items-start">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces17.jpg" alt="Susan Kelley"> <span class="avatar-badge busy" title="busy"></span></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Susan Kelley</a>
                          </h4>
                          <p class="list-group-item-text"> Web Developer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure align-items-start">
                          <!-- .dropdown -->
                          <div class="dropdown">
                            <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                            <div class="dropdown-arrow"></div>
                            <div class="dropdown-menu dropdown-menu-right">
                              <button type="button" class="dropdown-item">Message</button> <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                              <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                            </div>
                          </div><!-- /.dropdown -->
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item">
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure align-items-start">
                          <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces18.jpg" alt="Albert Newman"> <span class="avatar-badge offline" title="offline"></span></a>
                        </div><!-- /.list-group-item-figure -->
                        <!-- .list-group-item-body -->
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title">
                            <a href="#!">Albert Newman</a>
                          </h4>
                          <p class="list-group-item-text"> Web Developer </p>
                        </div><!-- /.list-group-item-body -->
                        <!-- .list-group-item-figure -->
                        <div class="list-group-item-figure align-items-start">
                          <!-- .dropdown -->
                          <div class="dropdown">
                            <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                            <div class="dropdown-arrow"></div>
                            <div class="dropdown-menu dropdown-menu-right">
                              <button type="button" class="dropdown-item">Message</button> <button type="button" class="dropdown-item">Invite to a team</button> <button type="button" class="dropdown-item">Copy member ID</button>
                              <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                            </div>
                          </div><!-- /.dropdown -->
                        </div><!-- /.list-group-item-figure -->
                      </div><!-- /.list-group-item -->
                    </div><!-- /.list-group -->
                  </div><!-- /#team-profile -->
                  <!-- #message-files -->
                  <div id="message-files" class="tab-pane fade" role="tabpanel" aria-labelledby="message-files">
                    <!-- .list-group -->
                    <div class="list-group list-group-reflow list-group-flush list-group-divider">
                      <!-- .list-group-header -->
                      <div class="list-group-header"> Today </div><!-- /.list-group-header -->
                      <!-- .list-group-item -->
                      <div class="list-group-item align-items-start">
                        <div class="list-group-item-figure">
                          <a href="#!" class="tile tile-circle bg-teal"><span class="fa fa-file-pdf"></span></a>
                        </div>
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title text-truncate">
                            <a href="#!">double-broccoli-quinoa.pdf</a>
                          </h4>
                          <p class="list-group-item-text"> 1.07 MB by Emma Griffin </p>
                          <p class="list-group-item-text small"> 12:20pm </p>
                        </div>
                        <div class="list-group-item-figure">
                          <!-- .dropdown -->
                          <div class="dropdown">
                            <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                            <div class="dropdown-arrow"></div>
                            <div class="dropdown-menu dropdown-menu-right">
                              <button type="button" class="dropdown-item">Download</button> <button type="button" class="dropdown-item">View details</button> <button type="button" class="dropdown-item">Share file</button> <button type="button" class="dropdown-item">Copy link file</button>
                              <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                            </div>
                          </div><!-- /.dropdown -->
                        </div>
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item align-items-start">
                        <div class="list-group-item-figure">
                          <a href="#!" class="tile tile-circle bg-teal"><span class="fa fa-file-pdf"></span></a>
                        </div>
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title text-truncate">
                            <a href="#!">Baked-Chicken-and-Spinach-Flautas.pdf</a>
                          </h4>
                          <p class="list-group-item-text"> 0.39 MB by Peter Willis </p>
                          <p class="list-group-item-text small"> 5:22pm </p>
                        </div>
                        <div class="list-group-item-figure">
                          <!-- .dropdown -->
                          <div class="dropdown">
                            <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                            <div class="dropdown-arrow"></div>
                            <div class="dropdown-menu dropdown-menu-right">
                              <button type="button" class="dropdown-item">Download</button> <button type="button" class="dropdown-item">View details</button> <button type="button" class="dropdown-item">Share file</button> <button type="button" class="dropdown-item">Copy link file</button>
                              <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                            </div>
                          </div><!-- /.dropdown -->
                        </div>
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item align-items-start">
                        <div class="list-group-item-figure">
                          <a href="#!" class="tile tile-circle bg-pink"><span class="fa fa-file-image"></span></a>
                        </div>
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title text-truncate">
                            <a href="#!">Cajun Chicken Egg Pasta.jpg</a>
                          </h4>
                          <p class="list-group-item-text"> 0.93 MB by Danielle Garza </p>
                          <p class="list-group-item-text small"> 9:32pm </p>
                        </div>
                        <div class="list-group-item-figure">
                          <!-- .dropdown -->
                          <div class="dropdown">
                            <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                            <div class="dropdown-arrow"></div>
                            <div class="dropdown-menu dropdown-menu-right">
                              <button type="button" class="dropdown-item">Download</button> <button type="button" class="dropdown-item">View details</button> <button type="button" class="dropdown-item">Share file</button> <button type="button" class="dropdown-item">Copy link file</button>
                              <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                            </div>
                          </div><!-- /.dropdown -->
                        </div>
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-header -->
                      <div class="list-group-header"> Jan 28, 2018 </div><!-- /.list-group-header -->
                      <!-- .list-group-item -->
                      <div class="list-group-item align-items-start">
                        <div class="list-group-item-figure">
                          <a href="#!" class="tile tile-circle bg-purple"><span class="fa fa-file-archive"></span></a>
                        </div>
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title text-truncate">
                            <a href="#!">looper_build-1209.zip</a>
                          </h4>
                          <p class="list-group-item-text"> 0.53 MB by Rose McCoy </p>
                          <p class="list-group-item-text small"> 3:13am </p>
                        </div>
                        <div class="list-group-item-figure">
                          <!-- .dropdown -->
                          <div class="dropdown">
                            <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                            <div class="dropdown-arrow"></div>
                            <div class="dropdown-menu dropdown-menu-right">
                              <button type="button" class="dropdown-item">Download</button> <button type="button" class="dropdown-item">View details</button> <button type="button" class="dropdown-item">Share file</button> <button type="button" class="dropdown-item">Copy link file</button>
                              <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                            </div>
                          </div><!-- /.dropdown -->
                        </div>
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item align-items-start">
                        <div class="list-group-item-figure">
                          <a href="#!" class="tile tile-circle bg-purple"><span class="fa fa-file-archive"></span></a>
                        </div>
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title text-truncate">
                            <a href="#!">looper_design-brief.zip</a>
                          </h4>
                          <p class="list-group-item-text"> 2.04 MB by Rose McCoy </p>
                          <p class="list-group-item-text small"> 11:27pm </p>
                        </div>
                        <div class="list-group-item-figure">
                          <!-- .dropdown -->
                          <div class="dropdown">
                            <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                            <div class="dropdown-arrow"></div>
                            <div class="dropdown-menu dropdown-menu-right">
                              <button type="button" class="dropdown-item">Download</button> <button type="button" class="dropdown-item">View details</button> <button type="button" class="dropdown-item">Share file</button> <button type="button" class="dropdown-item">Copy link file</button>
                              <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                            </div>
                          </div><!-- /.dropdown -->
                        </div>
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-header -->
                      <div class="list-group-header"> Jan 21, 2018 </div><!-- /.list-group-header -->
                      <!-- .list-group-item -->
                      <div class="list-group-item align-items-start">
                        <div class="list-group-item-figure">
                          <a href="#!" class="tile tile-circle bg-teal"><span class="fa fa-file-pdf"></span></a>
                        </div>
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title text-truncate">
                            <a href="#!">Usulan Beasiswa Tahun 2018.pdf</a>
                          </h4>
                          <p class="list-group-item-text"> 1.1 MB by Peter Willis </p>
                          <p class="list-group-item-text small"> 6:20pm </p>
                        </div>
                        <div class="list-group-item-figure">
                          <!-- .dropdown -->
                          <div class="dropdown">
                            <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                            <div class="dropdown-arrow"></div>
                            <div class="dropdown-menu dropdown-menu-right">
                              <button type="button" class="dropdown-item">Download</button> <button type="button" class="dropdown-item">View details</button> <button type="button" class="dropdown-item">Share file</button> <button type="button" class="dropdown-item">Copy link file</button>
                              <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                            </div>
                          </div><!-- /.dropdown -->
                        </div>
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item align-items-start">
                        <div class="list-group-item-figure">
                          <a href="#!" class="tile tile-circle bg-green"><span class="fa fa-file-excel"></span></a>
                        </div>
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title text-truncate">
                            <a href="#!">Daftar Peserta Ujian.xlsx</a>
                          </h4>
                          <p class="list-group-item-text"> 0.37 MB by Peter Willis </p>
                          <p class="list-group-item-text small"> 1:32pm </p>
                        </div>
                        <div class="list-group-item-figure">
                          <!-- .dropdown -->
                          <div class="dropdown">
                            <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                            <div class="dropdown-arrow"></div>
                            <div class="dropdown-menu dropdown-menu-right">
                              <button type="button" class="dropdown-item">Download</button> <button type="button" class="dropdown-item">View details</button> <button type="button" class="dropdown-item">Share file</button> <button type="button" class="dropdown-item">Copy link file</button>
                              <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                            </div>
                          </div><!-- /.dropdown -->
                        </div>
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-item -->
                      <div class="list-group-item align-items-start">
                        <div class="list-group-item-figure">
                          <a href="#!" class="tile tile-circle bg-blue"><span class="fa fa-file-word"></span></a>
                        </div>
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title text-truncate">
                            <a href="#!">data_not_verified.docx</a>
                          </h4>
                          <p class="list-group-item-text"> 0.94 MB by Kathryn Black </p>
                          <p class="list-group-item-text small"> 5:11am </p>
                        </div>
                        <div class="list-group-item-figure">
                          <!-- .dropdown -->
                          <div class="dropdown">
                            <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                            <div class="dropdown-arrow"></div>
                            <div class="dropdown-menu dropdown-menu-right">
                              <button type="button" class="dropdown-item">Download</button> <button type="button" class="dropdown-item">View details</button> <button type="button" class="dropdown-item">Share file</button> <button type="button" class="dropdown-item">Copy link file</button>
                              <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                            </div>
                          </div><!-- /.dropdown -->
                        </div>
                      </div><!-- /.list-group-item -->
                      <!-- .list-group-header -->
                      <div class="list-group-header"> Jan 19, 2018 </div><!-- /.list-group-header -->
                      <!-- .list-group-item -->
                      <div class="list-group-item align-items-start">
                        <div class="list-group-item-figure">
                          <a href="#!" class="tile tile-circle bg-red"><span class="fa fa-file-powerpoint"></span></a>
                        </div>
                        <div class="list-group-item-body">
                          <h4 class="list-group-item-title text-truncate">
                            <a href="#!">mockup-presentation.pptx</a>
                          </h4>
                          <p class="list-group-item-text"> 0.59 MB by Kathryn Black </p>
                          <p class="list-group-item-text small"> 6:50am </p>
                        </div>
                        <div class="list-group-item-figure">
                          <!-- .dropdown -->
                          <div class="dropdown">
                            <button class="btn btn-sm btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></button>
                            <div class="dropdown-arrow"></div>
                            <div class="dropdown-menu dropdown-menu-right">
                              <button type="button" class="dropdown-item">Download</button> <button type="button" class="dropdown-item">View details</button> <button type="button" class="dropdown-item">Share file</button> <button type="button" class="dropdown-item">Copy link file</button>
                              <div class="dropdown-divider"></div><button type="button" class="dropdown-item">Remove</button>
                            </div>
                          </div><!-- /.dropdown -->
                        </div>
                      </div><!-- /.list-group-item -->
                    </div><!-- /.list-group -->
                  </div><!-- /#message-files -->
                </div><!-- /Tab panes -->
              </div><!-- /.sidebar-section-fill -->
            </div><!-- /.page-sidebar -->
            <br><br>
          </div><!-- /.page -->
        @endforeach
  </div>
@endsection
