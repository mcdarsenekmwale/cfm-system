@extends('supervisor.master.master')

@section('content')
<div class="wrapper py-2">
  <!-- .page -->
  <div class="page">
    <!-- .page-inner -->
    <div class="page-inner">
      <!-- .page-title-bar -->
      <header class="page-title-bar">
        <div class="small text-muted"> <b>NEW CASE
          <span class="fa fa-angle-right fa-lg" style="position: relative; right: -10px;"></span><span class="fa fa-angle-right fa-lg " style="position: relative; right: -10px;"></span> </b>
          <span class="" style="position:absolute; right: 12em;">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">
                    <a href="{{route('call')}}" class="small text-muted active"> <b>New call </b></a>
                </li>
                <li class="breadcrumb-item">
                  <a href="javascript:window.location.reload();" class="small text-muted"> <b>New Case </b></a>
                </li>

            </ol>
          </span>
        </div>
      </header><!-- /.page-title-bar -->
      <!-- .page-section -->

      <!--  validation error checking-->
      @include('notifications')
      <div class="page-section">

        <!-- Regiter New Case: Form -->
        <div class="card fixed-right" id="modalReg" tabindex="-1" role="dialog" aria-hidden="true"  aria-labelledby="myLargeModalLabel" >
          <div class=" " role="document">
            <div class=" html">
              <div class="card-header  align-items-center" style="background-color:#343a40;">
                <h6 class="card-header-title">
                  <span class=" card-title text-center text-uppercase text-white mb-2 "> <b>Register New Case</b></span>
                </h6>
              </div>

              <div class="card-body body px-5">
                <div id="contact_form"></div>
                <form class="k-form k-form--label-right px-5" name="form_regcase" id="form_regcase" novalidate  method="post" action="{{route('savecase')}}" >
                  @csrf

                <div class="row">
                  <div class="col-lg-12">


                    <!--begin::Form-->
                    <div class="col-reg">
                      <div class="k-section py-3">
                        <h3 class="k-section__title small">
                          Client Information:
                        </h3>
                        <div class="k-section__content">
                          <div class=" row">
                            <div class="col-lg-6 form-group">
                              <label class="form-control-label text-muted">* Client Name:</label>
                              <input type="text" name="clientname" class="form-control" placeholder="Client Name" value="{{old('clientname')}}" required="true" autocomplete="on" autofocus>
                            </div>

                            <div class="col-lg-6 form-group">
                              <label class="form-control-label text-muted">*Today's Date:</label>
                              <input type="text" name="date" class="form-control " data-provide="datepicker" placeholder="" value="<?php date_default_timezone_set('Africa/Blantyre'); echo date("F j, Y,")?>">
                            </div>
                          </div>
                          <div class=" row">
                            <div class="form-group col-lg-6">
                              <label class="form-control-label text-muted ">* Traditional Authority:</label>
                              <input type="text" name="tradional_Authority" class="form-control " data-provide="datepicker" placeholder="" value="">
                            </div>
                            <div class="form-group col-lg-6">
                              <label class="form-control-label text-muted ">Gender *</label>
                              <div class="">
                                <div class="k-radio-inline" style="position: relative; top: 10px;">
                                  <label class="k-radio text-muted">
                                    <input type="radio" name="gender" value="Male" checked> Male
                                    <span></span>
                                  </label>
                                  <label class="k-radio text-muted">
                                    <input type="radio" name="gender" value="Female"> Female
                                    <span></span>
                                  </label>
                                  <label class="k-radio text-muted">
                                    <input type="radio" name="gender" value="others"> Others
                                    <span></span>
                                  </label>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="form-group row">
                            <div class="col-lg-6 form-group-sub">
                              <label class="form-control-label text-muted">* District:</label>
                              <select class="form-control" name="district" required>
                                <option value="">Select</option>
                                @foreach($districts as $district)
                                <option value="{{$district->id}}" >{{$district->name}}</option>
                                @endforeach
                              </select>
                            </div>
                            <div class="col-lg-6 form-group-sub">
                              <label class="form-control-label text-muted">* Age:</label>
                              <input type="phone" name="age" class="form-control" placeholder="years old" value="{{old('age')}}"  required>
                            </div>
                          </div>
                          <div class="form-group form-group-last row">
                            <div class="col-lg-6 form-group-sub">
                              <label class="form-control-label text-muted">* Nature :</label>
                              <select class="form-control" name="nature" required>
                                <option value="">Select</option>
                                @foreach($reasons as $nature)
                                <option value="{{$nature}}" >{{$nature}}</option>
                                @endforeach
                              </select>
                            </div>
                            <div class="col-lg-6">
                              <label class="form-control-label text-muted">* Pepetrator:</label>
                              <select class="form-control" name="perpetrator">
                                <option value="">Select</option>
                                @foreach($perpetrators as $perpetrator)
                                <option value="{{$perpetrator->id}}">{{$perpetrator->name}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                      <hr>
                      <div class="k-separator k-separator--border-dashed k-separator--space-xl"></div>

                      <div class="k-section">
                        <div class="k-section__content">
                          <div class=" row">
                            <div class="col-lg-6 form-group col-issue">
                              @foreach($issues->issues as $issue)
                              <label class="form-control-label text-muted border-bottom"><b>*{{$issue->name}} :</b></label>
                              @foreach($issue->subissues as $subissue)
                              <div class="  k-checkbox-list text-muted">
                                <label class="k-checkbox small-issue">
                                  <input type="checkbox" name="subissue" value="{{$subissue->id}}" > {{$subissue->name}}
                                  <span></span>
                                </label>
                              </div>
                              @endforeach
                              @endforeach
                            </div>
                            <div class="col-lg-6 form-group border-left">
                              <label class="form-control-label text-muted border-bottom"> <b>*CFM Channel :</b></label>
                              @foreach($cfmChannels as $cfmChannel)
                              <div class="  k-radio-list text-muted">
                                <label class="k-radio small-issue">
                                  <input type="radio" name="cfm" value="{{$cfmChannel->id}}" > {{$cfmChannel->Name}}
                                  <span></span>
                                </label>
                              </div>
                              @endforeach
                            </div>
                          </div>
                        </div>
                        <hr>
                        <div class="k-separator k-separator--border-dashed k-separator--space-xl"></div>

                        <div class="form-group row">
                          <label class="col-form-label col-lg-3 col-sm-12 text-muted">Case Brief *</label>
                          <div class="col-lg-9 col-md-9 col-sm-12">
                            <textarea class="form-control" name="brief" spellcheck="true" value="{{ old('brief')}}" placeholder="Enter a case summary" rows="6" required></textarea>
                            <span class="form-text text-muted">Please enter a case summary </span>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-form-label col-lg-3 col-sm-12 text-muted">Action Taken Brief **</label>
                          <div class="col-lg-9 col-md-9 col-sm-12">
                            <textarea class="form-control" name="action" spellcheck="true" value="{{ old('action')}}" placeholder="Enter a summary of an action taken" rows="6" required></textarea>
                            <span class="form-text text-muted">Please enter a summary of an action taken </span>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-form-label col-lg-3 col-sm-12 text-muted">Outcome Of Case **</label>
                          <div class="col-lg-9 col-md-9 col-sm-12">
                            <textarea class="form-control" name="outcome" spellcheck="true" value="{{ old('outcome')}}" placeholder="Enter an outcome of the case" rows="6" required></textarea>
                            <span class="form-text text-muted">Please enter an aoutcome of the case </span>
                          </div>
                        </div>
                        <hr>
                        <div class="form-group row  float-right">
                          <button type="button" class="btn btn-outline-primary btn-sm" >Cancel</button>
                          <button type="reset" class="btn btn-outline-primary btn-sm" id="reset">Reset</button>
                          <button type="submit" class="btn btn-outline-success btn-sm" >Save</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <br><br>
    </div><!-- /.page-section -->
  </div><!-- /.page-inner -->
</div><!-- /.page -->
</div>
@endsection
