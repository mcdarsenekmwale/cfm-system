@extends('supervisor.master.master')
@section('content')
<div class="wrapper py-2">
  <!-- .page -->
  <div class="page">
    <!-- .page-inner -->
    <div class="page-inner">
      <!-- .page-title-bar -->
      @include('supervisor.master.casemodal')
      <!-- .page-navs -->

      <!--  validation error checking-->
      @include('notifications')
      <div class="page-section">

        <!-- Regiter New Case: Form -->
        <div class="card fixed-right" id="modalReg" tabindex="-1" role="dialog" aria-hidden="true"  aria-labelledby="myLargeModalLabel" >
          <div class=" " role="document">
            <div class=" html">
              <div class="card-header  align-items-center" style="background-color:#343a40;">
                <h6 class="card-header-title">
                  <span class=" card-title text-center text-uppercase text-white mb-2 "> <b>Edit Case</b></span>
                </h6>
              </div>

              <div class="card-body body px-5">
                <div id="contact_form"></div>
                <form class="k-form k-form--label-right px-5" name="form_editcase"  novalidate  method="post" action="{{route('saveEdit')}}?id={{$callDetails->id}}" >
                  @csrf

                <div class="row">
                  <div class="col-lg-12">


                    <!--begin::Form-->
                    <div class="col-reg">
                      <div class="k-section py-3">
                        <h3 class="k-section__title small">
                          Client Information:
                        </h3>
                        <div class="k-section__content">
                          <div class=" row">
                            <div class="col-lg-6 form-group">
                              <label class="form-control-label text-muted">* Client Name:</label>
                              <input type="text" name="clientname" class="form-control"  value="{{$callDetails->client->name}}" required="true" autocomplete="on" disabled>
                            </div>

                            <div class="col-lg-6 form-group">
                              <label class="form-control-label text-muted">*Captured Date:</label>
                              <input type="text" name="date" class="form-control " data-provide="datepicker" placeholder="" value="{{$callDetails->date}}" disabled>
                            </div>
                          </div>
                          <div class=" row">
                            <div class="form-group col-lg-6">
                              <label class="form-control-label text-muted ">* Traditional Authority:</label>
                              <input type="text" name="tradional_Authority" class="form-control " data-provide="datepicker" placeholder="" value="">
                            </div>

                            <div class="form-group col-lg-6">
                              <label class="form-control-label text-muted ">Gender *</label>
                              <input type="text" name="gender" class="form-control" placeholder="years old" value="{{$callDetails->client->gender}}"  disabled>
                            </div>
                          </div>
                          <div class="form-group row">
                            <div class="col-lg-6 form-group-sub">
                              <label class="form-control-label text-muted">* District:</label>
                              <input type="phone" name="district" class="form-control" placeholder="district" value="{{$callDetails->ta->district->name }}"  disabled>
                            </div>
                            <div class="col-lg-6 form-group-sub">
                              <label class="form-control-label text-muted">* Age:</label>
                              <input type="phone" name="age" class="form-control" placeholder="years old" value="{{$callDetails->client->age}}"  disabled>
                            </div>
                          </div>
                          <div class="form-group form-group-last row">
                            <div class="col-lg-6 form-group-sub">
                              <label class="form-control-label text-muted">* Nature :</label>
                              <input type="text" name="nature" class="form-control" placeholder="years old" value="{{$callDetails->nature}}"  disabled>
                            </div>
                            <div class="col-lg-6">
                              <label class="form-control-label text-muted">* Partner:</label>
                              <input type="text" name="partner" class="form-control" placeholder="years old" value="{{$callDetails->partner->name}}"  disabled>
                            </div>
                          </div>

                          @if($callDetails->partner_id === 0)
                          <div class="form-group form-group-last row">
                            <div class="col-lg-6 form-group ">
                              <label class="form-control-label text-muted" for="form-field-1-1" data-toggle="tooltip" title=" make sure you select a partner here">*Select Partner :</label>
                              @foreach($partners as $partner)
                              @if($partner->id !=0)
                              <div class="  k-radio-list text-muted">
                                <label class="k-radio small-issue">
                                  <input type="radio" name="partner" value="{{$partner->id}}" > {{$partner->name}}
                                  <span></span>
                                </label>
                              </div>
                              @else

                              @endif
                              @endforeach
                            </div>
                            <div class="col-lg-6 form-group ">
                              <label class="form-control-label text-muted border-bottom">*Intervention :</label>
                              @foreach($callDetails->interventions as $intervention)
                              <div class="  k-radio-list text-muted">
                                <label class="k-checkbox small-issue">
                                  <input type="checkbox" name="intervention" value="{{$intervention->id}}" disabled> {{$intervention->name}}
                                  <span></span>
                                </label>
                              </div>
                              @endforeach
                            </div>

                            @endif

                          </div>
                        </div>
                      </div>
                      <hr>
                      <div class="k-separator k-separator--border-dashed k-separator--space-xl"></div>

                      <div class="k-section">
                        <div class="k-section__content">
                          <div class=" row">
                            <div class="col-lg-6 form-group">
                              @foreach($callDetails->subissues as $subissue)
                              <label class="form-control-label text-muted border-bottom h6">*{{$subissue->issue->name}}:</label>
                              <div class="  k-checkbox-list text-muted">
                                <label class="k-checkbox small-issue">
                                  <input type="checkbox" name="subissue" value="{{$subissue->id}}" > {{$subissue->name}}
                                  <span></span>
                                </label>
                              </div>
                              @endforeach
                            </div>
                            <div class="col-lg-6 form-group border-left">
                              <label class="form-control-label text-muted border-bottom h6">*Referral :</label>
                              @foreach($callDetails->referrals as $referral)
                              <div class="  k-radio-list text-muted">
                                <label class="k-checkbox small-issue">
                                  <input type="checkbox" name="intervention" value="{{$referral->id}}" disabled> {{$referral->name}}
                                  <span></span>
                                </label>
                              </div>
                              @endforeach
                            </div>
                          </div>
                        </div>
                        <hr>
                        <div class="k-separator k-separator--border-dashed k-separator--space-xl"></div>

                        <div class="form-group row">
                          <label class="col-form-label col-lg-3 col-sm-12 text-muted">Case Brief *</label>
                          <div class="col-lg-9 col-md-9 col-sm-12">
                            <textarea class="form-control" name="brief" spellcheck="true" value="{{old('outcome')}}" placeholder="{{$callDetails->comments}}" rows="6" required>{{$callDetails->comments}}</textarea>
                            <span class="form-text text-muted">Please enter a case summary </span>
                          </div>
                        </div>
                        @if($outcomeCount != 0)
                            <div class="form-group row">
                              <label class="col-form-label col-lg-3 col-sm-12 text-muted">Action Taken Brief **</label>
                              <div class="col-lg-9 col-md-9 col-sm-12">
                                <textarea class="form-control" name="action" spellcheck="true" value="{{old('outcome')}}" placeholder="{{$callDetails->outcome->action_taken}}" rows="6" required>{{$callDetails->outcome->action_taken}}</textarea>
                                <span class="form-text text-muted">Please enter a summary of an action taken </span>
                              </div>
                            </div>
                            <div class="form-group row">
                          <label class="col-form-label col-lg-3 col-sm-12 text-muted">Outcome Of Case **</label>
                          <div class="col-lg-9 col-md-9 col-sm-12">
                            <textarea class="form-control" name="outcome" spellcheck="true" value="{{ old('outcome')}}" placeholder="{{$callDetails->outcome->outcome}}" rows="6" required>{{ $callDetails->outcome->outcome}}</textarea>
                            <span class="form-text text-muted">Please enter an aoutcome of the case </span>
                          </div>
                        </div>
                        @endif
                        <hr>
                        <div class="form-group row  float-right">
                          <button type="button" class="btn btn-outline-primary btn-sm" onclick="javascript: history.back();">Cancel</button>
                          <button type="reset" class="btn btn-outline-secondary btn-sm" id="reset">Reset</button>
                          @if(true)
                            <button type="reset" class=" btn btn-outline-warning btn-sm" >Inpend Case</button>
                            <input name="status" value="{{$callDetails->status}}" type="hidden">
                          @else
                          <input name="status" value="{{$callDetails->status}}" type="hidden">
                          @endif

                          <button type="submit" class="btn btn-outline-success btn-sm" >Save</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <br><br>
    </div><!-- /.page-section -->
  </div><!-- /.page-inner -->
</div><!-- /.page -->
</div>
@endsection
