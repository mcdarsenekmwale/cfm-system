<!-- .app-aside -->
<aside class="app-aside app-aside-expand-md app-aside-light py-4 card-color">
  <!-- .aside-content -->
  <div class="aside-content">
    <!-- .aside-header -->
    <header class="aside-header d-block d-md-none">
      <!-- .btn-account -->
      <button class="btn-account" type="button" data-toggle="collapse" data-target="#dropdown-aside"><span class="user-avatar user-avatar-lg"><img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt=""></span> <span class="account-icon"><span class="fa fa-caret-down fa-lg"></span></span> <span class="account-summary"><span class="account-name">{{Auth::user()->firstname}} {{Auth::user()->lastname}}</span>
        <span class="account-description">{{Auth::user()->type}}</span></span></button>
      <!-- .dropdown-aside -->
      <div id="dropdown-aside" class="dropdown-aside collapse">
        <!-- dropdown-items -->
        <div class="pb-3">
          <a class="dropdown-item" href="{{route('profile')}}"><span class="dropdown-icon fa fa-user-circle"></span> Profile</a> <a class="dropdown-item" href="{{ route('logout') }}"><span class="dropdown-icon fa fa-sign-out"></span> Logout</a>
          <div class="dropdown-divider"></div><a class="dropdown-item" href="#!">Help Center</a> <a class="dropdown-item" href="#!">Ask Forum</a> <a class="dropdown-item" href="#!">Keyboard Shortcuts</a>
        </div><!-- /dropdown-items -->
      </div><!-- /.dropdown-aside -->
    </header><!-- /.aside-header -->
    <!-- .aside-menu -->
    <section class="aside-menu overflow-hidden">
      <!-- .stacked-menu -->
      <nav id="stacked-menu" class="stacked-menu stacked-menu-has-collapsible">
        <!-- .menu -->
        <ul class="nav flex-column px-4  py-5 border-bottom">
          <li class="nav-item active">
            <a class="nav-link nav-i " href="{{url('/')}}">
              <span data-feather="home"></span>
              Dashboard <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-i" href="{{route('supervisor.partners')}}">
              <span data-feather="users"></span>
              Partners

            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link nav-i" href="{{route('supervisor.newreport')}}">
              <span data-feather="file-text"></span>
              Reports
              <span class="badge badge-success float-right "> <span class="pulse">new</span></span>
            </a>

          </li>
          <li class="nav-item">
            <a class="nav-link nav-i" href="{{{route('integrations')}}}">
              <span data-feather="layers"></span>
              Integrations
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-i" href="{{{route('supervisor.calendar')}}}">
              <span data-feather="calendar"></span>
              Calendar
            </a>
          </li>
          <li class="nav-item">
            <a href="#collapseAnalytics" data-toggle="collapse" class=" nav-link nav-i"><span data-feather="activity"></span> <span class="menu-text">Analytics</span><i class="fa fa-caret-square-o-right  fa-fw float-right"></i></a> <!-- child menu -->
            <ul class="collapse list-unstyled px-4" id="collapseAnalytics"><li class="menu-subhead">Analytics</li>
              <li class="menu-item">
                <a href="{{route('supervisor.charts')}}" class="nav-link nav-i" tabindex="-1"><i data-feather="pie-chart"></i> Charts</a>
              </li>
              <li class="menu-item">
                <a href="{{route('supervisor.maps')}}" class="nav-link nav-i" tabindex="-1"><i data-feather="globe"></i> Map</a>
              </li>
            </ul><!-- /child menu -->
          </li>
          <li class="nav-item">
            <a href="#collapseCouncellors" data-toggle="collapse" class=" nav-link nav-i"><span data-feather="users"></span> <span class="menu-text">Councellors</span><i class="fa fa-caret-square-o-right  fa-fw float-right"></i></a> <!-- child menu -->
            <ul class="collapse list-unstyled px-4" id="collapseCouncellors"><li class="menu-subhead">Councellors</li>
              <li class="menu-item">
                <a href="{{route('supervisor.online')}}" class="nav-link nav-i" tabindex="-1"><i class="text-success fa fa-circle fa-fw pulse"></i> Online</a>
              </li>
              <li class="menu-item">
                <a href="{{route('supervisor.active')}}" class="nav-link nav-i" tabindex="-1"><i class="fa fa-circle fa-fw "></i> Active</a>
              </li>
            </ul><!-- /child menu -->
          </li>
          <li class="nav-item">
            <a href="#collapseCases" data-toggle="collapse" class=" nav-link nav-i"><span data-feather="copy"></span> <span class="menu-text">Cases</span><i class="fa fa-caret-square-o-right  fa-fw float-right"></i></a> <!-- child menu -->
            <ul class="collapse list-unstyled px-3" id="collapseCases"><li class="menu-subhead">Cases</li>
              <li class="menu-item">
                <a href="{{route('supervisor.resolvedcases')}}/?status=3" class="nav-link nav-i" tabindex="-1"><i data-feather="folder-plus"></i> Resolved Cases</a>
              </li>
              <li class="menu-item">
                <a href="{{route('supervisor.unresolvedcases')}}/?status=2" class="nav-link nav-i" tabindex="-1"><i data-feather="folder-minus"></i>  Unresolved Cases</a>
              </li>
              <li class="menu-item">
                <a href="{{route('supervisor.pendingcases')}}/?status=4" class="nav-link nav-i" tabindex="-1"><i data-feather="folder"></i>  Pending Cases</a>
              </li>
              <li  class="dropdown-divider"></li>
              <li class="menu-item">
                <a href="{{route('supervisor.unrefferedcases')}}/?status=2" class="nav-link nav-i" tabindex="-1"><i data-feather="folder-minus"></i> Unreffered Cases</a>
              </li>
              <li class="menu-item">
                <a href="{{route('supervisor.refferedcases')}}/?status=1" class="nav-link nav-i" tabindex="-1"><i data-feather="folder-plus"></i> Reffered Cases</a>
              </li>
            </ul><!-- /child menu -->
          </li>

        </ul>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <span>Saved reports</span>
          <a class="d-flex align-items-center text-muted" href="#">
            <span data-feather="plus-circle"></span>
          </a>
        </h6>
        <ul class="nav flex-column mb-2">
          <li class="nav-item">
            <a class="nav-link" href="#">
              <span data-feather="file-text"></span>
              Current month
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <span data-feather="file-text"></span>
              Last quarter
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <span data-feather="file-text"></span>
              Social engagement
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <span data-feather="file-text"></span>
              Year-end sale
            </a>
          </li>
        </ul>

      </nav><!-- /.stacked-menu -->
    </section><!-- /.aside-menu -->
    <!-- /Skin changer -->
  </div><!-- /.aside-content -->
</aside><!-- /.app-aside -->
