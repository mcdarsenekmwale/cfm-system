<header class="page-cover">
  <div class="text-center">
    <a href="{{route('supervisor.profile')}}" class="user-avatar user-avatar-xl"><img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt=""></a>
    <h2 class="h4 mt-2 mb-0"> {{Auth::user()->firstname}} {{Auth::user()->lastname}}</h2>
    <div class="my-1">
      @if(Auth::user()->username == 'McDarsenek')
        <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i>
      @else
      <i class="fa fa-star text-yellow"></i> <i class="fa fa-star text-yellow"></i> <i class="fa fa-star-o text-yellow"></i> <i class="fa fa-star-o text-yellow"></i> <i class="fa fa-star-o text-yellow"></i>
      @endif
    </div>
    <p class="text-muted text-capitalize"> {{Auth::user()->type}} </p>
    <p>  </p>
  </div><!-- .cover-controls --><!-- /.cover-controls -->
  <div class="cover-controls cover-controls-bottom">
    <a href="#!" class="btn btn-light" data-toggle="modal" data-target="#RequestModal"><span class="fa fa-fw fa-bell pulse"></span> 2,159 Notifications</a>
     <a href="#!" class="btn btn-light" data-toggle="modal" data-target="#MessagesModal"><span class="fa fa-fw fa-envelope pulse"></span>136 Messages</a>
  </div><!-- /.cover-controls -->
</header><!-- /.page-cover -->
<!-- Followers Modal -->
<!-- .modal -->
<div class="modal fade " id="RequestModal" tabindex="-1" role="dialog" aria-labelledby="RequestModalLabel" aria-hidden="true">
  <!-- .modal-dialog -->
  <div class="modal-dialog modal-dialog-vert" role="document">
    <!-- .modal-content -->
    <div class="modal-content">
      <!-- .modal-header -->
      <div class="modal-header">
        <h6 class="modal-title"> Followers </h6>
      </div><!-- /.modal-header -->
      <!-- .modal-body -->
      <div class="modal-body px-0">
        <!-- .list-group -->
        <div class="list-group list-group-flush list-group-divider border">

          <!-- .list-group-item -->
          <div class="list-group-item">
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces7.jpg" alt="Julia Silva"></a>
            </div><!-- /.list-group-item-figure -->
            <!-- .list-group-item-body -->
            <div class="list-group-item-body">
              <h4 class="list-group-item-title">
                <a href="#!">Julia Silva</a>
              </h4>
              <p class="list-group-item-text"> Photographer </p>
            </div><!-- /.list-group-item-body -->
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <button type="button" class="btn btn-sm btn-primary">Follow</button>
            </div><!-- /.list-group-item-figure -->
          </div><!-- /.list-group-item -->
          <!-- .list-group-item -->
          <div class="list-group-item">
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces1.jpg" alt="Joe Hanson"></a>
            </div><!-- /.list-group-item-figure -->
            <!-- .list-group-item-body -->
            <div class="list-group-item-body">
              <h4 class="list-group-item-title">
                <a href="#!">Joe Hanson</a>
              </h4>
              <p class="list-group-item-text"> Logistician </p>
            </div><!-- /.list-group-item-body -->
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <button type="button" class="btn btn-sm btn-secondary">Follow</button>
            </div><!-- /.list-group-item-figure -->
          </div><!-- /.list-group-item -->
          <!-- .list-group-item -->
          <div class="list-group-item">
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces11.jpg" alt="Brenda Griffin"></a>
            </div><!-- /.list-group-item-figure -->
            <!-- .list-group-item-body -->
            <div class="list-group-item-body">
              <h4 class="list-group-item-title">
                <a href="#!">Brenda Griffin</a>
              </h4>
              <p class="list-group-item-text"> Medical Assistant </p>
            </div><!-- /.list-group-item-body -->
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <button type="button" class="btn btn-sm btn-primary">Follow</button>
            </div><!-- /.list-group-item-figure -->
          </div><!-- /.list-group-item -->
          <!-- .list-group-item -->
          <div class="list-group-item">
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces12.jpg" alt="Ryan Jimenez"></a>
            </div><!-- /.list-group-item-figure -->
            <!-- .list-group-item-body -->
            <div class="list-group-item-body">
              <h4 class="list-group-item-title">
                <a href="#!">Ryan Jimenez</a>
              </h4>
              <p class="list-group-item-text"> Photographer </p>
            </div><!-- /.list-group-item-body -->
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <button type="button" class="btn btn-sm btn-primary">Follow</button>
            </div><!-- /.list-group-item-figure -->
          </div><!-- /.list-group-item -->
          <!-- .list-group-item -->
          <div class="list-group-item">
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces13.jpg" alt="Bryan Hayes"></a>
            </div><!-- /.list-group-item-figure -->
            <!-- .list-group-item-body -->
            <div class="list-group-item-body">
              <h4 class="list-group-item-title">
                <a href="#!">Bryan Hayes</a>
              </h4>
              <p class="list-group-item-text"> Computer Systems Analyst </p>
            </div><!-- /.list-group-item-body -->
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <button type="button" class="btn btn-sm btn-primary">Follow</button>
            </div><!-- /.list-group-item-figure -->
          </div><!-- /.list-group-item -->
          <!-- .list-group-item -->
          <div class="list-group-item">
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces14.jpg" alt="Cynthia Clark"></a>
            </div><!-- /.list-group-item-figure -->
            <!-- .list-group-item-body -->
            <div class="list-group-item-body">
              <h4 class="list-group-item-title">
                <a href="#!">Cynthia Clark</a>
              </h4>
              <p class="list-group-item-text"> Web Developer </p>
            </div><!-- /.list-group-item-body -->
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <button type="button" class="btn btn-sm btn-primary">Follow</button>
            </div><!-- /.list-group-item-figure -->
          </div><!-- /.list-group-item -->
          <!-- .list-group-item -->


        </div><!-- /.list-group -->
        <!-- .loading -->
        <div class="loading border-bottom">
          <div class="loader loader-sm fa-spin"></div>
          <div class="sr-only"> Loading more content </div>
        </div><!-- /.loading -->
      </div><!-- /.modal-body -->
      <!-- .modal-footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger btn-sm" data-dismiss="modal">Close</button>
      </div><!-- /.modal-footer -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /Followers Modal -->
<!-- Following Modal -->
<!-- .modal -->
<div class="modal fade " id="MessagesModal" tabindex="-1" role="dialog" aria-labelledby="MessagesModalLabel" aria-hidden="true">
  <!-- .modal-dialog -->
  <div class="modal-dialog modal-dialog-vert" role="document">
    <!-- .modal-content -->
    <div class="modal-content">
      <!-- .modal-header -->
      <div class="modal-header">
        <h6 class="modal-title"> Messages </h6>
      </div><!-- /.modal-header -->
      <!-- .modal-body -->
      <div class="modal-body px-0">
        <!-- .list-group -->
        <div class="list-group list-group-flush list-group-divider border">
          <!-- .list-group-item -->
          <div class="list-group-item">
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces2.jpg" alt="Johnny Day"></a>
            </div><!-- /.list-group-item-figure -->
            <!-- .list-group-item-body -->
            <div class="list-group-item-body">
              <h4 class="list-group-item-title">
                <a href="#!">Johnny Day</a>
              </h4>
              <p class="list-group-item-text"> Computer Hardware Engineer </p>
            </div><!-- /.list-group-item-body -->
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <button class="btn btn-sm btn-secondary">Following</button>
            </div><!-- /.list-group-item-figure -->
          </div><!-- /.list-group-item -->
          <!-- .list-group-item -->
          <div class="list-group-item">
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces3.jpg" alt="Sarah Bishop"></a>
            </div><!-- /.list-group-item-figure -->
            <!-- .list-group-item-body -->
            <div class="list-group-item-body">
              <h4 class="list-group-item-title">
                <a href="#!">Sarah Bishop</a>
              </h4>
              <p class="list-group-item-text"> Designer </p>
            </div><!-- /.list-group-item-body -->
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <button class="btn btn-sm btn-secondary">Following</button>
            </div><!-- /.list-group-item-figure -->
          </div><!-- /.list-group-item -->
          <!-- .list-group-item -->
          <div class="list-group-item">
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces13.jpg" alt="Bryan Hayes"></a>
            </div><!-- /.list-group-item-figure -->
            <!-- .list-group-item-body -->
            <div class="list-group-item-body">
              <h4 class="list-group-item-title">
                <a href="#!">Bryan Hayes</a>
              </h4>
              <p class="list-group-item-text"> Computer Systems Analyst </p>
            </div><!-- /.list-group-item-body -->
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <button class="btn btn-sm btn-secondary">Following</button>
            </div><!-- /.list-group-item-figure -->
          </div><!-- /.list-group-item -->
          <!-- .list-group-item -->
          <div class="list-group-item">
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces14.jpg" alt="Cynthia Clark"></a>
            </div><!-- /.list-group-item-figure -->
            <!-- .list-group-item-body -->
            <div class="list-group-item-body">
              <h4 class="list-group-item-title">
                <a href="#!">Cynthia Clark</a>
              </h4>
              <p class="list-group-item-text"> Web Developer </p>
            </div><!-- /.list-group-item-body -->
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <button class="btn btn-sm btn-secondary">Following</button>
            </div><!-- /.list-group-item-figure -->
          </div><!-- /.list-group-item -->
          <!-- .list-group-item -->
          <div class="list-group-item">
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces6.jpg" alt="Martha Myers"></a>
            </div><!-- /.list-group-item-figure -->
            <!-- .list-group-item-body -->
            <div class="list-group-item-body">
              <h4 class="list-group-item-title">
                <a href="#!">Martha Myers</a>
              </h4>
              <p class="list-group-item-text"> Writer </p>
            </div><!-- /.list-group-item-body -->
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <button class="btn btn-sm btn-secondary">Following</button>
            </div><!-- /.list-group-item-figure -->
          </div><!-- /.list-group-item -->
          <!-- .list-group-item -->
          <div class="list-group-item">
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces5.jpg" alt="Tammy Beck"></a>
            </div><!-- /.list-group-item-figure -->
            <!-- .list-group-item-body -->
            <div class="list-group-item-body">
              <h4 class="list-group-item-title">
                <a href="#!">Tammy Beck</a>
              </h4>
              <p class="list-group-item-text"> Designer </p>
            </div><!-- /.list-group-item-body -->
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <button class="btn btn-sm btn-secondary">Following</button>
            </div><!-- /.list-group-item-figure -->
          </div><!-- /.list-group-item -->
          <!-- .list-group-item -->
          <div class="list-group-item">
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces7.jpg" alt="Susan Kelley"></a>
            </div><!-- /.list-group-item-figure -->
            <!-- .list-group-item-body -->
            <div class="list-group-item-body">
              <h4 class="list-group-item-title">
                <a href="#!">Susan Kelley</a>
              </h4>
              <p class="list-group-item-text"> Web Developer </p>
            </div><!-- /.list-group-item-body -->
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <button class="btn btn-sm btn-secondary">Following</button>
            </div><!-- /.list-group-item-figure -->
          </div><!-- /.list-group-item -->
          <!-- .list-group-item -->
          <div class="list-group-item">
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces1.jpg" alt="Albert Newman"></a>
            </div><!-- /.list-group-item-figure -->
            <!-- .list-group-item-body -->
            <div class="list-group-item-body">
              <h4 class="list-group-item-title">
                <a href="#!">Albert Newman</a>
              </h4>
              <p class="list-group-item-text"> Web Developer </p>
            </div><!-- /.list-group-item-body -->
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <button class="btn btn-sm btn-secondary">Following</button>
            </div><!-- /.list-group-item-figure -->
          </div><!-- /.list-group-item -->
          <!-- .list-group-item -->
          <div class="list-group-item">
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <a href="#!" class="user-avatar"><img src="http://uselooper.com/assets/images/avatars/uifaces9.jpg" alt="Kyle Grant"></a>
            </div><!-- /.list-group-item-figure -->
            <!-- .list-group-item-body -->
            <div class="list-group-item-body">
              <h4 class="list-group-item-title">
                <a href="#!">Kyle Grant</a>
              </h4>
              <p class="list-group-item-text"> Designer </p>
            </div><!-- /.list-group-item-body -->
            <!-- .list-group-item-figure -->
            <div class="list-group-item-figure">
              <button class="btn btn-sm btn-secondary">Following</button>
            </div><!-- /.list-group-item-figure -->
          </div><!-- /.list-group-item -->
        </div><!-- /.list-group -->
        <!-- .loading -->
        <div class="loading border-bottom">
          <div class="loader loader-sm"></div>
          <div class="sr-only"> Loading more content </div>
        </div><!-- /.loading -->
      </div><!-- /.modal-body -->
      <!-- .modal-footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger btn-sm" data-dismiss="modal">Close</button>
      </div><!-- /.modal-footer -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /Following Modal -->
