@extends('supervisor.master.master')
@section('content')
<div class="wrapper py-2">
  <!-- .page -->
  <div class="page">
    <!-- .page-inner -->
    <div class="page-inner">
      <!-- .page-title-bar -->
      @include('supervisor.master.casemodal')
      <!-- .page-section -->

      <!--  validation error checking-->
      @include('notifications')
      <div class="page-section">

        <!-- Regiter New Case: Form -->
        <div class="card fixed-right" id="modalReg" tabindex="-1" role="dialog" aria-hidden="true"  aria-labelledby="myLargeModalLabel" >
          <div class=" " role="document">
            <div class=" html">
              <div class="card-header  align-items-center" style="background-color:#343a40;">
                <h6 class="card-header-title">
                  <span class=" card-title text-center text-uppercase text-white mb-2 "> <b>View Case</b></span>
                </h6>
              </div>

              <div class="card-body body px-5">
                <div id="contact_form"></div>
                <div class="py-5">
                  <div class="text-left">
                      <div class=" row">

                        <div class=" col-6">
                          <p class="text-muted "><strong class="text-uppercase">Client Name :</strong> <span class="ml-2">{{$callDetails->client->name}}</span></p>
                          <p class="text-muted "><strong class="text-uppercase">Age :</strong> <span class="ml-2">{{$callDetails->client->age}} </span></p>
                          <p class="text-muted "><strong class="text-uppercase">Nature :</strong><span class="ml-2">{{$callDetails->nature}}  </span>
                          </p>
                          <p class="text-muted "><strong class="text-uppercase">Issue :</strong>
                            @foreach($callDetails->subissues as $subissue)
                              <span class="ml-2">{{$subissue->issue->name}}</span>
                            @endforeach

                          </p>
                          <p class="text-muted "><strong class="text-uppercase">Interventions :</strong>

                            @foreach($callDetails->Interventions as $intervention)
                              <span class="ml-2">{{$intervention->name}}</span>
                            @endforeach
                          </p>
                      </div>
                      <div class=" col-6">
                        <p class="text-muted ">
                        <strong class="text-uppercase">   Gender :</strong> <span class="ml-2">{{$callDetails->client->gender}}</span></p>
                         <p class="text-muted "><strong class="text-uppercase">Mobile :</strong><span class="ml-2">(265)  {{$callDetails->client->phonenumber}}</span></p>
                          <p class="text-muted "><strong class="text-uppercase">District :</strong> <span class="ml-2">{{$callDetails->ta->district->name}}</span></p>
                          <p class="text-muted "><strong class="text-uppercase">Subissue :</strong>
                            @foreach($callDetails->subissues as $subissue)
                              <span class="ml-2">{{$subissue->name}}</span>
                            @endforeach
                          </p>
                          <p class="text-muted "><strong class="text-uppercase">Resolved Case:</strong> <span class="ml-2 ">{{date("d/m/Y", strtotime($callDetails->date))}}</span></p>
                      </div>
                    </div>
                      <p class="text-muted mb-0"><strong class="text-uppercase">Comments :</strong>
                      </p>

                    </div>
                    <p class="text-muted font-13">
                       {{$callDetails->comments}}
                    </p>
                    @if($outcomeCount != 0)
                    <p class="text-muted mb-0"><strong class="text-uppercase">Action Taken :</strong>
                    </p>
                    <p class="text-muted font-13">
                      {{$callDetails->outcome->action_taken}}
                     </p>
                    <p class="text-muted mb-0 "><strong class="text-uppercase">Outcome :</strong>
                    </p>
                  <p class="text-muted font-13">
                        {{$callDetails->outcome}}
                  </p>
                  @endif
                  <p class="text-muted small">
                    <p class="text-muted small">
                    <p class="text-muted "><strong></strong> <span class="ml-2"></span></p>
                </div>
                <hr>
                <div class=" ">
                  <button type="button" class="btn btn-outline-secondary btn-sm" onclick="javascript: history.back();">Cancel</button>
                  <button type="button" class="btn btn-outline-danger btn-sm" id="caseDelete">Delete </button>
                  <button type="button" class="btn btn-outline-primary btn-sm" onclick="javascript:window.location.href='/caseEdit?id={{$callDetails->id}}';">Edit </button>
                  <button type="button" class="btn btn-outline-success btn-sm">Export </button>
                </div>
                <br>
            </div>

          </div>
        </div>
      </div>
      <br><br>
    </div><!-- /.page-section -->
  </div><!-- /.page-inner -->
</div><!-- /.page -->
</div>
@endsection
