@extends('supervisor.master.master')

<script src="https://plugins.tinymce.com/powerpaste/stable/js/wordimport.js" type="text/javascript" async="async"></script>
@section('content')

<div class="wrapper">
  <!-- .page -->
  <div class="page">
    <!-- .page-inner -->
    <div class="page-inner">
      <!-- .page-title-bar -->
      <header class="page-title-bar">
        <!-- .breadcrumb -->

        <div class="d-md-flex align-items-md-start  ">
          <div class="small text-muted text-center page-title mr-sm-auto">
          </div>
          <div class="btn-toolbar">
            <button type="button" class="btn btn-light" id="export"><i class="fa fa-file-excel-o"></i> <span class="ml-1">Export</span>
            </button> <button type="button" class="btn btn-light"><i class="fa fa-upload"></i> <span class="ml-1">Import</span></button>
            <div class="dropdown">
              <button type="button" class="btn btn-light" data-toggle="dropdown"><span>More</span> <span class="fa fa-caret-down"></span></button>
              <div class="dropdown-arrow dropdown-arrow-right"></div>
              <div class="dropdown-menu dropdown-menu-right">
                <a href="" class="dropdown-item">Add Picture</a>
                <a href="#!" class="dropdown-item">Check Cases</a>
                <div class="dropdown-divider"></div><a href="#!" class="dropdown-item">Share</a> <a href="#!" class="dropdown-item">Archive</a> <a href="#!" class="dropdown-item">Remove</a>
              </div>
            </div>
          </div><!-- /.btn-toolbar -->
        </div><!-- /title and toolbar -->
        <!-- floating action -->
        <button type="file" class="btn btn-success btn-floated" id="file-upload"><span title="upload a file" class="fa fa-paperclip"></span></button> <!-- /floating action -->
        <!-- title and toolbar -->
        <input type="file"  class="k-hide" name="upload" id="my-file">
      </header><!-- /.page-title-bar -->
      <!-- .page-section -->
      <div class="page-section">
        <!-- .card -->
        <div class="col-xl-12">
          <!-- .card -->
          <section class="card card-fluid">
            <!-- .card-header -->
            <header class="card-header border-0">
              <div class="d-flex align-items-center">
                <span class="mr-auto">Counsellors</span> <button type="button" class="btn btn-icon btn-light " ><i class="fa fa-copy" title="copy"></i></button> <button type="button" class="btn btn-icon btn-light"><i class="fa fa-download" title="download"></i></button>
              </div>
            </header><!-- /.card-header -->

            <!--  -->

            <div class="card card-table">
              <div class="card-header">Export Functions
                <div class="tools dropdown"><span class="icon mdi mdi-download"></span><a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown"><span class="icon mdi mdi-more-vert"></span></a>
                  <div class="dropdown-menu" role="menu"><a class="dropdown-item" href="#">Action</a><a class="dropdown-item" href="#">Another action</a><a class="dropdown-item" href="#">Something else here</a>
                    <div class="dropdown-divider"></div><a class="dropdown-item" href="#">Separated link</a>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <div id="mceu_16" class="mce-tinymce mce-container mce-panel" hidefocus="1" tabindex="-1" role="application" style="visibility: hidden; border-width: 1px; width: 100%;"><div id="mceu_16-body" class="mce-container-body mce-stack-layout"><div id="mceu_17" class="mce-top-part mce-container mce-stack-layout-item mce-first"><div id="mceu_17-body" class="mce-container-body"><div id="mceu_18" class="mce-container mce-menubar mce-toolbar mce-first" role="menubar" style="border-width: 0px 0px 1px;"><div id="mceu_18-body" class="mce-container-body mce-flow-layout"><div id="mceu_19" class="mce-widget mce-btn mce-menubtn mce-flow-layout-item mce-first mce-btn-has-text mce-toolbar-item" tabindex="-1" aria-labelledby="mceu_19" role="menuitem" aria-haspopup="true" aria-expanded="false"><button id="mceu_19-open" role="presentation" type="button" tabindex="-1"><span class="mce-txt">File</span> <i class="mce-caret"></i></button></div><div id="mceu_20" class="mce-widget mce-btn mce-menubtn mce-flow-layout-item mce-btn-has-text" tabindex="-1" aria-labelledby="mceu_20" role="menuitem" aria-haspopup="true"><button id="mceu_20-open" role="presentation" type="button" tabindex="-1"><span class="mce-txt">Edit</span> <i class="mce-caret"></i></button></div><div id="mceu_21" class="mce-widget mce-btn mce-menubtn mce-flow-layout-item mce-btn-has-text" tabindex="-1" aria-labelledby="mceu_21" role="menuitem" aria-haspopup="true"><button id="mceu_21-open" role="presentation" type="button" tabindex="-1"><span class="mce-txt">View</span> <i class="mce-caret"></i></button></div><div id="mceu_22" class="mce-widget mce-btn mce-menubtn mce-flow-layout-item mce-btn-has-text mce-toolbar-item" tabindex="-1" aria-labelledby="mceu_22" role="menuitem" aria-haspopup="true" aria-expanded="false"><button id="mceu_22-open" role="presentation" type="button" tabindex="-1"><span class="mce-txt">Insert</span> <i class="mce-caret"></i></button></div><div id="mceu_23" class="mce-widget mce-btn mce-menubtn mce-flow-layout-item mce-btn-has-text mce-toolbar-item" tabindex="-1" aria-labelledby="mceu_23" role="menuitem" aria-haspopup="true" aria-expanded="false"><button id="mceu_23-open" role="presentation" type="button" tabindex="-1"><span class="mce-txt">Format</span> <i class="mce-caret"></i></button></div><div id="mceu_24" class="mce-widget mce-btn mce-menubtn mce-flow-layout-item mce-btn-has-text" tabindex="-1" aria-labelledby="mceu_24" role="menuitem" aria-haspopup="true"><button id="mceu_24-open" role="presentation" type="button" tabindex="-1"><span class="mce-txt">Tools</span> <i class="mce-caret"></i></button></div><div id="mceu_25" class="mce-widget mce-btn mce-menubtn mce-flow-layout-item mce-btn-has-text mce-toolbar-item" tabindex="-1" aria-labelledby="mceu_25" role="menuitem" aria-haspopup="true" aria-expanded="false"><button id="mceu_25-open" role="presentation" type="button" tabindex="-1"><span class="mce-txt">Table</span> <i class="mce-caret"></i></button></div><div id="mceu_26" class="mce-widget mce-btn mce-menubtn mce-flow-layout-item mce-last mce-btn-has-text" tabindex="-1" aria-labelledby="mceu_26" role="menuitem" aria-haspopup="true"><button id="mceu_26-open" role="presentation" type="button" tabindex="-1"><span class="mce-txt">Help</span> <i class="mce-caret"></i></button></div></div></div><div id="mceu_27" class="mce-toolbar-grp mce-container mce-panel mce-last" hidefocus="1" tabindex="-1" role="group"><div id="mceu_27-body" class="mce-container-body mce-stack-layout"><div id="mceu_28" class="mce-container mce-toolbar mce-stack-layout-item mce-first mce-last" role="toolbar"><div id="mceu_28-body" class="mce-container-body mce-flow-layout"><div id="mceu_29" class="mce-container mce-flow-layout-item mce-first mce-btn-group" role="group"><div id="mceu_29-body"><div id="mceu_0" class="mce-widget mce-btn mce-menubtn mce-fixed-width mce-listbox mce-first mce-last mce-btn-has-text" tabindex="-1" aria-labelledby="mceu_0" role="button" aria-haspopup="true"><button id="mceu_0-open" role="presentation" type="button" tabindex="-1"><span class="mce-txt">Heading 1</span> <i class="mce-caret"></i></button></div></div></div><div id="mceu_30" class="mce-container mce-flow-layout-item mce-btn-group" role="group"><div id="mceu_30-body"><div id="mceu_1" class="mce-widget mce-btn mce-first" tabindex="-1" aria-pressed="false" role="button" aria-label="Bold"><button id="mceu_1-button" role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-bold"></i></button></div><div id="mceu_2" class="mce-widget mce-btn" tabindex="-1" aria-pressed="false" role="button" aria-label="Italic"><button id="mceu_2-button" role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-italic"></i></button></div><div id="mceu_3" class="mce-widget mce-btn" tabindex="-1" aria-pressed="false" role="button" aria-label="Strikethrough"><button id="mceu_3-button" role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-strikethrough"></i></button></div><div id="mceu_4" class="mce-widget mce-btn mce-splitbtn mce-colorbutton" role="button" tabindex="-1" aria-haspopup="true" aria-label="Text color"><button role="presentation" hidefocus="1" type="button" tabindex="-1"><i class="mce-ico mce-i-forecolor"></i><span id="mceu_4-preview" class="mce-preview"></span></button><button type="button" class="mce-open" hidefocus="1" tabindex="-1"> <i class="mce-caret"></i></button></div><div id="mceu_5" class="mce-widget mce-btn mce-splitbtn mce-colorbutton mce-last" role="button" tabindex="-1" aria-haspopup="true" aria-label="Background color"><button role="presentation" hidefocus="1" type="button" tabindex="-1"><i class="mce-ico mce-i-backcolor"></i><span id="mceu_5-preview" class="mce-preview"></span></button><button type="button" class="mce-open" hidefocus="1" tabindex="-1"> <i class="mce-caret"></i></button></div></div></div><div id="mceu_31" class="mce-container mce-flow-layout-item mce-btn-group" role="group"><div id="mceu_31-body"><div id="mceu_6" class="mce-widget mce-btn mce-first mce-last" tabindex="-1" aria-pressed="false" role="button" aria-label="Insert/edit link"><button id="mceu_6-button" role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-link"></i></button></div></div></div><div id="mceu_32" class="mce-container mce-flow-layout-item mce-btn-group" role="group"><div id="mceu_32-body"><div id="mceu_7" class="mce-widget mce-btn mce-first" tabindex="-1" aria-pressed="false" role="button" aria-label="Align left"><button id="mceu_7-button" role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-alignleft"></i></button></div><div id="mceu_8" class="mce-widget mce-btn mce-active" tabindex="-1" aria-pressed="true" role="button" aria-label="Align center"><button id="mceu_8-button" role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-aligncenter"></i></button></div><div id="mceu_9" class="mce-widget mce-btn" tabindex="-1" aria-pressed="false" role="button" aria-label="Align right"><button id="mceu_9-button" role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-alignright"></i></button></div><div id="mceu_10" class="mce-widget mce-btn mce-last" tabindex="-1" aria-pressed="false" role="button" aria-label="Justify"><button id="mceu_10-button" role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-alignjustify"></i></button></div></div></div><div id="mceu_33" class="mce-container mce-flow-layout-item mce-btn-group" role="group"><div id="mceu_33-body"><div id="mceu_11" class="mce-widget mce-btn mce-splitbtn mce-menubtn mce-first" role="button" aria-pressed="false" tabindex="-1" aria-label="Numbered list" aria-haspopup="true"><button type="button" hidefocus="1" tabindex="-1"><i class="mce-ico mce-i-numlist"></i></button><button type="button" class="mce-open" hidefocus="1" tabindex="-1"> <i class="mce-caret"></i></button></div><div id="mceu_12" class="mce-widget mce-btn mce-splitbtn mce-menubtn" role="button" aria-pressed="false" tabindex="-1" aria-label="Bullet list" aria-haspopup="true"><button type="button" hidefocus="1" tabindex="-1"><i class="mce-ico mce-i-bullist"></i></button><button type="button" class="mce-open" hidefocus="1" tabindex="-1"> <i class="mce-caret"></i></button></div><div id="mceu_13" class="mce-widget mce-btn" tabindex="-1" role="button" aria-label="Decrease indent"><button id="mceu_13-button" role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-outdent"></i></button></div><div id="mceu_14" class="mce-widget mce-btn mce-last" tabindex="-1" role="button" aria-label="Increase indent"><button id="mceu_14-button" role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-indent"></i></button></div></div></div><div id="mceu_34" class="mce-container mce-flow-layout-item mce-last mce-btn-group" role="group"><div id="mceu_34-body"><div id="mceu_15" class="mce-widget mce-btn mce-first mce-last" tabindex="-1" role="button" aria-label="Clear formatting"><button id="mceu_15-button" role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-removeformat"></i></button></div></div></div></div></div></div></div></div></div><div id="mceu_35" class="mce-edit-area mce-container mce-panel mce-stack-layout-item" hidefocus="1" tabindex="-1" role="group" style="border-width: 1px 0px 0px;"><iframe id="mce_0_ifr" frameborder="0" allowtransparency="true" title="Rich Text Area. Press ALT-F9 for menu. Press ALT-F10 for toolbar. Press ALT-0 for help" style="width: 100%; height: 500px; display: block;"></iframe></div><div id="mceu_36" class="mce-statusbar mce-container mce-panel mce-stack-layout-item mce-last" hidefocus="1" tabindex="-1" role="group" style="border-width: 1px 0px 0px;"><div id="mceu_36-body" class="mce-container-body mce-flow-layout"><div id="mceu_37" class="mce-path mce-flow-layout-item mce-first"><div role="button" class="mce-path-item mce-last" data-index="0" tabindex="-1" id="mceu_37-0" aria-level="1">h1</div></div><span id="mceu_40" class="mce-wordcount mce-widget mce-label mce-flow-layout-item">156 words</span><div id="mceu_38" class="mce-flow-layout-item mce-resizehandle"><i class="mce-ico mce-i-resize"></i></div><span id="mceu_39" class="mce-branding mce-widget mce-label mce-flow-layout-item mce-last"> Powered by <a href="https://www.tiny.cloud/?utm_campaign=editor_referral&amp;utm_medium=poweredby&amp;utm_source=tinymce" rel="noopener" target="_blank" role="presentation" tabindex="-1">Tiny</a></span></div></div></div></div>
                <textarea id="mce_0" aria-hidden="true" style="display: none;">&lt;p style="text-align: center; font-size: 15px;"&gt;&lt;img title="TinyMCE Logo" src="//www.tinymce.com/images/glyph-tinymce@2x.png" alt="TinyMCE Logo" width="110" height="97" /&gt;
  &lt;/p&gt;
  &lt;h1 style="text-align: center;"&gt;Welcome to the TinyMCE Cloud demo!&lt;/h1&gt;
  &lt;h5 style="text-align: center;"&gt;Note, this includes some "enterprise/premium" features.&lt;br&gt;Visit the &lt;a href="https://www.tinymce.com/pricing/#demo-enterprise"&gt;pricing page&lt;/a&gt; to learn more about our premium plugins.&lt;/h5&gt;
  &lt;p&gt;Please try out the features provided in this full featured example.&lt;/p&gt;

  &lt;h2&gt;Got questions or need help?&lt;/h2&gt;
  &lt;ul&gt;
    &lt;li&gt;Our &lt;a href="//www.tinymce.com/docs/"&gt;documentation&lt;/a&gt; is a great resource for learning how to configure TinyMCE.&lt;/li&gt;
    &lt;li&gt;Have a specific question? Visit the &lt;a href="https://community.tinymce.com/forum/"&gt;Community Forum&lt;/a&gt;.&lt;/li&gt;
    &lt;li&gt;We also offer enterprise grade support as part of &lt;a href="https://tinymce.com/pricing"&gt;TinyMCE premium subscriptions&lt;/a&gt;.&lt;/li&gt;
  &lt;/ul&gt;

  &lt;h2&gt;A simple table to play with&lt;/h2&gt;
  &lt;table style="text-align: center;border-collapse: collapse; width: 100%;"&gt;
    &lt;thead&gt;
      &lt;tr&gt;
        &lt;th&gt;Product&lt;/th&gt;
        &lt;th&gt;Cost&lt;/th&gt;
        &lt;th&gt;Really?&lt;/th&gt;
      &lt;/tr&gt;
    &lt;/thead&gt;
    &lt;tbody&gt;
      &lt;tr&gt;
        &lt;td&gt;TinyMCE Cloud&lt;/td&gt;
        &lt;td&gt;Get started for free&lt;/td&gt;
        &lt;td&gt;YES!&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;Plupload&lt;/td&gt;
        &lt;td&gt;Free&lt;/td&gt;
        &lt;td&gt;YES!&lt;/td&gt;
      &lt;/tr&gt;
    &lt;/tbody&gt;
  &lt;/table&gt;

  &lt;h2&gt;Found a bug?&lt;/h2&gt;
  &lt;p&gt;If you think you have found a bug please create an issue on the &lt;a href="https://github.com/tinymce/tinymce/issues"&gt;GitHub repo&lt;/a&gt; to report it to the developers.&lt;/p&gt;

  &lt;h2&gt;Finally ...&lt;/h2&gt;
  &lt;p&gt;Don't forget to check out our other product &lt;a href="http://www.plupload.com" target="_blank"&gt;Plupload&lt;/a&gt;, your ultimate upload solution featuring HTML5 upload support.&lt;/p&gt;
  &lt;p&gt;Thanks for supporting TinyMCE! We hope it helps you and your users create great content.&lt;br&gt;All the best from the TinyMCE team.&lt;/p&gt;
</textarea>
            </div>
            <!--  -->
          </section><!-- /.card -->
        </div><!-- /grid column -->
      </div><!-- /.page-section -->
      <br><br><br>
    </div><!-- /.page-inner -->
  </div><!-- /.page -->
</div>



@endsection
