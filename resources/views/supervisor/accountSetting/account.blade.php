@extends('supervisor.master.master')

@section('content')
<div class="wrapper">
          <!-- .page -->
          <div class="page">
            <!-- .page-cover -->
              @include('supervisor.master.casemodal')
            <!-- .page-navs -->
            <nav class="page-navs">
              <!-- .nav-scroller -->
              <div class="nav-scroller">
                <!-- .nav -->
                <div class="nav nav-center nav-tabs">
                  <a class="nav-link" href="{{route('supervisor.profile')}}">Profile Overview</a>
                  <a class="nav-link " href="{{route('supervisor.request')}}">Request <span class="badge">16</span></a>
                   <a class="nav-link" href="{{route('supervisor.notifications')}}">Notifications</a>
                   <a class="nav-link active" href="javascript: window.location.reload();">Settings</a>
                </div><!-- /.nav -->
              </div><!-- /.nav-scroller -->
            </nav><!-- /.page-navs -->
            <!-- .page-inner -->
            <div class="page-inner">
              <!-- .page-title-bar -->
              <header class="page-title-bar">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                      <a href="user-profile.html"><i class="breadcrumb-icon fa fa-angle-left mr-2"></i>Overview</a>
                    </li>
                  </ol>
                </nav>
              </header><!-- /.page-title-bar -->
              <!-- .page-section -->
              <div class="page-section">
                <!-- grid row -->
                <div class="row">
                  <!-- grid column -->
                  <div class="col-lg-4">
                    <!-- .card -->
                    <div class="card card-fluid">
                      <h6 class="card-header"> Your Details </h6><!-- .nav -->
                      <nav class="nav nav-tabs flex-column border-0">
                        <a href="#" class="nav-link active">Account</a>
                        <a href="#" class="nav-link">Roles</a>

                      </nav><!-- /.nav -->
                    </div><!-- /.card -->
                  </div><!-- /grid column -->
                  <!-- grid column -->
                  <div class="col-lg-8">
                    <!-- .card -->
                    <div class="card card-fluid">
                      <h6 class="card-header text-white text-uppercase" style="background-color:#343a40;"> Account </h6><!-- .card-body -->
                      <div class="card-body">
                        <!-- form -->
                        <form method="post"  name="formupdate" target="_parent" action="{{route('updateaccount')}}">
                          <!-- form row -->
                          <div class="form-row">
                            <!-- form column -->
                            <div class="col-md-6 mb-3">
                              <label class="form-control-label text-muted">First Name</label> <input type="text" class="form-control" name="firstname"  value="{{Auth::user()->firstname}}" required="">
                            </div><!-- /form column -->
                            <!-- form column -->
                            <div class="col-md-6 mb-3">
                              <label class="form-control-label text-muted">Last Name</label> <input type="text" class="form-control" name="lastname" value="{{Auth::user()->lastname}}" required="">
                            </div><!-- /form column -->
                          </div><!-- /form row -->
                          <!-- .form-group -->
                          <div class="form-group">
                            <label class="form-control-label text-muted">Email</label> <input type="email" class="form-control" name="email" value="{{Auth::user()->email}}" required="">
                          </div><!-- /.form-group -->
                          <!-- .form-group -->
                          <div class="form-group">
                            <label class="form-control-label text-muted">New Password</label> <input type="password" class="form-control" name="newpassword" value="{{old('password')}}" required="">
                          </div><!-- /.form-group -->
                          <!-- .form-group -->
                          <div class="form-group">
                            <label class="form-control-label text-muted">Username</label> <input type="text" class="form-control" name="username" value="{{Auth::user()->username}}" required="">
                          </div><!-- /.form-group -->
                          <hr>
                          <!-- .form-actions -->
                          <div class="form-actions">
                            <!-- enable submit btn when user type their current password -->
                            <input type="password" class="form-control ml-auto mr-3" name="oldpassword" placeholder="Enter Current Password" required=""> <button type="submit" class="btn btn-outline-success">Update Account</button>
                          </div><!-- /.form-actions -->
                        </form><!-- /form -->
                      </div><!-- /.card-body -->
                    </div><!-- /.card -->
                  </div><!-- /grid column -->
                </div><!-- /grid row -->
              </div><!-- /.page-section -->

            </div><!-- /.page-inner -->
          </div><!-- /.page -->
          <br><br><br><br>
        </div>
    @endsection
