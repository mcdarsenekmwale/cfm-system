@extends('supervisor.master.master')

@section('content')
<div class="wrapper">
          <!-- .page -->
          <div class="page">
            <!-- .page-cover -->
            @include('supervisor.master.casemodal')
            <!-- .page-navs -->
            <nav class="page-navs">
              <!-- .nav-scroller -->
              <div class="nav-scroller">
                <!-- .nav -->
                <div class="nav nav-center nav-tabs">
                  <a class="nav-link active" href="javascript:window.location.reload();">Profile Overview</a>
                  <a class="nav-link" href="{{route('supervisor.request')}}">Requests <span class="badge">16</span></a>
                  <a class="nav-link" href="{{route('supervisor.notifications')}}">Notifications</a>
                  <a class="nav-link " href="{{route('supervisor.account')}}">Settings</a>
                </div><!-- /.nav -->
              </div><!-- /.nav-scroller -->
            </nav><!-- /.page-navs -->
            <!-- .page-inner -->
            <div class="page-inner">
              <!-- .page-title-bar -->
              <header class="page-title-bar">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                      <a href="#"><i class="breadcrumb-icon fa fa-angle-left mr-2"></i>Overview</a>
                    </li>
                  </ol>
                </nav>
              </header><!-- /.page-title-bar -->
              <!-- .page-section -->
              <div class="page-section">
                <!-- grid row -->

                  <form class="" action="index.html" method="post">
                    <div class="row">
                      <!-- grid column -->
                      <div class="col-lg-4">
                        <!-- .card -->
                        <div class="card card-fluid">
                          <h6 class="card-header text-white text-uppercase" style="background-color:#343a40;"> Your Details </h6><!-- .nav -->
                          <nav class="nav nav-tabs flex-column border-0" id="myTab" role="tablist">
                            <a class="nav-link active"  id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true"> Profile</a>
                            <a  class="nav-link "  id="social-tab" data-toggle="tab" href="#social" role="tab" aria-controls="social" aria-selected="true">Social Networks</a>
                          </nav><!-- /.nav -->
                        </div><!-- /.card -->
                      </div><!-- /grid column -->
                      <!-- grid column -->
                      <div class="col-lg-8">
                        <!-- .card -->
                        <div class="tab-content my--2" id="nav-tabContent">
                          <div class="tab-content" id="myTabContent">
                            <!-- profile -->
                            <div class="tab-pane fade show  active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                              <!-- .card -->

                                  <div class="card card-fluid">
                                    <h6 class="card-header text-white text-uppercase" style="background-color:#343a40;"> Public Profile </h6><!-- .card-body -->
                                    <div class="card-body">
                                      <!-- .media -->
                                      <div class="media mb-3">
                                        <!-- avatar -->
                                        <div class="user-avatar user-avatar-xl fileinput-button">
                                          <div class="fileinput-button-label"> Change photo </div><img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt=""> <input id="fileupload-avatar" type="file" name="avatar">
                                        </div><!-- /avatar -->
                                        <!-- .media-body -->
                                        <div class="media-body pl-3">
                                          <h3 class="card-title"> Public Photo </h3>
                                          <h6 class="card-subtitle text-muted"> Click here to change your photo. </h6>
                                          <p class="card-text">
                                            <small></small>
                                          </p><!-- The avatar upload progress bar -->
                                          <div id="progress-avatar" class="progress progress-xs fade">
                                            <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                                          </div><!-- /avatar upload progress bar -->
                                        </div><!-- /.media-body -->
                                      </div><!-- /.media -->
                                      <!-- form -->

                                        <!-- form row -->
                                        <div class="form-row">
                                          <!-- form column -->
                                          <label for="input01" class="col-md-3">Cover photo</label> <!-- /form column -->
                                          <!-- form column -->
                                          <div class="col-md-9 mb-3">
                                            <div class="custom-file">
                                              <input type="file" class="custom-file-input" id="input01" multiple=""> <label class="custom-file-label" for="input01">Choose cover</label>
                                            </div><small class="text-muted"></small>
                                          </div><!-- /form column -->
                                        </div><!-- /form row -->
                                        <!-- form row -->
                                        <div class="form-row">
                                          <!-- form column -->
                                          <label for="input02" class="col-md-3">Institution</label> <!-- /form column -->
                                          <!-- form column -->
                                          <div class="col-md-9 mb-3">
                                            <input type="text" class="form-control" id="input02" value="">
                                          </div><!-- /form column -->
                                        </div><!-- /form row -->
                                        <!-- form row -->
                                        <div class="form-row">
                                          <!-- form column -->
                                          <label for="input03" class="col-md-3">Profile Heading</label> <!-- /form column -->
                                          <!-- form column -->
                                          <div class="col-md-9 mb-3">
                                            <textarea type="text" class="form-control" id="input03"></textarea> <small class="text-muted"></small>
                                          </div><!-- /form column -->
                                        </div><!-- /form row -->
                                        <!-- form row -->
                                        <div class="form-row">
                                          <!-- form column -->
                                          <label for="input04" class="col-md-3">supervisor</label> <!-- /form column -->
                                          <!-- form column -->
                                          <div class="col-md-9 mb-3">
                                            <div class="custom-control custom-checkbox">
                                              <input type="checkbox" class="custom-control-input" id="input04" checked=""> <label class="custom-control-label" for="input04">Yes</label>
                                            </div>
                                          </div><!-- /form column -->
                                        </div><!-- /form row -->
                                        <hr>
                                        <!-- .form-actions -->
                                        <div class="form-actions">
                                          <button type="submit" class="btn btn-primary ml-auto">Update Profile</button>
                                        </div><!-- /.form-actions -->

                                    </div><!-- /.card-body -->
                                  </div><!-- /.card -->
                            </div>
                            <div class="tab-pane fade show  " id="social" role="tabpanel" aria-labelledby="social-tab">
                                  <!-- .card -->
                                  <div class="card card-fluid">
                          <h6 class="card-header text-white text-uppercase text-center" style="background-color:#343a40;"> Social Networks </h6><!-- form -->

                            <!-- .list-group -->
                            <div class="list-group list-group-flush mt-3 mb-0">
                              <!-- .list-group-item -->
                              <div class="list-group-item">
                                <!-- .list-group-item-figure -->
                                <div class="list-group-item-figure">
                                  <div class="tile tile-md bg-twitter">
                                    <i class="fa fa-twitter"></i>
                                  </div>
                                </div><!-- /.list-group-item-figure -->
                                <!-- .list-group-item-body -->
                                <div class="list-group-item-body">
                                  <input type="text" class="form-control" id="twitter" placeholder="Twitter Username" value="@stilearningTwit">
                                </div><!-- /.list-group-item-body -->
                              </div><!-- /.list-group-item -->
                              <!-- .list-group-item -->
                              <div class="list-group-item">
                                <!-- .list-group-item-figure -->
                                <div class="list-group-item-figure">
                                  <div class="tile tile-md bg-facebook">
                                    <i class="fa fa-facebook-f"></i>
                                  </div>
                                </div><!-- /.list-group-item-figure -->
                                <!-- .list-group-item-body -->
                                <div class="list-group-item-body">
                                  <input type="text" class="form-control" id="facebook" placeholder="Facebook Username">
                                </div><!-- /.list-group-item-body -->
                              </div><!-- /.list-group-item -->
                              <!-- .list-group-item -->
                              <div class="list-group-item">
                                <!-- .list-group-item-figure -->
                                <div class="list-group-item-figure">
                                  <div class="tile tile-md bg-linkedin">
                                    <i class="fa fa-linkedin"></i>
                                  </div>
                                </div><!-- /.list-group-item-figure -->
                                <!-- .list-group-item-body -->
                                <div class="list-group-item-body">
                                  <input type="text" class="form-control" id="linkedin" placeholder="Linkedin Username">
                                </div><!-- /.list-group-item-body -->
                              </div><!-- /.list-group-item -->
                              <!-- .list-group-item -->
                              <div class="list-group-item">
                                <!-- .list-group-item-figure -->
                                <div class="list-group-item-figure">
                                  <div class="tile tile-md bg-dribbble">
                                    <i class="fa fa-dribbble"></i>
                                  </div>
                                </div><!-- /.list-group-item-figure -->
                                <!-- .list-group-item-body -->
                                <div class="list-group-item-body">
                                  <input type="text" class="form-control" id="dribbble" placeholder="Dribbble Username">
                                </div><!-- /.list-group-item-body -->
                              </div><!-- /.list-group-item -->
                              <!-- .list-group-item -->
                              <div class="list-group-item">
                                <!-- .list-group-item-figure -->
                                <div class="list-group-item-figure">
                                  <div class="tile tile-md bg-github">
                                    <i class="fa fa-github"></i>
                                  </div>
                                </div><!-- /.list-group-item-figure -->
                                <!-- .list-group-item-body -->
                                <div class="list-group-item-body">
                                  <input type="text" class="form-control" id="github" placeholder="Github Username">
                                </div><!-- /.list-group-item-body -->
                              </div><!-- /.list-group-item -->
                            </div><!-- /.list-group -->
                            <!-- .card-body -->
                            <div class="card-body">
                              <hr>
                              <!-- .form-actions -->
                              <div class="form-actions">
                                <button type="submit" class="btn btn-primary ml-auto">Update Socials</button>
                              </div><!-- /.form-actions -->
                            </div><!-- /.card-body -->

                        </div><!-- /.card -->
                            </div><!-- /grid column -->
                          </div><!-- /grid row -->
                        </div><!-- /grid column -->
                      </div><!-- /grid column -->
                    </div><!-- /grid row -->
                  </form>

              </div><!-- /.page-section -->
            </div><!-- /.page-inner -->
          </div><!-- /.page -->
        </div>
@endsection
