@extends('supervisor.master.master')

@section('content')
<div class="wrapper">
          <!-- .page -->
          <div class="page">
            <!-- .page-cover -->
            @include('supervisor.master.casemodal')
            <!-- .page-navs -->
            <nav class="page-navs">
              <!-- .nav-scroller -->
              <div class="nav-scroller">
                <!-- .nav -->
                <div class="nav nav-center nav-tabs">
                  <a class="nav-link" href="{{route('supervisor.profile')}}">Profile Overview</a>
                  <a class="nav-link" href="{{route('supervisor.request')}}">Request <span class="badge">16</span></a>
                   <a class="nav-link active" href="javascript: window.location.reload();">Notifications</a>
                   <a class="nav-link " href="{{route('supervisor.account')}}">Settings</a>
                </div><!-- /.nav -->
              </div><!-- /.nav-scroller -->
            </nav><!-- /.page-navs -->
            <!-- .page-inner -->
            <div class="page-inner">
              <!-- .page-title-bar -->
              <header class="page-title-bar">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                      <a href="#"><i class="breadcrumb-icon fa fa-angle-left mr-2"></i>Notifications</a>
                    </li>
                  </ol>
                </nav>
              </header><!-- /.page-title-bar -->
              <!-- .page-section -->
              <div class="page-section">
                <!-- grid row -->
                <div class="row">
                  <!-- grid column -->
                  <div class="col-lg-4">
                    <!-- .card -->
                    <div class="card card-fluid">
                      <h6 class="card-header text-white text-uppercase" style="background-color:#343a40;"> Notification Details </h6><!-- .nav -->
                      <nav class="nav nav-tabs flex-column border-0">

                        <a href="#" class="nav-link active">Notification Settings</a>
                        <a href="{{route('supervisor.profile')}}" class="nav-link">Profile Settings</a>
                      </nav><!-- /.nav -->
                    </div><!-- /.card -->
                  </div><!-- /grid column -->
                  <!-- grid column -->
                  <div class="col-lg-8">
                    <!-- .card -->
                    <div class="card card-fluid">
                      <h6 class="card-header text-white text-uppercase text-center" style="background-color:#343a40;"> Notifications Settings</h6><!-- .list-group -->
                      <div class="list-group list-group-flush">
                        <!-- .list-group-item -->
                        <div class="list-group-header category-title"> General </div>
                        <div class="list-group-item d-flex justify-content-between align-items-center"> New user added
                          <!-- .switcher -->
                          <label class="switcher-control switcher-control-success">
                            <input type="checkbox" name="notif01" class="switcher-input" checked="">
                            <span class="switcher-indicator"></span>
                          </label> <!-- /.switcher -->
                        </div><!-- /.list-group-item -->

                        <!-- .list-group-item -->
                        <div class="list-group-item d-flex justify-content-between align-items-center"> New partner added
                          <!-- .switcher -->
                          <label class="switcher-control switcher-control-success"><input type="checkbox" name="notif02" class="switcher-input" checked=""> <span class="switcher-indicator"></span></label> <!-- /.switcher -->
                        </div><!-- /.list-group-item -->
                        <!-- .list-group-item -->
                        <div class="list-group-item d-flex justify-content-between align-items-center"> Someone sends you a message
                          <!-- .switcher -->
                          <label class="switcher-control switcher-control-success"><input type="checkbox" name="notif03" class="switcher-input" checked=""> <span class="switcher-indicator"></span></label> <!-- /.switcher -->
                        </div><!-- /.list-group-item -->
                        <div class="list-group-header category-title"> Cases Notifications </div><!-- .list-group-item -->
                        <div class="list-group-item d-flex justify-content-between align-items-center"> New case or call added
                          <!-- .switcher -->
                          <label class="switcher-control switcher-control-success"><input type="checkbox" name="notif04" class="switcher-input" checked=""> <span class="switcher-indicator"></span></label> <!-- /.switcher -->
                        </div><!-- /.list-group-item -->
                        <!-- .list-group-item -->
                        <div class="list-group-item d-flex justify-content-between align-items-center"> Case has updated
                          <!-- .switcher -->
                          <label class="switcher-control switcher-control-success"><input type="checkbox" name="notif05" class="switcher-input" checked=""> <span class="switcher-indicator"></span></label> <!-- /.switcher -->
                        </div><!-- /.list-group-item -->
                        <!-- .list-group-item -->
                        <div class="list-group-item d-flex justify-content-between align-items-center"> Case has been reffered
                          <!-- .switcher -->
                          <label class="switcher-control switcher-control-success"><input type="checkbox" name="notif06" class="switcher-input"> <span class="switcher-indicator"></span></label> <!-- /.switcher -->
                        </div><!-- /.list-group-item -->
                        <!-- .list-group-item -->
                        <div class="list-group-item d-flex justify-content-between align-items-center"> Case has been resolved
                          <!-- .switcher -->
                          <label class="switcher-control switcher-control-success"><input type="checkbox" name="notif07" class="switcher-input" checked=""> <span class="switcher-indicator"></span></label> <!-- /.switcher -->
                        </div><!-- /.list-group-item -->
                        <div class="list-group-header category-title"> Partner Notifications </div><!-- .list-group-item -->
                        <div class="list-group-item d-flex justify-content-between align-items-center"> Partner updates a case
                          <!-- .switcher -->
                          <label class="switcher-control switcher-control-success"><input type="checkbox" name="notif08" class="switcher-input" checked=""> <span class="switcher-indicator"></span></label> <!-- /.switcher -->
                        </div><!-- /.list-group-item -->
                        <!-- .list-group-item -->
                        <div class="list-group-item d-flex justify-content-between align-items-center"> Partner adds new Case
                          <!-- .switcher -->
                          <label class="switcher-control switcher-control-success"><input type="checkbox" name="notif09" class="switcher-input" checked=""> <span class="switcher-indicator"></span></label> <!-- /.switcher -->
                        </div><!-- /.list-group-item -->
                        <!-- .list-group-item -->
                        <div class="list-group-item d-flex justify-content-between align-items-center"> Partner request to delete a case
                          <!-- .switcher -->
                          <label class="switcher-control switcher-control-success"><input type="checkbox" name="notif10" class="switcher-input" checked=""> <span class="switcher-indicator"></span></label> <!-- /.switcher -->
                        </div><!-- /.list-group-item -->
                        <div class="list-group-header category-title"> News &amp; Trending </div><!-- .list-group-item -->
                        <div class="list-group-item d-flex justify-content-between align-items-center"> Top members this week
                          <!-- .switcher -->
                          <label class="switcher-control switcher-control-success"><input type="checkbox" name="notif11" class="switcher-input" checked=""> <span class="switcher-indicator"></span></label> <!-- /.switcher -->
                        </div><!-- /.list-group-item -->
                        <!-- .list-group-item -->
                        <div class="list-group-item d-flex justify-content-between align-items-center"> Top Teams this week
                          <!-- .switcher -->
                          <label class="switcher-control switcher-control-success"><input type="checkbox" name="notif12" class="switcher-input" checked=""> <span class="switcher-indicator"></span></label> <!-- /.switcher -->
                        </div><!-- /.list-group-item -->
                        <!-- .list-group-item -->
                        <div class="list-group-item d-flex justify-content-between align-items-center"> Rating reminders
                          <!-- .switcher -->
                          <label class="switcher-control switcher-control-success"><input type="checkbox" name="notif13" class="switcher-input"> <span class="switcher-indicator"></span></label> <!-- /.switcher -->
                        </div><!-- /.list-group-item -->
                        <!-- .list-group-item -->
                        <div class="list-group-item d-flex justify-content-between align-items-center"> Project deadline
                          <!-- .switcher -->
                          <label class="switcher-control switcher-control-success"><input type="checkbox" name="notif14" class="switcher-input" checked=""> <span class="switcher-indicator"></span></label> <!-- /.switcher -->
                        </div><!-- /.list-group-item -->
                      </div><!-- /.list-group -->
                    </div><!-- /.card -->
                  </div><!-- /grid column -->
                </div><!-- /grid row -->
              </div><!-- /.page-section -->
            </div><!-- /.page-inner -->
              <br><br>
          </div><!-- /.page -->
        </div>
@endsection
