@extends('supervisor.master.master')

@section('content')
<div class="wrapper">
          <!-- .page -->
          <div class="page">
            <!-- .page-inner -->
            <div class="page-inner">
              <!-- .page-title-bar -->
              <header class="page-title-bar">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                      <a href="#!">Cases<i class="breadcrumb-icon px-3 fa fa-angle-right mr-2"></i></a>
                    </li>
                  </ol>
                </nav>
                <h1 class="page-title mr-sm-auto"> </h1>
                <p class="text-muted"> </p>
              </header><!-- /.page-title-bar -->
              <!-- .page-section -->
              <div class="page-section">
                <!-- grid row -->
                <div class="row">
                  <section class="card card-fluid">
                    <!-- .card-header -->
                    <header class="card-header border-0">
                      <div class="d-flex align-items-center">
                        <span class="mr-auto">Cases</span> <button type="button" class="btn btn-icon btn-light"><i class="fa fa-copy" title="copy"></i></button> <button type="button" class="btn btn-icon btn-light"><i class="fa fa-download" title="download"></i></button>
                      </div>
                    </header><!-- /.card-header -->

                    <div  class="dataTables_wrapper dt-bootstrap4 no-footer">
                          <div class="text-muted px-3"> Showing 1 to {!!$data['cases']->count()!!} of {!!$data['cases']->total()!!} cases </div>
                        <div class="table-responsive be-datatable-header"  >
                        <!-- .table -->
                        <table  class="table table-sm mb-0 table-fw-widget ">
                          <!-- thead -->
                          <thead class="thead-light">
                            <tr>
                              <th style="width:140px; min-width:100px;">
                                <a href="#" class="text-muted sort small" data-sort="goal-name">
                                <b>Date</b>
                                </a>
                              </th>
                              <th class="" style="width:140px; min-width:100px;">
                                <a href="#" class="text-muted sort small " data-sort="goal-district">
                                <b>District</b>
                                </a>
                               </th>
                              <th style="width:140px; min-width:100px;">
                                <a href="#" class="text-muted sort small" data-sort="goal-status">
                                <b>Status</b>
                               </a>
                               </th>
                               <th >
                                 <a href="#" class="text-muted sort small" data-sort="goal-comments">
                                  <b>Comments</b>
                                </a>
                                </th>

                               <th style="width:140px; min-width:100px;"> &nbsp; </th>
                            </tr>
                          </thead><!-- /thead -->
                          <!-- tbody -->
                          <tbody>
                            <!-- tr -->
                            @if($data['cases']->total() == 0)
                            <tr class="card-body" style="height:10rem; width:56rem;"></tr>
                            <tr class="alert  align-items-center" role="alert " >
                              <td class="alert-text text-center " colspan="3" >
                                No recent partner with a case data yet
                                <b>{{Auth::user()->username}}  </b>
                              </td>
                            </tr>
                            <tr class="card-body" style="height:10.7rem; width:56rem;"></tr>
                            @else
                            @foreach($cases as $case)
                              <tr class="hoverable ">
                                <td class="">
                                  <time >
                                    <script >
                                    function dateConvert(dateobj, format){
                                      var year = dateobj.getFullYear();
                                      var month= ("0" + (dateobj.getMonth()+1)).slice(-2);
                                      var date = ("0" + dateobj.getDate()).slice(-2);
                                      var hours = ("0" + dateobj.getHours()).slice(-2);
                                      var minutes = ("0" + dateobj.getMinutes()).slice(-2);
                                      var seconds = ("0" + dateobj.getSeconds()).slice(-2);
                                      var day = dateobj.getDay();
                                      var months = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
                                      var dates = ["SUN","MON","TUE","WED","THU","FRI","SAT"];
                                      var converted_date = "";

                                      switch(format){
                                        case "YYYY-MMM-DD":
                                        converted_date = date  + "-" + months[parseInt(month)-1] + "-" +year ;
                                        break;
                                        case "DD-MMM":
                                        converted_date = date + " " + months[parseInt(month)-1];
                                        break;
                                      }

                                      return converted_date;
                                    }
                                    var i_date = "{{$case->date}}";
                                    var j_date = new Date(Date.parse(i_date));
                                    var date = dateConvert(j_date, "YYYY-MMM-DD");
                                    document.write('<b>'+date+'</b>');
                                    </script>

                                  </time>
                                 </td>
                                <td class="goal-district text-uppercase"><b> {{$case->ta->district->name}}</b> </td>
                                <td class="goal-status">
                                    @if($case->status == '1' )
                                      <span class="text-warning"><i class="fa fa-fw fa-circle mr-1"></i> </span> <b> Unresolved</b>
                                    @elseif($case->status =='0' OR $case->status =='2')
                                      <span class="text-success"><i class="fa fa-fw fa-circle mr-1"></i></span> <b>Resolved</b>
                                    @elseif($case->status =='3')
                                      <span class="text-warning"><i class="fa fa-fw fa-circle mr-1"></i></span> <b>Pending</b>
                                    @endif
                                   </td>
                                <td class="goal-comments">
                                  {{ substr(trim($case->comments) , 0, 213)}}..
                                </td>
                                <td class="align-middle text-right">
                                  <a href="{{route('caseEdit')}}?id={{$case->id}}" data-num1="{{$case->id}}" class="btn btn-sm btn-icon btn-dark"><i class="fa fa-pencil" title="edit"></i> <span class="sr-only">Edit</span></a>
                                  <a href="{{route('caseDelete')}}?id={{$case->id}}" data-num="{{$case->id}}" class="btn btn-sm btn-icon btn-danger"><i class="fa fa-trash" title="delete"></i> <span class="sr-only">Remove</span></a>
                                </td>
                              </tr>
                            @endforeach

                            @endif
                          </tbody><!-- /tbody -->
                        </table><!-- /.table -->
                      </div><!-- /.table-responsive -->
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <b class="col-11"></b>
                        <b class="col-1 float-right" >
                          <a class="text-muted " href="{!! $data['cases']->url($data['cases']->currentPage()-1)!!}" aria-label="Previous">
                            <span class="fa fa-chevron-circle-left fa-lg"></span>
                            <span class="sr-only">Previous</span>
                          </a>

                          <a class="text-muted " href="{!! $data['cases']->url($data['cases']->currentPage()+1)!!}" aria-label="Next">
                            <span class="fa fa-chevron-circle-right fa-lg"></span>
                            <span class="sr-only">Next</span>
                          </a>
                        </b>
                      </div>

                    </div>
                  </section><!-- /.card -->
                </div><!-- /grid row -->
              </div><!-- /.page-section -->
            </div><!-- /.page-inner -->
          </div><!-- /.page -->
          <br><br>
        </div>
@endsection
