

<nav class="col-sm d-none d-md-block card-color sidebar ">
  <div class="sidebar-sticky">
    <ul class="nav flex-column px-4  py-5 border-bottom">
      <li class="nav-item active">
        <a class="nav-link nav-i " href="{{url('/')}}">
          <span data-feather="home"></span>
          Dashboard <span class="sr-only">(current)</span>
        </a>
      </li>
      @if(Auth::User()->id == "134")
      <li class="nav-item">
        <a class="nav-link nav-i" href="#">
          <span data-feather="cloud-drizzle"></span>
          Schemes
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link nav-i" href="#">
          <span data-feather="users"></span>
          Counsellors

        </a>
      </li>
      @endif

      <li class="nav-item">
        <a class="nav-link nav-i" href="{{route('partner.newreport')}}">
          <span data-feather="file-text"></span>
          Reports
          <span class="badge badge-success float-right "> <span class="pulse">new</span></span>
        </a>

      </li>
      <li class="nav-item">
        <a class="nav-link nav-i" href="{{route('analytics')}}" >
          <span data-feather="activity"></span>
          Analytics
        </a>

      </li>
      <li class="nav-item">
        <a class="nav-link nav-i" href="{{{route('integrations')}}}">
          <span data-feather="layers"></span>
          Integrations
        </a>
      </li>
    </ul>

    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
      <span>Saved reports</span>
      <a class="d-flex align-items-center text-muted" href="#">
        <span data-feather="plus-circle"></span>
      </a>
    </h6>
    <ul class="nav flex-column mb-2">
      <li class="nav-item">
        <a class="nav-link" href="#">
          <span data-feather="file-text"></span>
          Current month
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">
          <span data-feather="file-text"></span>
          Last quarter
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">
          <span data-feather="file-text"></span>
          Social engagement
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">
          <span data-feather="file-text"></span>
          Year-end sale
        </a>
      </li>
    </ul>

    <!-- Push content down -->
    <div class="mt-auto py-4 "></div>


    <!-- register -->
    <div class="px-4 py-4 mb-1">
      <a href="#modalReg" class="btn btn-light btn-pill btn-elevate btn-font-brand btn-outline-secondary mb-4" data-toggle="modal" >
        <i class="fa fa-sliders mr-2"></i> Regiter New Case
      </a>
    </div>

  </div>
</nav>

<!-- success -->
<div class="alert alert-success form_success" role="alert" >
  <div class="alert-icon"><i class="fa fa-check"></i></div>
  <div class="alert-text">
    <b>{{Auth::user()->username}}  added a new case successfully</b>

  </div>
  <div class="alert-close" style="position: absolute; top: 2px;
  right:2px;">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true"><i class="fa fa-close"></i></span>
  </button>
</div>
</div>

<!--  -->
<!-- Regiter New Case: Form -->
<div class="modal fade px-5 " id="modalReg" tabindex="-1" role="dialog" aria-hidden="true"  aria-labelledby="myLargeModalLabel" >
  <div class="modal-dialog modal-dialog-centered px-5  modal-full " role="document">
    <div class="modal-content html">
      <div class="modal-header bg-dark align-items-center">
        <h6 class="card-header-title">
          <span class=" card-title text-center text-uppercase text-white mb-2 "> <b>Register New Case</b></span>
        </h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="text-danger fa fa-times-circle"></span>
        </button>
      </div>

      <div class="modal-body body px-5">
        <div id="contact_form"></div>
        <form class="k-form k-form--label-right px-5" name="form_regcase" id="form_regcase" novalidate  method="post" action="{{route('savecase')}}" >
          @csrf
          <!--  validation error checking-->
          <div class="form-group form-group-last error">
            <div class="alert alert-danger" role="alert" id="k_form_1_msg">
              <div class="alert-text">
                <span class="alert-icon"><i class="fa fa-warning"></i></span> Oh snap! Check few things up and try submitting again.
              </div>
              <div class="alert-close" style="position: absolute; top: 2px;
              right:2px;">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="fa fa-close"></i></span>
              </button>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">

            <div class="h4 text-muted border-bottom py-3"> <b>CASE DETAILS 	 <span class="fa fa-angle-right fa-lg" style="position: relative; right: -10px;"></span><span class="fa fa-angle-right fa-lg " style="position: relative; right: -10px;"></span> </b>
            </div>
            <!--begin::Form-->
            <div class="col-reg">
              <div class="k-section py-3">
                <h3 class="k-section__title small">
                  Client Information:
                </h3>
                <div class="k-section__content">
                  <div class=" row">
                    <div class="col-lg-5 form-group">
                      <label class="form-control-label text-muted">* Client Name:</label>
                      <input type="text" name="clientname" class="form-control" placeholder="Client Name" value="{{old('clientname')}}" required="true" autocomplete="on" autofocus>
                    </div>

                    <div class="col-lg-6 form-group">
                      <label class="form-control-label text-muted">*Today's Date:</label>
                      <input type="text" name="date" class="form-control datepicker" data-provide="datepicker" placeholder="" value="<?php date_default_timezone_set('Africa/Blantyre'); echo date("F j, Y,")?>">
                    </div>
                  </div>
                  <div class=" row">
                    <div class="form-group col-lg-5">
                      <label class="form-control-label text-muted ">* Traditional Authority:</label>
                      <input type="text" name="tradional_Authority" class="form-control " data-provide="datepicker" placeholder="" value="">
                    </div>
                    <div class="form-group col-lg-7">
                      <label class="form-control-label text-muted ">Gender *</label>
                      <div class="">
                        <div class="k-radio-inline" style="position: relative; top: 10px;">
                          <label class="k-radio text-muted">
                            <input type="radio" name="gender" value="Male" checked> Male
                            <span></span>
                          </label>
                          <label class="k-radio text-muted">
                            <input type="radio" name="gender" value="Female"> Female
                            <span></span>
                          </label>
                          <label class="k-radio text-muted">
                            <input type="radio" name="gender" value="others"> Others
                            <span></span>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-lg-5 form-group-sub">
                      <label class="form-control-label text-muted">* District:</label>
                      <select class="form-control" name="district" required>
                        <option value="">Select</option>
                        @foreach($districts as $district)
                        <option value="{{$district->id}}" >{{$district->name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-lg-4 form-group-sub">
                      <label class="form-control-label text-muted">* Age:</label>
                      <input type="phone" name="age" class="form-control" placeholder="years old" value="{{old('age')}}"  required>
                    </div>
                  </div>
                  <div class="form-group form-group-last row">
                    <div class="col-lg-5 form-group-sub">
                      <label class="form-control-label text-muted">* Nature :</label>
                      <select class="form-control" name="nature" required>
                        <option value="">Select</option>
                        @foreach($reasons as $nature)
                        <option value="{{$nature}}" >{{$nature}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-lg-4">
                      <label class="form-control-label text-muted">* Pepetrator:</label>
                      <select class="form-control" name="perpetrator">
                        <option value="">Select</option>
                        @foreach($perpetrators as $perpetrator)
                        <option value="{{$perpetrator->id}}">{{$perpetrator->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <hr>
              <div class="k-separator k-separator--border-dashed k-separator--space-xl"></div>

              <div class="k-section">
                <div class="k-section__content">
                  <div class=" row">
                    <div class="col-lg-6 form-group">
                      @foreach($issues->issues as $issue)
                      <label class="form-control-label text-muted border-bottom">*{{$issue->name}} :</label>
                      @foreach($issue->subissues as $subissue)
                      <div class="  k-checkbox-list text-muted">
                        <label class="k-checkbox small-issue">
                          <input type="checkbox" name="subissue" value="{{$subissue->id}}" > {{$subissue->name}}
                          <span></span>
                        </label>
                      </div>
                      @endforeach
                      @endforeach
                    </div>
                    <div class="col-lg-6 form-group border-left">
                      <label class="form-control-label text-muted border-bottom">*CFM Channel :</label>
                      @foreach($cfmChannels as $cfmChannel)
                      <div class="  k-radio-list text-muted">
                        <label class="k-radio small-issue">
                          <input type="radio" name="cfm" value="{{$cfmChannel->id}}" > {{$cfmChannel->Name}}
                          <span></span>
                        </label>
                      </div>
                      @endforeach
                    </div>
                  </div>
                </div>
                <hr>
                <div class="k-separator k-separator--border-dashed k-separator--space-xl"></div>

                <div class="form-group row">
                  <label class="col-form-label col-lg-3 col-sm-12 text-muted">Case Brief *</label>
                  <div class="col-lg-9 col-md-9 col-sm-12">
                    <textarea class="form-control" name="brief" spellcheck="true" value="{{ old('brief')}}" placeholder="Enter a case summary" rows="6" required></textarea>
                    <span class="form-text text-muted">Please enter a case summary </span>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-form-label col-lg-3 col-sm-12 text-muted">Action Taken Brief **</label>
                  <div class="col-lg-9 col-md-9 col-sm-12">
                    <textarea class="form-control" name="action" spellcheck="true" value="{{ old('action')}}" placeholder="Enter a summary of an action taken" rows="6" required></textarea>
                    <span class="form-text text-muted">Please enter a summary of an action taken </span>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-form-label col-lg-3 col-sm-12 text-muted">Outcome Of Case **</label>
                  <div class="col-lg-9 col-md-9 col-sm-12">
                    <textarea class="form-control" name="outcome" spellcheck="true" value="{{ old('outcome')}}" placeholder="Enter an outcome of the case" rows="6" required></textarea>
                    <span class="form-text text-muted">Please enter an aoutcome of the case </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="modal-footer ">
      <button type="button" class="btn btn-outline-secondary btn-sm" data-dismiss="modal">Close</button>
      <button type="reset" class="btn btn-outline-secondary btn-sm" id="reset">Reset</button>
      <button type="submit" class="btn btn-outline-success btn-sm" >Save</button>
    </div>

  </div>
</div>
</div>
