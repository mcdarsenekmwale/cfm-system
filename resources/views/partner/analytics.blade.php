@extends('partner.layouts.master')

@section('content')

<div class="container-fluid py-5">

	<div class="d-md-flex align-items-md-start  ">
		<div class=" text-muted text-center mr-sm-auto">
			<ol class="breadcrumb breadcrumb-sm">
				<li class="breadcrumb-item ">
					<a class="breadcrumb-link text-muted" href="#"><b>Age</b></a>
				</li>
				<li class="breadcrumb-item">
					<a class="breadcrumb-link text-muted" href="#"><b>Gender</b></a>
				</li>
				<li class="breadcrumb-item">
					<a class="breadcrumb-link text-muted" href="#"><b>Issues</b></a>
				</li>
				<li class="breadcrumb-item">
					<a class="breadcrumb-link text-muted" href="#"><b>District</b></a>
				</li>
			</ol>
		</div>
		<div class="btn-toolbar">
			<button type="button" class="btn btn-light"><i class="fa fa-data-transfer-download"></i> <span class="ml-1">Export</span></button> <button type="button" class="btn btn-light"><i class="oi oi-data-transfer-upload"></i> <span class="ml-1">Import</span></button>
			<div class="dropdown">
				<button type="button" class="btn btn-light" data-toggle="dropdown"><span>More</span> <span class="fa fa-caret-down"></span></button>
				<div class="dropdown-arrow dropdown-arrow-right"></div>
				<div class="dropdown-menu dropdown-menu-right">
					<a href="{{route('newpartner')}}" class="dropdown-item">Add Partners</a>
					<a href="#!" class="dropdown-item">Check Cases</a>
					<div class="dropdown-divider"></div><a href="#!" class="dropdown-item">Share</a> <a href="#!" class="dropdown-item">Archive</a> <a href="#!" class="dropdown-item">Remove</a>
				</div>
			</div>
		</div><!-- /.btn-toolbar -->
	</div><!-- /title and toolbar -->

	<div class="row py-2 ">

		<!-- age component -->
		<div class="col-6">
			<div class="order-lg-1 order-xl-1">
				<!--begin::Portlet-->
				<div class="k-portlet k-portlet--height-fluid k-widget " >
					<div class="k-portlet__body">
						<div id="k-widget-slider-age" class="k-slider carousel slide" data-ride="carousel" data-interval="10000">
							<div class="k-slider__head">
								<div class="k-slider__label">AGE </div>
								<div class="k-slider__nav">
									<a class="k-slider__nav-prev carousel-control-prev" href="#k-widget-slider-age" role="button" data-slide="prev">
										<i class="fa fa-angle-left"></i>
									</a>
									<a class="k-slider__nav-next carousel-control-next" href="#k-widget-slider-age" role="button" data-slide="next">
										<i class="fa fa-angle-right"></i>
									</a>

									<div class="col-auto dropdown droppdown-inline" >
										<!-- Toggle -->
										<a href="javascript: void(0);" class="icons drop-note" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" >
											<span class="icon ">
												<i class="fa fa-ellipsis-h text-muted"></i>
											</span>
										</a>

										<!-- Menu -->
										<div class="dropdown-menu  droppdown-menu droppdown-menu-right " x-placement="bottom-end" style="">
											<ul class="k-nav">
												<li class="k-nav__section k-nav__section--first">
													<span class="k-nav__section-text">Export Tools</span>
												</li>
												<li class="k-nav__item">
													<a href="javascript: window.print($('#k-widget-slider-age').outerHTML);" class="k-nav__link">
														<i class="k-nav__link-icon la fa fa-print"></i>
														<span class="k-nav__link-text link-text">Print</span>
													</a>
												</li>
												<li class="k-nav__item">
													<a href="#" class="k-nav__link">
														<i class="k-nav__link-icon la fa fa-copy"></i>
														<span class="k-nav__link-text link-text">Copy</span>
													</a>
												</li>
												<li class="k-nav__item">
													<a href="#" class="k-nav__link">
														<i class="k-nav__link-icon la fa fa-file-excel-o"></i>
														<span class="k-nav__link-text link-text">Excel</span>
													</a>
												</li>
												<li class="k-nav__item">
													<a href="#" class="k-nav__link link-text">
														<i class="k-nav__link-icon la fa fa-file-text-o"></i>
														<span class="k-nav__link-text link-text">CSV</span>
													</a>
												</li>
												<li class="k-nav__item">
													<a href="#" class="k-nav__link">
														<i class="k-nav__link-icon la fa fa-file-pdf-o"></i>
														<span class="k-nav__link-text link-text">PDF</span>
													</a>
												</li>
											</ul>
										</div> <!-- / .dropdown-menu -->
									</div>
								</div>
							</div>
							<div class="carousel-inner" style="height:280px;">
								<div class="carousel-item k-slider__body active" >
									<div class="k-widget-13">
										<div class="k-widget-13__body">
											<div class="k-widget-9__chart">

												<div class="table-responsive mb-0" data-toggle="lists" data-lists-values="[&quot;goal-gender&quot;, &quot;goal-ageValue&quot;, &quot;goal-remarks&quot;]">
													<table class="table table-lg table-nowrap card-table table-horver " >
														<thead class="card-color  border-top  border-right border-left">
															<tr>
																<th>
																	<a href="#" class="text-muted sort" data-sort="goal-age">
																		Age
																	</a>
																</th>
																<th>
																	<a href="#" class="text-muted sort" data-sort="goal-ageValue">
																		Number of Cases
																	</a>
																</th>
																<th>
																	<a href="#" class="text-muted sort" data-sort="goal-remarks">
																		Progress
																	</a>
																</th>

															</tr>
														</thead>
														<tbody class="lists">
															<tr class="hoverable">
																<td class="goal-age "> <b>18</b></td>
																<td class="goal-ageValue">25</td>
																<td class="goal-remarks ">
																	<div class="progress">
																		<div class="progress-bar bg-success" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
																	</div>

																</td>
															</tr>
															<tr class="hoverable">
																<td class="goal-age "><b>19</b></td>
																<td class="goal-ageValue ">55</td>
																<td class="goal-remarks ">
																	<div class="progress">
																		<div class="progress-bar bg-success" role="progressbar" style="width: 55%;" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100">55%</div>
																	</div>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="carousel-item k-slider__body" >
									<div class="k-widget-13">
										<div class="k-widget-13__body">
											<div class="chart chart-appended chartjs-size-monitor chartjs-size-monitor-expand">
												<canvas id="myStatistics" class="chart-canvas chartjs-render-monitor"  height="30" width="100" ></canvas>
												<!-- Legend -->
											</div>

										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>

				<!--end::Portlet-->
			</div>
		</div>

		<!-- gender component -->
		<div class="col-6">
			<div class="order-lg-1 order-xl-1">
				<!--begin::Portlet-->
				<div class="k-portlet k-portlet--height-fluid k-widget " >
					<div class="k-portlet__body">
						<div id="k-widget-slider" class="k-slider carousel slide" data-ride="carousel" data-interval="10000">
							<div class="k-slider__head">
								<div class="k-slider__label">Gender </div>
								<div class="k-slider__nav">
									<a class="k-slider__nav-prev carousel-control-prev" href="#k-widget-slider" role="button" data-slide="prev">
										<i class="fa fa-angle-left"></i>
									</a>
									<a class="k-slider__nav-next carousel-control-next" href="#k-widget-slider" role="button" data-slide="next">
										<i class="fa fa-angle-right"></i>
									</a>

									<div class="col-auto dropdown droppdown-inline" >
										<!-- Toggle -->
										<a href="javascript: void(0);" class="icons drop-note" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" >
											<span class="icon ">
												<i class="fa fa-ellipsis-h text-muted"></i>
											</span>
										</a>

										<!-- Menu -->
										<div class="dropdown-menu  droppdown-menu droppdown-menu-right " x-placement="bottom-end" style="">
											<ul class="k-nav">
												<li class="k-nav__section k-nav__section--first">
													<span class="k-nav__section-text">Export Tools</span>
												</li>
												<li class="k-nav__item">
													<a href="javascript: window.print();" class="k-nav__link">
														<i class="k-nav__link-icon la fa fa-print"></i>
														<span class="k-nav__link-text link-text">Print</span>
													</a>
												</li>
												<li class="k-nav__item">
													<a href="#" class="k-nav__link">
														<i class="k-nav__link-icon la fa fa-copy"></i>
														<span class="k-nav__link-text link-text">Copy</span>
													</a>
												</li>
												<li class="k-nav__item">
													<a href="#" class="k-nav__link">
														<i class="k-nav__link-icon la fa fa-file-excel-o"></i>
														<span class="k-nav__link-text link-text">Excel</span>
													</a>
												</li>
												<li class="k-nav__item">
													<a href="#" class="k-nav__link link-text">
														<i class="k-nav__link-icon la fa fa-file-text-o"></i>
														<span class="k-nav__link-text link-text">CSV</span>
													</a>
												</li>
												<li class="k-nav__item">
													<a href="#" class="k-nav__link">
														<i class="k-nav__link-icon la fa fa-file-pdf-o"></i>
														<span class="k-nav__link-text link-text">PDF</span>
													</a>
												</li>
											</ul>
										</div> <!-- / .dropdown-menu -->
									</div>
								</div>
							</div>
							<div class="carousel-inner" style="height:280px;">
								<div class="carousel-item k-slider__body active" >
									<div class="k-widget-13">
										<div class="k-widget-13__body">
											<div class="k-widget-9__chart">

												<div class="table-responsive mb-0" data-toggle="lists" data-lists-values="[&quot;goal-gender&quot;, &quot;goal-Value&quot;, &quot;goal-progress&quot;]">
													<table class="table table-lg table-nowrap card-table table-horver " >
														<thead class="card-color  border-top  border-right border-left">
															<tr>
																<th>
																	<a href="#" class="text-muted sort" data-sort="goal-gender">
																		Gender
																	</a>
																</th>
																<th>
																	<a href="#" class="text-muted sort" data-sort="goal-Value">
																		Number of Cases
																	</a>
																</th>
																<th>
																	<a href="#" class="text-muted sort" data-sort="goal-progress">
																		Progress
																	</a>
																</th>

															</tr>
														</thead>
														<tbody class="lists">
															<tr class="hoverable">
																<td class="goal-gender "> <b>Male</b></td>
																<td class="goal-Value">25</td>
																<td class="goal-remarks ">
																	<div class="progress">
																		<div class="progress-bar bg-dark" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
																	</div>

																</td>
															</tr>
															<tr class="hoverable">
																<td class="goal-gender "><b>Female</b></td>
																<td class="goal-Value ">55</td>
																<td class="goal-progress ">
																	<div class="progress">
																		<div class="progress-bar bg-dark" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
																	</div>

																</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="carousel-item k-slider__body" >
									<div class="k-widget-13">
										<div class="k-widget-13__body">
											<!-- Chart -->
											<div class="chart chart-appended chartjs-size-monitor chartjs-size-monitor-expand">
												<canvas id="mypieGender" class="chart-canvas chartjs-render-monitor" data-target="#ChartLegend" height="30" width="100" ></canvas>
												<!-- Legend -->
												<div id="ChartLegend" class="pie-legend"><span class="pie-legend-item">
													<i class="pie-legend-indicator" style="background-color: #1cac81 !important;"></i>Male</span>
													<span class="pie-legend-item"><i class="pie-legend-indicator" style="background-color: #41675c !important;"></i>Female</span>
												</div>
											</div>
										</div>

									</div>
								</div>

							</div>
						</div>
					</div>
				</div>

				<!--end::Portlet-->
			</div>
		</div>

	</div>

	<div class="row py-1 ">

		<!-- district component -->
		<div class="col-6">
			<div class="order-lg-1 order-xl-1">
				<!--begin::Portlet-->
				<div class="k-portlet k-portlet--height-fluid k-widget " >
					<div class="k-portlet__body">
						<div id="k-widget-slider-district" class="k-slider carousel slide" data-ride="carousel" data-interval="10000">
							<div class="k-slider__head">
								<div class="k-slider__label">DISTRICT </div>
								<div class="k-slider__nav">
									<a class="k-slider__nav-prev carousel-control-prev" href="#k-widget-slider-district" role="button" data-slide="prev">
										<i class="fa fa-angle-left"></i>
									</a>
									<a class="k-slider__nav-next carousel-control-next" href="#k-widget-slider-district" role="button" data-slide="next">
										<i class="fa fa-angle-right"></i>
									</a>

									<div class="col-auto dropdown droppdown-inline" >
										<!-- Toggle -->
										<a href="javascript: void(0);" class="icons drop-note" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" >
											<span class="icon ">
												<i class="fa fa-ellipsis-h text-muted"></i>
											</span>
										</a>

										<!-- Menu -->
										<div class="dropdown-menu  droppdown-menu droppdown-menu-right " x-placement="bottom-end" style="">
											<ul class="k-nav">
												<li class="k-nav__section k-nav__section--first">
													<span class="k-nav__section-text">Export Tools</span>
												</li>
												<li class="k-nav__item">
													<a href="javascript: window.print();" class="k-nav__link">
														<i class="k-nav__link-icon la fa fa-print"></i>
														<span class="k-nav__link-text link-text">Print</span>
													</a>
												</li>
												<li class="k-nav__item">
													<a href="#" class="k-nav__link">
														<i class="k-nav__link-icon la fa fa-copy"></i>
														<span class="k-nav__link-text link-text">Copy</span>
													</a>
												</li>
												<li class="k-nav__item">
													<a href="#" class="k-nav__link">
														<i class="k-nav__link-icon la fa fa-file-excel-o"></i>
														<span class="k-nav__link-text link-text">Excel</span>
													</a>
												</li>
												<li class="k-nav__item">
													<a href="#" class="k-nav__link link-text">
														<i class="k-nav__link-icon la fa fa-file-text-o"></i>
														<span class="k-nav__link-text link-text">CSV</span>
													</a>
												</li>
												<li class="k-nav__item">
													<a href="#" class="k-nav__link">
														<i class="k-nav__link-icon la fa fa-file-pdf-o"></i>
														<span class="k-nav__link-text link-text">PDF</span>
													</a>
												</li>
											</ul>
										</div> <!-- / .dropdown-menu -->
									</div>
								</div>
							</div>
							<div class="carousel-inner" style="height:280px;">
								<div class="carousel-item k-slider__body active" >
									<div class="k-widget-13">
										<div class="k-widget-13__body">
											<div class="k-widget-9__chart">

												<div class="table-responsive mb-0" data-toggle="lists" data-lists-values="[&quot;goal-district&quot;, &quot;goal-dValue&quot;, &quot;goal-remarks&quot;]">
													<table class="table table-lg table-nowrap card-table table-horver " >
														<thead class="card-color  border-top  border-right border-left">
															<tr>
																<th>
																	<a href="#" class="text-muted sort" data-sort="goal-district">
																		District
																	</a>
																</th>
																<th>
																	<a href="#" class="text-muted sort" data-sort="goal-dValue">
																		Number of Cases
																	</a>
																</th>
																<th>
																	<a href="#" class="text-muted sort" data-sort="goal-remarks">
																		Progress
																	</a>
																</th>

															</tr>
														</thead>
														<tbody class="lists">
															<tr class="hoverable">
																<td class="goal-district "> <b>Lilongwe</b></td>
																<td class="goal-dValue">25</td>
																<td class="goal-remarks ">
																	<div class="progress">
																		<div class="progress-bar bg-success" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
																	</div>

																</td>
															</tr>
															<tr class="hoverable">
																<td class="goal-age "><b>Zomba</b></td>
																<td class="goal-ageValue ">55</td>
																<td class="goal-remarks ">
																	<div class="progress">
																		<div class="progress-bar bg-success" role="progressbar" style="width: 55%;" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100">55%</div>
																	</div>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="carousel-item k-slider__body" >
									<div class="k-widget-13">
										<div class="k-widget-13__body">
											<div class="chart chart-appended chartjs-size-monitor chartjs-size-monitor-expand">
												<canvas id="districtStatistics" class="chart-canvas chartjs-render-monitor"  height="30" width="100" ></canvas>
												<!-- Legend -->
											</div>

										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>

				<!--end::Portlet-->
			</div>
		</div>

		<!-- issues component -->
		<div class="col-6">
			<div class="order-lg-1 order-xl-1">
				<!--begin::Portlet-->
				<div class="k-portlet k-portlet--height-fluid k-widget " >
					<div class="k-portlet__body">
						<div id="k-widget-slider-issue" class="k-slider carousel slide" data-ride="carousel" data-interval="10000">
							<div class="k-slider__head">
								<div class="k-slider__label">ISSUES </div>
								<div class="k-slider__nav">
									<a class="k-slider__nav-prev carousel-control-prev" href="#k-widget-slider-issue" role="button" data-slide="prev">
										<i class="fa fa-angle-left"></i>
									</a>
									<a class="k-slider__nav-next carousel-control-next" href="#k-widget-slider-issue" role="button" data-slide="next">
										<i class="fa fa-angle-right"></i>
									</a>

									<div class="col-auto dropdown droppdown-inline" >
										<!-- Toggle -->
										<a href="javascript: void(0);" class="icons drop-note" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" >
											<span class="icon ">
												<i class="fa fa-ellipsis-h text-muted"></i>
											</span>
										</a>

										<!-- Menu -->
										<div class="dropdown-menu  droppdown-menu droppdown-menu-right " x-placement="bottom-end" style="">
											<ul class="k-nav">
												<li class="k-nav__section k-nav__section--first">
													<span class="k-nav__section-text">Export Tools</span>
												</li>
												<li class="k-nav__item">
													<a href="javascript: window.print();" class="k-nav__link">
														<i class="k-nav__link-icon la fa fa-print"></i>
														<span class="k-nav__link-text link-text">Print</span>
													</a>
												</li>
												<li class="k-nav__item">
													<a href="#" class="k-nav__link">
														<i class="k-nav__link-icon la fa fa-copy"></i>
														<span class="k-nav__link-text link-text">Copy</span>
													</a>
												</li>
												<li class="k-nav__item">
													<a href="#" class="k-nav__link">
														<i class="k-nav__link-icon la fa fa-file-excel-o"></i>
														<span class="k-nav__link-text link-text">Excel</span>
													</a>
												</li>
												<li class="k-nav__item">
													<a href="#" class="k-nav__link link-text">
														<i class="k-nav__link-icon la fa fa-file-text-o"></i>
														<span class="k-nav__link-text link-text">CSV</span>
													</a>
												</li>
												<li class="k-nav__item">
													<a href="#" class="k-nav__link">
														<i class="k-nav__link-icon la fa fa-file-pdf-o"></i>
														<span class="k-nav__link-text link-text">PDF</span>
													</a>
												</li>
											</ul>
										</div> <!-- / .dropdown-menu -->
									</div>
								</div>
							</div>
							<div class="carousel-inner" style="height:280px;">
								<div class="carousel-item k-slider__body active" >
									<div class="k-widget-13">
										<div class="k-widget-13__body">
											<div class="k-widget-9__chart">

												<div class="table-responsive mb-0" data-toggle="lists" data-lists-values="[&quot;goal-gender&quot;, &quot;goal-Value&quot;, &quot;goal-progress&quot;]">
													<table class="table table-lg table-nowrap card-table table-horver " >
														<thead class="card-color  border-top  border-right border-left">
															<tr>
																<th>
																	<a href="#" class="text-muted sort" data-sort="goal-gender">
																		Issue Name
																	</a>
																</th>
																<th>
																	<a href="#" class="text-muted sort" data-sort="goal-Value">
																		Number of Cases
																	</a>
																</th>
																<th>
																	<a href="#" class="text-muted sort" data-sort="goal-progress">
																		Progress
																	</a>
																</th>

															</tr>
														</thead>
														<tbody class="lists">
															<tr class="hoverable">
																<td class="goal-gender "> <b>Emergency response WFP</b></td>
																<td class="goal-Value">25</td>
																<td class="goal-remarks ">
																	<div class="progress">
																		<div class="progress-bar bg-dark" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
																	</div>

																</td>
															</tr>
															<tr class="hoverable">
																<td class="goal-gender "><b>Physical</b></td>
																<td class="goal-Value ">55</td>
																<td class="goal-progress ">
																	<div class="progress">
																		<div class="progress-bar bg-dark" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
																	</div>

																</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="carousel-item k-slider__body" >
									<div class="k-widget-13">
										<div class="k-widget-13__body">
											<!-- Chart -->
											<div class="chart chart-appended chartjs-size-monitor chartjs-size-monitor-expand">
												<canvas id="issuePie" class="chart-canvas chartjs-render-monitor" data-target="#IssueLegend" height="30" width="100" ></canvas>
												<!-- Legend -->
												<div id="IssueLegend" class="pie-legend"><span class="pie-legend-item">
													<i class="pie-legend-indicator" style="background-color: #1cac81 !important;"></i>Male</span>
													<span class="pie-legend-item"><i class="pie-legend-indicator" style="background-color: #41675c !important;"></i>Female</span>
												</div>
											</div>
										</div>

									</div>
								</div>

							</div>
						</div>
					</div>
				</div>

				<!--end::Portlet-->
			</div>
		</div>



			<!-- district component -->
			<div class="col-6">
				<div class="order-lg-1 order-xl-1">
					<!--begin::Portlet-->
					<div class="k-portlet k-portlet--height-fluid k-widget " >
						<div class="k-portlet__body">
							<div id="k-widget-slider-subissue" class="k-slider carousel slide" data-ride="carousel" data-interval="10000">
								<div class="k-slider__head">
									<div class="k-slider__label">SUBISSUES </div>
									<div class="k-slider__nav">
										<a class="k-slider__nav-prev carousel-control-prev" href="#k-widget-slider-subissue" role="button" data-slide="prev">
											<i class="fa fa-angle-left"></i>
										</a>
										<a class="k-slider__nav-next carousel-control-next" href="#k-widget-slider-subissue" role="button" data-slide="next">
											<i class="fa fa-angle-right"></i>
										</a>

										<div class="col-auto dropdown droppdown-inline" >
											<!-- Toggle -->
											<a href="javascript: void(0);" class="icons drop-note" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" >
												<span class="icon ">
													<i class="fa fa-ellipsis-h text-muted"></i>
												</span>
											</a>

											<!-- Menu -->
											<div class="dropdown-menu  droppdown-menu droppdown-menu-right " x-placement="bottom-end" style="">
												<ul class="k-nav">
													<li class="k-nav__section k-nav__section--first">
														<span class="k-nav__section-text">Export Tools</span>
													</li>
													<li class="k-nav__item">
														<a href="javascript: window.print();" class="k-nav__link">
															<i class="k-nav__link-icon la fa fa-print"></i>
															<span class="k-nav__link-text link-text">Print</span>
														</a>
													</li>
													<li class="k-nav__item">
														<a href="#" class="k-nav__link">
															<i class="k-nav__link-icon la fa fa-copy"></i>
															<span class="k-nav__link-text link-text">Copy</span>
														</a>
													</li>
													<li class="k-nav__item">
														<a href="#" class="k-nav__link">
															<i class="k-nav__link-icon la fa fa-file-excel-o"></i>
															<span class="k-nav__link-text link-text">Excel</span>
														</a>
													</li>
													<li class="k-nav__item">
														<a href="#" class="k-nav__link link-text">
															<i class="k-nav__link-icon la fa fa-file-text-o"></i>
															<span class="k-nav__link-text link-text">CSV</span>
														</a>
													</li>
													<li class="k-nav__item">
														<a href="#" class="k-nav__link">
															<i class="k-nav__link-icon la fa fa-file-pdf-o"></i>
															<span class="k-nav__link-text link-text">PDF</span>
														</a>
													</li>
												</ul>
											</div> <!-- / .dropdown-menu -->
										</div>
									</div>
								</div>
								<div class="carousel-inner" style="height:280px;">
									<div class="carousel-item k-slider__body active" >
										<div class="k-widget-13">
											<div class="k-widget-13__body">
												<div class="k-widget-9__chart">

													<div class="table-responsive mb-0" data-toggle="lists" data-lists-values="[&quot;goal-subissues&quot;, &quot;goal-dValue&quot;, &quot;goal-remarks&quot;]">
														<table class="table table-lg table-nowrap card-table table-horver " >
															<thead class="card-color  border-top  border-right border-left">
																<tr>
																	<th>
																		<a href="#" class="text-muted sort" data-sort="goal-subissues">
																			Subissue Name
																		</a>
																	</th>
																	<th>
																		<a href="#" class="text-muted sort" data-sort="goal-dValue">
																			Number of Cases
																		</a>
																	</th>
																	<th>
																		<a href="#" class="text-muted sort" data-sort="goal-remarks">
																			Progress
																		</a>
																	</th>

																</tr>
															</thead>
															<tbody class="lists">
																<tr class="hoverable">
																	<td class="goal-subissues "> <b>Lilongwe</b></td>
																	<td class="goal-dValue">25</td>
																	<td class="goal-remarks ">
																		<div class="progress">
																			<div class="progress-bar bg-success" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
																		</div>

																	</td>
																</tr>
																<tr class="hoverable">
																	<td class="goal-age "><b>Zomba</b></td>
																	<td class="goal-ageValue ">55</td>
																	<td class="goal-remarks ">
																		<div class="progress">
																			<div class="progress-bar bg-success" role="progressbar" style="width: 55%;" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100">55%</div>
																		</div>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="carousel-item k-slider__body" >
										<div class="k-widget-13">
											<div class="k-widget-13__body">
												<div class="chart chart-appended chartjs-size-monitor chartjs-size-monitor-expand">
													<canvas id="subissuesStatistics" class="chart-canvas chartjs-render-monitor"  height="30" width="100" ></canvas>
													<!-- Legend -->
												</div>

											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>

					<!--end::Portlet-->
				</div>
			</div>

			<!-- issues component -->
			<div class="col-6">
				<div class="order-lg-1 order-xl-1">
					<!--begin::Portlet-->
					<div class="k-portlet k-portlet--height-fluid k-widget " >
						<div class="k-portlet__body">
							<div id="k-widget-slider-ds" class="k-slider carousel slide" data-ride="carousel" data-interval="10000">
								<div class="k-slider__head">
									<div class="k-slider__label">District BY Subissue </div>
									<div class="k-slider__nav">
										<a class="k-slider__nav-prev carousel-control-prev" href="#k-widget-slider-ds" role="button" data-slide="prev">
											<i class="fa fa-angle-left"></i>
										</a>
										<a class="k-slider__nav-next carousel-control-next" href="#k-widget-slider-ds" role="button" data-slide="next">
											<i class="fa fa-angle-right"></i>
										</a>

										<div class="col-auto dropdown droppdown-inline" >
											<!-- Toggle -->
											<a href="javascript: void(0);" class="icons drop-note" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" >
												<span class="icon ">
													<i class="fa fa-ellipsis-h text-muted"></i>
												</span>
											</a>

											<!-- Menu -->
											<div class="dropdown-menu  droppdown-menu droppdown-menu-right " x-placement="bottom-end" style="">
												<ul class="k-nav">
													<li class="k-nav__section k-nav__section--first">
														<span class="k-nav__section-text">Export Tools</span>
													</li>
													<li class="k-nav__item">
														<a href="javascript: window.print();" class="k-nav__link">
															<i class="k-nav__link-icon la fa fa-print"></i>
															<span class="k-nav__link-text link-text">Print</span>
														</a>
													</li>
													<li class="k-nav__item">
														<a href="#" class="k-nav__link">
															<i class="k-nav__link-icon la fa fa-copy"></i>
															<span class="k-nav__link-text link-text">Copy</span>
														</a>
													</li>
													<li class="k-nav__item">
														<a href="#" class="k-nav__link">
															<i class="k-nav__link-icon la fa fa-file-excel-o"></i>
															<span class="k-nav__link-text link-text">Excel</span>
														</a>
													</li>
													<li class="k-nav__item">
														<a href="#" class="k-nav__link link-text">
															<i class="k-nav__link-icon la fa fa-file-text-o"></i>
															<span class="k-nav__link-text link-text">CSV</span>
														</a>
													</li>
													<li class="k-nav__item">
														<a href="#" class="k-nav__link">
															<i class="k-nav__link-icon la fa fa-file-pdf-o"></i>
															<span class="k-nav__link-text link-text">PDF</span>
														</a>
													</li>
												</ul>
											</div> <!-- / .dropdown-menu -->
										</div>
									</div>
								</div>
								<div class="carousel-inner" style="height:280px;">
									<div class="carousel-item k-slider__body active" >
										<div class="k-widget-13">
											<div class="k-widget-13__body">
												<div class="k-widget-9__chart">

													<div class="table-responsive mb-0" data-toggle="lists" data-lists-values="[&quot;goal-district&quot;,&quot;goal-subissue&quot;, &quot;goal-Value&quot;, &quot;goal-progress&quot;]">
														<table class="table table-lg table-nowrap card-table table-horver " >
															<thead class="card-color  border-top  border-right border-left">
																<tr>
																	<th>
																		<a href="#" class="text-muted sort" data-sort="goal-district">
																			District
																		</a>
																	</th>
																	<th>
																		<a href="#" class="text-muted sort" data-sort="goal-subissue">
																			Subissue
																		</a>
																	</th>
																	<th>
																		<a href="#" class="text-muted sort" data-sort="goal-Value">
																			Number of Cases
																		</a>
																	</th>
																	<th>
																		<a href="#" class="text-muted sort" data-sort="goal-progress">
																			Progress
																		</a>
																	</th>

																</tr>
															</thead>
															<tbody class="lists">
																<tr class="hoverable">
																	<td class="goal-district "> <b>Lilongwe</b></td>
																	<td class="goal-subissue "> <b>Emergency response WFP</b></td>
																	<td class="goal-Value">25</td>
																	<td class="goal-remarks ">
																		<div class="progress">
																			<div class="progress-bar bg-dark" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
																		</div>

																	</td>
																</tr>
																<tr class="hoverable">
																	<td class="goal-district "> <b>Zomba</b></td>
																	<td class="goal-subissue "> <b>Physical</b></td>
																	<td class="goal-Value ">55</td>
																	<td class="goal-progress ">
																		<div class="progress">
																			<div class="progress-bar bg-dark" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
																		</div>

																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="carousel-item k-slider__body" >
										<div class="k-widget-13">
											<div class="k-widget-13__body">
												<!-- Chart -->
												<div class="chart chart-appended chartjs-size-monitor chartjs-size-monitor-expand">
													<canvas id="mypieGender" class="chart-canvas chartjs-render-monitor" data-target="#ChartLegend" height="30" width="100" ></canvas>
													<!-- Legend -->
													<div id="ChartLegend" class="pie-legend"><span class="pie-legend-item">
														<i class="pie-legend-indicator" style="background-color: #1cac81 !important;"></i>Male</span>
														<span class="pie-legend-item"><i class="pie-legend-indicator" style="background-color: #41675c !important;"></i>Female</span>
													</div>
												</div>
											</div>

										</div>
									</div>

								</div>
							</div>
						</div>
					</div>

					<!--end::Portlet-->
				</div>
			</div>

		</div>

</div>
@endsection
