@extends('partner.layouts.master')

@section('content')

<div class="container-fluid py-5">
  <div class="row py-3 ">
    @if(Session::has('message'))
  	<div class="alert alert-success" role="alert" id="form_msg">
  		<div class="alert-icon"><i class="fa fa-check"></i></div>
  		<div class="alert-text">
  			<b>{{Auth::user()->username}} </b>
  			{{Session::get('message')}}
  		</div>
  		<div class="alert-close" style="position: absolute; top: 2px;
  		right:2px;">
  			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
  				<span aria-hidden="true"><i class="fa fa-close"></i></span>
  			</button>
  		</div>
  	</div>
  	<script type="text/javascript">
  		$(document).ready(function(){
  			var note = setTimeout(refresh , 3000);
  			clearTimeout(note);

  			function refresh() {
  				// body...
  				$("#form_msg").fadeOut('slow', function() {
              $("#form_msg").hide();
          });

  			}
  		});
  	</script>
  	@endif
    <div class="">
        <!-- Popular Cases -->
        <div class="card " >
          <div class="card-header card-color p-3">
            <div class="row align-items-center">
              <div class="col">

                <!-- Title -->
                <h5 class="card-header-title">
                  <span class="card-title text-uppercase text-muted mb-2"> <b>{{$caseStatus}}</b></span>
                </h5>

              </div>
              <div class="col-auto">

                <!-- Link -->
                <a href="#!" class=" text-muted">
                  <span class="small">View all</span>
                </a>

              </div>
            </div> <!-- / .row -->
          </div>
          <div  class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="table-responsive mb-0 be-datatable-header" data-toggle="lists" data-lists-values="[&quot;goal-project&quot;, &quot;goal-status&quot;, &quot;goal-progress&quot;, &quot;goal-date&quot;]">
              <table class="table table-sm table-nowrap card-table table-hover table-fw-widget" id="dvData" style="width:70rem;">
                <thead class="thead ">
                  <tr>
                    <th>
                      <a href="#" class="text-muted h6" data-sort="goal-project">
                        <b>Date</b>
                      </a>
                    </th>
                    <th>
                      <a href="#" class="text-muted sort h6" data-sort="goal-status">
                        <b>District</b>
                      </a>
                    </th>

                    <th>
                      <a href="#" class="text-muted sort h6" data-sort="goal-date">
                        <b>Comment</b>
                      </a>
                    </th>
                    <th class="text-right ">

                    </th>
                    <th class="text-right ">

                    </th>
                  </tr>
                </thead>
                <tbody class="lists">
                  @if($data['cases']->total() == 0)
                  <tr class="card-body" style="height:10rem; width:56rem;"></tr>
                  <tr class="alert  align-items-center" role="alert " >
                    <td class="alert-text text-center " colspan="3" >
                      No recent {{$caseStatus}} data yet
                      <b>{{Auth::user()->username}}  </b>
                    </td>
                  </tr>
                  <tr class="card-body" style="height:10.7rem; width:56rem;"></tr>
                  @else
                  @foreach($cases as $case)
                  <tr class="hoverable" data-id="{{$case->id}}"  id="viewcasedetails">
                    <td>
                      <time >
                        <script >
                        function dateConvert(dateobj, format){
                          var year = dateobj.getFullYear();
                          var month= ("0" + (dateobj.getMonth()+1)).slice(-2);
                          var date = ("0" + dateobj.getDate()).slice(-2);
                          var hours = ("0" + dateobj.getHours()).slice(-2);
                          var minutes = ("0" + dateobj.getMinutes()).slice(-2);
                          var seconds = ("0" + dateobj.getSeconds()).slice(-2);
                          var day = dateobj.getDay();
                          var months = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
                          var dates = ["SUN","MON","TUE","WED","THU","FRI","SAT"];
                          var converted_date = "";

                          switch(format){
                            case "YYYY-MMM-DD":
                            converted_date = date  + "-" + months[parseInt(month)-1] + "-" +year ;
                            break;
                            case "DD-MMM":
                            converted_date = date + " " + months[parseInt(month)-1];
                            break;
                          }

                          return converted_date;
                        }
                        var i_date = "{{$case->date}}";
                        var j_date = new Date(Date.parse(i_date));
                        var date = dateConvert(j_date, "YYYY-MMM-DD");
                        document.write( '<b>'+date+'</b>');
                        </script>
                      </time>
                    </td>
                    <td>
                      <b class="">{{$case->ta->district->name}}</b>
                    </td>
                    <td class="comment" title="click to see the details">
                      <script type="">
                      String.prototype.breakAt = function(breakAt) {
                        return this.substr(0,breakAt) + '<br/>' + this.substr(breakAt);
                      };
                      var string = "{{ substr(trim($case->comments) , 0, 280)}}...";
                      var	res = string.breakAt(140);
                      document.write('' + res +'');
                      </script>

                    </td>
                    <td class="border-left " id="case-edit" data-num1="{{$case->id}}">
                      <a class="text-muted fa-icon" href="/caseEdit?id={{$case->id}}">
                        <i class="fa fa-pencil fa-lg" title="edit"></i>
                      </a>
                    </td>
                    <td class="border-left" id="case-delete" data-num="{{$case->id}}">
                      <a class="text-danger fa-icon" href="/caseDelete?id={{$case->id}}">
                        <i class="fa fa-trash fa-lg" title="delete"></i>
                      </a>
                    </td>
                  </tr>
                  @endforeach
                  <tr class="alert card-color align-items-center" role="alert " >
                    <td class="alert-text  "  colspan="4">
                      <span class="list-inline row">
                        <b class="col-2">{!!$data['cases']->total()!!} Cases</b>
                        <b class="col-2">Showing {!!$data['cases']->count()!!} case per page
                        </b>
                        <b class="col-7"></b>
                        <b class="col-1 float-right" >
                          <a class="text-muted " href="{!! $data['cases']->url($data['cases']->currentPage()-1)!!}" aria-label="Previous">
                            <span class="fa fa-chevron-circle-left fa-lg"></span>
                            <span class="sr-only">Previous</span>
                          </a>

                          <a class="text-muted " href="{!! $data['cases']->url($data['cases']->currentPage()+1)!!}" aria-label="Next">
                            <span class="fa fa-chevron-circle-right fa-lg"></span>
                            <span class="sr-only">Next</span>
                          </a>
                        </b>
                      </span>

                    </td>
                  </tr>
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div> <!-- / .card-body -->


        <!--  modal for card details-->
        <div class="modal fade bd-example-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header bg-dark align-items-center">
                <h6 class="card-header-title">
                  <span class=" card-title text-center text-uppercase text-white mb-2 "> <b>Client Information</b></span>
                </h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true" class="text-danger fa fa-times-circle">x</span>
                </button>
              </div>
              <div class="modal-body ">
                  <div class="col-reg">
                    <span id="modal-case"></span>
                  </div>
              </div>
              <div class="modal-footer">

                <button type="button" class="btn btn-outline-secondary btn-sm" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline-danger btn-sm" id="caseDelete">Delete </button>
                <button type="button" class="btn btn-outline-primary btn-sm">Share </button>
                <button type="button" class="btn btn-outline-success btn-sm">Export </button>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
@endsection
