@extends('partner.layouts.master')

@section('content')

<div class="container-fluid py-5">
	<div class="row py-3 ">

		<div class="col-sm-3">
			<div class="card hoverable" id="total-cases">
				<div class="card-body">
					<div class="row align-items-center">
						<div class="col">
							<h6><small class="card-title text-uppercase text-muted mb-2"> <b>TOTAL CASES </b></small></h6>
							<!-- Heading -->
							<span class="h3 mb-0">
								<b >
									<script>
									function totalCases(unresolved, resolved) {
										// body...
										var result = unresolved + resolved;
										return result;
									}
									var tot = setInterval(totalCases, 3000);
									clearInterval(tot);
									var res = totalCases(<?php echo $totalResolvedCases ?>, <?php echo $totalClosedCases ?>);
									document.write(res);

									</script>
								</b>
							</span>

							<!-- Badge -->
							<span class="badge badge-success mt-1">
								+3.5%
							</span>
						</div>
						<div class="col-auto">
							<!-- Icon -->
							<span class="fa-2x fa fa-clone  text-muted mb-1"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="card hoverable" id="total-resolved-cases">
				<div class="card-body">
					<div class="row align-items-center">
						<div class="col">
							<h6><small class="card-title text-uppercase text-muted mb-2"> <b>TOTAL CASES RESOLVED</b></small></h6>
							<!-- Heading -->
							<span class="h3 mb-0">
								<b>
									<script>
									var res = <?php echo $totalResolvedCases ?>;
									var tot = setInterval(res, 3000);
									clearInterval(tot);
									document.write(res);

									</script>
								</b>
							</span>

							<!-- Badge -->
							<span class="badge badge-success mt-1">
								+3.5%
							</span>
						</div>
						<div class="col-auto">
							<!-- Icon -->
							<span class="fa-2x fa fa-folder  text-muted mb-1"></span>

						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="card hoverable" id="total-unresolved-cases">
				<div class="card-body">
					<div class="row align-items-center">
						<div class="col">
							<h6><small class="card-title text-uppercase text-muted mb-2"> <b>TOTAL CASES UNRESOLVED</b></small></h6>
							<!-- Heading -->
							<span class="h3 mb-0">
								<b>
									<script>
									var res = <?php echo $totalClosedCases ?>;
									var tot = setInterval(res, 3000);
									clearInterval(tot);
									document.write(res);

									</script>
								</b>
							</span>

							<!-- Badge -->
							<span class="badge badge-success mt-1">
								+3.5%
							</span>
						</div>
						<div class="col-auto">
							<!-- Icon -->
							<span class="fa-2x fa fa-folder-open  text-muted mb-1"></span>

						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="card hoverable" id="total-pending-cases">
				<div class="card-body">
					<div class="row align-items-center">
						<div class="col">
							<h6><small class="card-title text-uppercase text-muted mb-2"> <b>TOTAL CASES PENDING</b></small></h6>
							<!-- Heading -->
							<span class="h3 mb-0">
								<b>
									<script>
									var res = <?php echo $totalPendingCases ?>;
									var tot = setInterval(res, 3000);
									clearInterval(tot);
									document.write(res);

									</script>

								</b>
							</span>

							<!-- Badge -->
							<span class="badge badge-success mt-1">
								+3.5%
							</span>
						</div>
						<div class="col-auto">
							<!-- Icon -->
							<span class="fa-2x fa fa-folder-o  text-muted mb-1"></span>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row py-3 ">
		<div class="col-sm-7">
			<div class=" ">
				<div class="">
					<div class="order-lg-1 order-xl-1">
						<!--begin::Portlet-->
						<div class="k-portlet k-portlet--height-fluid k-widget " >
							<div class="k-portlet__body">
								<div id="k-widget-slider-13-2" class="k-slider carousel slide" data-ride="carousel" data-interval="10000">
									<div class="k-slider__head">
										<div class="k-slider__label">CASE STATUS TREND</div>
										<div class="k-slider__nav">
											<a class="k-slider__nav-prev carousel-control-prev" href="#k-widget-slider-13-2" role="button" data-slide="prev">
												<i class="fa fa-angle-left"></i>
											</a>
											<a class="k-slider__nav-next carousel-control-next" href="#k-widget-slider-13-2" role="button" data-slide="next">
												<i class="fa fa-angle-right"></i>
											</a>

											<div class="col-auto dropdown droppdown-inline" >
												<!-- Toggle -->
												<a href="javascript: void(0);" class="icons drop-note" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" >
													<span class="icon ">
														<i class="fa fa-ellipsis-h text-muted"></i>
													</span>
												</a>

												<!-- Menu -->
												<div class="dropdown-menu  droppdown-menu droppdown-menu-right " x-placement="bottom-end" style="">
													<ul class="k-nav">
														<li class="k-nav__section k-nav__section--first">
															<span class="k-nav__section-text">Export Tools</span>
														</li>
														<li class="k-nav__item">
															<a href="javascript: window.print();" class="k-nav__link">
																<i class="k-nav__link-icon la fa fa-print"></i>
																<span class="k-nav__link-text link-text">Print</span>
															</a>
														</li>
														<li class="k-nav__item">
															<a href="#" class="k-nav__link">
																<i class="k-nav__link-icon la fa fa-copy"></i>
																<span class="k-nav__link-text link-text">Copy</span>
															</a>
														</li>
														<li class="k-nav__item">
															<a href="#" class="k-nav__link">
																<i class="k-nav__link-icon la fa fa-file-excel-o"></i>
																<span class="k-nav__link-text link-text">Excel</span>
															</a>
														</li>
														<li class="k-nav__item">
															<a href="#" class="k-nav__link link-text">
																<i class="k-nav__link-icon la fa fa-file-text-o"></i>
																<span class="k-nav__link-text link-text">CSV</span>
															</a>
														</li>
														<li class="k-nav__item">
															<a href="#" class="k-nav__link">
																<i class="k-nav__link-icon la fa fa-file-pdf-o"></i>
																<span class="k-nav__link-text link-text">PDF</span>
															</a>
														</li>
													</ul>
												</div> <!-- / .dropdown-menu -->
											</div>
										</div>
									</div>
									<div class="carousel-inner">
										<div class="carousel-item k-slider__body active">
											<div class="k-widget-13">
												<div class="k-widget-13__body" id="chart">
													<div class="k-widget-9__chart">

														<div class="chartjs-size-monitor" >
															<div class="chartjs-size-monitor-expand" >
																<div ></div>
															</div>
															<div class="chartjs-size-monitor-shrink" >
																<div >

																</div>
															</div>
														</div>

														<canvas id="statistics" height="285.8" width="1077" class="chartjs-render-monitor" ></canvas>
													</div>
												</div>
											</div>
										</div>

										<div class="carousel-item k-slider__body" style="height: 18.9rem;">
											<div class="k-widget-13">
												<div class="k-widget-13__body">
													<!-- Chart -->
													<div class="chart chart-appended chartjs-size-monitor chartjs-size-monitor-expand">
														<canvas id="myDoughnut" class="chart-canvas chartjs-render-monitor" data-target="#devicesChartLegend" height="150" width="672" ></canvas>
													</div>

													<!-- Legend -->
													<div id="devicesChartLegend" class="chart-legend"><span class="chart-legend-item"><i class="chart-legend-indicator" style="background-color: #1cac81 !important;"></i>Resolved</span><span class="chart-legend-item"><i class="chart-legend-indicator" style="background-color: #41675c !important;"></i>Unresolved</span><span class="chart-legend-item"><i class="chart-legend-indicator" style="background-color: #80c3af !important;"></i>Pending</span></div>
												</div>

											</div>
										</div>

									</div>
								</div>
							</div>
						</div>

						<!--end::Portlet-->
					</div>
					</div>

				</div>
			</div>
			<div class="col-sm-5">
				<div class="card" >
					<div class="  ">
						<div class="order-lg-1 order-xl-1">
							<!--begin::Portlet-->
							<div class="k-portlet k-portlet--height-fluid k-widget " >
								<div class="k-portlet__body">
									<div id="k-widget-slider-13-1" class="k-slider carousel slide" data-ride="carousel" data-interval="8000">
										<div class="k-slider__head">
											<div class="k-slider__label">Announcements</div>
											<div class="k-slider__nav">
												<a class="k-slider__nav-prev carousel-control-prev" href="#k-widget-slider-13-1" role="button" data-slide="prev">
													<i class="fa fa-angle-left"></i>
												</a>
												<a class="k-slider__nav-next carousel-control-next" href="#k-widget-slider-13-1" role="button" data-slide="next">
													<i class="fa fa-angle-right"></i>
												</a>
											</div>
										</div>
										<div class="carousel-inner">
											<div class="carousel-item k-slider__body active">
												<div class="k-widget-13">
													<div class="k-widget-13__body">
														<a class="k-widget-13__title" href="#"><b>More Calls under GBV in Balaka</b> </a>
														<div class="k-widget-13__desc">
															To start a blog, think of a topic about and first brainstorm party is ways to write details
														</div>
													</div>
													<div class="k-widget-13__foot">
														<div class="k-widget-13__label">
															<div class="btn btn-sm btn-label btn-bold">
																07 OCT, 2018
															</div>
														</div>
														<div class="k-widget-13__toolbar">
															<a href="#" class="btn btn-default btn-sm btn-bold btn-upper">View</a>
														</div>
													</div>
												</div>
											</div>

											<div class="carousel-item k-slider__body">
												<div class="k-widget-13">
													<div class="k-widget-13__body">
														<a class="k-widget-13__title" href="#"> <b>Incredibly Positive Change</b></a>
														<div class="k-widget-13__desc">
															To start a blog, think of a topic about and first brainstorm party is ways to write details
														</div>
													</div>
													<div class="k-widget-13__foot">
														<div class="k-widget-13__title">
															<div class="btn btn-sm btn-label btn-bold">
																17 NOV, 2018
															</div>
														</div>
														<div class="k-widget-13__toolbar">
															<a href="#" class="btn btn-default btn-sm btn-bold btn-upper">View</a>
														</div>
													</div>
												</div>
											</div>

											<div class="carousel-item k-slider__body">
												<div class="k-widget-13">
													<div class="k-widget-13__body">
														<a class="k-widget-13__title" href="#"><b>Recent increase change pending Cases</b></a>
														<div class="k-widget-13__desc">
															To start a blog, think of a topic about and first brainstorm party is ways to write details
														</div>
													</div>
													<div class="k-widget-13__foot">
														<div class="k-widget-13__label">
															<div class="btn btn-sm btn-label btn-bold">
																03 SEP, 2018
															</div>
														</div>
														<div class="k-widget-13__toolbar">
															<a href="#" class="btn btn-default btn-sm btn-bold btn-upper">View</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<!--end::Portlet-->
						</div>
					</div>
						<div class="card-body" style="height: 6.7rem;">
							<canvas id="myChart" width="220" height="70" class="chartjs-render-monitor" style="display: block; position: absolute; top: 19em;"></canvas>
						</div>

					</div>
				</div>
			</div>

			<div class="row py-3 ">
				<div class="col-sm-5" >

					<!-- Popular Cases -->
					<div class="card " >
						<div class="card-header card-color p-3">
							<div class="row align-items-center">
								<div class="col">

									<!-- Title -->
									<h6 class="card-header-title">
										<span class="card-title text-uppercase text-muted mb-2"> <b>Popular Cases</b></span>
									</h6>

								</div>
								<div class="col-auto">

									<!-- Link -->
									<a href="#!" class=" text-muted">
										<span class="small">View all</span>
									</a>
								</div>
							</div> <!-- / .row -->
						</div>
						<div class="table-responsive mb-0" data-toggle="lists" data-lists-values="[&quot;goal-project&quot;, &quot;goal-status&quot;, &quot;goal-progress&quot;, &quot;goal-date&quot;]">
							<table class="table table-sm table-nowrap card-table table-hover" >
								<thead class="thead">
									<tr>
										<th>
											<a href="#" class="text-muted sort" data-sort="goal-project">
												Issue
											</a>
										</th>

										<th>
											<a href="#" class="text-muted sort" data-sort="goal-date">
												Last Update
											</a>
										</th>
										<th class="text-right ">
											value
										</th>

									</tr>
								</thead>
								<tbody class="lists">
									@foreach($partnerIssues as $case)
									@if($case->value > 0)

									<tr class="hoverable">
										<td class="goal-project text-left">
											<script type="">
											String.prototype.breakAt = function(breakAt) {
												return this.substr(0,breakAt) + '<br/>' + this.substr(breakAt);
											};
											var string = "{{$case->name}}";
											var	res = string.breakAt(26);
											document.write('<b>' + res +'</b>');
											</script>

										</td>

										<td class="goal-date text-left">
											<time datetime="2018-10-24">{{$case->date}}</time>
										</td>
										<td class="text-center">
											<b>{{$case->value}}</b>
										</td>

									</tr>
									@endif
									@endforeach

								</tbody>
							</table>
						</div>
					</div> <!-- / .card-body -->
				</div> <!-- / .col-sm-5 -->

				<div class="col-sm-7">
					<div class="card" >
						<div class="card-header card-color h-3" >
							<div class="row align-items-center">
								<div class="col ">

									<!-- Title -->
									<h6 class="card-header-title">
										<span class="card-title text-uppercase text-muted mb-2"> <span class="fa fa-clone"></span> <b>RECENT CASES</b></span>
									</h6>

								</div>

								<div class="col-auto" >
									<ul class=" nav nav-tabs nav-tabs-sm card-header-tabs " id="myTab" role="tablist">
										<li class="nav-item">
											<a class="nav-link active text-uppercase text-muted " id="resolved-tab" data-toggle="tab" href="#resolved" role="tab" aria-controls="resolved" aria-selected="true">
												<span class="fa fa-folder"></span>	<b>Resolved</b>
											</a>
										</li>
										<li class="nav-item">
											<a class="nav-link text-uppercase text-muted" id="unresolved-tab" data-toggle="tab" href="#unresolved" role="tab" aria-controls="unresolved" aria-selected="false">
												<span class="fa fa-folder-open"></span><b>UnResolved</b></a>
											</li>

										</ul>
									</div>

								</div> <!-- / .row -->

							</div>
							<div class="card-header card-color" >

								<div class="row">
									<div class="col-12">

										<!-- Form -->
										<form>
											<div class="input-group input-group-flush input-group-merge">
												<input type="search" class="form-control form-control-prepended search" placeholder="Search">
												<div class="input-group-prepend">
													<div class="input-group-text">
														<span class="fa fa-search"></span>
													</div>
												</div>
											</div>
										</form>

									</div>
								</div> <!-- / .row -->
							</div>
							<div class="card-body ">
								<div class="tab-content my--2" id="nav-tabContent">
									<div class="tab-content" id="myTabContent">
										<div class="tab-pane fade show active " id="resolved" role="tabpanel" aria-labelledby="resolved-tab">
											<!-- List -->
											<ul class="list-group list-group-lg list-group-flush list my--4 resolved" data-spy="scroll">
												@foreach($latestResolvedCases as $latestResolvedCase)

												<li class="list-group-item " >
													<div class="row align-items-center ">
														<div class="col ml--2 list-hoverable" id="case-view" data-id="{{ $latestResolvedCase->id}}" data-url="/caseClose?">
															<!-- Title -->
															<p class="card-title mb-1 name">
																{{ substr(trim($latestResolvedCase->comments) , 0, 158)}}...
															</p>
															<!-- Time -->
															<p class="card-text small text-muted">
																Uploaded by Dianna Smiley on <time datetime="2018-01-03">Jan 3, 2018</time>
															</p>

														</div>


														<div class="col-auto">

															<a class=" card-link float-right card2-popover"  tabindex="-1" role="button" data-html="true" data-toggle="popover" data-container="body" data-placement="bottom" data-trigger="focus"  data-popover-content="#body">
																<b>
																	<i title="more" class="text-muted fa fa-ellipsis-v fa-lg"></i>
																</b>
															</a>
															<div class=" card2-share hidden" id="body">

																<div class=" btn-group btn-group-sm">
																	<!-- view -->
																	<button  class="btn btn-outline-secondary   btn-sm " id="case-download" data-url="/casedownload?" data-id="{{ $latestResolvedCase->id}}"><i class="fa fa-download" title="download" ></i></button>
																	<!-- Edit -->
																	<a  class="btn btn-outline-success  btn-sm " href="/caseEdit?id={{$latestResolvedCase->id}}"><i class="fa fa-edit"  ></i>
																	</a>
																	<a  class="btn btn-outline-danger   btn-sm " href="/caseDelete?id={{$latestResolvedCase->id}}"><i class="fa fa-trash-o" title="delete" ></i></a>


																</div>


															</div>
														</div> <!-- / .row -->

													</li>
													@endforeach

												</ul>
											</div>

											<div class="tab-pane fade my--3" id="unresolved" role="tabpanel" aria-labelledby="unresolved-tab">
												<ul class="list-group list-group-lg list-group-flush list my--4">
													@foreach($latestClosedCases as $latestClosedCase)

													<li class="list-group-item ">
														<div class="row align-items-center">
															<div class="col ml--2">
																<!-- Title -->
																<h6 class="card-title mb-1 name text-muted">
																	{{ substr(trim($latestClosedCase->comments) , 0, 90)}}...
																</h6>
																<!-- Time -->
																<p class="card-text small text-muted">
																	Uploaded by Dianna Smiley on <time datetime="2018-01-03">2018-01-03</time>
																</p>

															</div>

															<div class="col-auto">

																<a class=" card-link float-right card-popover"  tabindex="-1" role="button" data-html="true" data-toggle="popover" data-container="body" data-placement="bottom" data-trigger="focus"  data-popover-content="#body">
																	<b>
																		<i title="more" class="text-muted fa fa-ellipsis-v fa-lg"></i>
																	</b>
																</a>
																<div class=" card-share hidden" id="body">

																	<div class=" btn-group btn-group-sm">
																		<!-- view -->
																		<button  class="btn btn-outline-secondary   btn-sm " id="case-download" data-url="/casedownload?" data-id="{{ $latestClosedCase->id}}"><i class="fa fa-download" title="download" ></i></button>
																		<!-- Edit -->
																		<a  class="btn btn-outline-success  btn-sm " href="/caseEdit?id={{$latestClosedCase->id}}"><i class="fa fa-edit"  ></i>
																		</a>
																		<a  class="btn btn-outline-danger   btn-sm " href="/caseDelete?id={{$latestClosedCase->id}}"><i class="fa fa-trash-o" title="delete" ></i></a>


																	</div>


																</div>

															</div>
														</div> <!-- / .row -->

													</li>
													@endforeach

												</ul>
											</div>
										</div>
									</div>

								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
	@endsection
