@extends('partner.layouts.master')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4  mb-1 ">
      <h1 class="text-muted h5">Reports</h1>
      <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
          <button class="btn btn-sm btn-outline-secondary">Share</button>
          <button class="btn btn-sm btn-outline-secondary">Export</button>
        </div>
        <button type="file" class="btn btn-sm btn-outline-secondary " id="file-upload">
          <span title="upload a file" class="fa fa-paperclip"></span>
          Add File
          </button> <!-- /floating action -->
          <!-- title and toolbar -->
          <input type="file"  class="k-hide" name="upload" id="my-file">
      </div>
    </div>
<div class="container-fluid py-1">
  <!--  -->

  <div class="row py-3 ">
    <div class="col-sm-5" >

      <!-- Popular Cases -->
      <div class="card " >
        <div class="card-header card-color p-3">
          <div class="row align-items-center">
            <div class="col">

              <!-- Title -->
              <h6 class="card-header-title">
                <span class="card-title text-uppercase text-muted mb-2"> <b>Popular Cases</b></span>
              </h6>

            </div>
            <div class="col-auto">

              <!-- Link -->
              <a href="#!" class=" text-muted">
                <span class="small">View all</span>
              </a>
            </div>
          </div> <!-- / .row -->
        </div>
        <div class="table-responsive mb-0" data-toggle="lists" data-lists-values="[&quot;goal-project&quot;, &quot;goal-status&quot;, &quot;goal-progress&quot;, &quot;goal-date&quot;]">
          <table class="table table-sm table-nowrap card-table table-hover" >
            <thead class="thead">
              <tr>
                <th>
                  <a href="#" class="text-muted sort" data-sort="goal-project">
                    Issue
                  </a>
                </th>
                <th>
                  <a href="#" class="text-muted sort" data-sort="goal-status">
                    Status
                  </a>
                </th>

                <th>
                  <a href="#" class="text-muted sort" data-sort="goal-date">
                    Last Update
                  </a>
                </th>
                <th class="text-right ">
                  value
                </th>

              </tr>
            </thead>
            <tbody class="lists">

            </tbody>
          </table>
        </div>
      </div> <!-- / .card-body -->
    </div> <!-- / .col-sm-5 -->

    <div class="col-sm-7">
      <div class="card" >
        <div class="card-header card-color h-3" >
          <div class="row align-items-center">
            <div class="col ">

              <!-- Title -->
              <h6 class="card-header-title">
                <span class="card-title text-uppercase text-muted mb-2"> <span class="fa fa-clone"></span> <b>RECENT CASES</b></span>
              </h6>

            </div>

            <div class="col-auto" >
              <ul class=" nav nav-tabs nav-tabs-sm card-header-tabs " id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active text-uppercase text-muted " id="resolved-tab" data-toggle="tab" href="#resolved" role="tab" aria-controls="resolved" aria-selected="true">
                    <span class="fa fa-folder"></span>	<b>Resolved</b>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link text-uppercase text-muted" id="unresolved-tab" data-toggle="tab" href="#unresolved" role="tab" aria-controls="unresolved" aria-selected="false">
                    <span class="fa fa-folder-open"></span><b>UnResolved</b></a>
                  </li>

                </ul>
              </div>

            </div> <!-- / .row -->

          </div>
          <div class="card-header card-color" >

            <div class="row">
              <div class="col-12">

                <!-- Form -->
                <form>
                  <div class="input-group input-group-flush input-group-merge">
                    <input type="search" class="form-control form-control-prepended search" placeholder="Search">
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        <span class="fa fa-search"></span>
                      </div>
                    </div>
                  </div>
                </form>

              </div>
            </div> <!-- / .row -->
          </div>
          <div class="card-body ">
            <div class="tab-content my--2" id="nav-tabContent">
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active " id="resolved" role="tabpanel" aria-labelledby="resolved-tab">
                  <!-- List -->
                </div>

                <div class="tab-pane fade my--3" id="unresolved" role="tabpanel" aria-labelledby="unresolved-tab">

                </div>
              </div>
            </div>

          </div>

        </div>
      </div>
    </div>
  </div>

</div>
@endsection
