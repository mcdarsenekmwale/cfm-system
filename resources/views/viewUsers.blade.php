@extends('layouts.master')

@section('activeB','Manage Users')

@section('mainContent')

	<div class="page-content">
		<div class="ace-settings-container" id="ace-settings-container">
			<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
				<i class="ace-icon fa fa-cog bigger-130"></i>
			</div>
			<div class="ace-settings-box clearfix" id="ace-settings-box">
				<div class="pull-left width-50">
				</div><!-- /.pull-left -->
				<div class="pull-left width-50">
				</div><!-- /.pull-left -->
			</div><!-- /.ace-settings-box -->
		</div><!-- /.ace-settings-container -->
		<div class="page-header">
			<h1>Dashboard<small><i class="ace-icon fa fa-angle-double-right"></i>Users</small></h1>
		</div><!-- /.page-header -->
		<div class="row">
			@include('notifications')
			<div class="col-md-12">
				<div class="row">
					<div class="col-lg-8">
						<div class="panel panel-primary table-responsive">
							<table class="table table-hover table-striped">
								<thead class="thin-border-bottom panel-header">
									<tr>
										<th>Users</th>
										<th>Email</th>
										<th>Type</th>
										<th>edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
									@foreach($counsellors as $counsellor)
										<tr>
											<td>{{$counsellor->username}}</td>
											<td>{{$counsellor->email}}</td>
											<td>{{$counsellor->type}}</td>
											<td>
												<a href="editUser?id={{$counsellor->id}}" class="edit"><span class="glyphicon glyphicon-pencil"></span></a>
											</td>
											<td>
												<a href="deleteUser?id={{$counsellor->id}}" class="trash"><span class="glyphicon glyphicon-trash"></span></a>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						<div class="list-group-item" style="text-align:right">
								{{$counsellors->appends(['status' => Request::input('status')])->links()}}
						</div>
					</div>
				</div><!-- /.page-content -->
			</div>
		</div><!-- /.main-content -->
	@stop
