   @extends('.counsellor.OtherPartners.layout.master')


   @section('content')

   <div class="content-cards html py-5">
	   	<div class="body container-fluid k-container py-5">
	   		<div class="k-content k-grid__item k-grid__item--fluid" id="k_content">
	   			<div class="small text-muted"> <b>CONGOMA
	   		 	 	<span class="fa fa-angle-right fa-lg" style="position: relative; right: -10px;"></span><span class="fa fa-angle-right fa-lg " style="position: relative; right: -10px;"></span> </b>
	   			</div>
	   			@include('notifications')

					<!--begin::Portlet-->
				<form class="k-form k-form--label-right" name="savecall" novalidate method="post" action="{{route('savecall')}}">
					 @csrf
					<div class="row">
							<div class="col-lg-6">
							<div class="k-portlet" >
								<div class="k-portlet__head bg-dark ">
									<div class="k-portlet__head-label">
										<h3 class="k-portlet__head-title text-white  text-uppercase">CALL DETAILS </h3>
									</div>
								</div>
								<!--begin::Form-->

									<div class="k-portlet__body" style="height: 55.1rem;">
										<div class="k-section">
											<h3 class="k-section__title small">
												counsellor Information:
											</h3>
											<div class="k-section__content">
												<div class=" row">
													<div class="col-lg-6 form-group">
														<label class="form-control-label text-muted">{{ __('* counsellor:') }}</label>
														<input type="text" name="counsellor" class="form-control counsellor" placeholder="{{Auth::user()->username}}" value="" autocomplete="on" value="{{Auth::user()->username}}" autosave="true" required="true">

													</div>

													<div class="col-lg-6 form-group">
														<label class="form-control-label text-muted">*Today's Date:</label>
														<input type="text" name="date" class="form-control datepicker" data-provide="datepicker" placeholder="" value="<?php date_default_timezone_set('Africa/Blantyre'); echo date("F j, Y,")?>">
													</div>
												</div>
												<div class="form-group form-group-last row">
													<div class="col-lg-6 form-group-sub">
														<label class="form-control-label text-muted">* Start Time</label>
														<input type="time" class="form-control datetimepicker" name="starttime" size=19 value="<?php date_default_timezone_set('Africa/Blantyre'); echo date("H:i:s")?>" placeholder="">
													</div>

													<div class="col-lg-6 form-group-sub">
														<label class="form-control-label text-muted">* End Time</label>
														<input type="time" class="form-control datetimepicker" name="endtime" value="{{ old('endtime')}}" placeholder="" >
													</div>

												</div>
											</div>
										</div>

										<div class="k-separator k-separator--border-dashed k-separator--space-xl"></div>

										<div class="k-section">
											<h3 class="k-section__title small">
												<br>
												Client Information:
											</h3>
											<div class="k-section__content">
												<div class=" row">
													<div class="form-group col-lg-8">
														<label class="form-control-label text-muted">* Full Name:</label>
														<input type="text" name="fullname" class="form-control" placeholder="Full Name" value="{{old('fullname')}}" required="true" autocomplete="on" autofocus>
													</div>
													<div class="form-group col-lg-4">
														<label class="form-control-label text-muted">* Age:</label>
														<input type="number" name="age" class="form-control" placeholder="Age" min="1" value="{{old('age')}}" required autocomplete="on">
													</div>
												</div>
												<div class=" row">
													<div class="form-group col-lg-6">
														<label class="form-control-label text-muted">* Phone Number:</label>
														<input type="phone" name="phonenumber" class="form-control" placeholder="Enter phone" value="{{old('phonenumber')}}" maxlength="15" minlength="10" required>

													</div>
													<div class="form-group col-lg-6">
														<label class="form-control-label text-muted ">Gender *</label>
														<div class="">
															<div class="k-radio-inline" style="position: relative; top: 10px;">
																<label class="k-radio text-muted">
																	<input type="radio" name="gender" value="Male" checked> Male
																	<span></span>
																</label>
																<label class="k-radio text-muted">
																	<input type="radio" name="gender" value="Female"> Female
																	<span></span>
																</label>

															</div>

														</div>
												</div>
												</div>

												<div class="form-group form-group-last row">
													<div class="col-lg-6 form-group-sub">
														<label class="form-control-label">* District:</label>
														<select class="form-control" name="district" required>
															<option value="">Select</option>
															  @foreach($districts as $district)
									                            <option value="{{$district->id}}" >{{$district->name}}</option>
									                          @endforeach
														</select>
													</div>
                          <div class="col-lg-6 form-group-sub">
                            <label class="form-control-label">* Partner:</label>
                            <input type="text" name="partner" class="form-control " placeholder="{{$partners->name}}" autocomplete="on" value="{{$partners->name}}" autosave="true" required="true">
                          </div>

												</div>
											</div>
										</div>
										<hr>
										<div class="form-group row">
											<label class="col-form-label col-lg-2 col-sm-12 text-muted  h3">Nature: *</label>
											<div class="col-lg-10 col-md-10 col-sm-12"  style="position: relative; right: -2em;">
												<div class="row">
													<div class="col-lg-5 k-radio-list text-muted">
														<label class="k-radio">
														<input type="radio" name="nature" value="Information seeking">Information seeking
														<span></span>
														</label>
														<label class="k-radio">
														<input type="radio" name="nature" value="concerned client"> Concerned Client
														<span></span>
														</label>

													</div>
													<div class="col-lg-3 k-radio-list text-muted">
                            <label class="k-radio">
                            <input type="radio" name="nature" value="followup"> Follow Up
                            <span></span>
                            </label>
														<label class="k-radio">
														<input type="radio" name="nature"value="tipoff"> Tip Off
														<span></span>
														</label>

													</div>
                          <div class="col-lg-3 k-radio-list text-muted">

														<label class="k-radio">
														<input type="radio" name="nature" value="concerned adult"> Others
														<span></span>
														</label>
													</div>
												</div>
											</div>
										</div>
										<hr>
										<div class=" form-group row">
											<div class="col-lg-6">
												<label class="form-control-label text-muted">* Pepetrator:</label>
												<select class="form-control" name="perpetrator">
													<option value="">Select</option>
													@foreach($perpetrators as $perpetrator)
						                            	<option value="{{$perpetrator->id}}">{{$perpetrator->name}}</option>
						                            @endforeach
												</select>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<label class="form-control-label text-muted">* Area :</label>
													<input type="text" name="village" class="form-control" placeholder="Area" value="{{old('village')}}">
												</div>
											</div>
										</div>

										<div class="k-separator k-separator--border-dashed k-separator--space-xl"></div>

										@if ($errors->any())
										    <div class="alert alert-danger">
										        <ul>
										            @foreach ($errors->all() as $error)
										                <li>{{ $error }}</li>
										            @endforeach
										        </ul>
										    </div>
										@endif
										<div class="k-separator k-separator--border-dashed k-separator--space-xl"></div>

									</div>
									<div class="k-portlet__foot">
										<div class="k-form__actions">
											<div class="row">
												<div class="col-lg-12">
													<button type="button" class="btn btn-accent" data-target="#top" id="b_top">Continue</button>

												</div>
											</div>
										</div>
									</div>

								<!--end::Form-->
							</div>
							<!--end::Portlet-->
						</div>
						<div class="col-lg-6" id="top" name="top">
							<!--begin::Portlet-->
							<div class="k-portlet">
								<div class="k-portlet__head bg-dark">
									<div class="k-portlet__head-label">
										<h3 class="k-portlet__head-title text-white text-uppercase">ISSUE DETAILS</h3>
									</div>
								</div>
								<!--begin::Form-->

									<div class="k-portlet__body">
										<div class="form-group form-group-last k-hide">
												<div class="alert alert-danger" role="alert" id="k_form_1_msg">
													<div class="alert-icon"><i class="fa fa-warning"></i></div>
												  	<div class="alert-text">

														Oh snap! Change a few things up and try submitting again.
													</div>
													<div class="alert-close">
														<button type="button" class="close" data-dismiss="alert" aria-label="Close">
													    	<span aria-hidden="true"><i class="fa fa-close"></i></span>
													  	</button>
													</div>
												</div>

										</div>



										<div class="k-section">
											<h3 class="k-section__title small">
												Issue Information:
											</h3>
											<div class="k-section__content">
												<div class=" row col-issues" data-spy="scroll">
													@foreach($issues as $issue)
                          								@if($issue->description =="congoma" )
															<div class="col-lg-6 form-group ">
																<label class="form-control-label badge badge-lg text-muted border-bottom">* {{$issue->name}}:</label>
																@foreach($issue->subissues as $subissue)
																	<div class=" k-checkbox-list text-muted  ">
																		<label class="k-checkbox small-issue">
																		<input type="checkbox" name="subissue[]" value="{{$subissue->id}}"> {{$subissue->name}}
																		<span></span>
																		</label>
																	</div>
																@endforeach
															</div>
														@elseif($issue->name == "CONGOMA")
															<div class="col-lg-6 form-group">
																<label class="form-control-label text-muted border-bottom">*{{$issue->name}} :</label>
																@foreach($issue->subissues as $subissue)
																	<div class="  k-checkbox-list text-muted">
																		<label class="k-checkbox small-issue">
																		<input type="checkbox" name="subissue[]" value="{{$subissue->id}}" > {{$subissue->name}}
																		<span></span>
																		</label>
																	</div>
																@endforeach
															</div>
														@endif
                          							@endforeach
												</div>

												<hr>
												<div class="form-group row">
													<div class="col-lg-6 form-group-sub row text-muted">
														<label class="col-form-label col-lg-3 col-sm-12">Add *</label>
														<div class="col-lg-9 col-md-9 col-sm-12">
															<div class="k-checkbox-inline">
																<label class="k-checkbox">
																<input type="checkbox" name="other_subissue" data-toggle="collapse" href="#collapseOther" role="button" aria-expanded="false" aria-controls="collapseOther"> Other Subissues
																<span></span>
																</label>
															</div>
															<span class="form-text text-muted">Please tick the checkbox</span>
														</div>
													</div>

													<div class="col-lg-6 form-group-sub collapse " id="collapseOther">
														<label class="form-control-label text-primary">* <i>Specify</i>  :</label>
														<input type="text" class="form-control " name="other_subissue_specify" value="" placeholder="Enter Subissue" >
													</div>

												</div>
											</div>
										</div>

										<div class="k-section">
											<hr>
											<div class="k-section__content">
												<div class="form-group row">
													<label class="col-form-label col-lg-3 col-sm-12 text-muted">Intervention *</label>
													<div class="col-lg-9 col-md-9 col-sm-12">
														<div class="row">
															<div class=" col-lg-5 k-checkbox-list text-muted">
																<label class="k-checkbox">
																<input type="checkbox" name="intervention[]" value="1"> Needs Followup
																<span></span>
																</label>
																<label class="k-checkbox">
																<input type="checkbox" name="intervention[]" value="6"> Issue Escalation
																<span></span>
																</label>
																<label class="k-checkbox">
																<input type="checkbox" name="intervention[]" value="2"> Counselled
																<span></span>
																</label>
															</div>
															<div class="col-lg-4 k-checkbox-list text-muted float-right">
																<label class="k-checkbox">
																<input type="checkbox" name="intervention_ref" data-toggle="collapse" href="#collapseReffered" role="button" aria-expanded="false" aria-controls="collapseReffered"> Reffered
																<span></span>
																</label>
																<label class="k-checkbox">
																<input type="checkbox" name="intervention[]" value="3"> Guided
																<span></span>
																</label>
															</div>
														</div>

													</div>
												</div>
											</div>
										<hr>
											<div class="form-group row collapse border-bottom" id="collapseReffered">
													<label class="col-form-label col-lg-2 col-sm-12 text-muted">Referrals *</label>
													<div class="col-lg-10 col-md-10 col-sm-12" style="position: relative; right: -1em;">
														<div class="row col-refferal" data-spy="scroll" >

															<div class=" col-lg-4 k-checkbox-list text-muted">

																<label class="k-checkbox">
																<input type="checkbox" name="referrals[]" value="12" > CONGOMA
																<span></span>
																</label>
																<label class="k-checkbox">
																<input type="checkbox" name="referrals[]" value="13"> MoH/DHO
																<span></span>
																</label>
																<label class="k-checkbox">
																<input type="checkbox" name="referrals[]" value="15"> MqEST/DEM
																<span></span>
																</label>
															</div>
															<div class="col-lg-7 k-checkbox-list text-muted float-right">

																<label class="k-checkbox">
																<input type="checkbox" name="referrals[]" value="14"> District Water Development
																<span></span>
																</label>
                                <label class="k-checkbox">
                                <input type="checkbox" name="other_referral" data-toggle="collapse" href="#collapsereferral" role="button" aria-expanded="false" aria-controls="collapsereferral"> Other
                                <span></span>

                                <label class=" collapse col-sm-9" id="collapsereferral">
      														<input type="text" class="form-control " name="referrals" value="" placeholder="Specify" style="height:28px;">
                                <span></span>
                                </label>
                                </label>

															</div>
														</div>

													</div>

											</div>

										</div>

										<div class="form-group row">
											<label class="col-form-label col-lg-3 col-sm-12">Case Brief *</label>
											<div class="col-lg-9 col-md-9 col-sm-12">
												<textarea class="form-control" name="brief" value="{{ old('brief')}}" placeholder="Enter a comment" rows="8" required></textarea>
												<span class="form-text text-muted">Please enter a case brief </span>
											</div>
										</div>

										<div class="k-separator k-separator--border-dashed k-separator--space-xl"></div>

										<div class="form-group row">
											<label class="col-form-label col-lg-5 col-sm-12">Call Completeness *</label>
											<div class="col-lg-7 col-md-7 col-sm-12">
												<div class="k-radio-inline">
													<label class="k-radio">
													<input type="radio" name="callCompleteness" value="complete" checked> Complete
													<span></span>
													</label>
													<label class="k-radio">
													<input type="radio" name="callCompleteness" value="incomplete" >InComplete
													<span></span>
													</label>
												</div>
											</div>
										</div>
									</div>

									<div class="k-portlet__foot">
										<div class="k-form__actions">
											<div class="row">
												<div class="col-lg-9 ml-lg-auto">

													<button type="reset" class="btn btn-secondary">Cancel</button>
													<button type="submit" class="btn btn-outline-success" id="k_form_1">Submit</button>
												</div>
											</div>
										</div>
									</div>
								<!--end::Form-->
							</div>
							<!--end::Portlet-->
						</div>
					</div>
				</form>
			</div>

	   	</div>
   </div>

   @endsection
