   @extends('.counsellor.gbv.layout.master')


   @section('content')

   <div class="content-cards " >

	   	<div class="container-fluid ">
			<div class="row py-3 ">

					<div class="col-sm-3">
						<div class="card hoverable" id="total-cases">
							<div class="card-body">
								<div class="row align-items-center">
									<div class="col">
										<h6><small class="card-title text-uppercase text-muted mb-2"> <b>TOTAL CALLS </b></small></h6>
										<!-- Heading -->
					                    <span class="h3 mb-0">
					                     <b >
					                     	<script>
					                     		function totalCases(unresolved, resolved) {
													// body...
													var result = unresolved + resolved;
													return result;
												}
												var tot = setInterval(totalCases, 3000);
												clearInterval(tot);
												var res = totalCases(23, 34);
												document.write(res);

					                     	</script>
					                     </b>
					                    </span>

					                    <!-- Badge -->
					                    <span class="badge badge-success mt-1">
					                      +3.5%
					                    </span>
									</div>
									<div class="col-auto">
					                    <!-- Icon -->
					                    <span class="fa-2x fa fa-clone  text-muted mb-1"></span>
					                </div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="card hoverable" id="total-resolved-cases">
							<div class="card-body">
								<div class="row align-items-center">
									<div class="col">
										<h6><small class="card-title text-uppercase text-muted mb-2"> <b>TOTAL COMPLETE CALLS</b></small></h6>
										<!-- Heading -->
					                    <span class="h3 mb-0">
					                     <b>
					                     	<script>
												var res = 56;
												var tot = setInterval(res, 3000);
												clearInterval(tot);
												document.write(res);

					                     	</script>
					                     </b>
					                    </span>

					                    <!-- Badge -->
					                    <span class="badge badge-success mt-1">
					                      +3.5%
					                    </span>
									</div>
									<div class="col-auto">
					                    <!-- Icon -->
					                    <span class="fa-2x fa fa-folder  text-muted mb-1"></span>

				                    </div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="card hoverable" id="total-unresolved-cases">
							<div class="card-body">
								<div class="row align-items-center">
									<div class="col">
										<h6><small class="card-title text-uppercase text-muted mb-2"> <b>TOTAL INCOMPLETE CALLS</b></small></h6>
										<!-- Heading -->
					                    <span class="h3 mb-0">
					                     <b>
					                     	<script>
												var res = 7;
												var tot = setInterval(res, 3000);
												clearInterval(tot);
												document.write(res);

					                     	</script>
					                     </b>
					                    </span>

					                    <!-- Badge -->
					                    <span class="badge badge-success mt-1">
					                      +3.5%
					                    </span>
									</div>
									<div class="col-auto">
					                    <!-- Icon -->
					                    <span class="fa-2x fa fa-folder-open  text-muted mb-1"></span>

				                    </div>
								</div>
							</div>
							</div>
						</div>
						<div class="col-sm-3" >
							<div class="card hoverable" id="new-call">
								<div class="card-body">
									<div class="row align-items-center">
										<div class="col">
											<h6><small class="card-title text-uppercase text-muted mb-2"> <b>GENDER BASED VIOLENCE</b></small></h6>
											<!-- Heading -->
						                    <span class="h3 mb-0">
						                     <b >
						                     	NEW CALL

						                     </b>
						                    </span>

						                    <!-- Badge -->
						                    <span class="text-success mt-1">

						                    </span>
										</div>
										<div class="col-auto  text-success">
						                    <!-- Icon -->
						                    <span class="fa-2x fa fa-phone    mb-1"></span>
						                    <span class="fa-2x fa fa-phone   mb-1"></span>
					                    </div>
									</div>
								</div>
							</div>
					</div>
			</div>
   	</div>
	<div class="container-fluid ">
		<div class="col-sm-12 row">
			<div class="">
				<div class="card float-left card-wrapper4" >
									              <div class="card-header card-color p-3">
				                	<div class="row align-items-center">
						                  <div class="col">

						                    <!-- Title -->
						                    <h6 class="card-header-title">
						                      <span class="card-title text-uppercase text-muted mb-2"> <b>Recent Cases</b></span>
						                    </h6>

						                  </div>
						                  <div class="col-auto">

						                      <!-- Link -->
				                                <a href="#!" class=" text-muted">
				                                  <span class="small">View all</span>
				                                </a>
						                  </div>
					                </div> <!-- / .row -->
				              	</div>
				              	<div class="table-responsive mb-0" data-toggle="lists" data-lists-values="[&quot;goal-project&quot;, &quot;goal-status&quot;, &quot;goal-progress&quot;, &quot;goal-date&quot;]">
				              		<table class="table table-sm table-nowrap card-table table-hover" >
					                  <thead class="thead">
					                    <tr>
					                      <th>
					                        <a href="#" class="text-muted sort" data-sort="goal-project">
					                          Date
					                        </a>
					                      </th>
					                      <th>
					                        <a href="#" class="text-muted sort" data-sort="goal-status">
					                          District
					                        </a>
					                      </th>

					                      <th>
					                        <a href="#" class="text-muted sort" data-sort="goal-date">
					                           Comment
					                        </a>
					                      </th>

					                    </tr>
					                  </thead>
					                  <tbody class="lists">
					                  	@foreach($cases as $case)

							                  	<tr  class="hoverable" data-id="{{$case->id}}"  id="viewCase"  data-target=".bd-example-modal-lg">
							                  	  <td class="goal-date text-left">
							                        <time >
							                        	<script >
							                        		function dateConvert(dateobj, format){
														      var year = dateobj.getFullYear();
														      var month= ("0" + (dateobj.getMonth()+1)).slice(-2);
														      var date = ("0" + dateobj.getDate()).slice(-2);
														      var hours = ("0" + dateobj.getHours()).slice(-2);
														      var minutes = ("0" + dateobj.getMinutes()).slice(-2);
														      var seconds = ("0" + dateobj.getSeconds()).slice(-2);
														      var day = dateobj.getDay();
														      var months = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
														      var dates = ["SUN","MON","TUE","WED","THU","FRI","SAT"];
														      var converted_date = "";

														      switch(format){
														        case "YYYY-MMM-DD":
														          converted_date = date  + "-" + months[parseInt(month)-1] + "-" +year ;
														          break;
														        case "DD-MMM":
														            converted_date = date + " " + months[parseInt(month)-1];
														          break;
														      }

														      return converted_date;
														    }
							                        		var i_date = "{{$case->date}}";
							                        		var j_date = new Date(Date.parse(i_date));
                        									var date = dateConvert(j_date, "YYYY-MMM-DD");
                        									document.write(date);
							                        	</script>
							                        </time>
							                      </td>
							                      <td class="text-center">
							                      	<b>{{$case->district->name}}</b>
							                      </td>
							                      <td class="goal-project text-left">

							                   			 {{ substr(trim($case->comments) , 0, 70)}}..
							                      </td>
							                    </tr>
										@endforeach

					                </tbody>
				                </table>
				         </div>
		   		</div>
			</div>

			<div class="col-sm-5">
				<div class="  float-right">
			   			<div class=" card-wrapper5">
							<div class="">
									<div class="order-lg-1 order-xl-1">
										<!--begin::Portlet-->
								<div class="k-portlet k-portlet--height-fluid k-widget " >
									<div class="k-portlet__body">
										<div id="k-widget-slider-13-2" class="k-slider carousel slide" data-ride="carousel" data-interval="10000">
											<div class="k-slider__head">
												<div class="k-slider__label">MAIN ISSUES</div>
												<div class="k-slider__nav">
													<a class="k-slider__nav-prev carousel-control-prev" href="#k-widget-slider-13-2" role="button" data-slide="prev">
														<i class="fa fa-angle-left"></i>
													</a>
													<a class="k-slider__nav-next carousel-control-next" href="#k-widget-slider-13-2" role="button" data-slide="next">
														<i class="fa fa-angle-right"></i>
													</a>

													 <div class="col-auto dropdown droppdown-inline" >
														  <!-- Toggle -->
									                    <a href="javascript: void(0);" class="icons drop-note" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" >
									                      <span class="icon active">
									                        <i class="fa fa-ellipsis-h text-muted"></i>
									                      </span>
									                    </a>

									                        <!-- Menu -->
									                    <div class="dropdown-menu  droppdown-menu droppdown-menu-right " x-placement="bottom-end" style="">
									                        	<ul class="k-nav">
															        <li class="k-nav__section k-nav__section--first">
															            <span class="k-nav__section-text">Export Tools</span>
															        </li>
															        <li class="k-nav__item">
															            <a href="#" class="k-nav__link">
															                <i class="k-nav__link-icon la fa fa-print"></i>
															                <span class="k-nav__link-text link-text">Print</span>
															            </a>
															        </li>
															        <li class="k-nav__item">
															            <a href="#" class="k-nav__link">
															                <i class="k-nav__link-icon la fa fa-copy"></i>
															                <span class="k-nav__link-text link-text">Copy</span>
															            </a>
															        </li>
															        <li class="k-nav__item">
															            <a href="#" class="k-nav__link">
															                <i class="k-nav__link-icon la fa fa-file-excel-o"></i>
															                <span class="k-nav__link-text link-text">Excel</span>
															            </a>
															        </li>
															        <li class="k-nav__item">
															            <a href="#" class="k-nav__link link-text">
															                <i class="k-nav__link-icon la fa fa-file-text-o"></i>
															                <span class="k-nav__link-text link-text">CSV</span>
															            </a>
															        </li>
															        <li class="k-nav__item">
															            <a href="#" class="k-nav__link">
															                <i class="k-nav__link-icon la fa fa-file-pdf-o"></i>
															                <span class="k-nav__link-text link-text">PDF</span>
															            </a>
															        </li>
															    </ul>
							  	                    	</div> <!-- / .dropdown-menu -->
													</div>
												</div>
											</div>
											<div class="carousel-inner">
												<div class="carousel-item k-slider__body active">
													<div class="k-widget-13">
														<div class="k-widget-13__body">
																<div class="k-widget-9__chart">

																	<div class="chartjs-size-monitor" >
																		<div class="chartjs-size-monitor-expand" >
																			<div ></div>
																		</div>
																		<div class="chartjs-size-monitor-shrink" >
																			<div >

																			</div>
																		</div>
																	</div>

																<canvas id="statistics" height="285.8" width="77" class="chartjs-render-monitor" ></canvas>
															</div>
														</div>
													</div>
												</div>

												<div class="carousel-item k-slider__body" style="height: 18.9rem;">
													<div class="k-widget-13">
														<div class="k-widget-13__body">
															  <!-- Chart -->
												                <div class="chart chart-appended chartjs-size-monitor chartjs-size-monitor-expand">
												                  <canvas id="myDoughnut" class="chart-canvas chartjs-render-monitor" data-target="#devicesChartLegend" height="150" width="72" ></canvas>
												                </div>

												               <!-- Legend -->
											                <div id="devicesChartLegend" class="chart-legend"><span class="chart-legend-item"><i class="chart-legend-indicator" style="background-color: #1cac81 !important;"></i>Resolved</span><span class="chart-legend-item"><i class="chart-legend-indicator" style="background-color: #41675c !important;"></i>Unresolved</span><span class="chart-legend-item"><i class="chart-legend-indicator" style="background-color: #80c3af !important;"></i>Pending</span></div>
														</div>

													</div>
												</div>

											</div>
										</div>
									</div>
								</div>

		<!--end::Portlet-->	</div>
				           </div>

				        </div>
				    </div>
			</div>
	    </div>

   </div>

	<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			        ... the whole is here
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			        <button type="button" class="btn btn-primary">Save changes</button>
			      </div>
			    </div>
		  </div>
	</div>
   @endsection



   
