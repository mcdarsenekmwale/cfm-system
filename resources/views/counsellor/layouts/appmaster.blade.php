<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>CFM System</title>

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Fonts -->
  <link rel="dns-prefetch" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CRaleway:300,400,500,600,700" media="all">

  <!-- Fonts -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="dns-prefetch" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.css" integrity="sha256-/WnCqTIGflaXyhZUIQe/O4gwaMRNtN39rHMKIeKbN3c=" crossorigin="anonymous" >
  <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" />
  <!-- Material Design Bootstrap -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.13/css/mdb.min.css" rel="stylesheet">
  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('css/master.css') }}">
  <link rel="stylesheet" href="{{ asset('css/partner.css') }}">
  <link rel="stylesheet" href="{{ asset('counsellor/css/master.css') }}">

</head>
<body style=" background-color: #E6E6E6;">
  <div id="app">
    <nav class="navbar navbar-expand-sm navbar-dark bg-dark fixed-top shadow flex-md-nowrap  p-1">

      <a href="https://www.yoneco.org " class="zoom px-4">  <img src="{{url('/')}}/images/logo.jpg" alt="" class="rounded-circle bounceIn img-fluid  img-master"></a>
      <a class=" navbar-brand " href="{{ url('/') }}" title="Conflict Feedback Mechanism System">
        <b> <span class="badge badg-master ">CFM System</span></b>
      </a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarscfm" aria-controls="navbarscfm" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse px-5" id="navbarscfm">
        <form class="form-inline my-2 my-md-0 col-9 form-input">
          <input id="search" class="form-control w-100 bg-dark" type="search" placeholder="Search ..." style="border-left: 1px solid rgba(0,0,0,.12);">
        </form>
        <!-- Left Side Of Navbar mobile-->
        <div class="col-m sidebar-nav  py-2 list-nav">
          <form class="form-inline my-2 my-md-0 ">
            <input id="search" class="form-control w-100 bg-dark" type="search" placeholder="Search ..." style="border-left: 1px solid rgba(0,0,0,.12);">
          </form>
          <ul class="nav flex-column px-4">
            <li class="nav-item active">
              <a class="nav-link nav-i " href="#">
                <i class="fas fa-phone fa-lg lg "></i>
                <span class="px-3">About Calls</span>
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-i" href="#">

                <i class="fas fa-folder-open fa-lg lg "></i>
                <span class="px-3">About Cases</span>
              </a>
            </li>

            <li class="nav-item">
              <a class="nav-link nav-i" href="#">
                <i class="fa fa-bell fa-lg lg "></i>
                <span class="px-3">Notifications</span>

              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-i" href="#">
                <i class="far fa-user-circle fa-lg lg "></i>
                <span class="px-3">{{Auth::user()->firstname}} {{Auth::user()->lastname}}</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-i" href="logout"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
              <i class="fa fa-sign-out"></i>
              Logout
            </a>

            <form id="logout-form" action="logout" method="POST" style="display: none;">
              @csrf
            </form>
          </li>

        </ul>

      </div>
      <!-- <ul class="navbar-nav mr-auto">

    </ul> -->

    <!-- Right Side Of Navbar -->
    <ul class="navbar-nav mr-auto text-white ">
      <!-- Authentication Links -->

      @guest
      <li class="nav-item ">
        <a class="nav-link" href="{{ route('login') }}"></a>
      </li>
      <li class="nav-item">
        @if (Route::has('register'))
        <a class="nav-link" href="{{ route('register') }}"></a>
        @endif
      </li>
      @else
      <li class="nav-item  ">
        <a href="" class="nav-link icons  list-icon "><span class=" icon fa fa-folder-open fa-lg lg py-1" > </span></a>

      </li>
      <li class="nav-item  dropdown  d-md-flex ">
        <!-- Toggle -->
        <a href="javascript: void(0);" class="nav-link icon drop-note  " role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" >
          <span class="icon  " style="position:relative; top:-4px;">
            <i class="fa fa-bell fa-lg lg "></i>
          </span>
        </a>

        <!-- Menu -->
        <div class=" dropdown-menu dropdown-menu-right dropdown-menu-card ">
          <div class="card-header card-color">
            <div class="row align-items-center">
              <div class="col">

                <!-- Title -->
                <h6 class="card-header-title">
                  <span class="card-title text-uppercase text-muted mb-2"> <b>Notifications</b></span>
                </h6>

              </div>
              <div class="col-auto">

                <!-- Link -->
                <a href="#!" class=" text-muted">
                  <span class="small">Clear all</span>
                </a>

              </div>
            </div> <!-- / .row -->
          </div> <!-- / .card-header -->
          <div class="card-body">

            <!-- List group -->
            <div class="list-group list-group-flush my--3">
              <a class="list-group-item px-0" href="#!">

                <div class="row">
                  <div class="col-auto">

                    <!-- Avatar -->
                    <div class="avatar avatar-sm">
                      <img src=" https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(15).jpg" alt="..." class="avatar-img rounded-circle">
                    </div>

                  </div>
                  <div class="col ml--2">

                    <!-- Content -->
                    <div class="small text-muted">
                      <strong class="text-body">Dianna Smiley</strong> shared your post with <strong class="text-body">Ab Hadley</strong>, <strong class="text-body">Adolfo Hess</strong>, and <strong class="text-body">3 others</strong>.
                    </div>

                  </div>
                  <div class="col-auto">

                    <small class="text-muted">
                      2m
                    </small>

                  </div>
                </div> <!-- / .row -->

              </a>
              <a class="list-group-item px-0" href="#!">

                <div class="row">
                  <div class="col-auto">

                    <!-- Avatar -->
                    <div class="avatar avatar-sm">
                      <img src=" https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(18).jpg" alt="..." class="avatar-img rounded-circle">
                    </div>

                  </div>
                  <div class="col ml--2">

                    <!-- Content -->
                    <div class="small text-muted">
                      <strong class="text-body">Ab Hadley</strong> reacted to your post with a 😍
                    </div>

                  </div>
                  <div class="col-auto">

                    <small class="text-muted">
                      2m
                    </small>

                  </div>
                </div> <!-- / .row -->

              </a>

              <a class="list-group-item px-0" href="#!">

                <div class="row">
                  <div class="col-auto">

                    <!-- Avatar -->
                    <div class="avatar avatar-sm">
                      <img src=" https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(16).jpg" alt="..." class="avatar-img rounded-circle">
                    </div>

                  </div>
                  <div class="col ml--2">

                    <!-- Content -->
                    <div class="small text-muted">
                      <strong class="text-body">Daniela Dewitt</strong> subscribed to you.
                    </div>

                  </div>
                  <div class="col-auto">

                    <small class="text-muted">
                      2m
                    </small>

                  </div>
                </div> <!-- / .row -->
              </a>
              <a class="list-group-item px-0 " href="#!">

                <div class="row">
                  <div class="col-auto">

                    <!-- Avatar -->
                    <div class="avatar avatar-sm">
                      <img src=" https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(21).jpg"  class="avatar-img rounded-circle">
                    </div>

                  </div>
                  <div class="col ml--2">

                    <!-- Content -->
                    <div class="small text-muted">
                      <strong class="text-body">Ryu Duke</strong> reacted to your post with a 😍
                    </div>

                  </div>
                  <div class="col-auto">

                    <small class="text-muted">
                      2m
                    </small>

                  </div>
                </div> <!-- / .row -->

              </a>
            </div>
          </div>
          <div class="card-footer text-center align-items-center p-1 card-color  notify-item notify-all">
            <!-- Link -->
            <a href="#!" class=" text-success">
              <span class="small">View all</span>
            </a>
          </div>

        </div> <!-- / .dropdown-menu -->




      </li>
      <li class="nav-item ">
        <a href="#modalSettings" data-toggle="modal" class=" nav-link icon icons  list-icon">  <span class="icon fa fa-cog fa-lg lg py-1" > </span></a>

      </li>
      <li class="nav-item dropdown  logout ">
        <a id="navbarDropdown" class="nav-link btn-account d-none d-md-flex  " href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
          <span class="use-avatar float-left"><i class="fa fa-user-circle fa-2x"></i></span>

          <span class="accountt-summary  ">
            <span class="accountt-name">{{ Auth::user()->username }}</span>
            <span class="accountt-description">counsellor</span>
          </span>
        </a>

        <div class="dropdown-menu dropdown-menu-right bg-dark text-white" aria-labelledby="navbarDropdown">

          <a class="dropdown-item border-bottom dropdown-i text-white" href="#">
            <i class="fa fa-user-circle-o"></i>
            Profile  <i class="fa fa-angle-double-right"></i></a>
            <a class="dropdown-item dropdown-i text-white" href="#">
              <i class="fa fa-cog fa-spin"></i>
              <i class="fa fa-"></i>
              Settings
            </a>
            <a class="dropdown-item border-top dropdown-i text-white" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            <i class="fa fa-sign-out"></i>
            Logout
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </div>
      </li>
      @endguest

    </ul>

  </div>
</nav>
<!-- settings: Demo -->
<div class="modal fade fixed-right " id="modalSettings" tabindex="-1" role="dialog"  aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-dialog-vertical" role="document">
    <form class="modal-content" >
      <div class="modal-body">

        <!-- Close -->
        <button type="button" class="clos-x" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>

        <div class="text-center">
            <a href="https://www.yoneco.org " class="zoom ">
              <img src="{{url('/')}}/images/logo.jpg" alt="..." class=" img-fluid mb-3" height="210px" width="220px">
            </a>

        </div>

        <h4 class="text-center mb-2">
          <b>CFM SYSTEM</b>
        </h4>

        <hr class="mb-4">

        <h4 class="mb-1">
          Color Scheme
        </h4>

        <p class="small text-muted mb-3">
          Overall light or dark presentation.
        </p>

        <div class="btn-group-toggle d-flex mb-4" data-toggle="buttons">
          <label class="btn btn-white active col">
            <input type="radio" name="colorScheme" id="colorSchemeLight" value="light" checked=""> <i class="fa fa-sun-o mr-2"></i> Light Mode
          </label>
          <label class="btn btn-white col ml-2">
            <input type="radio" name="colorScheme" id="colorSchemeDark" value="dark"> <i class="fa fa-moon-o mr-2"></i> Dark Mode
          </label>
        </div>

        <h4 class="mb-1">
          Navigation Position
        </h4>

        <p class="small text-muted mb-3">
          Select the primary navigation paradigm for your app.
        </p>

        <div class="btn-group-toggle d-flex mb-4" data-toggle="buttons">
          <label class="btn btn-white col">
            <input type="radio" name="navPosition" id="navPositionSidenav" value="sidenav" checked=""> Sidenav
          </label>
          <label class="btn btn-white col ml-2">
            <input type="radio" name="navPosition" id="navPositionTopnav" value="topnav"> Topnav
          </label>
          <label class="btn btn-white col ml-2 active">
            <input type="radio" name="navPosition" id="navPositionCombo" value="combo"> Combo
          </label>
        </div>

        <h4 class="mb-1">
          Sidebar Color
        </h4>

        <p class="small text-muted mb-3">
          Usually dictated by the color scheme, but can be overriden.
        </p>

        <div class="btn-group-toggle d-flex mb-4" data-toggle="buttons">
          <label class="btn btn-white active col">
            <input type="radio" name="sidebarColor" id="sidebarColorDefault" value="default" checked=""> Default
          </label>
          <label class="btn btn-white col ml-2">
            <input type="radio" name="sidebarColor" id="sidebarColorVibrant" value="vibrant"> Vibrant
          </label>
        </div>

      </div>
      <div class="modal-footer border-0">

        <button type="submit" class="btn btn-block btn-dark mt-auto">
          Preview
        </button>

      </div>
    </form>
  </div>
</div>

<main class="py-4" role="main">
  @yield('content')

</main>
<footer class="white-text text-center border-top bg-light footer">
  <div class="container">
    <p class="float-right">
      <a href="#"> <span class="text-muted fa fa-chevron-circle-up"></span></a>
    </p>
    <span class="text-dark"><?php date_default_timezone_set('Africa/Blantyre'); echo date("Y")?> <i class="fa fa-copyright"></i> YONECOCCS</span>
    <small class="text-muted">YONECO ICT DEPARTMENT</small>
  </div>
</footer>

</div>

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.js" integrity="sha256-zTde1SYEpUiY54BwIFLX07JyfYU46JlHZvyTiCmg6ig=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"><\/script>')</script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.13/js/mdb.min.js"></script>
<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
feather.replace()
</script>
<!-- Google Analytics -->
<script>
window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
ga('create', 'UA-XXXXX-Y', 'auto');
ga('send', 'pageview');
</script>
<script async src='https://www.google-analytics.com/analytics.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
<script type="text/javascript">
window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/themes.getbootstrap.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.8.4"}};
!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,56826,8203,55356,56819),0,0),c=j.toDataURL(),b!==c&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55358,56794,8205,9794,65039),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55358,56794,8203,9794,65039),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
</script>
<script type="text/javascript">
/* <![CDATA[ */
var dokan = {"ajaxurl":"https:\/\/themes.getbootstrap.com\/wp-admin\/admin-ajax.php","nonce":"e06a0f8dca","ajax_loader":"http:\/\/themes.getbootstrap.com\/wp-content\/plugins\/dokan-lite\/assets\/images\/ajax-loader.gif","seller":{"available":"Available","notAvailable":"Not Available"},"delete_confirm":"Are you sure?","wrong_message":"Something went wrong. Please try again."};
/* ]]> */
</script>
<script type="text/javascript" src="http://themes.getbootstrap.com/wp-includes/js/jquery/jquery.js?ver=1.12.4"></script>
<script type="text/javascript" src="http://themes.getbootstrap.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1"></script>
<script type="text/javascript">
/* <![CDATA[ */
var userSettings = {"url":"\/","uid":"0","time":"1544693576","secure":""};
/* ]]> */
</script>
<script type="text/javascript" src="http://themes.getbootstrap.com/wp-includes/js/utils.min.js?ver=4.8.4"></script>
<script type="text/javascript" src="http://themes.getbootstrap.com/wp-includes/js/plupload/plupload.full.min.js?ver=2.1.8"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-67613229-3');
</script>
<script>
WebFont.load({
  google: {"families":["Poppins:300,400,500,600,700","Raleway:300,400,500,600,700"]},
  active: function() {
    sessionStorage.fonts = true;
  }
});
</script>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>
<script src="https://unpkg.com/moment" ></script>
<script src="js/app.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
</body>
</html>
