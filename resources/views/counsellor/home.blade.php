@extends('counsellor.layouts.appmaster')
@section('content')
<div class="py-5">
  <div class="container py-5">

    <div class="row">

      <!-- GBV  -->

      <div class="col-md-4">
        <div class="card mb-4  bounceOut shadow-sm">
          <div class="card-body  card-image  bg-light" >
            <div class="text-white text-center bg-primary py-4 px-4">
              <div class="">
                <!-- Content -->
                <h5 class="h4 "><i class=""></i>GBV </h5>
                <p>Gender Based Violence</p><br>
                <a class="btn btn-primary" href="{{url('/gbv/')}}"><i class="fa fa-clone left" ></i> View</a>
                <!-- Content -->
              </div>
            </div>
          </div>
        </div>
      </div>


      <div class="col-md-4">
        <div class="card mb-4  bounceOut shadow-sm">
          <div class="card-body  card-image  bg-light" >
            <div class="text-white text-center bg-success py-4 px-4 ">
             <div class="">
               <!-- Content -->

               <h5 class="h4 white-text"><i class=""></i> Alliance One Center</h5>
               <p >In Malawi</p><br>
               <a class="btn btn-success" href="{{url('/allianceone/')}}"><i class="fa fa-clone left"></i> View </a>

             </div>
           </div>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="card mb-4  bounceOut shadow-sm">
          <div class="card-body  card-image  bg-light" >
            <div class="text-white text-center bg-info py-4 px-4">
              <div class="">
                <!-- Content -->
                <h5 class="h4 white-text"><i class=""></i> CONGOMA </h5>
                <p><small class="text-white"> <i>Coucil for Non-Government Organisations in Mw</i></small> </p>
                  <br>
                <a class="btn btn-info" href="{{url('/congoma/')}}"><i class="fa fa-clone left"></i> View </a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-4 ">
        <div class=" card bg-success mb-3  bounceOut shadow-sm" >
          <div class="  card-image " >
            <div class="text-white text-center bg-light py-4 px-4">
              <div class="py-3">
                <!-- Content -->
                <h5 class="h4 text-success"><i class="fa fa-camera-retro"></i> Alliance One Center</h5>
                <p class="hidden">In Malawi</p><br>
                <a  class="btn btn-success"><i class="fa fa-clone left"></i> View </a>
              </div>
            </div>
          </div>

        </div>
      </div>


      <div class="col-md-4">
        <div class="card mb-4 shadow-sm card-image" style="background-image: url(https://mdbootstrap.com/img/Photos/Others/forest2.jpg);">

          <div class="text-white text-center rgba-stylish-strong py-4 px-4">
            <div class="py-3">
              <!-- Content -->
              <h5 class="h4 orange-text"><i class="fa fa-camera-retro"></i> Photography</h5>
              <p class="hidden">In Malawi</p><br>
              <a class="btn peach-gradient"><i class="fa fa-clone left"></i> View </a>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="card mb-4 shadow-sm card-image" style="background-image: url(https://mdbootstrap.com/img/Photos/Others/forest2.jpg);">

          <div class="text-white text-center rgba-stylish-strong py-4 px-4">
            <div class="py-3">
              <!-- Content -->
              <h5 class="h4 orange-text"><i class="fa fa-camera-retro"></i> Photography</h5>
                <p class="hidden">In Malawi</p><br>
              <a class="btn peach-gradient"><i class="fa fa-clone left"></i> View </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.13/js/mdb.min.js"></script>
</div>
@endsection
