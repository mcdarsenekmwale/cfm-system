    @extends('counsellor.layouts.appmaster')

    @section('content')
    <div class="col-lg-12 content-cards">
        <div class="container">
          <div class="row ">

            <!-- Alliance  -->
            <div class="col-sm-3 card mb-4 card-image bg-light text-white bounceIn" id="f1_container">

              <div class="" id="f1_card">
                <div class="front face">
                  <div class=" py-3 py-l x-2" >
                          <img class="helpline-img" src="{{url('/')}}/counsellor/images/title.jpg">
                  </div>
                </div>
                <div class="back face center">
                     <div class="text-white text-center bg-primary py-4 px-4 " >
                        <div class=" " >
                          <br>
                          <h5 class="h5 "><i class=""></i>GBV </h5>
                          <small>Gender Based Violence</small>
                          <a class="btn btn-primary" href="{{url('/gbv/')}}"><i class="fa fa-clone left" ></i> View</a>
                          <!-- Content -->
                          <br>
                      </div>
                  </div>
                </div>
              </div>
            </div>

              <!-- GBV -->
              <div class="col-sm-3 card mb-4 card-image bg-light text-white bounceOut" >
                <div class="f1_card">
                  <div class="face front ">
                    <div class="text-white text-center py-4 ">
                      <div class="">
                        <!-- Content -->
                        <img class="helpline-img" src="{{url('/')}}/counsellor/images/alliance1.png">
                      </div>
                    </div>
                  </div>
                  <div class="face1 back1">
                     <div class="text-white text-center bg-success py-4 px-4 ">
                      <div class="">
                        <!-- Content -->
                        <br>
                        <h5 class="h5 white-text"><i class=""></i> Alliance One Center</h5>
                        <a class="btn btn-success" href="{{url('/allianceone/')}}"><i class="fa fa-clone left"></i> View </a>
                        <br>
                      </div>
                    </div>
                  </div>

                </div>
              </div>

          <!-- the -->

       <!-- Congoma -->
            <div class="col-sm-3 card mb-4 card-image bg-light text-white bounceIn" >
              <div class="text-white text-center bg-info py-4 px-4">
                <div class="">
                  <!-- Content -->
                  <h5 class="h5 white-text"><i class="fa fa-camera-retro"></i> Coucil for Non Government Organisations in Malawi</h5>
                  <p class="text-white"> <i>CONGOMA</i></p>

                  <a class="btn btn-info" href="{{url('/congoma/')}}"><i class="fa fa-clone left"></i> View </a>
              </div>
          </div>
      </div>
    </div>
  </div>


<!--  -->

       <div class="contain2">
          <div class="row ">


        <div class="col-sm-3 card mb-4 card-image bg-success bounceOut shadow" >
              <div class="text-white text-center bg-light py-4 px-4">
                <div class="py-5">
                  <!-- Content -->
                  <h5 class="h5 text-success"><i class="fa fa-camera-retro"></i> Alliance One Center</h5>

                  <a class="btn btn-success"><i class="fa fa-clone left"></i> View </a>
              </div>
          </div>
        </div>
      <!-- Bank -->
      <div class="col-sm-3 card mb-4 card-image" style="background-image: url(https://mdbootstrap.com/img/Photos/Others/forest2.jpg);">
          <div class="text-white text-center rgba-stylish-strong py-4 px-4">
            <div class="py-4">
              <!-- Content -->
              <h5 class="h5 orange-text"><i class="fa fa-camera-retro"></i> Photography</h5>

              <a class="btn peach-gradient"><i class="fa fa-clone left"></i> View </a>
          </div>
      </div>
  </div>
  <!-- Bank -->
  <div class="col-sm-3 car mb-4 card-image" style="background-image: url(https://mdbootstrap.com/img/Photos/Others/forest2.jpg);">
      <div class="text-white text-center rgba-stylish-strong py-4 px-4">
        <div class="py-4">
          <!-- Content -->
          <h5 class="h5 orange-text"><i class="fa fa-camera-retro"></i> Photography</h5>

          <a class="btn peach-gradient"><i class="fa fa-clone left"></i> View </a>
      </div>
  </div>
</div>
</div>
<br>
</div>

<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.13/js/mdb.min.js"></script>
</div>
@endsection
