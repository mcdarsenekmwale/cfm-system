<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>CFM System</title>


  <!-- Fonts -->
  <link rel="dns-prefetch" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CRaleway:300,400,500,600,700" media="all">

  <link href="https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css" rel="stylesheet" type="text/css" >

  <!-- Fonts -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="dns-prefetch" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.css" integrity="sha256-/WnCqTIGflaXyhZUIQe/O4gwaMRNtN39rHMKIeKbN3c=" crossorigin="anonymous" >
  <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" />
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" type="text/css">

  <!-- Material Design Bootstrap -->
  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('css/master.css') }}">
  <link rel="stylesheet" href="{{ asset('css/partner.css') }}">
  <link rel="stylesheet" href="{{ asset('counsellor/css/gbv.css') }}">
  <link rel="stylesheet" href="{{ asset('counsellor/css/master.css') }}">
  <!-- <link rel="stylesheet" href="{{ asset('css/theme.css') }}"> -->
  <!-- <link rel="stylesheet" href="{{ asset('css/style.css') }}"> -->

</head>
<body >
  <div id="app">
    <nav class="navbar navbar-expand-sm navbar-dark bg-dark fixed-top shadow flex-md-nowrap  p-1">

          <a href="https://www.yoneco.org " class="zoom px-4">  <img src="{{url('/')}}/images/logo.jpg" alt="" class="rounded-circle bounceIn img-fluid  img-master"></a>
          <a class=" navbar-brand " href="{{ url('/') }}" title="Conflict Feedback Mechanism System">
            <b> <span class="badge badg-master ">CFM System</span></b>
          </a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarscfm" aria-controls="navbarscfm" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

          <!-- Left Side Of Navbar -->
          <!-- <ul class="navbar-nav mr-auto">

          </ul> -->
          <div class="collapse navbar-collapse px-5" id="navbarscfm">
            <form class="form-inline my-2 my-md-0 col-9 form-input">
              <input id="search" class="form-control w-100 bg-dark" type="search" placeholder="Search ..." style="border-left: 1px solid rgba(0,0,0,.12);">
            </form>
            <!-- Left Side Of Navbar mobile-->
            <div class="col-m sidebar-nav  py-2 list-nav">
              <form class="form-inline my-2 my-md-0 input-nav">
                <input id="search" class="form-control w-100 bg-dark" type="search" placeholder="Search ..." style="border-left: 1px solid rgba(0,0,0,.12);">
              </form>
              <ul class="nav flex-column px-4">
                <li class="nav-item active">
                  <a class="nav-link nav-i " href="#">
                    <i class="fas fa-phone fa-lg lg "></i>
                    <span class="px-3">About Calls</span>
                    <span class="sr-only">(current)</span>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link nav-i" href="#">

                    <i class="fas fa-folder-open fa-lg lg "></i>
                    <span class="px-3">About Cases</span>
                  </a>
                </li>

                <li class="nav-item">
                  <a class="nav-link nav-i" href="#">
                    <i class="fa fa-bell fa-lg lg "></i>
                    <span class="px-3">Notifications</span>

                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link nav-i" href="#">
                    <i class="far fa-user-circle fa-lg lg "></i>
                    <span class="px-3">{{Auth::user()->firstname}} {{Auth::user()->lastname}}</span>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link nav-i" href="logout"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                  <i class="fa fa-sign-out"></i>
                  Logout
                </a>

                <form id="logout-form" action="logout" method="POST" style="display: none;">
                  @csrf
                </form>
              </li>

            </ul>

          </div>
          <!-- Right Side Of Navbar -->
          <ul class="navbar-nav mr-auto text-white ">
            <!-- Authentication Links -->
            @guest
            <li class="nav-item">
              <a class="nav-link" href="{{ route('login') }}"></a>
            </li>
            <li class="nav-item">
              @if (Route::has('register'))
              <a class="nav-link" href="{{ route('register') }}"></a>
              @endif
            </li>
            @else
            <li class="nav-item  ">
              <a href="" class="nav-link icons  list-icon "><span class=" icon fa fa-folder-open fa-lg lg py-1" > </span></a>

            </li>
            <li class="nav-item  dropdown  d-md-flex ">
              <!-- Toggle -->
              <a href="javascript: void(0);" class="nav-link icon drop-note  " role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" >
                <span class="icon  " style="position:relative; top:-4px;">
                  <i class="fa fa-bell fa-lg lg "></i>
                </span>
              </a>

              <!-- Menu -->
              <div class=" dropdown-menu dropdown-menu-right dropdown-menu-card ">
                <div class="card-header card-color">
                  <div class="row align-items-center">
                    <div class="col">

                      <!-- Title -->
                      <h6 class="card-header-title">
                        <span class="card-title text-uppercase text-muted mb-2"> <b>Notifications</b></span>
                      </h6>

                    </div>
                    <div class="col-auto">

                      <!-- Link -->
                      <a href="#!" class=" text-muted">
                        <span class="small">Clear all</span>
                      </a>

                    </div>
                  </div> <!-- / .row -->
                </div> <!-- / .card-header -->
                <div class="card-body">

                  <!-- List group -->
                  <div class="list-group list-group-flush my--3">
                    <a class="list-group-item px-0" href="#!">

                      <div class="row">
                        <div class="col-auto">

                          <!-- Avatar -->
                          <div class="avatar avatar-sm">
                            <img src=" https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(15).jpg" alt="..." class="avatar-img rounded-circle">
                          </div>

                        </div>
                        <div class="col ml--2">

                          <!-- Content -->
                          <div class="small text-muted">
                            <strong class="text-body">Dianna Smiley</strong> shared your post with <strong class="text-body">Ab Hadley</strong>, <strong class="text-body">Adolfo Hess</strong>, and <strong class="text-body">3 others</strong>.
                          </div>

                        </div>
                        <div class="col-auto">

                          <small class="text-muted">
                            2m
                          </small>

                        </div>
                      </div> <!-- / .row -->

                    </a>
                    <a class="list-group-item px-0" href="#!">

                      <div class="row">
                        <div class="col-auto">

                          <!-- Avatar -->
                          <div class="avatar avatar-sm">
                            <img src=" https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(18).jpg" alt="..." class="avatar-img rounded-circle">
                          </div>

                        </div>
                        <div class="col ml--2">

                          <!-- Content -->
                          <div class="small text-muted">
                            <strong class="text-body">Ab Hadley</strong> reacted to your post with a 😍
                          </div>

                        </div>
                        <div class="col-auto">

                          <small class="text-muted">
                            2m
                          </small>

                        </div>
                      </div> <!-- / .row -->

                    </a>

                    <a class="list-group-item px-0" href="#!">

                      <div class="row">
                        <div class="col-auto">

                          <!-- Avatar -->
                          <div class="avatar avatar-sm">
                            <img src=" https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(16).jpg" alt="..." class="avatar-img rounded-circle">
                          </div>

                        </div>
                        <div class="col ml--2">

                          <!-- Content -->
                          <div class="small text-muted">
                            <strong class="text-body">Daniela Dewitt</strong> subscribed to you.
                          </div>

                        </div>
                        <div class="col-auto">

                          <small class="text-muted">
                            2m
                          </small>

                        </div>
                      </div> <!-- / .row -->
                    </a>
                    <a class="list-group-item px-0 " href="#!">

                      <div class="row">
                        <div class="col-auto">

                          <!-- Avatar -->
                          <div class="avatar avatar-sm">
                            <img src=" https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(21).jpg"  class="avatar-img rounded-circle">
                          </div>

                        </div>
                        <div class="col ml--2">

                          <!-- Content -->
                          <div class="small text-muted">
                            <strong class="text-body">Ryu Duke</strong> reacted to your post with a 😍
                          </div>

                        </div>
                        <div class="col-auto">

                          <small class="text-muted">
                            2m
                          </small>

                        </div>
                      </div> <!-- / .row -->

                    </a>
                  </div>
                </div>
                <div class="card-footer text-center align-items-center p-1 card-color  notify-item notify-all">
                  <!-- Link -->
                  <a href="#!" class=" text-success">
                    <span class="small">View all</span>
                  </a>
                </div>

              </div> <!-- / .dropdown-menu -->

            </li>
            <li class="nav-item ">
              <a href="#modalSettings" data-toggle="modal" class=" nav-link icon icons  list-icon">  <span class="icon fa fa-cog fa-lg lg py-1" > </span></a>

            </li>
            <li class="nav-item dropdown  logout ">
              <a id="navbarDropdown" class="nav-link btn-account d-none d-md-flex  " href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                <span class="use-avatar float-left"><i class="fa fa-user-circle fa-2x"></i></span>

                <span class="accountt-summary  ">
                  <span class="accountt-name">{{ Auth::user()->username }}</span>
                  <span class="accountt-description">{{Auth::user()->type}}</span>
                </span>
              </a>

              <div class="dropdown-menu dropdown-menu-right bg-dark text-white" aria-labelledby="navbarDropdown">

                <a class="dropdown-item border-bottom dropdown-i text-white" href="#">
                  <i class="fa fa-user-circle-o"></i>
                  Profile  <i class="fa fa-angle-double-right"></i></a>
                  <a class="dropdown-item dropdown-i text-white" href="#">
                    <i class="fa fa-cog fa-spin"></i>
                    <i class="fa fa-"></i>
                    Settings
                  </a>
                  <a class="dropdown-item border-top dropdown-i text-white" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                  <i class="fa fa-sign-out"></i>
                  Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
              </div>
            </li>
            @endguest

        </ul>

      </div>
    </nav>
    <!-- settings: Demo -->
    <div class="modal fade fixed-right " id="modalSettings" tabindex="-1" role="dialog"  aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog  modal-dialog-vertical" role="document">
        <form class="modal-content" >
          <div class="modal-body">

            <!-- Close -->
            <button type="button" class="clos-x" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>

            <div class="text-center">
                <a href="https://www.yoneco.org " class="zoom ">
                  <img src="{{url('/')}}/images/logo.jpg" alt="..." class=" img-fluid mb-3" height="210px" width="220px">
                </a>

            </div>

            <h4 class="text-center mb-2">
              <b>CFM SYSTEM</b>
            </h4>

            <hr class="mb-4">

            <h4 class="mb-1">
              Color Scheme
            </h4>

            <p class="small text-muted mb-3">
              Overall light or dark presentation.
            </p>

            <div class="btn-group-toggle d-flex mb-4" data-toggle="buttons">
              <label class="btn btn-white active col">
                <input type="radio" name="colorScheme" id="colorSchemeLight" value="light" checked=""> <i class="fa fa-sun-o mr-2"></i> Light Mode
              </label>
              <label class="btn btn-white col ml-2">
                <input type="radio" name="colorScheme" id="colorSchemeDark" value="dark"> <i class="fa fa-moon-o mr-2"></i> Dark Mode
              </label>
            </div>

            <h4 class="mb-1">
              Navigation Position
            </h4>

            <p class="small text-muted mb-3">
              Select the primary navigation paradigm for your app.
            </p>

            <div class="btn-group-toggle d-flex mb-4" data-toggle="buttons">
              <label class="btn btn-white col">
                <input type="radio" name="navPosition" id="navPositionSidenav" value="sidenav" checked=""> Sidenav
              </label>
              <label class="btn btn-white col ml-2">
                <input type="radio" name="navPosition" id="navPositionTopnav" value="topnav"> Topnav
              </label>
              <label class="btn btn-white col ml-2 active">
                <input type="radio" name="navPosition" id="navPositionCombo" value="combo"> Combo
              </label>
            </div>

            <h4 class="mb-1">
              Sidebar Color
            </h4>

            <p class="small text-muted mb-3">
              Usually dictated by the color scheme, but can be overriden.
            </p>

            <div class="btn-group-toggle d-flex mb-4" data-toggle="buttons">
              <label class="btn btn-white active col">
                <input type="radio" name="sidebarColor" id="sidebarColorDefault" value="default" checked=""> Default
              </label>
              <label class="btn btn-white col ml-2">
                <input type="radio" name="sidebarColor" id="sidebarColorVibrant" value="vibrant"> Vibrant
              </label>
            </div>

          </div>
          <div class="modal-footer border-0">

            <button type="submit" class="btn btn-block btn-dark mt-auto">
              Preview
            </button>

          </div>
        </form>
      </div>
    </div>


    <div class="sidebar-m">
      <div class="absolute ">
        <div class=" ">
          <a href="javascript: history.go(-1);"  class="btn btn-dark  btn-sm ml-2">
            <i class="fa fa-home"></i>
          </a>

          <a href="javascript: void(0);" id="date_range" class="btn btn-dark  btn-sm ml-2" role="button" data-html="true" data-container="body" data-toggle="popover"  data-placement="bottom" >
            <i class="fa fa-calendar"></i>
          </a>
          <div class=" hidden bg-secondary col-md-2" id="body">
            <div class="input-group  input-group-sm " >
              <input type="text" class="form-control bg-dark text-white  datepicker" id="k_dashboard_daterangepicker" data-provide="datepicker" value="<?php date_default_timezone_set('Africa/Blantyre'); echo date("F j, Y")?>">
            </div>
          </div>

          <a href="javascript: void(0);" id="refresh" class="btn btn-dark  btn-sm ml-2" >
            <i class="fa fa-refresh"></i>
          </a>

          <a href="javascript: void(0);" id="filter" class="btn btn-dark btn-sm  ml-2">
            <i class="fa fa-filter"></i>
          </a>
        </div>
      </div>

    </div>

    <div class="sidebar">
      <div class="absolute ">
        <div class=" ">
          <a href="javascript: history.go(-1);"  class="btn btn-outline-dark  ml-2">
            <i class="fa fa-home"></i>
          </a>
          <br>
          <a href="javascript: void(0);" id="date_range" class="btn btn-outline-dark ml-2" role="button" data-html="true" data-container="body" data-toggle="popover"  data-placement="right" >
            <i class="fa fa-calendar"></i>
          </a>
          <div class=" hidden bg-secondary col-md-2" id="body">
            <div class="input-group  input-group-sm " >
              <input type="text" class="form-control bg-dark text-white  datepicker" id="k_dashboard_daterangepicker" data-provide="datepicker" value="<?php date_default_timezone_set('Africa/Blantyre'); echo date("F j, Y")?>">
            </div>
          </div>
          <br>
          <a href="javascript: void(0);" id="refresh" class="btn btn-outline-dark ml-2" >
            <i class="fa fa-refresh"></i>
          </a>
          <br>
          <a href="javascript: void(0);" id="filter" class="btn btn-outline-dark  ml-2">
            <i class="fa fa-filter"></i>
          </a>
        </div>
      </div>

    </div>

    <main class="py-4 ">
      @yield('content')
    </main>

    <footer class="white-text text-center border-top bg-light footer">
      <div class="container">
        <p class="float-right">
          <a class="tongue tongue-bottom scroll-me" href="#"> <span class="text-muted fa fa-chevron-circle-up"></span></a>
        </p>
        <span class="text-dark"><?php date_default_timezone_set('Africa/Blantyre'); echo date("Y")?> <i class="fa fa-copyright"></i> CFM SYSTEM</span>
        <small class="text-muted">YONECO ICT DEPARTMENT</small>
      </div>
    </footer>
  </div>

  <!-- jQuery -->
  <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.js" integrity="sha256-zTde1SYEpUiY54BwIFLX07JyfYU46JlHZvyTiCmg6ig=" crossorigin="anonymous"></script>
  <script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js" type="text/javascript"></script>
  <script>window.jQuery || document.write('<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"><\/script>')</script>
  <script type="text/javascript" src="http://code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>

  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
  <!-- Icons -->
  <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
  <script>
  feather.replace()
</script>
<!-- Google Analytics -->
<script>
window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
ga('create', 'UA-XXXXX-Y', 'auto');
ga('send', 'pageview');
</script>
<script async src='https://www.google-analytics.com/analytics.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
<script>
WebFont.load({
  google: {"families":["Poppins:300,400,500,600,700","Raleway:300,400,500,600,700"]},
  active: function() {
    sessionStorage.fonts = true;
  }
});
</script>
<!-- Scripts -->
<!-- <script src="{{ asset('js/app.js') }}" defer></script> -->
<script src="{{ asset('counsellor/js/gbv.js') }}" defer></script>
<script src="https://unpkg.com/moment" ></script>
<!-- <script src="{{ asset('js/master.js') }}" defer></script> -->
<script src="{{ asset('js/dashboard_counsellor.js') }}" defer></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>

<script type="text/javascript">
$(function() {
  checkuser('{{ Auth::user()->type }}');
  //autocomplete
  $(".search").autocomplete({
    source: "http://google.com",
    minLength: 1
  });

});
</script>
</body>
</html>
