  @extends('.counsellor.gbv.layout.master')


  @section('content')

  <div class="content-cards " >

    <div class="container-fluid ">

      <div class="small text-muted"> <b>{{$title}} 	 <span class="fa fa-angle-right fa-lg" style="position: relative; right: -10px;"></span><span class="fa fa-angle-right fa-lg " style="position: relative; right: -10px;"></span> </b>
      </div>
      <div class="row py-3 ">

        <div class="col-sm-3">
          <div class="card hoverable" id="total-calls">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="col">
                  <h6><small class="card-title text-uppercase text-muted mb-2"> <b> TOTAL CALLS </b></small></h6>
                  <!-- Heading -->
                  <span class="h3 mb-0">
                    <b >
                      <script>
                      function totalCalls(complete, inComplete) {
                        // body...
                        var result = complete + inComplete;
                        return result;
                      }
                      var tot = setInterval(totalCalls, 3000);
                      clearInterval(tot);
                      var res = totalCalls(23, 34);
                      document.write(res);

                      </script>
                    </b>
                  </span>

                  <!-- Badge -->
                  <span class="badge badge-success mt-1">
                    +3.5%
                  </span>
                </div>
                <div class="col-auto">
                  <!-- Icon -->
                  <span class="fa-2x fa fa-clone  text-muted mb-1"></span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="card hoverable" id="total-Complete-calls">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="col">
                  <h6><small class="card-title text-uppercase text-muted mb-2"> <b>TOTAL COMPLETE CALLS</b></small></h6>
                  <!-- Heading -->
                  <span class="h3 mb-0">
                    <b>
                      <script>
                      var comp = 56;
                      var tot = setInterval(comp, 3000);
                      clearInterval(tot);
                      document.write(comp);

                      </script>
                    </b>
                  </span>

                  <!-- Badge -->
                  <span class="badge badge-success mt-1">
                    +3.5%
                  </span>
                </div>
                <div class="col-auto">
                  <!-- Icon -->
                  <span class="fa-2x fa fa-folder  text-muted mb-1"></span>

                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="card hoverable" id="total-inComplete-calls">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="col">
                  <h6><small class="card-title text-uppercase text-muted mb-2"> <b>TOTAL INCOMPLETE CALLS</b></small></h6>
                  <!-- Heading -->
                  <span class="h3 mb-0">
                    <b>
                      <script>
                      var res = 7;
                      var tot = setInterval(res, 3000);
                      clearInterval(tot);
                      document.write(res);

                      </script>
                    </b>
                  </span>

                  <!-- Badge -->
                  <span class="badge badge-success mt-1">
                    +3.5%
                  </span>
                </div>
                <div class="col-auto">
                  <!-- Icon -->
                  <span class="fa-2x fa fa-folder-open  text-muted mb-1"></span>

                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-3" >
          <div class="card hoverable" id="new-call" data-url="{{$pagename}}">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="col">
                  <h6><small class="card-title text-uppercase text-muted mb-2"> <b><span class="size-768">{{$title}}</span><span class="size-76">GBV</span></b></small></h6>
                  <!-- Heading -->
                  <span class="h3 mb-0">
                    <b >
                      NEW CALL

                    </b>
                  </span>

                  <!-- Badge -->
                  <span class="text-success mt-1">

                  </span>
                </div>
                <div class="col-auto  text-success">
                  <!-- Icon -->
                  <span class="fa-2x fa fa-phone    mb-1"></span>
                  <span class="fa-2x fa fa-phone   mb-1"></span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid ">
      <div class="col-sm-12 row">
        <div class="col-sm-7">
          <div class="card float-left  card-wrapper4" >
            <div class="card-header card-color p-3">
              <div class="row align-items-center">
                <div class="col">

                  <!-- Title -->
                  <h6 class="card-header-title">
                    <span class="card-title text-uppercase text-muted mb-2"> <b>Recent Cases Captured</b></span>
                  </h6>

                </div>
                <div class="col-auto">

                  <!-- Link -->
                  <a href="#!" class=" text-muted">
                    <span class="small">View all</span>
                  </a>
                </div>
              </div> <!-- / .row -->
            </div>
            <div class="table-responsive mb-0" >
              <table class="table table-sm table-nowrap card-table table-hover " id="tables" >
                <thead class="thead">
                  <tr>
                    <th>
                      <a href="#" class="text-muted " data-sort="goal-project">
                        Date
                      </a>
                    </th>
                    <th>
                      <a href="#" class="text-muted sort" data-sort="goal-status">
                        District
                      </a>
                    </th>

                    <th>
                      <a href="#" class="text-muted sort" data-sort="goal-date">
                        Comment
                      </a>
                    </th>

                  </tr>
                </thead>
                <tbody class="lists">
                  @if($data['cases']->total() == 0)
                  <tr class="card-body" style="height:8rem;"></tr>
                  <tr class="alert  align-items-center" role="alert " >
                    <td class="alert-text text-center " colspan="3" >
                      No recent data has been captured yet by
                      <b>{{Auth::user()->firstname}} {{Auth::user()->lastname}} </b>
                    </td>
                  </tr>
                  <tr class="card-body" style="height:7.7rem;"></tr>
                  @else
                  @foreach($cases as $case)

                  <tr  class="hoverable" data-id="{{$case->id}}"  id="viewCase"  >
                    <td class="goal-date text-left">
                      <time >
                        <script >
                        function dateConvert(dateobj, format){
                          var year = dateobj.getFullYear();
                          var month= ("0" + (dateobj.getMonth()+1)).slice(-2);
                          var date = ("0" + dateobj.getDate()).slice(-2);
                          var hours = ("0" + dateobj.getHours()).slice(-2);
                          var minutes = ("0" + dateobj.getMinutes()).slice(-2);
                          var seconds = ("0" + dateobj.getSeconds()).slice(-2);
                          var day = dateobj.getDay();
                          var months = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
                          var dates = ["SUN","MON","TUE","WED","THU","FRI","SAT"];
                          var converted_date = "";

                          switch(format){
                            case "YYYY-MMM-DD":
                            converted_date = date  + "-" + months[parseInt(month)-1] + "-" +year ;
                            break;
                            case "DD-MMM":
                            converted_date = date + " " + months[parseInt(month)-1];
                            break;
                          }

                          return converted_date;
                        }
                        var i_date = "{{$case->date}}";
                        var j_date = new Date(Date.parse(i_date));
                        var date = dateConvert(j_date, "YYYY-MMM-DD");
                        document.write(date);
                        </script>
                      </time>
                    </td>
                    <td class="text-center">
                      <b>{{$case->ta->district->name}}</b>
                    </td>
                    <td class="goal-project text-left">

                      {{ substr(trim($case->comments) , 0, 70)}}..
                    </td>
                  </tr>
                  @endforeach
                  <tr class="alert card-color align-items-center" role="alert " >
                    <td class="alert-text  "  colspan="3">
                      <span class="list-inline row">
                        <b class="col-2">{!!$data['cases']->total()!!} Cases</b>
                        <b class="col-2">Showing {!!$data['cases']->count()!!} case per page
                        </b>
                        <b class="col-7"></b>
                        <b class="col-1 float-right" >
                          <a class="text-muted " href="{!! $data['cases']->url($data['cases']->currentPage()-1)!!}" aria-label="Previous">
                            <span class="fa fa-chevron-circle-left fa-lg"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="text-muted " href="{!! $data['cases']->url($data['cases']->currentPage()+1)!!}" aria-label="Next">
                            <span class="fa fa-chevron-circle-right fa-lg"></span>
                            <span class="sr-only">Next</span>
                          </a>
                        </b>
                      </span>

                    </td>
                  </tr>
                  @endif

                </tbody>

              </table>
              <!--  -->
              <div class=" card-color card-h"></div>
            </div>

          </div>

        </div>

        <div class="col-lg-5">
          <div class="  float-right">
            <div class=" col-12">
              <div class="col-12 card-wrapper5">
                <div class="order-lg-1 order-xl-1">
                  <!--begin::Portlet-->
                  <div class="k-portlet k-portlet--height-fluid k-widget " >
                    <div class="k-portlet__body">
                      <div id="k-widget-slider-13-2" class="k-slider carousel slide" data-ride="carousel" data-interval="10000">
                        <div class="k-slider__head">
                          <div class="k-slider__label">CALL CAPTURING TREND</div>
                          <div class="k-slider__nav">
                            <a class="k-slider__nav-prev carousel-control-prev" href="#k-widget-slider-13-2" role="button" data-slide="prev">
                              <i class="fa fa-angle-left"></i>
                            </a>
                            <a class="k-slider__nav-next carousel-control-next" href="#k-widget-slider-13-2" role="button" data-slide="next">
                              <i class="fa fa-angle-right"></i>
                            </a>

                            <div class="col-auto dropdown droppdown-inline" >
                              <!-- Toggle -->
                              <a href="javascript: void(0);" class="icons drop-note" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" >
                                <span class="icon active">
                                  <i class="fa fa-ellipsis-h text-muted"></i>
                                </span>
                              </a>

                              <!-- Menu -->
                              <div class="dropdown-menu  droppdown-menu droppdown-menu-right " x-placement="bottom-end" style="">
                                <ul class="k-nav">
                                  <li class="k-nav__section k-nav__section--first">
                                    <span class="k-nav__section-text">Export Tools</span>
                                  </li>
                                  <li class="k-nav__item">
                                    <a href="#" class="k-nav__link">
                                      <i class="k-nav__link-icon la fa fa-print"></i>
                                      <span class="k-nav__link-text link-text">Print</span>
                                    </a>
                                  </li>
                                  <li class="k-nav__item">
                                    <a href="#" class="k-nav__link">
                                      <i class="k-nav__link-icon la fa fa-copy"></i>
                                      <span class="k-nav__link-text link-text">Copy</span>
                                    </a>
                                  </li>
                                  <li class="k-nav__item">
                                    <a href="#" class="k-nav__link">
                                      <i class="k-nav__link-icon la fa fa-file-excel-o"></i>
                                      <span class="k-nav__link-text link-text">Excel</span>
                                    </a>
                                  </li>
                                  <li class="k-nav__item">
                                    <a href="#" class="k-nav__link link-text">
                                      <i class="k-nav__link-icon la fa fa-file-text-o"></i>
                                      <span class="k-nav__link-text link-text">CSV</span>
                                    </a>
                                  </li>
                                  <li class="k-nav__item">
                                    <a href="#" class="k-nav__link">
                                      <i class="k-nav__link-icon la fa fa-file-pdf-o"></i>
                                      <span class="k-nav__link-text link-text">PDF</span>
                                    </a>
                                  </li>
                                </ul>
                              </div> <!-- / .dropdown-menu -->
                            </div>
                          </div>
                        </div>
                        <div class="carousel-inner">
                          <div class="carousel-item k-slider__body active">
                            <div class="k-widget-13">
                              <div class="k-widget-13__body">
                                <div class="k-widget-9__chart">

                                  <div class="chartjs-size-monitor" >
                                    <div class="chartjs-size-monitor-expand" >
                                      <div ></div>
                                    </div>
                                    <div class="chartjs-size-monitor-shrink" >
                                      <div >

                                      </div>
                                    </div>
                                  </div>

                                  <canvas id="statistics" height="285.8" width="77" class="chartjs-render-monitor" ></canvas>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="carousel-item k-slider__body" style="height: 18.9rem;">
                            <div class="k-widget-13">
                              <div class="k-widget-13__body">
                                <!-- Chart -->
                                <div class="chart chart-appended chartjs-size-monitor chartjs-size-monitor-expand">
                                  <canvas id="myDoughnut" class="chart-canvas chartjs-render-monitor" data-target="#devicesChartLegend" height="150" width="72" ></canvas>
                                </div>

                                <!-- Legend -->
                                <div id="devicesChartLegend" class="chart-legend"><span class="chart-legend-item"><i class="chart-legend-indicator" style="background-color: #1cac81 !important;"></i>Complete</span>
                                  <span class="chart-legend-item"><i class="chart-legend-indicator" style="background-color: #41675c !important;"></i>InComplete</span>
                                </div>
                              </div>

                            </div>
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>

                  <!--end::Portlet-->	</div>
                </div>

              </div>
            </div>
          </div>
        </div>
        <br><br>
      </div>

      <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header bg-dark align-items-center">
              <h6 class="card-header-title">
                <span class=" card-title text-center text-uppercase text-white mb-2 "> <b>Client Information</b></span>
              </h6>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" class="text-danger fa fa-times-circle"></span>
              </button>
            </div>
            <div class="modal-body">
              <span id="modal-case"></span>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-outline-secondary btn-sm" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-outline-success btn-sm">Save </button>
            </div>
          </div>
        </div>
      </div>

    </div>
      @endsection
