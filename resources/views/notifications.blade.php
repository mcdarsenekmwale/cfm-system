
<div class="form-group form-group-last ">
	@if(Session::has('error'))
	<div class="alert alert-danger" role="alert" id="k_form_1_msg">
		<div class="alert-icon"><i class="fa fa-warning"></i></div>
		<div class="alert-text px-5">
			<b>{{Session::get('error')}}</b> <br>
			Oh snap! Check few things up and try submitting again.
		</div>
		<div class="alert-close" style="position: absolute; top: 2px;
		right:2px;">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="fa fa-close"></i></span>
			</button>
		</div>
	</div>
	@endif
	@if(Session::has('message'))
	<div class="alert alert-success" role="alert" id="k_form_1_msg">
		<div class="alert-icon"><i class="fa fa-check"></i></div>
		<div class="alert-text px-5">
			<b>{{Auth::user()->firstname}} {{Auth::user()->lastname}} added a new call and the</b><br>
			{{Session::get('message')}}

		</div>
		<form action="{{route('calldetails')}}" method="post" class="float-right" style="position: absolute; top: 18px;
			right:52px;">
			<input type="hidden" value="{{session('callCode')}}" name="id">
			@csrf
			<input type="submit" value="view " class="btn btn-outline-success btn-sm">
		</form>
		<div class="alert-close" style="position: absolute; top: 2px;
		right:2px;">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="fa fa-close"></i></span>
			</button>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			var note = setTimeout(refresh , 3000);
			clearTimeout(note);

			function refresh() {
				// body...
				window.location.reload();

			}
		});
	</script>
	@endif
	</div>
