@extends('layouts.master')

@section('activeB','Register Call')

@section('mainContent')
  <div class="col-md-10 col-md-offset-1">
    @include('notifications')
    <div class="list-group-item active" >
      <b>
        <ul class="nav nav-tabs nav-justified" role="tablist" >
          <li role="presentation" class="active"><a class="text-white" href="#gbv" aria-controls="gbv" role="tab" data-toggle="tab" onclick="hideOtherDetails()" >GBV</a></li>
          <li role="presentation" ><a class="text-white" href="#alliance_one"  aria-controls="alliance_one" role="tab" data-toggle="tab" onclick="hideOtherDetails()"><span >ALLIANCE ONE</span></a></li>
          <li role="presentation" ><a class="text-white" href="#congoma"  aria-controls="congoma" role="tab" data-toggle="tab" onclick="showOtherDetails()"><span >CONGOMA</span></a></li>
        </ul>
      </b>
    </div>
    <p class="text-left"> <small><i class="text-primary " > *Please fill the form below to add a new call</i></small> </p>
    @if(session('registeration_error'))
      <div class="list-item-group alert alert-warning alert-dismissable"  style="background:#D05141; color:#fff">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><b>{{session('registeration_error')}}</b>
      </div>
    @elseif(session('callCode'))
      <div class="list-item-group alert alert-success alert-dismissable" >
        <span>
          <span class=""><b>Call succesfully saved </b></span>
          <span class="">
            <form action="/callDetails" method="post">
              <input type="hidden" value="{{session('callCode')}}" name="callCode">
              {!! csrf_field() !!}
              <input type="submit" value="view " class="btn btn-primary btn-xs">
            </form>
          </span>
        </span>
      </div>
    @endif
    <div style="background:#fff; ">
      <form name="addcall" action="/addCall" method="post" >
        <p class="row">
          <div class="col-md-12">
            <span class="list-group-item" style="background:#cfd6d6"><b>DATE & TIME OF CALLS</b></span>
            <span class="list-group-item">
              <td class="col-md-4"><b>Date : </b><input type="text" name="date" size=19 value="<?php date_default_timezone_set('Africa/Blantyre'); echo date('Y-m-d'); ?>"></td>
              <td class="col-md-4"><b>Start Time : </b><input type="text" name="starttime" size=19 value="<?php date_default_timezone_set('Africa/Blantyre'); echo date("H:i:s")?>"></td>
              <td class="col-md-4"><b>End Time : </b><input type="time" name="endtime" value="{{ old('endtime')}}" size=19></td>
              <span>
              </div>
            </p>
            <p class="row">
              <div class="col-md-12">
                <span class="list-group-item" style="background:#cfd6d6"><b>CLIENT DETAILS</b></span>
                <span class="list-group-item">
                  <td width="24%"><b>Name</b></td>
                  <td width="1%">:</td>
                  <td width="24%"><input type="text" name="name" value="{{ old('name')}}"size=30> </td>
                  <span width="26%" align="right" class=""> <b>Phone number</b></span>
                  <td width="1%">:</td>
                  <td width="24%"><input type="text" name="phonenumber" value="{{ old('phonenumber')}}"size=30></td>
                  <!-- </span>
                  <span class="list-group-item">-->
                  <td><b>Gender<b></td>
                    <td>:</td>
                    <td>
                      <input type="radio" name="gender" value="Female" checked>Female
                      <input type="radio" name="gender" value="Male">Male
                    </td>
                    <span class="col-md-offset-1">Age</span>
                    <td>:</td>
                    <td ><input  style="width:40px" type="text" name="age" value="{{ old('age')}}"  onblur="showhidefield()"> </td>
                  </span>
                </div>
              </p>
              <p class="row">
                <div class="col-md-12">
                  <span class="list-group-item" style="background:#cfd6d6"><b>CALL DETAILS</b></span>
                  <span class="list-group-item">

                    <td width="13%"><b>District<b></td>
                      <td  width="1%">:</td>
                      <td width="86%" align="left">
                        <select name="district">
                          @foreach($districts as $district)
                            <option value="{{$district->id}}">{{$district->name}}</option>
                          @endforeach
                        </select>
                      </td>
                    </span>
                    <span class="list-group-item">
                      <span class="row">
                        <span valign="top" class="col-md-1"><b>Nature: </b></span>
                        <div class="col-md-3">
                          <input type="radio" name="nature" value="information seeking" checked> Information seeking
                          <input type="radio" name="nature" value="followup">Followup
                          <input type="radio" name="nature" value="followup">Tipoff
                        </div>
                        <div class="col-md-3">
                          <div id='hideablearea1'  class="" style='visibility:hidden;'>
                            <input type="radio" name="nature" value="concerned child"> Concerned child
                            <input type="radio" name="nature" value="victimised child"> Victimised child
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div id='hideablearea2' class="col-md-" style='visibility:hidden;'>
                            <input type="radio" name="nature" value="concerned adult"> Concerned adult
                            <input type="radio" name="nature" value="victimised adult" focus> Victimised adult
                          </div>
                        </div>
                        <div  class="col-md-3" style=''>
                          perpetrator
                          <select name="perpetrator">
                            @foreach($perpetrators as $perpetrator)
                              <option value="{{$perpetrator->id}}">{{$perpetrator->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </span>
                    </span>
                  </div>
                </p>
                <p class="row">
                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="gbv">
                      <div class="col-md-12 panel panel-default">
                        <div class="panel-heading" data-toggle="collapse" data-target="#collapseGbv" aria-expanded="false" aria-controls="collapseGbv"><b>Issues/Sub-issues</b> [<a style="text-decoration: none;" href="./helpfiles/index.html" target="blank">Help</a>]
                          <span class="fa fa-angle-double-down fa-2x pull-right" title="click here to see the details"></span>
                        </div>
                        <div class=" panel-body collapse" id ="collapseGbv">
                          @foreach($issues as $issue)
                            @if($issue->name != "Other" AND $issue->name != "GBV" AND $issue->name !="Alliance One" AND $issue->description !="congoma" )
                              <div class="col-md-3 " style="">
                                <span class="list-group-item" style="background:#337AB7; color:#fff" ><b>{{$issue->name}}</b></span>
                                @foreach($issue->subissues as $subissue)
                                  <span class="col-md-12"><input type="checkbox" name="subissue[]" value="{{$subissue->id}}" > {{$subissue->name}}</span>
                                @endforeach
                              </div>
                            @elseif($issue->name == "GBV")
                              <div class="col-md-3 " style="height:100px">
                                <span class="list-group-item" style="background:#337AB7; color:#fff" ><b>{{$issue->name}}</b></span>
                                @foreach($issue->subissues as $subissue)
                                    <span class="col-md-12"><input type="checkbox" name="subissue[]" value="{{$subissue->id}}" > {{$subissue->name}}</span>
                                @endforeach
                              </div>
                            @endif
                          @endforeach
                        </div>
                      </div>
                    </div>
                    <div role="tabpanel" class="tab-pane " id="alliance_one">
                      <div class="panel panel-default">
                        <div class="panel-heading" data-toggle="collapse" data-target="#collapseAlliance" aria-expanded="false" aria-controls="collapseAlliance">ISSUE DETAILS
                          <span class="fa fa-angle-double-down fa-2x pull-right" title="click here to see the details"></span>
                        </div>
                        <div class="panel-body collapse" id="collapseAlliance">
                          <div class="row">
                            <div class="col-lg-4">
                              <div class="panel panel-primary">
                                @foreach($issues as $issue)
                                  @if($issue->name != "Other" AND $issue->name != "GBV" AND $issue->name =="Alliance One" )
                                    <div class="panel-heading">
                                      <b>{{$issue->name}}</b>
                                      <input type="hidden" name ="alliance" value="{{$issue->name}}">
                                    </div>
                                    <div class="panel-body">
                                      <div class=" " style="">
                                        @foreach($issue->subissues as $subissue)
                                            <span class="col-md-12"><input type="checkbox" name="subissue[]" value="{{$subissue->id}}" > {{$subissue->name}}</span>
                                        @endforeach
                                      </div>
                                    @elseif($issue->name == "Alliance One")
                                      <div class=" " style="height:100px">
                                        <span class="list-group-item" style="background:#337AB7; color:#fff" ><b>{{$issue->name}}</b></span>
                                        @foreach($issue->subissues as $subissue)
                                            <span class=""><input type="checkbox" name="subissue[]" value="{{$subissue->id}}" > {{$subissue->name}}</span>
                                          @endforeach
                                      </div>
                                    @endif
                                  @endforeach
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-4">
                              <div class="panel panel-primary">
                                <div class="panel-heading">Labour Type</div>
                                <div class="panel-body">
                                  @foreach($labour_types as $lab_type)
                                    <label class="radio-inline">
                                      <input type="radio" name="lab_type[]"  value="{{$lab_type->id}}"> <b> {{$lab_type->name}}</b>
                                    </label>
                                  @endforeach
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-4">
                              <div class="panel panel-primary">
                                <div class="panel-heading">Status</div>
                                <div class="panel-body">
                                  <label class="radio-inline">
                                    <input type="radio" name="Status" onchange="hideWorker()" value="4" data-toggle="collapse" data-target="#worker" checked> <b>Farmer</b>
                                  </label>
                                  <label class="radio-inline">
                                    <input type="radio" name="Status" onchange="showWorker()" value="Worker" data-toggle="collapse" data-target="#worker"> <b> Worker </b>
                                  </label>
                                  <label class=" pull-right collapse" id="worker">
                                    @foreach($workers as $worker)
                                      <div class="checkbox ">
                                        @if($worker->id != "4")
                                          <input type="checkbox" name="worker_status[]" value="{{$worker->id}}"> <strong>{{$worker->name}}</strong>
                                        @endif
                                      </div>
                                    @endforeach
                                  </label>
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-4">
                              <div class="panel panel-primary">
                                <div class="panel-heading">ALP Committees</div>
                                <div class="panel-body">
                                  <label class="radio-inline">
                                    <input type="radio" name="ALP_committee "  value="Matutu"><b> Matutu </b>
                                  </label>
                                  <label class="radio-inline">
                                    <input type="radio" name="ALP_committee "  value="Mkanda"> <b> Mkanda </b>
                                  </label>
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-4">
                              <div class="panel panel-primary">
                                <div class="panel-heading">
                                  Scheme :
                                  <select  name="scheme" class=" pull-right bg-primary">
                                    @foreach($schemes as $scheme)
                                      <option value="{{$scheme->id}}"> <span class=" text-muted">{{$scheme->name}}</span></option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="panel-footer">
                        <div class="row">
                          <div class="col-lg-6">
                            <b style="padding-right:5px;">PILOT </b>
                            <label class="radio-inline">
                              <input type="radio" name="pilot"  value="Within Pilot"> Within Pilot
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="pilot" value="Outside Pilot"> Outside Pilot
                            </label>
                          </div>
                  </div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane " id="congoma">
                <div class="panel panel-default">
                  <div class="panel-heading hoverable" data-toggle="collapse" data-target="#collapseCongoma" aria-expanded="false" aria-controls="collapseCongoma">
                    ISSUE DETAILS <a class="" style="text-decoration: none; " href="./helpfiles/index.html" target="blank">Help</a><span class="fa fa-angle-double-down fa-2x pull-right" title="click here to see the details"></span></div>
                    <div class="panel-body collapse" id="collapseCongoma">
                      <div class="row">
                        @foreach($issues as $issue)
                          @if( $issue->description =="congoma" )
                            <div class="col-md-3 " style="">
                              <span class="list-group-item" style="background:#337AB7; color:#fff" ><b>{{$issue->name}}</b></span>
                              @foreach($issues as $issue)
                                @if($issue->issue == $issue->name AND $issue->issue != "Other")
                                  <span class="col-md-12"><input type="checkbox" name="subissue[]" value="{{$issue->subissue_code}}" > {{$issue->subissue}}</span>
                                @endif
                              @endforeach
                            </div>
                          @elseif($issue->name == "CONGOMA")
                            <div class="col-md-3 " style="height:100px">
                              <span class="list-group-item" style="background:#337AB7; color:#fff" ><b>{{$issue->name}}</b></span>
                              @foreach($issues as $issue)
                                @if($issue->issue == $issue->name AND $issue->issue != "Other")
                                  <span class="col-md-12"><input type="checkbox" name="subissue[]" value="{{$issue->subissue_code}}" > {{$issue->subissue}}</span>
                                @endif
                              @endforeach
                            </div>
                          @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </p>
          <p class="row">
            <p><b>Other issue</b></p>
            <span class="list-group-item">
              <td width="17%"><input type="checkbox" name="other_subissue" value=22 onclick="showhidefield3()"> Other sub-issue </td>
              <td width="83%">
                <div id='hideablearea3' style='visibility:hidden;'>
                  <font color="red">(specify)</font><input type="text" name="other_subissue_specify" value="" size=81>
                </div>
              </span>
            </p>
            {!! csrf_field() !!}
            <p class="row">
              <span class="col-md-12">
                <p class="list-group-item"><b>Intervention</b>
                  <td valign="top">:</td>
                  @foreach($interventions as $intervention)
                    @if($intervention->name =="Referred")
                      <!--<input type="checkbox" name="intervention[]" value=4 onclick="showhidefield4()"/>Referred-->
                      <input type="checkbox" name="intervention_ref" value=4 onclick="showhidefield4()"/>Referred
                    @elseif($intervention->description=="congoma")
                      <span id="congoma-issue" style="visibility:hidden; display:none;">
                        <input type="checkbox" name="intervention[]" value="{{$intervention->id}}"/> {{$intervention->name}}
                      </span>
                    @else
                      <input type="checkbox" name="intervention[]" value="{{$intervention->id}}"/> {{$intervention->name}}
                    @endif
                  @endforeach
                </p>
              </span>
            </p>
            <span  style='visibility:hidden;'>
              <td valign="top"><div id='hideablearea4' style='visibility:hidden;'>Referrals</div></td>
              <td valign="top"><div id='hideablearea5' style='visibility:hidden;' >:</div></td>
              <td align="left"><div id='hideablearea6'  style='visibility:hidden;'>
                @foreach($refferals as $refferal)
                  <input type="checkbox" name="referrals[]" id={{ $refferal->id }} value={{ $refferal->id }}>{{ $refferal->name }}
                @endforeach
              </div>
            </td>
          </span>
          <br><br>
          <span class="list-group-item">
            <td valign="top"><b>Comment/Case brief</b></td>
            <td valign="top">:</td>
            <td align="left">
              <textarea rows=2 cols=105  name="brief" value="{{ old('brief')}}"></textarea>
            </td>
          </span>
          <span class="list-group-item">
            <td valign="top"><b>Call completeness</b></td>
            <td valign="top">:</td>
            <td align="left">
              <input type="radio" name="callCompleteness" value="complete" checked>Complete
              <input type="radio" name="callCompleteness" value="incomplete">Incomplete
            </td>
          </span>
          <span class="list-group-item">
            <td align="center" colspan=3>	<input type="submit" name="submit" value="Save Call" class="btn btn-success btn-md"></td>
          </span>
        </p>
      </form>
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function(){
    $('#worker').collapse('hide');
    document.getElementById("worker").style.visibility = "hidden";
    document.getElementById("worker").style.display = "none";
  });

  function showWorker()
  {
    document.getElementById("worker").style.visibility = "visible";
    document.getElementById("work").style.display = "block";
  }


  function showOtherDetails(){
    document.getElementById("congoma-issue").style.visibility = "visible";
    document.getElementById("congoma-issue").style.display = "block";
    document.getElementById("congoma-refferal").style.visibility = "visible";
    document.getElementById("congoma-refferal").style.display = "block";

  }

  function hideOtherDetails(){
    document.getElementById("congoma-issue").style.visibility = "hidden";
    document.getElementById("congoma-issue").style.display = "none";
    document.getElementById("congoma-refferal").style.visibility = "hidden";
    document.getElementById("congoma-refferal").style.display = "none";

  }

  function hideWorker()
  {
    document.getElementById("worker").style.visibility = "hidden";
    document.getElementById("worker").style.display = "none";
  }

  function validateForm()
  {
    var date=document.forms["addcall"]["date"].value;
    if (date==null || date=="")
    {
      alert("Date can't be blank");
      return false;
    }

    var starttime = document.forms["addcall"]["starttime"].value;
    if (starttime==null || starttime =="")
    {
      alert("Start time can't be blank");
      return false;
    }


    var other_subissue_specify = document.forms["addcall"]["other_subissue_specify"].value;
    if ((document.addcall.other_subissue.checked) && (other_subissue_specify==null || other_subissue_specify ==""))
    {
      alert("You must enter the name of the other sub-issue.");
      return false;
    }

  }

  function showhidefield()
  {
    if (document.addcall.age.value > 0 && document.addcall.age.value <= 18){
      document.getElementById("hideablearea1").style.visibility = "visible";
      document.getElementById("hideablearea1").style.display = "block";

      document.getElementById("hideablearea2").style.visibility = "hidden";
      document.getElementById("hideablearea2").style.display = "none";
    }  else {
      document.getElementById("hideablearea1").style.visibility = "hidden";
      document.getElementById("hideablearea1").style.display = "none";

      document.getElementById("hideablearea2").style.visibility = "visible";
      document.getElementById("hideablearea2").style.display = "block";
    }

  }

  function showhidefield2()
  {
    if (document.addcall.age.value > 18){
      document.getElementById("hideablearea2").style.visibility = "visible";
      document.getElementById("hideablearea2").style.display = "block";
    }  else {
      document.getElementById("hideablearea2").style.visibility = "hidden";
      document.getElementById("hideablearea2").style.display = "none";
    }

  }

  function showhidefield3()
  {
    if (document.addcall.other_subissue.checked){
      document.getElementById("hideablearea3").style.visibility = "visible";
      document.getElementById("hideablearea3").style.display = "block";
    }  else {
      document.getElementById("hideablearea3").style.visibility = "hidden";
      document.getElementById("hideablearea3").style.display = "none";
    }

  }

  function showhidefield4()
  {
    if (document.addcall.intervention_ref.checked){
      document.getElementById("hideablearea4").style.visibility = "visible";
      document.getElementById("hideablearea4").style.display = "block";
      document.getElementById("hideablearea5").style.visibility = "visible";
      document.getElementById("hideablearea5").style.display = "block";
      document.getElementById("hideablearea6").style.visibility = "visible";
      document.getElementById("hideablearea6").style.display = "block";
    }  else {
      document.getElementById("hideablearea4").style.visibility = "hidden";
      document.getElementById("hideablearea4").style.display = "none";
      document.getElementById("hideablearea5").style.visibility = "hidden";
      document.getElementById("hideablearea5").style.display = "none";
      document.getElementById("hideablearea6").style.visibility = "hidden";
      document.getElementById("hideablearea6").style.display = "none";
    }

  }

</script>

@stop
