@extends('layouts.master')

@section('activeB','Counsellors Reports')

@section('mainContent')

	<div class="page-content">
		<div class="ace-settings-container" id="ace-settings-container">
			<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
				<i class="ace-icon fa fa-cog bigger-130"></i>
			</div>
			<div class="ace-settings-box clearfix" id="ace-settings-box">
				<div class="pull-left width-50">
				</div><!-- /.pull-left -->
				<div class="pull-left width-50">
				</div><!-- /.pull-left -->
			</div><!-- /.ace-settings-box -->
		</div><!-- /.ace-settings-container -->
		<div class="page-header">
			<h1>Dashboard<small><i class="ace-icon fa fa-angle-double-right"></i>{{$stage}} Returns</small></h1>
		</div><!-- /.page-header -->
		<div class="row">
			@include('notifications')
			<div class="col-md-12">
				<div class="row">
					<div class="col-lg-8">
						<div class="col-lg-8">
							<input class = "form-control" id="search" type = "text" placeholder = "filter table results...">
						</div>
						<div>
							<div class="dropdown col-lg-4">
								<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Filter By:
									<span class="caret"></span></button>
									<ul class="dropdown-menu">
										<li><a href="viewStats?stage=daily">Today's Report</a></li>
										<li><a href="viewStats?stage=weekly">Weekly Report</a></li>
										<li><a href="viewStats?stage=monthly">Monthly Report</a></li>
										<li><a href="viewStats?stage=quaterly">Quaterly Report</a></li>
										<li><a href="viewStats?stage=annual">Annual Report</a></li>
									</ul>
								</div>
							</div>
							<br>
							<div class="panel panel-primary table-responsive">
								<table class="table table-hover table-striped">
									<thead class="thin-border-bottom panel-header">
										<col>
										<colgroup span="3"></colgroup>
										<colgroup span="2"></colgroup>

										<tr>
											<th rowspan="2" style="font-size:16px; text-align: center">Officer</th>
											<th colspan="3" scope="colgroup" style="font-size:18px; text-align: center">Number Of Calls</th>
										</tr>

										<tr>
											<th style="font-size:16px"><b class="red">Incomplete</b></th>
											<th style="font-size:16px"><b class="blue">Complete</b></th>
											<th style="font-size:186x"><b class="green">Total</b></th>
										</tr>
									</thead>
									<tbody id="test">
										@foreach($stats as $stat)
											<tr @if(($stat->complete + $stat->incomplete) >= 70) class="alert alert-success" @else class="alert alert-danger" @endif>
												<td>{{$stat->username}}</td>
												<td>{{$stat->incomplete}}</td>
												<td>{{$stat->complete}}</td>
												<td >{{$stat->complete + $stat->incomplete}}</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							<div class="list-group-item" style="text-align:right">
							</div>
						</div>
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
		@stop
