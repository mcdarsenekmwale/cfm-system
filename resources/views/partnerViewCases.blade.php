@extends('layouts.master')

@section('activeB','View Cases')

@section('mainContent')
  <div class="page-content">
    <div class="ace-settings-container" id="ace-settings-container">
      <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
        <i class="ace-icon fa fa-cog bigger-130"></i>
      </div>
      <div class="ace-settings-box clearfix" id="ace-settings-box">
        <div class="pull-left width-50">
        </div><!-- /.pull-left -->
        <div class="pull-left width-50">

        </div><!-- /.pull-left -->
      </div><!-- /.ace-settings-box -->
    </div><!-- /.ace-settings-container -->

    <div class="page-header">
      <h1>
        Cases
        <small>
          <i class="ace-icon fa fa-angle-double-right"></i>
          @if($status == 1)
            <b class="blue" style="font-size:18px">Unresoved cases</b>
          @elseif($status == 2)
            <b class="green" style="font-size:18px"> Resolved cases</b>
          @elseif($status == 3)
            <b class="black" style="font-size:18px">Search Results for {{$key_word}}</b>
          @endif
        </small>
      </h1>
    </div><!-- /.page-header -->
    <!-- the form -->
    <div class="row">
      @include('notifications')
      <div class="col-xs-12"> <!-- case table -->
        <table class="table table-bordered table-striped">
          <thead class="thin-border-bottom">
            <tr>
              <th>Date</th>
              <th>District</th>
              <th>Partner</th>
              <th>Comment</th>
              @if($status == 1)
                <th>Resolve</th>
              @endif
            </tr>
          </thead>
          <tbody>
            @foreach($cases as $case)
              <tr>
                <td>{{$case->date}}</td>
                <td><b class="green">{{$case->district}}</b></td>
                <td><b class="green">{{$case->partner}}</b></td>
                @if($status == 3)
                  <td style="font-size:16px">
                    {!! str_replace($key_word, "<b><span style='background-color:yellow'>". $key_word." </span></b>", substr(trim($case->comments) , 0, 400))!!}
                  </td>
                  <?php Session::flash('SearchKeyWord', $key_word); ?>
                @else
                  <td style="font-size:16px">
                    {{ substr(trim($case->comments) , 0, 200)}}...
                  </td>
                @endif
                @if($status == 1)
                  <td>
                    <a href="/caseClose?id={{$case->id}}">
                      <i class="glyphicon glyphicon-pencil"></i>
                    </a>
                  </td>
                @endif
              </tr>
            @endforeach
          </tbody>
        </table>
        {{$cases->appends(['status' => Request::input('status')])->links()}}
        <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
          <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
        </a>
      </div><!-- /.main-container -->
    </div>
  </div>
@stop
