<div class="footer">
    <div class="footer-inner">
        <div class="footer-content">
						<span class="bigger-120">
							<span class="blue bolder">YONECO ICT</span>
					           &copy; 2018
						</span>
            &nbsp; &nbsp;
            <span class="action-buttons">
							<a href="#">
								<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-rss-square orange bigger-150"></i>
							</a>
						</span>
            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>

            <!-- basic scripts -->

            <!--[if !IE]> -->
            <script src="@yield('base')/assets/js/jquery-2.1.4.min.js"></script>

            <!-- <![endif]-->

            <!--[if IE]>
            <script src="assets/js/jquery-1.11.3.min.js"></script>
            <![endif]-->
            <script type="text/javascript">
                if ('ontouchstart' in document.documentElement) document.write("<script src='@yield('base')/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
            </script>
            <script src="@yield('base')/assets/js/bootstrap.min.js"></script>

            <!-- page specific plugin scripts -->

            <!--[if lte IE 8]>
            <script src="assets/js/excanvas.min.js"></script>
            <![endif]-->
            <script src="@yield('base')/assets/js/jquery-ui.custom.min.js"></script>
            <script src="@yield('base')/assets/js/jquery.ui.touch-punch.min.js"></script>
            <script src="@yield('base')/assets/js/jquery.easypiechart.min.js"></script>
            <script src="@yield('base')/assets/js/jquery.sparkline.index.min.js"></script>
            <script src="@yield('base')/assets/js/jquery.flot.min.js"></script>
            <script src="@yield('base')/assets/js/jquery.flot.pie.min.js"></script>
            <script src="@yield('base')/assets/js/jquery.flot.resize.min.js"></script>

            <!-- ace scripts -->
            <script src="@yield('base')/assets/js/ace-elements.min.js"></script>
            <script src="@yield('base')/assets/js/ace.min.js"></script>

            <script src="@yield('base')/morrisjs/raphael.min.js"></script>
            <script src="@yield('base')/morrisjs/morris.min.js"></script>

            <!-- inline scripts related to this page -->
            <script src="@yield('base')/js/indexjs.js"></script>
            <script src="@yield('base')/js/search.js"></script>
        </div>
    </div>
</div>
