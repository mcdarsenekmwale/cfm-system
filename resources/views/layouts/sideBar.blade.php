<div id="sidebar" class="sidebar responsive ace-save-state">
    <script type="text/javascript">
        try {
            ace.settings.loadState('sidebar')
        } catch (e) {
        }
    </script>

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <button class="btn btn-success">
                <i class="ace-icon fa fa-signal"></i>
            </button>

            <button class="btn btn-info">
                <i class="ace-icon fa fa-pencil"></i>
            </button>

            <button class="btn btn-warning">
                <i class="ace-icon fa fa-users"></i>
            </button>

            <button class="btn btn-danger">
                <i class="ace-icon fa fa-cogs"></i>
            </button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div><!-- /.sidebar-shortcuts -->

    <ul class="nav nav-list">
        <li class="active">
            <a href="/home">
                <i class="menu-icon fa fa-tachometer"></i>
                <span class="menu-text"> Dashboard </span>
            </a>

            <b class="arrow"></b>
        </li>

        @if(Auth::User()->type == "supervisor")
          <li class="">
            <a href="/viewCases?status=1" class="black">
              <i class="menu-icon fa fa-caret-right"></i>
              <b style="font-size:14px"> UnReferred Cases</b>
              <b class="arrow"></b>
            </a>
          </li>
        @endif
        @if(Auth::user()->type != "admin")
          <li class="">
              <a href="/viewCases?status=2" class="blue" style="">
                  <i class="menu-icon fa fa-caret-right"></i>
                  @if(Auth::User()->type == "partner")
                      <b style="font-size:14px">Unresolved Cases</b>
                  @else
                    <b style="font-size:15px">Referred Cases</b>
                  @endif
              </a>
          </li>
          <li class="">
              <a href="/viewCases?status=3" class="green" style="">
                  <i class="menu-icon fa fa-caret-right"></i>
                  <b style="font-size:15px">Resolved Cases</b>
              </a>
          </li>
        @else
          <li class="">
              <a href="/viewUsers?status=1" class="black" style="">
                  <i class="menu-icon fa fa-caret-right"></i>
                  <b style="font-size:15px">Partners</b>
              </a>
          </li>
          <li class="">
              <a href="/viewUsers?status=2" class="blue" style="">
                  <i class="menu-icon fa fa-caret-right"></i>
                  <b style="font-size:15px">Supervisors</b>
              </a>
          </li>
          <li class="">
              <a href="/viewUsers?status=3" class="green" style="">
                  <i class="menu-icon fa fa-caret-right"></i>
                  <b style="font-size:15px">Administrators</b>
              </a>
          </li>
          <li class="">
              <a href="/viewUsers?status=4" class="brown" style="">
                  <i class="menu-icon fa fa-caret-right"></i>
                  <b style="font-size:15px">Counsellors</b>
              </a>
          </li>
        @endif
    </ul>
    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state"
           data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>
</div>
