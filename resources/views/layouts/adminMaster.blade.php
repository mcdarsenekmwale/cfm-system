<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Comfort Liwambano" >
    <title>CFM YONECO</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
	<script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<link href="summernote.css" rel="stylesheet">
    <script src="summernote.min.js"></script>
    <link rel="stylesheet" href="morrisjs/morris.css">
	<script src="morrisjs/raphael.min.js"></script>
    <script src="morrisjs/morris.min.js"></script>
    <style>
        a:active {
            background-color: yellow;
        }
		body{
			background-color:#E6E6E6;
		}
		.dropdown a{
			color:#000;
		}
    </style>
</head>
<body style=" width:100%; margin:auto; border-right:solid thin #8AC007;" >
    <div class="row" style="height:130px; background-color:#337AB7">
		<img src="images/logo.jpg" style="height:150px; margin-left:10px">
        <span class=" col-md-offset-2" style="color:#fff; font-size:30px;" ><b><i>GBV Crisis Line Management system</i></b></span>
		<span class="dropdown pull-right" style="margin-right:50px;margin-top:50px;padding-top:10px;">
			<i class="fa  fa-th fa-2x dropdown-toggle " id="dropdownMenu1" data-toggle="dropdown"  style="color:#fff"></i>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="box-shadow:#F5F5F5; width:10px;">
				<li><a href="#" style="font-size:20px;color:#337AB7"><i class=""></i ><b>>user profile</b></a></li>
				<li><a href="logout" style="font-size:20px;color:#337AB7"><i class=""></i><b>>logout </b></a></li>
				 </ul>
	    </span>
	</div>
    <nav class="navbar  navbar-default" role="navigation" style="background:#fff;color:#000; height:42px;border:thin solid #fff;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            </div>
        <div class="">
			<span  class="" style="padding-top:10px" style="" >
						<form method="POST" action='/adminSearch'>
						{!! csrf_field() !!}
							<input type="text" name="word" placeholder="email,username,firstname,lastname" style="color:black" class="input-lg col-md-2 col-md-offset-8">
							<button class="btn btn-primary btn-lg"type="submit"><i class="fa fa-search"></i></button>
						</form>
			    </span>
        </div>
    </nav>
	<div style="background:;">
		@yield('leftSideBar')
		@yield('mainContent')
	<p class="row" > <div class="row" style="background:#337AB7;color:#fff; height:40px; text-align:center"><b><i>Yoneco-Ict department-{{date('Y')}}</i></b></div></p>
	</div>

	<script>
		$('#summernote').summernote({
		  height: 300,                 // set editor height
		  minHeight: null,             // set minimum height of editor
		  maxHeight: null,             // set maximum height of editor
		  focus: true                  // set focus to editable area after initializing summernote
		});
	</script>
</body>
</html>
