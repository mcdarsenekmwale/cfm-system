<div id="navbar" class="navbar navbar-default ace-save-state">
    <div class="navbar-container ace-save-state" id="navbar-container">
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data - target="#sidebar">
            <span class="sr-only"> Toggle sidebar </span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>
        </button>

        <div class="navbar-header pull-left">
            <a href="/home" class="navbar-brand">
                <small>
                    <i class="fa"></i>
                    <b> CFM system </b>
                </small>
            </a>
        </div>
        <div class="pull-left"><img src="images/logo.jpg" style="height:44px; margin:1px"/></div>
        <div class="navbar-buttons navbar-header pull-right" role="navigation">
            <ul class="nav ace-nav">
              @if(Auth::User()->type == "admin")
                <li>
                  <a href="/addUser">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add new user</span>
                  </a>
                </li>
              @elseif(Auth::User()->type == "common")
                <li>
                  <a href="/addCall">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Register new Call as user</span>
                  </a>
                </li>
              @elseif(Auth::User()->type == "partner")
                <li>
                  <a href="/addCase">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Register new Case</span>
                  </a>
                </li>
              @elseif(Auth::User()->type == "supervisor")
                <li>
                  <a href="/viewStats?stage=daily">
                    <span>View Stats</span>
                  </a>
                </li>
                <li class="grey dropdown-modal">
                    @if($totalOpenCases !== 0)
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#" style="background:red; color:#fff"
                           title="{{$totalOpenCases}}  UnReferred Cases.">
                            <i class="ace-icon fa fa-tasks icon-animated-vertical"></i>
                            <span class="badge badge-grey">{{$totalOpenCases}}</span>
                        </a>
                    @else
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#" style="background:green; color:white"
                           title="{{$totalOpenCases}} UnReferred Cases">
                            <i class="ace-icon fa fa-tasks icon-animated-vertical"></i>
                            <span class="badge badge-grey">{{$totalOpenCases}}</span>
                        </a>
                    @endif
                    <ul class="dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
                        @if($totalOpenCases !== 0)
                            <li class="dropdown-header" style="background:red; color:#fff">
                                <i class="ace-icon fa fa-tasks"></i>
                                {{$totalOpenCases}} Cases waiting referral
                            </li>
                        @else
                            <li class="dropdown-header" style="background:green;">
                                <i class="ace-icon fa fa-tasks"></i>
                                {{$totalOpenCases}} no Case waiting referral
                            </li>
                        @endif
                        <li class="dropdown-content">
                            <ul class="dropdown-menu dropdown-navbar">
                                @foreach($latestOpenCases  as $case)
                                    <li>
                                        <a href="#">
                                            <div class="clearfix">
                                                <span class="pull-left"> {{ substr(trim($case->comments) , 0, 35)}}
                                                    ... </span>
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>

                        <li class="dropdown-footer">
                            <a href="/viewCases/?status=1">
                                See cases with details
                                <i class="ace-icon fa fa-arrow-right"></i>
                            </a>
                        </li>
                    </ul>
                </li>
              @endif
              @if(Auth::User()->type != "admin" && Auth::User()->type != "common")

                <li class="blue dropdown-modal" @if(Auth::User()->type == "partner") title="Un resolved cases" @else title="reffered cases" @endif>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#" title="{{$totalClosedCases}} @if(Auth::User()->type == 'partner') Un resolved cases @else reffered cases @endif">
                        <i class="ace-icon fa fa-tasks icon-animated-vertical"></i>
                        <span class="badge badge-success" style="background:white; color:#000">{{$totalClosedCases}}</span>
                    </a>
                    <ul class="dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
                        @if($totalClosedCases != 0)
                            <li class="dropdown-header" style="background:red; color:#fff">
                                <i class="ace-icon fa fa-tasks"></i>
                                {{$totalClosedCases}} Cases waiting to be resolved
                            </li>
                        @else
                            <li class="dropdown-header" style="background:green;">
                                <i class="ace-icon fa fa-tasks"></i>
                                {{$totalClosedCases}} no Case not yet resolved
                            </li>
                        @endif
                        <li class="dropdown-content">
                            <ul class="dropdown-menu dropdown-navbar">
                                @foreach($latestClosedCases  as $case)
                                    <li>
                                        <a href="#">
                                            <div class="clearfix">
                                                <span class="pull-left"> {{ substr(trim($case->comments) , 0, 35)}}... </span>
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="dropdown-footer">
                            <a href="/viewCases/?status=2">
                                See cases with details
                                <i class="ace-icon fa fa-arrow-right"></i>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="green dropdown-modal" title="resolved cases">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"
                       title="{{$totalResolvedCases}} resolved cases.">
                        <i class="ace-icon fa fa-tasks icon-animated-vertical"></i>
                        <span class="badge badge-success"
                              style="background:white; color:#000"><b>{{$totalResolvedCases}}</b></span>
                    </a>

                    <ul class="dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">

                        @if($totalResolvedCases !== 0)
                            <li class="dropdown-header" style="background:red; color:#fff">
                                <i class="ace-icon fa fa-tasks"></i>
                                {{$totalResolvedCases}} Cases resolved
                            </li>
                        @else
                            <li class="dropdown-header" style="background:green;">
                                <i class="ace-icon fa fa-tasks"></i>
                                {{$totalResolvedCases}} no Case resolved
                            </li>
                        @endif
                        <li class="dropdown-content">
                            <ul class="dropdown-menu dropdown-navbar">
                                @foreach($latestResolvedCases  as $case)
                                    <li>
                                        <a href="#">
                                            <div class="clearfix">
                                                <span class="pull-left"> {{ substr(trim($case->comments) , 0, 35)}}... </span>
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="dropdown-footer">
                            <a href="/viewCases/?status=3">
                                See cases with details
                                <i class="ace-icon fa fa-arrow-right"></i>
                            </a>
                        </li>
                    </ul>
                </li>
              @endif
                <li class="light-blue dropdown-modal">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                        <img class="nav-user-photo" src="@yield('base')/assets/images/profile.png" alt="Jason's Photo"/>
                        <span class="user-info">
									<small>Welcome,</small>
									<small>{{Auth::user()->username}}</small>
								</span>

                        <i class="ace-icon fa fa-caret-down"></i>
                    </a>

                    <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                        <li class="divider"></li>
                        <li>
                            <a href="/logout">
                                <i class="ace-icon fa fa-power-off"></i>
                                Logout
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div><!-- /.navbar-container -->
</div>
