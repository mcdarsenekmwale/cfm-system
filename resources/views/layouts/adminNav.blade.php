<div id="navbar" class="navbar navbar-default ace-save-state">
  <div class="navbar-container ace-save-state" id="navbar-container">
    <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data - target="#sidebar">
      <span class="sr-only"> Toggle sidebar </span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <div class="navbar-header pull-left">
      <a href="/home" class="navbar-brand">
        <small><i class="fa"></i><b> CFM system </b></small>
      </a>
    </div>
    <div class="pull-left"><img src="images/logo.jpg" style="height:44px; margin:1px"/></div>
    <div class="navbar-buttons navbar-header pull-right" role="navigation">
      <ul class="nav ace-nav">
        <li>
          <a href="/addUser">
            <i class="glyphicon glyphicon-plus"></i>
            <span>Add new user</span>
          </a>
        </li>
        <li class="light-blue dropdown-modal">
          <a data-toggle="dropdown" href="#" class="dropdown-toggle">
            <img class="nav-user-photo" src="@yield('base')/assets/images/profile.png" alt="Jason's Photo"/>
            <span class="user-info">
              <small>Welcome,</small>
              <small>{{Auth::user()->username}}</small>
            </span>

            <i class="ace-icon fa fa-caret-down"></i>
          </a>

          <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
            <li class="divider"></li>
            <li>
              <a href="/logout">
                <i class="ace-icon fa fa-power-off"></i>
                Logout
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </div><!-- /.navbar-container -->
</div>
