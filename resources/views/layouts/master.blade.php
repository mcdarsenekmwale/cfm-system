<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
  <meta charset="utf-8"/>
  <title>CFM YONECO</title>

  <meta name="description" content="overview &amp; stats"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

  <!-- bootstrap & fontawesome -->
  <link rel="stylesheet" href="@yield('base')/assets/css/bootstrap.css"/>
  <link rel="stylesheet" href="@yield('base')/assets/font-awesome/4.5.0/css/font-awesome.min.css"/>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <!-- page specific plugin styles -->

  <!-- text fonts -->
  <link rel="stylesheet" href="@yield('base')/assets/css/fonts.googleapis.com.css"/>

  <!-- ace styles -->
  <link rel="stylesheet" href="@yield('base')/assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style"/>


  <link rel="stylesheet" href="@yield('base')/assets/css/ace-skins.min.css"/>
  <link rel="stylesheet" href="@yield('base')/assets/css/ace-rtl.min.css"/>

  <script src="@yield('base')/assets/js/ace-extra.min.js"></script>


</head>

<body class="no-skin" style="font-family:Open Sans">
  @include('layouts.navBar')

  <div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
    try {
      ace.settings.loadState('main-container')
    } catch (e) {
    }
    </script>

    @include('layouts.sideBar')
    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-home home-icon"></i>
              <a href="/home">Home</a>
            </li>
            <li class="active">@yield('activeB')</li>
          </ul><!-- /.breadcrumb -->
          <div class="nav-search" id="nav-search">
            <!-- search the form section-->
            <form class="form-inline form-search" style=" position:relative; right:-1em;" method="POST"
            action="/search">
            {!! csrf_field() !!}
            <div class="input-group input-group-sm">
              <input type="text" name="searchWord" placeholder="search ..." style="color:black"
              class="input-md form-control input-sm nav-search-input" id="nav-search-input">
              <span class="input-group-btn">
                <button class="btn btn-default btn-sm" type="submit"
                type="button"><span
                class="fa fa-search"></span></button>
              </span>
            </div><!-- /input-group -->
          </form>
          <div class="col-lg-6">
          </div><!-- /.col-lg-6 -->
        </div><!-- /.nav-search -->
      </div>
      @yield('mainContent')
    </div><!-- /.main-container -->
  </div>
</div>
@include('layouts.footer')

</body>
</html>
