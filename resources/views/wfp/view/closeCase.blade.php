@extends('layout.master')
@section('username')
	
@stop
@section('leftSideBar')
	<div class="col-md-2">
    </div>
@stop
@section('mainContent')
	<div class="col-md-10 list-group-item"  >
	    @if($case[0]->status == 'closed')
			<p class="list-group-item" style="background:#286090; color:#fff"><b>OPENING A CASE</b><span class="badge">CASE NUMBER {{$case[0]->code}}</span></p>
		@else
			<p class="list-group-item" style="background:#286090; color:#fff">CLOSING A CASE <span class="badge">CASE NUMBER  {{$case[0]->code}}</span></p>
		@endif
	    <form action="{{Route('closeOpen')}}" method="post">
		    <p class="">
				<span class="col-md-12"><b>COMMENTS</b></span> <span class="col-md-12" >{{$case[0]->comments}}</span>
			</p>
			<br><br>
			{!! csrf_field() !!}
			<input value="{{$case[0]->code}}" name="code" type="hidden">
			<input value="{{$case[0]->status}}" name="status" type="hidden">
			<div class="col-md-12">
				<label >CONCLUSION</label>
				<textarea id="" name="conclusion" class="col-md-12"></textarea>
			</div>
			<div class="col-md-12">
				<input type="submit" class="btn btn-md btn-success" value="submit">
			</div>
		</form>
    </div>
@stop
