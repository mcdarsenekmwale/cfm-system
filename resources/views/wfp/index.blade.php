<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <title>cfm yoneco</title>

    <meta name="description" content="overview &amp; stats"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="assets/css/bootstrap.css"/>
    <link rel="stylesheet" href="assets/font-awesome/4.5.0/css/font-awesome.min.css"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- page specific plugin styles -->

    <!-- text fonts -->
    <link rel="stylesheet" href="assets/css/fonts.googleapis.com.css"/>

    <!-- ace styles -->
    <link rel="stylesheet" href="assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style"/>

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="assets/css/ace-part2.min.css" class="ace-main-stylesheet"/>
    <![endif]-->
    <link rel="stylesheet" href="assets/css/ace-skins.min.css"/>
    <link rel="stylesheet" href="assets/css/ace-rtl.min.css"/>

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="assets/css/ace-ie.min.css"/>
    <![endif]-->

    <!-- inline styles related to this page -->

    <!-- ace settings handler -->
    <script src="assets/js/ace-extra.min.js"></script>

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if lte IE 8]>
    <script src="assets/js/html5shiv.min.js"></script>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="no-skin" style="font-family:Open Sans">
    <div id="navbar" class="navbar navbar-default          ace-save-state">
        <div class="navbar-container ace-save-state" id="navbar-container">
            <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
                <span class="sr-only">Toggle sidebar</span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>
            </button>

            <div class="navbar-header pull-left">
                <a href="supervisor.home" class="navbar-brand">
                    <small>
                        <i class="fa"></i>
                        <b>CFM system</b>
                    </small>
                </a>
            </div>
            <div class="pull-left"><img src="images/logo.jpg" style="height:44px; margin:1px"/></div>
            <div class="pull-left"><img src="images/wfp.jpg" style="height:44px; margin:1px"/></div>
            <div class="navbar-buttons navbar-header pull-right" role="navigation">
                <ul class="nav ace-nav">
                    <li class="grey dropdown-modal">
                        @if($totalOpenCases != 0)
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" style="background:red; color:#fff"
                               title="{{$totalOpenCases}} .">
                                <i class="ace-icon fa fa-tasks icon-animated-vertical"></i>
                                <span class="badge badge-grey">{{$totalOpenCases}}</span>
                            </a>
                        @else
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" style="background:green; color:white"
                               title="{{$totalOpenCases}} .">
                                <i class="ace-icon fa fa-tasks icon-animated-vertical"></i>
                                <span class="badge badge-grey">{{$totalOpenCases}}</span>
                            </a>
                        @endif
                        <ul class="dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">

                            @if($totalOpenCases != 0)
                                <li class="dropdown-header" style="background:red; color:#fff">
                                    <i class="ace-icon fa fa-tasks"></i>
                                    {{$totalOpenCases}} Cases waiting referral
                                </li>
                            @else
                                <li class="dropdown-header" style="background:green;">
                                    <i class="ace-icon fa fa-tasks"></i>
                                    {{$totalOpenCases}} no Case waiting referral
                                </li>
                            @endif
                            <li class="dropdown-content">
                                <ul class="dropdown-menu dropdown-navbar">
                                    @foreach($latestOpenCases  as $case)
                                        <li>
                                            <a href="#">
                                                <div class="clearfix">
                                                    <span class="pull-left"> {{ substr(trim($case->comments) , 0, 35)}}
                                                        ... </span>
                                                </div>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>

                            <li class="dropdown-footer">
                                <a href="supervisor.cases.unreferred">
                                    See cases with details
                                    <i class="ace-icon fa fa-arrow-right"></i>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="blue dropdown-modal" title="reffered cases">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"
                           title="{{$totalClosedCases}} referred cases.">
                            <i class="ace-icon fa fa-tasks icon-animated-vertical"></i>
                            <span class="badge badge-success"
                                  style="background:white; color:#000">{{$totalClosedCases}}</span>
                        </a>

                        <ul class="dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
                            <li class="dropdown-header">
                                <i class="ace-icon fa fa-tasks"></i>
                                {{$totalClosedCases}} referred cases.
                            </li>
                        </ul>
                    </li>
                    <li class="green dropdown-modal" title="resolved cases">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"
                           title="{{$totalResolvedCases}} resolved cases.">
                            <i class="ace-icon fa fa-tasks icon-animated-vertical"></i>
                            <span class="badge badge-success"
                                  style="background:white; color:#000"><b>{{$totalResolvedCases}}</b></span>
                        </a>

                        <ul class="dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
                            <li class="dropdown-header">
                                <i class="ace-icon fa fa-tasks"></i>
                                {{$totalResolvedCases}} referred cases.
                            </li>
                        </ul>
                    </li>
                    <li class="light-blue dropdown-modal">
                        <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                            <img class="nav-user-photo" src="assets/images/profile.png" alt="Jason's Photo"/>
                            <span class="user-info">
    									<small>Welcome,</small>
    									<small>{{Auth::user()->username}}</small>
    								</span>

                            <i class="ace-icon fa fa-caret-down"></i>
                        </a>

                        <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                            <li>
                                <a href="#">
                                    <i class="ace-icon fa fa-cog"></i>
                                    Settings
                                </a>
                            </li>

                            <li>
                                <a href="profile.html">
                                    <i class="ace-icon fa fa-user"></i>
                                    Profile
                                </a>
                            </li>

                            <li class="divider"></li>

                            <li>
                                <a href="logout">
                                    <i class="ace-icon fa fa-power-off"></i>
                                    Logout
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div><!-- /.navbar-container -->
    </div>

    <div class="main-container ace-save-state" id="main-container">
        <script type="text/javascript">
            try {
                ace.settings.loadState('main-container')
            } catch (e) {
            }
        </script>

        <div id="sidebar" class="sidebar                  responsive                    ace-save-state">
            <script type="text/javascript">
                try {
                    ace.settings.loadState('sidebar')
                } catch (e) {
                }
            </script>

            <div class="sidebar-shortcuts" id="sidebar-shortcuts">
                <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
                    <button class="btn btn-success">
                        <i class="ace-icon fa fa-signal"></i>
                    </button>

                    <button class="btn btn-info">
                        <i class="ace-icon fa fa-pencil"></i>
                    </button>

                    <button class="btn btn-warning">
                        <i class="ace-icon fa fa-users"></i>
                    </button>

                    <button class="btn btn-danger">
                        <i class="ace-icon fa fa-cogs"></i>
                    </button>
                </div>

                <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
                    <span class="btn btn-success"></span>

                    <span class="btn btn-info"></span>

                    <span class="btn btn-warning"></span>

                    <span class="btn btn-danger"></span>
                </div>
            </div><!-- /.sidebar-shortcuts -->

            <ul class="nav nav-list">
                <li class="active">
                    <a href="supervisor.home">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text"> Dashboard </span>
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="">
                    <a href="supervisor.cases.unreferred" class="black">
                        <i class="menu-icon fa fa-caret-right"></i>
                        @if(Auth::User()->firstname == "wfp")
                            <b fstyle="font-size:15px"> WFP Cases</b>
                        @else
                            <b style="font-size:15px"> Un Referred</b>
                        @endif
                        <b class="arrow"></b>
                    </a>

                    <b class="arrow"></b>

                    <ul class="submenu">

                    </ul>
                </li>

                <li class="">
                    <a href="supervisor.cases.referred" class="blue" style="">
                        <i class="menu-icon fa fa-caret-right"></i>
                        <b style="font-size:15px"> Reffered</b>
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="supervisor.cases.resolved" class="green" style="">
                        <i class="menu-icon fa fa-caret-right"></i>
                        <b style="font-size:15px"> Resolved</b>
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
            <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
                <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state"
                   data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
            </div>
        </div>

        <div class="main-content">
            <div class="main-content-inner">
                <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="ace-icon fa fa-home home-icon"></i>
                            <a href="supervisor.home">Home</a>
                        </li>
                        <li class="active">Dashboard</li>
                    </ul><!-- /.breadcrumb -->

                    <div class="nav-search" id="nav-search">

                        <!-- search the form section-->
                        <form  class="form-inline form-search" style=" position:relative; right:-1em;" method="POST" action="supervisor.search">
                            {!! csrf_field() !!}
                            <div class="input-group input-group-sm">
                                <input type="text" name="searchWord" placeholder="search ..." style="color:black" class="input-md form-control input-sm nav-search-input" id="nav-search-input">
                                <span class="input-group-btn">
                                                            <button class="btn btn-default btn-sm" type="submit" type="button"><span class="fa fa-search"></span></button>
                                                          </span>
                            </div><!-- /input-group -->
                        </form>
                        <div class="col-lg-6">

                        </div><!-- /.col-lg-6 -->


                    </div><!-- /.nav-search -->
                </div>

                <div class="page-content">
                    <div class="ace-settings-container" id="ace-settings-container">
                        <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                            <i class="ace-icon fa fa-cog bigger-130"></i>
                        </div>

                        <div class="ace-settings-box clearfix" id="ace-settings-box">
                            <div class="pull-left width-50">
                            </div><!-- /.pull-left -->

                            <div class="pull-left width-50">

                            </div><!-- /.pull-left -->
                        </div><!-- /.ace-settings-box -->
                    </div><!-- /.ace-settings-container -->

                    <div class="page-header">
                        <h1>
                            Dashboard
                            <small>
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                overview &amp; stats
                            </small>
                        </h1>
                    </div><!-- /.page-header -->
                    @if(Session::has('partner'))
                        <div class="alert alert-warning alert-success"
                             style="color:#fff; background:#1C733F; text-align:center">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" style="color:#fff">&times;</a><b>The
                                case has been referred to {{session('partner')}}.</b>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-12">

                            <div class="row">

                                <div class="col-lg-8">
                          
                                    <div class="panel panel-primary table-responsive">
                                        <table class="table table-hover table-striped">
                                            <thead class="thin-border-bottom panel-header">
                                            <col>
                                            <colgroup span="3"></colgroup>
                                            <colgroup span="2"></colgroup>
                                            <tr>
                                                <th rowspan="2" style="font-size:16px; text-align: center">PARTNERS</th>
                                                <th colspan="3" scope="colgroup" style="font-size:18px; text-align: center">STATUS</th>
                                            </tr>
                                            <tr>
                                                <th style="font-size:16px">
                                                    <b class="red">UNREFERRED</b>
                                                </th>

                                                <th style="font-size:16px">
                                                    <b class="blue">REFERRED</b>
                                                </th>

                                                <th style="font-size:186x">
                                                    <b class="green">RESOLVED</b>
                                                </th>
                                            </tr>

                                            </thead>

                                            <tbody>
                                            @foreach($issues as $issue)
                                                <tr>
                                                    <td>
                                                        {{$issue->partner}}
                                                    </td>
                                                    <td>
                                                        {{$issue->unref}}
                                                    </td>
                                                    <td>
                                                        {{$issue->ref}}
                                                    </td>
                                                    <td>
                                                        {{$issue->res}}
                                                    </td>
                                                </tr>

                                            @endforeach

                                            </tbody>
                                        </table>

                                    </div>



                                </div>

                                <div class="vspace-12-sm"></div>

                                <div class="col-sm-4">
                                    <div class="widget-box panel panel-primary">
                                        <!--Draw a pie chart-->
                                        <div class="widget-header panel-header widget-header-flat widget-header-small">
                                            <h5 class="widget-title panel-title">
                                                <i class="ace-icon fa fa-signal"></i>
                                                Main Issues
                                            </h5>

                                            <div class="widget-toolbar no-border">
                                                <div class="inline dropdown-hover">
                                                    <button class="btn btn-minier btn-primary">
                                                        This Week
                                                        <i class="ace-icon fa fa-angle-down icon-on-right bigger-110"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right dropdown-125 dropdown-lighter dropdown-close dropdown-caret">
                                                        <li class="active">
                                                            <a href="#" class="blue">
                                                                <i class="ace-icon fa fa-caret-right bigger-110">&nbsp;</i>
                                                                This Week
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="ace-icon fa fa-caret-right bigger-110 invisible">&nbsp;</i>
                                                                Last Week
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="ace-icon fa fa-caret-right bigger-110 invisible">&nbsp;</i>
                                                                This Month
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="ace-icon fa fa-caret-right bigger-110 invisible">&nbsp;</i>
                                                                Last Month
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="widget-body panel-body">
                                            <div class="widget-main">
                                                <div id="piechart-placeholder" style="height:180px"></div>



                                            </div><!-- /.widget-main -->
                                        </div><!-- /.widget-body -->
                                        <div class="panel-footer" >
                                            <!-- displays the pie chart hints-->
                                            <span id="legend1"></span>
                                        </div>

                                    </div><!-- /.widget-box -->

                                </div><!-- /.col -->

                            </div><!-- /.row -->

                            <div class="hr hr32 hr-dotted"></div>

                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="widget-box transparent">
                                        <div class="widget-header widget-header-flat">
                                            <h4 class="widget-title lighter">
                                                <i class="ace-icon fa fa-star orange"></i>
                                                Popular Cases
                                            </h4>

                                            <div class="widget-toolbar">
                                                <a href="#" data-action="collapse">
                                                    <i class="ace-icon fa fa-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="widget-body">
                                            <div class="widget-main no-padding">
                                                <table class="table table-bordered table-striped">
                                                    <thead class="thin-border-bottom">
                                                    <tr>
                                                        <th>
                                                            <i class="ace-icon fa fa-caret-right blue"></i>Issue
                                                        </th>

                                                        <th>
                                                            <i class="ace-icon fa fa-caret-right blue"></i>Total
                                                        </th>
                                                    </tr>
                                                    </thead>

                                                    <tbody>
                                                    @foreach($partnerIssues as $case)
                                                        <tr>
                                                            <td>{{$case->label}}</td>

                                                            <td>

                                                                <b class="green">{{$case->value}}</b>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div><!-- /.widget-main -->
                                        </div><!-- /.widget-body -->
                                    </div><!-- /.widget-box -->
                                </div><!-- /.col -->

                                <div class="col-sm-7">
                                    <div class="widget-box transparent" id="recent-box">
                                        <div class="widget-header">
                                            <h4 class="widget-title lighter smaller">
                                                <i class="ace-icon fa fa-list orange"></i>RECENT CASES
                                            </h4>

                                            <div class="widget-toolbar no-border">
                                                <ul class="nav nav-tabs" id="recent-tab">
                                                    <li class="active">

                                                        <!-- restricting wfp members from seeing the unreffered cases -->
                                                        @if(Auth::User()->firstname !="wfp")
                                                            <a data-toggle="tab" href="#task-tab"
                                                               style="font-size:18px; font-family:times new romans"><i
                                                                        class="ace-icon fa fa-list red"></i> Un referred
                                                                <span class="badge"
                                                                      style="background:red"><b>{{$totalOpenCases}}</b></span></a>
                                                        @else
                                                            <a data-toggle="tab" href="#task-tab"
                                                               style="font-size:18px; font-family:times new romans"
                                                               title="These are cases for WFP to resolve"><i
                                                                        class="ace-icon fa fa-list red"></i> WFP Cases <span
                                                                        class="badge"
                                                                        style="background:red"><b>{{$totalOpenCases}}</b></span></a>
                                                        @endif
                                                    </li>

                                                    <li>
                                                        <a data-toggle="tab" href="#member-tab"
                                                           style="font-size:18px; font-family:times new romans"
                                                           title="These are cases referred to partners."><i
                                                                    class="ace-icon fa fa-list blue"></i> Referred <span
                                                                    class="badge badge-grey"
                                                                    style="background:blue"><b>{{$totalClosedCases}}</b></span></a>
                                                    </li>
                                                    <li>
                                                        <a data-toggle="tab" href="#resolved-tab"
                                                           style="font-size:18px; font-family:times new romans"
                                                           title="These are cases resolved  by  partners."><i
                                                                    class="ace-icon fa fa-list green"></i>Resolved <span
                                                                    class="badge badge-grey"
                                                                    style="background:green"><b>{{$totalResolvedCases}}</b></span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="widget-body">
                                            <div class="widget-main padding-4">
                                                <div class="tab-content padding-8">
                                                    <div id="task-tab" class="tab-pane active">

                                                        <!-- restricting wfp members from seeing the unreffered cases -->
                                                        @if(Auth::User()->firstname !="wfp")
                                                            <h4 class="smaller lighter green">
                                                                <i class="ace-icon fa fa-list"></i>
                                                                Un referred
                                                            </h4>
                                                        @else
                                                            <h4 class="smaller lighter green">
                                                                <i class="ace-icon fa fa-list"></i>
                                                                Our Cases
                                                            </h4>

                                                        @endif

                                                        <ul id="tasks" class="item-list">
                                                            @foreach($latestOpenCases as $latestOpenCase)
                                                                <li class="item-orange clearfix">
                                                                    <label class="inline">
                                                                        <span class="lbl"
                                                                              style="font-size:18px; padding:2px; font-family:times new romans">{{ substr(trim($latestOpenCase->comments) , 0, 105)}} </span>
                                                                    </label>

                                                                    <div class="pull-right action-buttons">
                                                                        <form method="post" action="super.case.close">

                                                                            <input type="hidden" name="code"
                                                                                   value="{{ $latestOpenCase->code }}"/>
                                                                            <button type="submit"
                                                                                    class="ace-icon fa fa-pencil bigger-130 blue"/>
                                                                            {!! csrf_field() !!}
                                                                        </form>
                                                                    </div>
                                                                </li>
                                                            @endforeach
                                                        </ul>

                                                    </div>

                                                    <div id="member-tab" class="tab-pane">
                                                        <h4 class="smaller lighter green"
                                                            style="font-size:18px; padding:2px; font-family:times new romans">
                                                            <i class="ace-icon fa fa-list"></i>
                                                            Referred
                                                        </h4>

                                                        <ul id="tasks" class="item-list">
                                                            @foreach($latestClosedCases as $latestClosedCase)
                                                                <li class="item-orange clearfix">
                                                                    <label class="inline">
                                                                        <span class="lbl"
                                                                              style="font-size:18px; padding:2px; font-family:times new romans">{{ substr(trim($latestClosedCase->comments) , 0, 100)}}</span>
                                                                    </label>

                                                                    <div class="pull-right action-buttons">
                                                                        <form method="post" action="super.case.close">

                                                                            <input type="hidden" name="code"
                                                                                   value="{{ $latestClosedCase->code }}"/>
                                                                            <button type="submit"
                                                                                    class="ace-icon fa fa-pencil bigger-130 blue"/>
                                                                            {!! csrf_field() !!}
                                                                        </form>
                                                                    </div>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                    <div id="resolved-tab" class="tab-pane">
                                                        <h4 class="smaller lighter green"
                                                            style="font-size:18px; padding:2px; font-family:times new romans">
                                                            <i class="ace-icon fa fa-list"></i>
                                                            Resolved
                                                        </h4>

                                                        <ul id="tasks" class="item-list">
                                                            @foreach($ResolvedCases as $ResolvedCase)
                                                                <li class="item-orange clearfix">
                                                                    <label class="inline">
                                                                        <span class="lbl"
                                                                              style="font-size:18px; padding:2px; font-family:times new romans">{{ substr(trim($ResolvedCase->comments) , 0, 100)}}</span>
                                                                    </label>

                                                                    <div class="pull-right action-buttons">
                                                                        <form method="post" action="super.case.close">

                                                                            <input type="hidden" name="code"
                                                                                   value="{{ $ResolvedCase->code }}"/>
                                                                            <button type="submit"
                                                                                    class="ace-icon fa fa-pencil bigger-130 blue"/>
                                                                            {!! csrf_field() !!}
                                                                        </form>
                                                                    </div>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div><!-- /.widget-main -->
                                        </div><!-- /.widget-body -->
                                    </div><!-- /.widget-box -->
                                </div><!-- /.col -->
                            </div><!-- /.row -->

                            <div class="hr hr32 hr-dotted"></div>

                            <div class="row">

                            </div><!-- /.row -->
                        </div><!-- /.page-content -->
                    </div>
                </div><!-- /.main-content -->

                <div class="footer">
                    <div class="footer-inner">
                        <div class="footer-content">
    						<span class="bigger-120">
    							<span class="blue bolder">Yoneco ICT</span>
    					           &copy; 2018
    						</span>

                            &nbsp; &nbsp;
                            <span class="action-buttons">
    							<a href="#">
    								<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
    							</a>

    							<a href="#">
    								<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
    							</a>

    							<a href="#">
    								<i class="ace-icon fa fa-rss-square orange bigger-150"></i>
    							</a>
    						</span>
                        </div>
                    </div>
                </div>

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->

            <!-- basic scripts -->

            <!--[if !IE]> -->
            <script src="assets/js/jquery-2.1.4.min.js"></script>

            <!-- <![endif]-->

            <!--[if IE]>
            <script src="assets/js/jquery-1.11.3.min.js"></script>
            <![endif]-->
            <script type="text/javascript">
                if ('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
            </script>
            <script src="assets/js/bootstrap.min.js"></script>

            <!-- page specific plugin scripts -->

            <!--[if lte IE 8]>
            <script src="assets/js/excanvas.min.js"></script>
            <![endif]-->
            <script src="assets/js/jquery-ui.custom.min.js"></script>
            <script src="assets/js/jquery.ui.touch-punch.min.js"></script>
            <script src="assets/js/jquery.easypiechart.min.js"></script>
            <script src="assets/js/jquery.sparkline.index.min.js"></script>
            <script src="assets/js/jquery.flot.min.js"></script>
            <script src="assets/js/jquery.flot.pie.min.js"></script>
            <script src="assets/js/jquery.flot.resize.min.js"></script>

            <!-- ace scripts -->
            <script src="assets/js/ace-elements.min.js"></script>
            <script src="assets/js/ace.min.js"></script>

            <script src="morrisjs/raphael.min.js"></script>
            <script src="morrisjs/morris.min.js"></script>

            <!-- inline scripts related to this page -->
            <script type="text/javascript">


                var districts = new Array();
                var total = new Array();
                var districts_ = new Array();
                var color = ['#673ab7', '#ff51b5', '#0d47a1', '#804d40', '#d0e0e0', '#1b5e20', '#e65100', '#000000', '#bf360c', '#757575', ' #ffc107', '#82ed17', '#185e20', '#64dd17',
                    '#1b5e20', '#69f0ae', ' #ff9800', '#795548', '#000000', ' #607d8b', '#3e2723', '#bf360c', '#00c853', '#827717', '#0091ea', '#1a237e', '#c51162', '#880e4f', '#dd2c00', '#5c6bc0'];

                //////
                $.ajax({
                    type: "GET",
                    url: "super.issues.dash",
                    dataType: 'json',
                })
                    .done(function (data) {
                        X = 0
                        data.forEach(function (obj) {
                                //total[X]=parseInt(obj.total);
                                obj.value = parseInt(obj.value);
                                obj.color = color[X];
                                districts_[X] = obj;
                                X++;
                            }
                        );
                        var dg = Morris.Donut({
                            element: 'piechart-placeholder',
                            data: districts_
                        });
                        ////

                        //$('#legend1').show();
                        dg.options.data.forEach(function (label, i) {
                            var legendItem = $('<span></span>').text(label['label'] + " (" + label['value'] + ")").prepend('<br><span>&nbsp;</span>');
                            legendItem.find('span')
                                .css('backgroundColor', label['color'])
                                .css('width', '20px')
                                .css('display', 'inline-block')
                                .css('margin', '5px');
                            $('#legend1').append(legendItem);
                        });

                        //Android's default browser somehow is confused when tapping on label which will lead to dragging the task
                        //so disable dragging when clicking on label
                        var agent = navigator.userAgent.toLowerCase();
                        if (ace.vars['touch'] && ace.vars['android']) {
                            $('#tasks').on('touchstart', function (e) {
                                var li = $(e.target).closest('#tasks li');
                                if (li.length == 0)return;
                                var label = li.find('label.inline').get(0);
                                if (label == e.target || $.contains(label, e.target)) e.stopImmediatePropagation();
                            });
                        }

                        $('#tasks').sortable({
                                opacity: 0.8,
                                revert: true,
                                forceHelperSize: true,
                                placeholder: 'draggable-placeholder',
                                forcePlaceholderSize: true,
                                tolerance: 'pointer',
                                stop: function (event, ui) {
                                    //just for Chrome!!!! so that dropdowns on items don't appear below other items after being moved
                                    $(ui.item).css('z-index', 'auto');
                                }
                            }
                        );
                        $('#tasks').disableSelection();
                        $('#tasks input:checkbox').removeAttr('checked').on('click', function () {
                            if (this.checked) $(this).closest('li').addClass('selected');
                            else $(this).closest('li').removeClass('selected');
                        });


                        //show the dropdowns on top or bottom depending on window height and menu position
                        $('#task-tab .dropdown-hover').on('mouseenter', function (e) {
                            var offset = $(this).offset();

                            var $w = $(window)
                            if (offset.top > $w.scrollTop() + $w.innerHeight() - 100)
                                $(this).addClass('dropup');
                            else $(this).removeClass('dropup');
                        });

                    })
            </script>
</body>
</html>
