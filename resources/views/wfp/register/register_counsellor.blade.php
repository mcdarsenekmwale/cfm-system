@extends('layout.master')
@section('username')

@stop
@section('leftSideBar')
    <div class="">
        <div class="col-md-2">
            <div class="list-group">
                <a href="{{route('cases.open')}}" class="list-group-item"><b>OPEN CASES</b></a>
                <a href="{{route('cases.closed')}}" class="list-group-item"><b>CLOSED CASES</b></a>
            </div>
        </div>
    </div>
@stop
@section('mainContent')
    <div class="col-md-7">
	@if(Session::get('register'))
	<div class="alert alert-success alert-dismissable">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{Session::get('register')}}
	</div>
    @elseif(Session::get('register-error'))
	<div class="alert alert-danger alert-dismissable" style="color:#000">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<b>{{Session::get('register-error')}}</b>
	</div>
   @endif
	<div class="list-group-item" style="background:#3C5173; color:#fff"><b>Please fill  the form below to register a counsellor</b></div>
    <div class="list-group-item">
        <form method="Post" action="{{Route('counsellor.register')}}">
		{!! csrf_field() !!}
		    <div class="form-group">
				<label for="title" class="col-md-2">Title</label>
				<select>
					<option value="mr">Mr</option>
					<option value="mrs">Mrs</option>
				</select>
			</div>
		    <div class="form-group">
				<label for="firstname" class="col-md-2">Firstname</label>
				<input type="text" name="firstname">
			</div>
			<div class="form-group">
				<label for="lastname" class="col-md-2">Lastname</label>
				<input type="text" name="lastname">
			</div>
		    <div class="form-group">
				<label for="username" class="col-md-2">Username</label>
				<input type="text" name="username">
			</div>
			<div class="form-group">
				<label for="password" class="col-md-2">Password</label>
				<input type="password" name="password">
			</div>
			<div class="form-group">
				<label for="email" class="col-md-2">Email</label>
				<input type="text" name="email" class="input-control">
			</div>
			<div class="form-group">
				<label for="type" class="col-md-2">Type</label>
				<select>
					<option value="common">Common</option>
					<option value="supervisor">Supervisor</option>
				</select>
			</div>
			<div class="form-group">
			    <label class="col-md-1">State</label>
			    <label for="state">Inactive</label>
				<input type="radio" name="state" value="passive">
				<label for="state">Active</label>
				<input type="radio" name="state" value="active">
			</div>
			<button class="btn btn-success btn-md">Register</button>
		</form>
    </div> 
@stop
