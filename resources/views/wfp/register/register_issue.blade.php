@extends('layout.master')
@section('username')

@stop
@section('leftSideBar')
    <div class="">
        <div class="col-md-2 well ">
            <div class="list-group">
                <a href="#" class="list-group-item"><b>View All Issues</b></a>
            </div>
        </div>
    </div>
@stop
@section('mainContent')
    <div class="col-md-10 ">
	@if(Session::get('registration_success'))
	<div class="alert alert-success alert-dismissable">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{Session::get('registration_success')}}
	</div>
    @elseif(Session::get('registration_error'))
	<div class="alert alert-danger alert-dismissable" style="color:#000">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<b>{{Session::get('registration_error')}}</b>
	</div>
   @endif
	<div class="well" style="background:#337AB7;"><b style="color:#fff">Please fill  the form below to add referral</b></div>
    <div class="well">
        <form method="Post" action="{{Route('issues.register')}}">
		{!! csrf_field() !!}
			<div class="form-group">
				<label for="issue" class="col-md-2">Name of issue</label>
				<input type="text" name="name">
			</div>
			<div class="form-group">
				<label for="issue" class="col-md-2">Description of issue</label>
				<textarea class="col-md-10" type="text" name="description">
				</textarea>
			</div>
			<div class="form-group">
				<input type="submit" class="btn btn-success btn-md" value="Save">
			</div>
		</form>
    </div> 
@stop
