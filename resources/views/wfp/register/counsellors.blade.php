@extends('layout.master')
@section('username')
	
@stop
@section('leftSideBar')
<div class="">
    <div class="col-md-2 well">
        <div class="list-group">
            <a href="{{route('calls.complete')}}" class="list-group-item"><b>Complete Calls</b></a>
            <a href="{{route('calls.incomplete')}}" class="list-group-item"><b>Incomplete Calls</b></a>
        </div>
    </div>
</div>
@stop
@section('mainContent')
@if(!$callDetails->isEmpty())
	<div class="col-md-10">
	    <div class="row" >
			<div class="col-md-12">
			    <div class="panel well" style="color:#000">
				<p class="list-group-item" style="background:#5AAD35">PHONE CALL DETAILS</p>
				<div class="panel-body">
                    <div class="col-md-3">
					   <p>
							<span class="col-md-6">
								<b>CLIENT NAME:</b>
							</span>
							<span class="badge">
						          {{strtoupper($callDetails[0]['name'])}}
							</span>
						</p>
						<p>
							<span class="col-md-6">
								<b>DATE OF CALL:</b>
							</span>
							<span class="col-md-6">
								{{$callDetails[0]['date']}}
							</span>
						</p>
						<p>
							<span class="col-md-6">
								<b>DISTRICT:</b>
							</span>
							<span class="col-md-6">
								{{$callDetails[0]['district']}}
							</span>
						</p>
						<span class="col-md-6">
							<b>AGE:</b>
						</span>
						<span class="col-md-6">
						   <span class="badge">
							 {{$callDetails[0]['age']}}
							</span>
						</span>
					</div>
                    <div class="col-md-3">
						<span class="col-md-6">
							START TIME:
						</span>
						<span class="col-md-6">
							{{$callDetails[0]['starttime']}}
						</span>
						<span class="col-md-5">
							PHONE:
						</span>
						<span class="col-md-6">
							{{$callDetails[0]['date']}}
						</span>
						<span class="col-md-6">
								NATURE :
						</span>
						<span class="col-md-6">
							{{$callDetails[0]['nature']}}
						</span>
					</div>
                    <div class="col-md-3">
						<span class="col-md-6">
							END TIME:
						</span>
						<span class="col-md-6">
							{{$callDetails[0]['endtime']}}
						</span>
						<span class="col-md-6">
							GENDER:
						</span>
						<span class="col-md-6">
							{{$callDetails[0]['gender']}}
						</span>
					</div><!--
                    <div class="col-md-3">
						<span class="col-md-3">
							DURATION:
						</span>
						<span class="col-md-9">
							{{$callDetails[0]['endtime'] - $callDetails[0]['starttime'] }}
						</span>
					</div> -->
					<p>
						<div class="col-md-12">
							   <div class="col-md-6">
									<div class="col-md-12" style="background:#E0E0E0"><b>ISSUE</b></div>
									<p class="col-md-12"> {{$callDetails[0]['issue']}}<p>
							   </div>
							   <div class="col-md-6">
									<div class="col-md-12" style="background:#E0E0E0"><b>SUBISSUE</b></div>
									<p class="col-md-6"> {{$callDetails[0]['subissue']}}<p>
							   </div>
						</div>
						</p>
					<p>
				    <div class="col-md-12">
                        <p class="col-md-12"><span>INTERVENTION: </span><span>{{$callDetails[0]['intervention']}}</span></p>
					</div>
					</p>
					<p>
				    <div class="col-md-12">
                        <p class="col-md-12"><span>REFFERAL: </span></p>
					</div>
				    </p>
				    <div class="col-md-12">
                        <p class="col-md-12"><span>COMMENTS: </span><span>{{$callDetails[0]['comments']}}</span></p>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
@else
    <div class="col-lg-10">
        <p class="alert alert-warning" style="text-align:center"><i class="fa fa-info-circle"></i>  Select  Call has insufficient details</p>
	</div>
@endif
@stop
