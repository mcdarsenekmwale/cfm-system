<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCounsellorsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      \DB::statement("
          CREATE VIEW helpline_counsellors
          AS
          SELECT username, firstname, lastname, type, email
          FROM helpline.counsellor
          WHERE type = 'common'
          UNION
          SELECT username, firstname, lastname, type, email
          FROM gbv1.counsellor
          WHERE type = 'common'
      ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
