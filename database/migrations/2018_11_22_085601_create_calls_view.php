<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      \DB::statement("
          CREATE VIEW helpline_calls
          AS
          SELECT counsellor_username, date, completeness, district_id, comments
          FROM gbv1.call
          UNION
          SELECT counsellor, date, completeness, district, comments
          FROM helpline.calls
      ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
