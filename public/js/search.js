$(document).ready(function(){
  // $("#printButton").click(function(){
  //   window.print();
  // });
  // $(document).ready(function(){
  //   $("#search").on("keyup", function() {
  //     var value = $(this).val().toLowerCase();
  //     $("#test tr").filter(function() {
  //       $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
  //     });
  //   });
  // });

    //
  $('#searching').on("keyup input", function(){
    /* Act on the event */

    var field = $("input[type=search]").val();
    if (field == "") {
           //Assigning empty value to "result" div in "search" .
           // alert("Nothing inputed");
       }
       //If name is not empty.
       else {
         $('#display').hide();
         $('#displayResults').show('slow');

           //AJAX is called.
           let URL = "/search/?search="+field;
           $.ajax({
               type: "GET",
							 url: URL,
							 dataType:'json',
               success: function(result) {
                 console.info(result.value);
                   $("#displayResults").find('#result').html(result);
               }
           });
           if (window.XMLHttpRequest) {
              // code for IE7+, Firefox, Chrome, Opera, Safari
              xmlhttp=new XMLHttpRequest();
            } else {  // code for IE6, IE5
              xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange=function() {
              if (this.readyState==4 && this.status==200) {
                document.getElementById("result").innerHTML=this.responseText;
                document.getElementById("result").style.border="1px solid #A5ACB2";
              }
            }
            xmlhttp.open("GET","/search/?search="+field,true);
            xmlhttp.send();
          }

  });

      // Set search input value on click of result item
      $(document).on("click", "#result p", function(){
          $(this).parents(".top-bar-search").find('input[type="search"]').val($(this).text());
          $(this).parent("#result").empty();
      });


      $('#searching').on('focus', function() {
        $(this).parent().siblings().addClass('active');
      }).on('blur', function() {
          if (!$(this).val()) {
              $(this).parent().siblings().removeClass('active');
          }
      });


    //bloodhound demo
    //documentation : https://github.com/corejavascript/typeahead.js/blob/master/doc/bloodhound.md

    //Documentation : https://github.com/corejavascript/typeahead.js/blob/master/doc/jquery_typeahead.md
    var states = function (query, process) {
        return $.get('/typeahead', { query: query }, function (data) {
                return data;
            });
    }

    var states = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        // `states` is an array of state names defined in "The Basics"
        local: states
    });

    $('#searching').typeahead({
              name: 'typeahead',
              limit : 500,
              minLength: 2,
              source:  function (query, process) {
              return $.get("/typeahead", { query: query }, function (data) {
                      return process(data);
                  });
              },
                  highlight: true,
                  hint: true,
                  minLength: 3,
                  callback: {
                     onClick: function (node, a, item, event) {

                         console.log(node)
                         console.log(a)
                         console.log(item)
                         console.log(event)

                         console.log('onClick function triggered');

                     },
                     onSubmit: function (node, form, item, event) {

                         console.log(node)
                         console.log(form)
                         console.log(item)
                         console.log(event)

                         console.log('onSubmit override function triggered');

                     }
                  }
    });

});
