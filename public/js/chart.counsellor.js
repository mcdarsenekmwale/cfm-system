"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

// Project Overview Demo
// =============================================================
var ProjectOverviewDemo =
/*#__PURE__*/
function () {
  function ProjectOverviewDemo() {
    _classCallCheck(this, ProjectOverviewDemo);

    this.init();
  }

  _createClass(ProjectOverviewDemo, [{
    key: "init",
    value: function init() {
      // event handlers
      this.handleActivityChart();
      this.handleInvoicesChart();
    }
  }, {
    key: "handleActivityChart",
    value: function handleActivityChart() {


      // do something with the value in variable named `id`
      var full_url = document.URL; // Get current url
      var url_array = full_url.split('id=') // Split the string into an array with / as separator
      var id = url_array[url_array.length-1];  // Get the last part of the array (-1)
       // Alert last segment
      $.ajax({
        url: "/admin.counsellordailytrend?id="+id,
        type: 'GET',
        dataType:'json',
        success:function (result) {
        },
        error:function (err) {
          console.log(err);
        }
      }).done(function( result ) {
            var index = 0;
            var partnerdailydata =[];
            var label =[];
            console.info(result);
            result.forEach(function(obj){
                    label[index] = obj.date;
                    var j_date = new Date(Date.parse(obj.date));
                    var date = dateConvert(j_date, "DD-MMM");
                    label[index] = date;
                    partnerdailydata[index]=obj.val;
                index++;
            });

            var data = {
              labels: label,
              datasets: [{
                label: 'Daily Cases Capturing',
                borderColor: Looper.colors.brand.teal,
                backgroundColor: Looper.colors.brand.teal,
                fill: false,
                data: partnerdailydata
              }] // init achievement chart

            };
            var canvas = $('#counsellor-achievement')[0].getContext('2d');
            var chart = new Chart(canvas, {
              type: 'bar',
              data: data,
              options: {
                tooltips: {
                  mode: 'index',
                  intersect: true
                },
                scales: {
                  xAxes: [{
                    gridLines: {
                      display: true,
                      drawBorder: false,
                      drawOnChartArea: false
                    }
                  }],
                  yAxes: [{
                    gridLines: {
                      display: true,
                      borderDash: [8, 4]
                    },
                    ticks: {
                      beginAtZero: true
                    }
                  }]
                }
              }
            });
      });

    }
  }, {
    key: "handleInvoicesChart",
    value: function handleInvoicesChart() {
      var isDarkSkin = Looper.skin === 'dark';
      var gray = Looper.getColors('gray');
      var borderColor = isDarkSkin ? gray[200] : Looper.colors.white;
      var partnerdata =[];

      // do something with the value in variable named `id`
      var full_url = document.URL; // Get current url
      var url_array = full_url.split('id=') // Split the string into an array with / as separator
      var id = url_array[url_array.length-1];  // Get the last part of the array (-1)
       // Alert last segment
      $.ajax({
        url: "/admin.counsellorcase?id="+id,
        type: 'GET',
        dataType:'json',
        success:function (result) {
        },
        error:function (err) {
          console.log(err);
        }
      }).done(function( result ) {

            partnerdata = result;

            //draw doughnut
            var data = {
              type: 'doughnut',
              data: {
                datasets: [{
                  data: [partnerdata[0].com, partnerdata[0].incomp],
                  borderColor: [borderColor, borderColor, borderColor],
                  backgroundColor: [Looper.colors.brand.teal, Looper.colors.brand.pink, Looper.getLightColor()],
                  label: partnerdata[0].username
                }],
                labels: ['Complete', 'InComplete']
              },
              options: {
                cutoutPercentage: 20,
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: true,
                    position: 'bottom',
                },
                animation: {
                  animateScale: true,
                  animateRotate: true
                },
                events: ['click']
              }
            };
            var canvas = $('#canvas-counsellor')[0].getContext('2d');
            var chart = new Chart(canvas, data);

      });
    }
  }]);
  // changing date format
  function dateConvert(dateobj, format){
    var year = dateobj.getFullYear();
    var month= ("0" + (dateobj.getMonth()+1)).slice(-2);
    var date = ("0" + dateobj.getDate()).slice(-2);
    var hours = ("0" + dateobj.getHours()).slice(-2);
    var minutes = ("0" + dateobj.getMinutes()).slice(-2);
    var seconds = ("0" + dateobj.getSeconds()).slice(-2);
    var day = dateobj.getDay();
    var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    var dates = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
    var converted_date = "";

    switch(format){
      case "YYYY-MMM-DD DDD":
        converted_date = year + "-" + months[parseInt(month)-1] + "-" + date + " " + dates[parseInt(day)];
        break;
      case "DD-MMM":
          converted_date = date + " " + months[parseInt(month)-1];
        break;
      case "DD":
            converted_date = dates[parseInt(day)-1];
        break;
    }

    return converted_date;
  }

  return ProjectOverviewDemo;
}();
/**
 * Keep in mind that your scripts may not always be executed after the theme is completely ready,
 * you might need to observe the `theme:load` event to make sure your scripts are executed after the theme is ready.
 */


$(document).on('theme:init', function () {
  new ProjectOverviewDemo();
});
