$(document).ready(function(){


    myChartFunction();
    pieChart();
    barchart();
    daterangepickerInit();


    function retrieveData() {
        // body...

    }

    function pieChart() {

        // body...
        if ($('#myDoughnut').length == 0) {
            return;
        }

        var randomScalingFactor = function() {
            return Math.round(Math.random() * 100);
        };
        $.ajax({
            type: "GET",
            url: "/councellor.issues.dash",
            dataType:'json',
            })
            .done(function( result ) {
                var index = 0

                result.forEach(function(obj){

                   var config = {
                        type: 'doughnut',
                        data: {
                            datasets: [{
                                data: [
                                    obj.com,obj.incomp
                                ],
                                backgroundColor: [
                                    '#1cac81',
                                   ' #41675c ',

                                ]
                            }],
                            labels: [
                                'Complete',
                                'Incomplete',

                            ]
                        },
                        options: {
                            cutoutPercentage: 75,
                            responsive: true,
                            maintainAspectRatio: false,
                            legend: {
                                display: false,
                                position: 'right',
                            },
                            title: {
                                display: true,
                                text: 'Call Distribution '
                            },
                            animation: {
                                animateScale: true,
                                animateRotate: true
                            },
                            tooltips: {
                                enabled: true,
                                intersect: false,
                                mode: 'nearest',
                                bodySpacing: 5,
                                yPadding: 10,
                                xPadding: 10,
                                caretPadding: 0,
                                displayColors: false,
                                backgroundColor: '#38c172',
                                titleFontColor: '#ffffff',
                                cornerRadius: 4,
                                footerSpacing: 0,
                                titleSpacing: 0
                            }
                        }
                    };

                    var ctx = document.getElementById('myDoughnut').getContext('2d');
                    var myDoughnut = new Chart(ctx, config);
                    setInterval(myDoughnut.update(), 3000);

                    index++;
                });
        });
    }

    function barchart() {
            if (!document.getElementById('statistics')) {
            return;
        }

        var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October'];

        var color = Chart.helpers.color;
        var barChartData = {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October'],
            datasets: [{
                label: 'Complete',
                backgroundColor: color('#38c172').alpha(1).rgbString(),
                borderWidth: 0,
                data: [20, 30, 40, 35, 45, 55, 45, 52, 65, 58]
            }, {
                label: 'InComplete',
                backgroundColor: color('#b7d4cf').alpha(1).rgbString(),
                borderWidth: 0,
                data: [25, 35, 45, 40, 50, 60, 50, 12, 20, 25]
            }
            // , {
            //     label: 'Pending',
            //     backgroundColor: color('#80c3af').alpha(1).rgbString(),
            //     borderWidth: 0,
            //     data: [5, 3, 4, 10, 12, 6, 15, 12, 2, 5]
            // }
            ]
        };

        var ctx = document.getElementById('statistics').getContext('2d');
        var myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: false,
                scales: {
                    xAxes: [{
                        categoryPercentage: 0.35,
                        barPercentage: 0.70,
                        display: true,
                        scaleLabel: {
                            display: false,
                            labelString: 'Month'
                        },
                        gridLines: false,
                        ticks: {
                            display: true,
                            beginAtZero: true,
                            fontColor: '#38c172',
                            fontSize: 13,
                            padding: 10
                        }
                    }],
                    yAxes: [{
                        categoryPercentage: 0.35,
                        barPercentage: 0.70,
                        display: true,
                        scaleLabel: {
                            display: false,
                            labelString: 'Value'
                        },
                        gridLines: {
                            color: ' #41675c',
                            drawBorder: false,
                            offsetGridLines: false,
                            drawTicks: false,
                            borderDash: [3, 4],
                            zeroLineWidth: 1,
                            zeroLineColor:' #41675c',
                            zeroLineBorderDash: [3, 4]
                        },
                        ticks: {

                            display: true,
                            beginAtZero: true,
                            fontColor: '#1cac81',
                            fontSize: 13,
                            padding: 10
                        }
                    }]
                },
                title: {
                    display: false
                },
                hover: {
                    mode: 'index'
                },
                tooltips: {
                    enabled: true,
                    intersect: false,
                    mode: 'nearest',
                    bodySpacing: 5,
                    yPadding: 10,
                    xPadding: 10,
                    caretPadding: 0,
                    displayColors: false,
                    backgroundColor: '#38c172',
                    titleFontColor: '#ffffff',
                    cornerRadius: 4,
                    footerSpacing: 0,
                    titleSpacing: 0
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 5,
                        bottom: 5
                    }
                }
            }
        });
    }


    function myChartFunction() {

            $.ajax({
            type: "GET",
            url: "/partner.issues.line",
            dataType:'json',
            })
            .done(function( result ) {
                var index = 0;
                var data =[];
                var label =[];
                result.forEach(function(obj){
                        label[index] = obj.date;
                        var j_date = new Date(Date.parse(obj.date));
                        var date = dateConvert(j_date, "DD-MMM");
                        label[index] = date;
                        data[index]=obj.val;
                    index++;
                });

                var ctx = document.getElementById('myChart').getContext("2d");
                        var color ='#38c172';
                        var max = 5;
                            var gradient = ctx.createLinearGradient(0, 0, 0, 120);
                            gradient.addColorStop(0, Chart.helpers.color(color).alpha(0.3).rgbString());
                            gradient.addColorStop(1, Chart.helpers.color(color).alpha(0).rgbString());

                            var mainConfig = {
                                type: 'line',
                                data: {
                                    labels: label,
                                    datasets: [{
                                        label: 'Cases',
                                        borderColor: color,
                                        borderWidth: 3,
                                        backgroundColor: gradient,
                                        pointBackgroundColor: '#38c172',
                                        data: data,
                                    }]
                                },
                                options: {
                                    responsive: true,
                                    maintainAspectRatio: true,
                                    title: {
                                        display: false,
                                        text: 'Total Cases'
                                    },
                                    tooltips: {
                                        enabled: true,
                                        intersect: false,
                                        mode: 'nearest',
                                        bodySpacing: 5,
                                        yPadding: 10,
                                        xPadding: 10,
                                        caretPadding: 0,
                                        displayColors: false,
                                        backgroundColor: '#38c172',
                                        titleFontColor: '#ffffff',
                                        cornerRadius: 4,
                                        footerSpacing: 0,
                                        titleSpacing: 0
                                    },
                                    legend: {
                                        display: false,
                                        labels: {
                                            usePointStyle: true
                                        }
                                    },
                                    hover: {
                                        mode: 'index'
                                    },
                                    scales: {
                                        xAxes: [{
                                            display: false,
                                            scaleLabel: {
                                                display: false,
                                                labelString: 'Month'
                                            },
                                            ticks: {
                                                display: false,
                                                beginAtZero: true,
                                            }
                                        }],
                                        yAxes: [{
                                            display: false,
                                            scaleLabel: {
                                                display: false,
                                                labelString: 'Value'
                                            },
                                            gridLines: {
                                                color: '#eef2f9',
                                                drawBorder: false,
                                                offsetGridLines: true,
                                                drawTicks: false
                                            },
                                            ticks: {
                                                
                                                display: false,
                                                beginAtZero: true
                                            }
                                        }]
                                    },
                                    elements: {
                                        point: {
                                            radius: 0,
                                            borderWidth: 0,
                                            hoverRadius: 0,
                                            hoverBorderWidth: 0
                                        }
                                    },
                                    layout: {
                                        padding: {
                                            left: 0,
                                            right: 0,
                                            top: 0,
                                            bottom: 0
                                        }
                                    }
                                }
                            };

                            var myChart = new Chart(ctx, mainConfig);
                            // Update chart on window resize
                            setInterval( myChart.update(), 2000);
        });
    }

     function daterangepickerInit() {
        if ($('#k_dashboard_daterangepicker').length == 0) {
            return;
        }

        var picker = document.getElementById('k_dashboard_daterangepicker');
        var start = moment();
        var end = moment();

        function cb(start, end, label) {
            var title = '';
            var range = '';

            if ((end - start) < 100 || label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D');
            } else {
                range = start.format('MMM D') + ' - ' + end.format('MMM D');
            }

            // picker.find('#k_dashboard_daterangepicker_date').html(range);
            // picker.find('#k_dashboard_daterangepicker_title').html(title);
        }

        // picker.daterangepicker({

        //     startDate: start,
        //     endDate: end,
        //     opens: 'left',
        //     applyClass: "btn btn-sm btn-primary",
        //     cancelClass: "btn btn-sm btn-secondary",
        //     ranges: {
        //         'Today': [moment(), moment()],
        //         'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        //         'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        //         'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        //         'This Month': [moment().startOf('month'), moment().endOf('month')],
        //         'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        //     }
        // }, cb);

        cb(start, end, '');
    }

    // changing date format
    function dateConvert(dateobj, format){
      var year = dateobj.getFullYear();
      var month= ("0" + (dateobj.getMonth()+1)).slice(-2);
      var date = ("0" + dateobj.getDate()).slice(-2);
      var hours = ("0" + dateobj.getHours()).slice(-2);
      var minutes = ("0" + dateobj.getMinutes()).slice(-2);
      var seconds = ("0" + dateobj.getSeconds()).slice(-2);
      var day = dateobj.getDay();
      var months = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
      var dates = ["SUN","MON","TUE","WED","THU","FRI","SAT"];
      var converted_date = "";

      switch(format){
        case "YYYY-MMM-DD DDD":
          converted_date = year + "-" + months[parseInt(month)-1] + "-" + date + " " + dates[parseInt(day)];
          break;
        case "DD-MMM":
            converted_date = date + " " + months[parseInt(month)-1];
          break;
      }

      return converted_date;
    }




});
