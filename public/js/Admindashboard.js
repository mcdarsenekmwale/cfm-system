

$(document).ready(function() {
   // Stuff to do as soon as the DOM is ready
   "use strict";

   function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

   function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

   function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

   // Dashboard Demo
   // =============================================================

   /**
    * Keep in mind that your scripts may not always be executed after the theme is completely ready,
    * you might need to observe the `theme:load` event to make sure your scripts are executed after the theme is ready.
    */
    $.ajax({
      url: "/casetrend",
      type: 'GET',
      dataType:'json',
      success:function (result) {
      },
      error:function (err) {
        console.log(err);
      }
    }).done(function( result ) {
          var index = 0;
          var casetrenddata =[];
          var label =[];
          result.forEach(function(obj){
                  label[index] = obj.date;
                  var j_date = new Date(Date.parse(obj.date));
                  var date = dateConvert(j_date, "DD-MMM");
                  label[index] = date;
                  casetrenddata[index]=obj.val;
              index++;
          });

          // draw chart
          var canvas = $('#completion-tasks')[0].getContext('2d');
          var gradient = canvas.createLinearGradient(0, 0, 0, 120);
          var data = {
            labels: label,
            datasets: [{
              backgroundColor: '#00a28a',
              borderColor: '#00a28a',
              borderWidth: 3,
              pointBackgroundColor: '#00a28a',
              data: casetrenddata
            }] // init chart bar

          };

          var chart = new Chart(canvas, {
            type: 'bar',
            data: data,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: false,
                scales: {
                    xAxes: [{
                        categoryPercentage: 0.35,
                        barPercentage: 0.70,
                        display: true,
                        scaleLabel: {
                            display: false,
                            labelString: 'Month'
                        },
                        gridLines: false,
                        ticks: {
                            display: true,
                            beginAtZero: true,
                            fontColor: '#00a28a',
                            fontSize: 13,
                            padding: 10
                        }
                    }],
                    yAxes: [{
                        categoryPercentage: 0.35,
                        barPercentage: 0.70,
                        display: true,
                        scaleLabel: {
                            display: false,
                            labelString: 'Value'
                        },
                        gridLines: {
                            color: ' #41675c',
                            drawBorder: false,
                            offsetGridLines: false,
                            drawTicks: false,
                            borderDash: [3, 4],
                            zeroLineWidth: 1,
                            zeroLineColor:' #41675c',
                            zeroLineBorderDash: [3, 4]
                        },
                        ticks: {
                            
                            display: true,
                            beginAtZero: true,
                            fontColor: '#1cac81',
                            fontSize: 13,
                            padding: 10
                        }
                    }]
                },
                title: {
                    display: false
                },
                hover: {
                    mode: 'index'
                },
                tooltips: {
                    enabled: true,
                    intersect: false,
                    mode: 'nearest',
                    bodySpacing: 5,
                    yPadding: 10,
                    xPadding: 10,
                    caretPadding: 0,
                    displayColors: false,
                    backgroundColor: '#00a28a',
                    titleFontColor: '#ffffff',
                    cornerRadius: 4,
                    footerSpacing: 0,
                    titleSpacing: 0
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 5,
                        bottom: 5
                    }
                }
            }
          });
      });

      // changing date format
      function dateConvert(dateobj, format){
        var year = dateobj.getFullYear();
        var month= ("0" + (dateobj.getMonth()+1)).slice(-2);
        var date = ("0" + dateobj.getDate()).slice(-2);
        var hours = ("0" + dateobj.getHours()).slice(-2);
        var minutes = ("0" + dateobj.getMinutes()).slice(-2);
        var seconds = ("0" + dateobj.getSeconds()).slice(-2);
        var day = dateobj.getDay();
        var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
        var dates = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
        var converted_date = "";

        switch(format){
          case "YYYY-MMM-DD DDD":
            converted_date = year + "-" + months[parseInt(month)-1] + "-" + date + " " + dates[parseInt(day)];
            break;
          case "DD-MMM":
              converted_date = date + " " + months[parseInt(month)-1];
            break;
          case "DD":
                converted_date = dates[parseInt(day)-1];
            break;
        }

        return converted_date;
      }
});
