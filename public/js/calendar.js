
/**
 * Keep in mind that your scripts may not always be executed after the theme is completely ready,
 * you might need to observe the `theme:load` event to make sure your scripts are executed after the theme is ready.
 */




$(document).ready(function() {

  "use strict";

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  // FullCalendar Demo
  // =============================================================
  var fcTheme = FullCalendar.Theme;

  var LooperCalendarTheme = function (fcTheme) {
    function LooperCalendarTheme() {
      fcTheme.apply(this, arguments);
    }

    if (fcTheme) LooperCalendarTheme.__proto__ = fcTheme;
    LooperCalendarTheme.prototype = Object.create(fcTheme && fcTheme.prototype);
    LooperCalendarTheme.prototype.constructor = LooperCalendarTheme;
    return LooperCalendarTheme;
  }(fcTheme);

  LooperCalendarTheme.prototype.classes = {
    widget: 'fc-bootstrap4',
    tableGrid: 'table-bordered',
    tableList: 'table',
    tableListHeading: 'bg-light',
    buttonGroup: 'btn-group',
    button: 'btn btn-secondary',
    stateActive: 'active',
    stateDisabled: 'disabled',
    today: 'highlight',
    popover: 'popover',
    popoverHeader: 'popover-header',
    popoverContent: 'popover-body',
    // day grid
    // for left/right border color when border is inset from edges (all-day in agenda view)
    // avoid `table` class b/c don't want margins/padding/structure. only border color.
    headerRow: 'table-bordered',
    dayRow: 'table-bordered',
    // list view
    listView: 'card card-reflow'
  };
  LooperCalendarTheme.prototype.iconClasses = {
    close: 'fa-times',
    prev: 'fa-chevron-left',
    next: 'fa-chevron-right',
    prevYear: 'fa-angle-double-left',
    nextYear: 'fa-angle-double-right'
  };
  LooperCalendarTheme.prototype.baseIconClass = 'fa';
  LooperCalendarTheme.prototype.iconOverrideOption = 'fontAwesome';
  LooperCalendarTheme.prototype.iconOverrideCustomButtonOption = 'fontAwesome';
  LooperCalendarTheme.prototype.iconOverridePrefix = 'fa-';
  FullCalendar.defineThemeSystem('looper', LooperCalendarTheme);




  /*#__PURE__*/

var data = [
              {
              "title": "Redesign website",
              "start": "2018-12-10",
              "textColor": "rgb(52, 108, 176)",
              "backgroundColor": "rgba(52, 108, 176, .12)",
              "borderColor": "rgb(52, 108, 176)"
            },
            {
              "title": "Write new content",
              "start": "2018-12-17",
              "end": "2018-12-10",
              "textColor": "rgb(0, 162, 138)",
              "backgroundColor": "rgba(0, 162, 138, .12)",
              "borderColor": "rgb(0, 162, 138)"
            },
            {
              "id": 999,
              "title": "Apply new styles",
              "start": "2018-12-19T16:00:00",
              "textColor": "rgb(95, 75, 139)",
              "backgroundColor": "rgba(95, 75, 139, .12)",
              "borderColor": "rgb(95, 75, 139)"
            },
            {
              "id": 999,
              "title": "Review",
              "start": "2018-12-16T16:00:00",
              "textColor": "rgb(0, 162, 138)",
              "backgroundColor": "rgba(0, 162, 138, .12)",
              "borderColor": "rgb(0, 162, 138)"
            },
            {
              "title": "Conference",
              "start": "2018-12-18",
              "end": "2018-12-13",
              "textColor": "rgb(1, 121, 168)",
              "backgroundColor": "rgba(1, 121, 168, .12)",
              "borderColor": "rgb(1, 121, 168)"
            },
            {
              "title": "Meeting",
              "start": "2018-12-12T10:30:00",
              "end": "2018-12-12T12:30:00",
              "textColor": "rgb(249, 172, 47)",
              "backgroundColor": "rgba(249, 172, 47, .12)",
              "borderColor": "rgb(249, 172, 47)"
            },
            {
              "title": "Deploy",
              "start": "2018-12-12T12:00:00",
              "textColor": "rgb(0, 162, 138)",
              "backgroundColor": "rgba(0, 162, 138, .12)",
              "borderColor": "rgb(0, 162, 138)"
            },
            {
              "title": "Meeting",
              "start": "2018-12-25T14:30:00",
              "textColor": "rgb(249, 172, 47)",
              "backgroundColor": "rgba(249, 172, 47, .12)",
              "borderColor": "rgb(249, 172, 47)"
            },
            {
              "title": "Go live!",
              "start": "2018-12-30T07:00:00",
              "textColor": "rgb(183, 107, 163)",
              "backgroundColor": "rgba(183, 107, 163, .12)",
              "borderColor": "rgb(183, 107, 163)"
            },
            {
              "title": "Click for Project",
              "url": "/reports/boardPartner",
              "start": "2018-12-28",
              "textColor": "rgb(95, 75, 139)",
              "backgroundColor": "rgba(95, 75, 139, .12)",
              "borderColor": "rgb(95, 75, 139)"
            }
];


$('#calendar-list').fullCalendar({
  themeSystem: 'looper',
  header: false,
  defaultView: 'listMonth',
  height: 'auto',
  navLinks: false,
  // can click day/week names to navigate views
  editable: false,
  eventLimit: false,
  // allow "more" link when too many events
  events: data,
  eventRender: function eventRender(event, element) {
    element.find('.fc-event-dot').css('background-color', event.borderColor);
  }
});
//
$('#calendar').fullCalendar({
  themeSystem: 'looper',
  header: {
    left: 'title',
    center: '',
    right: 'month,agendaWeek,agendaDay'
  },
  defaultView: 'month',
  height: 'auto',
  navLinks: true,
  // can click day/week names to navigate views
  editable: true,
  eventLimit: true,
  // allow "more" link when too many events
  events: data,
  viewRender: function viewRender(view, element) {
    // update today button state :disabled
    var isToday = $('#calendar').fullCalendar('getDate').format('YYYY-MM-DD') === moment().format('YYYY-MM-DD');

    if (isToday) {
      $('#calendar-today').attr('disabled', 'disabled');
    } else {
      $('#calendar-today').removeAttr('disabled');
    } // update calendar list view


    var listType = "list".concat();
    var range = listType === 'listDay' ? moment(view.start._i).format('YYYY-MM-DD') : {
      start: moment(view.start._i).format('YYYY-MM-DD'),
      end: moment(view.end._i).format('YYYY-MM-DD')
    };
    $('#calendar-list').fullCalendar('changeView', listType, range);
  }
}); // hook the event from our buttons

$('#calendar-prev').on('click', function () {
  $('#calendar, #calendar-list').fullCalendar('prev');
});
$('#calendar-today').on('click', function () {
  $('#calendar, #calendar-list').fullCalendar('today');
});
$('#calendar-next').on('click', function () {
  $('#calendar, #calendar-list').fullCalendar('next');
});


$('#print-preview').on('click', function () {
  var headerElements = document.getElementsByClassName('fc-header');//.style.display = 'none';
  for(var i = 0, length = headerElements.length; i < length; i++) {
    headerElements[i].style.display = 'none';
  }
  var toPrint = document.getElementById('printScheduleArea').cloneNode(true);

  for(var i = 0, length = headerElements.length; i < length; i++) {
        headerElements[i].style.display = '';
  }

  var linkElements = document.getElementsByTagName('link');
  var link = '';
  for(var i = 0, length = linkElements.length; i < length; i++) {
    link = link + linkElements[i].outerHTML;
  }

  var styleElements = document.getElementsByTagName('style');
  var styles = '';
  for(var i = 0, length = styleElements.length; i < length; i++) {
    styles = styles + styleElements[i].innerHTML;
   }

  var popupWin = window.open('', '_blank');
  popupWin.document.open();
  popupWin.document.write('<html><title>Schedule Preview</title>'+link
 +'<style>'+styles+'</style></head><body">')
  popupWin.document.write(toPrint.innerHTML);
  popupWin.document.write('</html>');
  popupWin.document.close();

  setTimeout(popupWin.print(), 20000);
});
});
