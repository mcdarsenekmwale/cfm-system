var districts = new Array();
var total = new Array();
var districts_ = new Array();
var color = ['#673ab7', '#ff51b5', '#0d47a1', '#804d40', '#d0e0e0', '#1b5e20', '#e65100', '#000000', '#bf360c', '#757575', ' #ffc107', '#82ed17', '#185e20', '#64dd17',
    '#1b5e20', '#69f0ae', ' #ff9800', '#795548', '#000000', ' #607d8b', '#3e2723', '#bf360c', '#00c853', '#827717', '#0091ea', '#1a237e', '#c51162', '#880e4f', '#dd2c00', '#5c6bc0'];

//////
$.ajax({
    type: "GET",
    url: "super.issues.dash",
    dataType: 'json',
})
    .done(function (data) {
        X = 0
        data.forEach(function (obj) {
                //total[X]=parseInt(obj.total);
                obj.value = parseInt(obj.value);
                obj.color = color[X];
                districts_[X] = obj;
                X++;
            }
        );
        var dg = Morris.Donut({
            element: 'piechart-placeholder',
            data: districts_
        });
        ////

        //$('#legend1').show();
        dg.options.data.forEach(function (label, i) {
            var legendItem = $('<span></span>').text(label['label'] + " (" + label['value'] + ")").prepend('<br><span>&nbsp;</span>');
            legendItem.find('span')
                .css('backgroundColor', label['color'])
                .css('width', '20px')
                .css('display', 'inline-block')
                .css('margin', '5px');
            $('#legend1').append(legendItem);
        });

        //Android's default browser somehow is confused when tapping on label which will lead to dragging the task
        //so disable dragging when clicking on label
        var agent = navigator.userAgent.toLowerCase();
        if (ace.vars['touch'] && ace.vars['android']) {
            $('#tasks').on('touchstart', function (e) {
                var li = $(e.target).closest('#tasks li');
                if (li.length == 0) return;
                var label = li.find('label.inline').get(0);
                if (label == e.target || $.contains(label, e.target)) e.stopImmediatePropagation();
            });
        }

        $('#tasks').sortable({
                opacity: 0.8,
                revert: true,
                forceHelperSize: true,
                placeholder: 'draggable-placeholder',
                forcePlaceholderSize: true,
                tolerance: 'pointer',
                stop: function (event, ui) {
                    //just for Chrome!!!! so that dropdowns on items don't appear below other items after being moved
                    $(ui.item).css('z-index', 'auto');
                }
            }
        );
        $('#tasks').disableSelection();
        $('#tasks input:checkbox').removeAttr('checked').on('click', function () {
            if (this.checked) $(this).closest('li').addClass('selected');
            else $(this).closest('li').removeClass('selected');
        });


        //show the dropdowns on top or bottom depending on window height and menu position
        $('#task-tab .dropdown-hover').on('mouseenter', function (e) {
            var offset = $(this).offset();

            var $w = $(window)
            if (offset.top > $w.scrollTop() + $w.innerHeight() - 100)
                $(this).addClass('dropup');
            else $(this).removeClass('dropup');
        });

    })
