$(document).ready(function(){

// prototypes
userdistribution();

function userdistribution() {
  // Themes begin
    am4core.useTheme(am4themes_dataviz);
    am4core.useTheme(am4themes_animated);
    // Themes end

    var chart = am4core.create("userchart", am4charts.PieChart3D);
    chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

    chart.legend = new am4charts.Legend();

    var chartdata = [];
    $.ajax({
      type: "GET",
      url: "/admin.userdistribution",
      dataType:'json',
    })
    .done(function( result ) {
      chartdata = result;
      console.info(chartdata);
      var index = 0;
      var data =[];
      var label =[];
      result.forEach(function(obj){
        label[index] = obj.type;
        data[index]=obj.user_count;
        index++;
      });
      chart.data = chartdata;

      var series = chart.series.push(new am4charts.PieSeries3D());
      series.dataFields.value = "user_count";
      series.dataFields.category = "type";
      });
  }


});
