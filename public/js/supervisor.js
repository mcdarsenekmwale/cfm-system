$(document).ready(function() {
  // new search code
  $('#search').on("keyup input", function(){
    /* Act on the event */

    var search = $("input[id=search]").val();
    if (search == "") {
           //Assigning empty value to "result" div in "search" .
           // alert("Nothing inputed");
       }
       //If name is not empty.
       else {
         $('#display').hide();
         $('#displayResults').show('slow');

           //AJAX is called.
           let URL = "/searching/?search="+search;
           $.ajax({
               type: "GET",
               url: URL,
               dataType: 'json',
               success: function(result) {
               }
           });

           if (window.XMLHttpRequest) {
                  // code for IE7+, Firefox, Chrome, Opera, Safari
                  xmlhttp=new XMLHttpRequest();
                } else {  // code for IE6, IE5
                  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange=function() {
                  if (this.readyState==4 && this.status==200) {
                    document.getElementById("result").innerHTML=this.responseText;
                    document.getElementById("result").style.border="1px solid #A5ACB2";
                  }
                }
                xmlhttp.open("GET",URL,true);
                xmlhttp.send();
              }

  });

  checkuser();
  function checkuser(){

      var today = new Date();
      function getActualMonth(date) {
        var month= ("0" + (date.getMonth()+1)).slice(-2);
        var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
      return months[parseInt(month)-1];
      }

      function getYesterday() {
        today.setDate(today.getDate()-1);
        return today.getDate() + ' ' + getActualMonth(today);
      }

      function startOfWeek(date){
          var diff = date.getDate() - date.getDay() + (date.getDay() === 0 ? -6 : 1);
          return new Date(date.setDate(diff));
      }
      function getDaysInMonth(month,year) {
       return new Date(year, month, 0).getDate();
      };
      var firstday = new Date(today.setDate(today.getDate()-3));
      var lastday = new Date(today.setDate(today.getDate() + 3));
        document.getElementById("today").innerHTML = today.getDate() + ' ' + getActualMonth(today);
        document.getElementById("yesterday").innerHTML = getYesterday();
        document.getElementById("week").innerHTML = firstday.getDate()+ ' '  +getActualMonth(firstday)+' - ' + lastday.getDate()+' '+getActualMonth(lastday) ;
        document.getElementById("month").innerHTML = getActualMonth(today)+  ' 1 - '+ getDaysInMonth(today.getMonth()+1, today.getFullYear());



  }
});
