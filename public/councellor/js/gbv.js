$(document).ready(function(){

	var myVar = setInterval(setNavBrand, 3000);

	function setNavBrand() {

	    var tithandizane = document.getElementById('tithandizane');
	    var tithandizanehelpline = document.getElementById('tithandizanehelpline');

	    if (tithandizane) {

	    	tithandizane.style.visibility = tithandizane.style.visibility == "visible" ? "hidden" : "visible";
	    	tithandizane.style.display = tithandizane.style.display == "block" ? "none" : "block";
	    	tithandizanehelpline.style.visibility = tithandizanehelpline.style.visibility == "hidden" ? "visible" : "hidden";
	    	tithandizanehelpline.style.display = tithandizanehelpline.style.display == "none" ? "block" : "none";


	    	// tithandizane.style.visibility ="hidden";
	    	// tithandizane.style.display = "none";
	    }
	    else{

	    }

	}

	function stopNavBrand() {
	    clearInterval(myVar);
	}
	//dataTables
	var table = $('#table').DataTable(
		{
			"paging":true,
			"lengthMenu": [[5, 20, 50, -1], [5, 20, 50, "All"]],
			"ordering":true,
			"info":false,
			"searching":false,
			"stateSave":true,
				"scrollCollapse":true,
			"dom": '<"top">rt<"bottom"p><"clear">'
		}
	);


	$('[type="search"]').autocomplete({
	    source: "http://google.com",
	     select: function( event, ui ) {
            event.preventDefault();
            $('[type="search"]').val(ui.item.id);
        }
	});

	$('.resolved-cases').on('click', function () {
      var id = $(this).data('id');
      var url = 'resolved-cases/'+id;
      if(id){
        $.ajax({
          url: url,
          type: 'GET',
          success:function (result) {
            window.location.href="/home";
            console.log('This is what you get'+ result);

          },
          error:function (err) {
            console.log(err);
          }


        })
      }
  });

	// view case
	$('#tables tbody').on('click', 'tr', function () {
			var infoModal = $('.modal');
			var url = '/recentcalls?';
			var id = $(this).data('id');
			let URL = url + "id="+id;
				if(id){
			 $.ajax({
				 url: URL,
				 type: 'GET',
				 // dataType:'json',
				 success:function (result) {
						var data = JSON.parse(result);
						var html = 		`<link rel="stylesheet" href="{{ asset('css/theme.css') }}">
													<div class="text-left">
															<div class=" row">
																<div class=" col-6">
																	<p class="text-muted "><strong>Full Name :</strong> <span class="ml-2">`+data.client.name+`</span></p>
																	<p class="text-muted "><strong>Age :</strong> <span class="ml-2">`+data.client.phonenumber+`</span></p>
				                          <p class="text-muted "><strong>Nature :</strong><span class="ml-2">`+data.nature +` </span>
				                          </p>
																	<p class="text-muted "><strong>Issue :</strong><span class="ml-2">`+data.subissues[0].issue.name +` </span>
				                          </p>
				                      </div>
				                      <div class=" col-6">
															<p class="text-muted "><strong>Age :</strong> <span class="ml-2">`+data.client.age+'  ' +`</span>
															<strong>   Gender :</strong> <span class="ml-2">`+data.client.gender+`</span></p>
				                       <p class="text-muted "><strong>Mobile :</strong><span class="ml-2">(+265)  `+data.client.phonenumber+`</span></p>
				                        <p class="text-muted "><strong>District :</strong> <span class="ml-2">`+data.ta.district.name+`</span></p>
																<p class="text-muted "><strong>Subissue :</strong> <span class="ml-2">`+data.subissues.name+`</span></p>
				                      </div>
				                    </div>
															<p class="text-muted mb-0"><strong>Comments :</strong>

															</p>

														</div>
														<p class="text-muted font-13">`
					                     +data.comments+
					                  `</p><p class="text-muted small">
														<p class="text-muted "><strong>Completeness :</strong> <span class="ml-2">`+data.completeness+`</span></p>`;
														infoModal.find('#modal-case').html(html);

				 },
				 error:function (err) {
					 console.log(err);
				 }
			 });
			 infoModal.modal('show');
			 infoModal.modal('toggle');
		 }

	} );

	$('.bd-example-modal').on('show.bs.modal',function(event){
		var button = $(event.relatedTarget);
		var recipient = button.data('whatever');
		var modal = $(this);
		var dataString = $(this).data('id');

		$.ajax({
				type: "GET",
				url: '/recentcases?' + "id=" + id,
				data:dataString,
				cache:false,
				})
				.done(function( data ) {

						modal.find("#modal-case").html(data);
						console.log(data);
		});

	});



	$('#new-call').on('click', function () {
		var pagename = $(this).data('url');
	    var url = pagename +'/call';
	    window.location.href=url;
	});

	 $("#k_form_1").submit(function(event) {

        	$( "input[type=text]" ).focus(function() {
			  $( this ).blur();
			});
        event.preventDefault();
    });

	 function submit(event) {

        	$( "input[type=text]" ).focus(function() {
			  $( this ).blur();
			});
        event.preventDefault();
    }


	$('#date_range').popover(
	  {
	       html:true,
            container: 'body',
            content: function() {
                    var content = $(this).attr("data-popover-content");
                    return $(".hidden").html();


          }
	 });

	// refresh
	$('#refresh').on('click', function () {
	         location.reload(true);
	    }
	);

	// popover for reffered cases
	$('#filter').popover(
	  {
	        html : true,
	        content: function() {
	          var content = $(this).attr("data-popover-content");
	          return $(content).children(".alert-success").html();
	        }
	    }
	);

	// document.querySelector("input[type=date]").value = new Date();
});
