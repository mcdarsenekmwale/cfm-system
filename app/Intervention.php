<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Call as Call;

class Intervention extends Model
{
    public $table ="intervention";
	  public $timestamps =false;

    public function calls(){
        return $this->belongsToMany('App\Call', 'call_intervention', 'intervention_id' , 'call_id');
    }
}
