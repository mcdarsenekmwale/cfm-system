<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Issue extends Model
{
    public $table="issue";
	public $timestamps=false;

    public function subissues(){
        return $this->hasMany('App\Subissue', 'issue_id');
    }
    public function calls(){
    	return $this->hasManyThrough('App\Call','App\Subissue', 'issue_id');
    }
    public function partners(){
    	return $this->belongsToMany('App\Partner','issue_partner', 'issue_id', 'partner_id');
    }
}
