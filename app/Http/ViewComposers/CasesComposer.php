<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Call;

class CasesComposer
{
    /**
     * The user repository implementation.
     *
     */
    protected $cases;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(Call $cases)
    {
        // Dependencies automatically resolved by service container...
        $this->cases = $cases;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('count', $this->users->count());
    }
}
