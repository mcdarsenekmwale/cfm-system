<?php

namespace App\Http\Middleware;

use Closure;

class CheckType
{
  /**
  * Handle an incoming request.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \Closure  $next
  * @return mixed
  */
  public function handle($request, Closure $next, ...$types)
  {
    if(empty($types)) return redirect('/');

    foreach($types as $type) {
      if($request->user()->type === $type) {
        return $next($request);
      }
    }
    return redirect('/');
  }
}
