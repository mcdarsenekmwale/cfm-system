<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use App\Counsellor;

class StatsController extends Controller
{
  public function stats(){
    if(request('stage')){
    }
    $stage = request('stage');

    switch($stage){
      case 'daily':
        $start = date("Y-m-d");
        break;
      case 'weekly':
        $previous = strtotime('-1 week +1 day');
        $start = date('Y-m-d', $previous);
        break;
      case 'monthly':
        $previous = strtotime('-1 month +1 day');
        $start = date('Y-m-d', $previous);
        break;
      case 'quaterly':
        $previous = strtotime('-4 month +1 day');
        $start = date('Y-m-d', $previous);
        break;
      case 'annual':
        $previous = strtotime('-1 year +1 day');
        $start = date('Y-m-d', $previous);
        break;
      default:
        return Redirect::back()->with(['error' => 'The page failed to display, please try again later or contact the ICT department.']);
    }
    //return $start
    $stats = Counsellor::withCount(['calls' => function($query) use($start){
                                      $query->where('date', '>=', $start ." 00:00:00");
                                  },'calls as complete' => function($query) use($start){
                                      $query->where('date', '>=', $start ." 00:00:00")
                                            ->where('completeness', 'complete');
                                  }, 'calls as incomplete' => function($query) use($start){
                                      $query->where('date', '>=', $start ." 00:00:00")
                                            ->where('completeness', 'incomplete');
                                  }
                        ])->where('type', '=', 'common')
                          ->orderBy('complete', 'DESC')
                          ->get();
    //return $stats;
    return view('stats', compact('stats','stage'));
  }
  public function extras(){
    $stats = Counsellor::with(['calls'])->addSelect('username')
                        ->where('type', '=', 'common')
                        ->get();

                          return $stats;
  }
}
