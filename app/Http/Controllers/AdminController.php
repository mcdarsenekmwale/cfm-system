<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Cfm as Cfm;
use App\User as User;
use App\Call as Call;
use App\Casee as Casee;
use App\subissue as Subissue;
use App\Issue as Issue;
use App\Partner as Partner;
use App\District as District;
use App\Intervention as Intervention;
use App\Referral as Referral;
use App\Perpetrator as Perpetrator;
use App\Client as Client;
use App\Outcome as Outcome;
use App\WorkerStatus as Worker;
use App\Scheme as Scheme;
use App\LabourType as Labour;
use App\AllianceOne as AllianceOne;
use App\AlpCommitte as Committee;
use App\Counsellor as Counsellor;

class AdminController extends Controller
{
  public function viewUsers(){
    if(request('status')){
      $status = request('status');
      if($status == 1){
        $counsellors = User::where('type','=', 'partner')->paginate(10);
      }elseif($status == 2){
        $counsellors = User::where('type','=', 'supervisor')->paginate(10);
      }elseif($status == 3){
        $counsellors = User::where('type','=', 'admin')->paginate(10);
      }elseif($status == 4){
        $counsellors = User::where('type','=', 'common')->paginate(10);
      }else{
        return redirect::back();
      }
      return view('viewUsers', compact('counsellors'));
    }
  }

  public function addUser(Request $request){
    $password = $request->input('password');
    $confirmPassword = $request->input('confirmpassword');
    $username = $request->input('name');
    if ($password != $confirmPassword) {
      Redirect::back()->withErrors(['error' => 'passwords don\'t match']);
    }
    $user = new User();
    $user->username = $username;
    $user->title = $request->input('title');
    $user->firstname = $request->input('fname');
    $user->lastname = $request->input('lname');
    $user->email = $request->input('email');
    if($request->input('active') == Null){
      $user->state = 0;
    }else{
      $user->state = $request->input('active');
    }
    $user->type = $request->input('type');
    $user->password = bcrypt($password);
    $user->save();
    if($request->input('type') == "partner"){
      $partner = new Partner();
      $partner->name = $request->input('fname') ." " .$request->input('lname');
      $partner->save();
    }
    return  Redirect::to('/home')->with(['message' => 'User ' .$username .' added successfully!']);
  }
  //
  public function deleteUser(){
    if(request('id')){
      $id = request('id');
      $user = User::where('id', $id)->first();
      $username = $user->username;
      $user->delete();
      return Redirect::to('/home')->with(['message' => 'User ' .$username .' deleted successfully!']);
    }
  }

  public function edit(Request $request){
    if(request('id')){
      $id = request('id');
      $user = User::where('id', $id)->first();

      if($request->input('fname') != $user->firstname){
        $user->firstname = $request->input('fname');
      }
      if($request->input('lname') != $user->lastname){
        $user->lastname = $request->input('lname');
      }
      if($request->input('email') != $user->email){
        $user->email = $request->input('email');
      }
      if($request->input('type') != $user->type){
        $user->type = $request->input('type');
      }
      if($request->input('name') != $user->username){
        $user->username = $request->input('name');
      }
      if($request->input('title') != $user->title){
        $user->title = $request->input('title');
      }
      if($request->input('active') != $user->state){
        if($request->input('active') == Null){
          $user->state = 0;
        }else{
          $user->state = $request->input('active');
        }
      }
      $user->save();
      return Redirect::to('/home')->with(['message' => 'successfully updated '.$user->username.'\'s profile']);
    }
  }

  public function editUser(){
    if(request('id')){
      $id = request('id');
      $user = User::find($id);
      return view('editUser', compact('user'));
    }
  }

  public function passwordReset(){
    if(Request('id')){
      $id = request('id');
      $password = str_random(8);
      $user = User::find($id);
      $user->password = bcrypt($password);
      $user->save();
      return redirect::back()->with(['message' => 'password reset successful. new password is ' .$password]);
    }
  }

  // form functions
  public function createCall($value='')
  {
    // code...
    $districts = District::all();
    $interventions= Intervention::all();
    $issues  = Issue::with(['subissues'])->get();
    $refferals = Referral::all();
    $perpetrators =Perpetrator::all();
    $schemes = Scheme::all();
    $labour_types = Labour::all();
    $workers = Worker::all();
    $partners = Partner::all();
    $counsellors =User::all()->where('type', '>=',"admin");
    return view('admin.forms.newcall', compact('districts','issues','workers','schemes','labour_types','interventions','refferals','perpetrators', 'counsellors', 'partners'));
  }

  public function createCase($value='')
  {
    // code...
    $districts = District::all();
    $issues = Partner::with(['issues.subissues'])->where('partner.id','=',134)->first();
    $perpetrators = Perpetrator::all();
    $cfmChannels = Cfm::all();
    $reasons = ['unknown', 'concerned child','concerned adult','victimised adult','victimised child','followup','information seeking'];

      return view('admin.forms.newcase',compact('cfmChannels','perpetrators','districts','issues', 'reasons'));
  }

  public function createPartner($value='')
  {
    // code...
      return view('admin.forms.newpartner');
  }

  public function createUser($value='')
  {
    // code...
      return view('admin.forms.newuser');
  }

    public function accountSettings($value='')
    {
      // code...
      return view('admin.user.account');
    }

    public function CheckUser($value='')
    {
      // code...
      $users = User::where('type','!=', 'partner')
      ->paginate(10);
      $users->setPath(url()->current());
      $data['users']= $users;
          return view('admin.user.users', compact('users','data'));
    }

    // save users
    public function saveUser(Request $request)
    {
      // code...
          $password = $request->input('password');
          $confirmPassword = $request->input('confirmpassword');
          $username = $request->input('name');
          $user = new User();
          $user->username = $username;
          $user->title = $request->input('title');
          $user->firstname = $request->input('firstname');
          $user->lastname = $request->input('lastname');
          $user->email = $request->input('email');
          if($request->input('active') == Null){
            $user->state = 0;
          }else{
            $user->state = $request->input('active');
          }
          $user->type = $request->input('user_type');
          $user->password = bcrypt($password);
          $user->save();
          if($request->input('type') == "partner"){
            $partner = new Partner();
            $partner->name = $request->input('fname') ." " .$request->input('lname');
            $partner->save();
          }
          return  Redirect::to('/home')->with(['message' => 'User ' .$username .' added successfully!']);
      }

// table view
      public function viewCounsellors($value='')
      {
        // code...
        $counsellors = User::where('type','=', 'common')->paginate(10);
        $counsellors->setPath(url()->current());
        $data['counsellor']= $counsellors;
        return view('admin.tables.counsellors', compact('counsellors', 'data' ));
      }

      public function viewCases($value='')
      {
        // code...
        // code...
        $cases = Call::with(['ta.district'])
                ->orderBy('date','DESC')
                ->paginate(10);
        $cases->setPath(url()->current());
        $data['cases']= $cases;
        return view('admin.tables.cases', compact('cases', 'data'));
      }

      public function viewsPartners($value='')
      {
        // code...
        $issues = Partner::withCount(['calls as unref' => function ($query) {$query->where('status', 0);},
              'calls as ref' => function ($query){$query->where('status', 1);},
              'calls as res' => function ($query){$query->where('status', 2);}])
              ->orderBy('name','ASC')
              ->paginate(10);
        $issues->setPath(url()->current());
        $data['issues']= $issues;


        return view('admin.tables.partners', compact( 'issues', 'data'));
      }

      public function viewCalls($value='')
      {
        // code...
        $calls = Call::with(['ta.district'])
                ->orderBy('date','ASC')
                ->paginate(10);
        $calls->setPath(url()->current());
        $data['calls']= $calls;
        return view('admin.tables.calls', compact('calls', 'data'));
      }

      public function viewSupervisors($value='')
      {
        // code...

        return view('admin.tables.supervisors');
      }

      // collections functions
      public function collectionMaps($value='')
      {
        // code...

        return view('admin.collections.maps');
      }

      public function collectionCharts($value='')
      {
        // code...
        $userdistribution = $this->getUserDistribution();

        return view('admin.collections.charts', compact('userdistribution'));
      }


      //
      public function getRequests($value='')
      {
        // code...\
          return view('admin.reports.request');
      }
      public function getUserDistribution($value='')
      {
        // code...
        $users = User::select(DB::raw('count(*) as user_count, type'))
                     ->where('type', '!=', 'admin')
                     ->groupBy('type')
                     ->get();

        return $users;
      }

      public function getCaseDistribution($value='')
        {
              // code...
              $cases = DB::table('call')
                        ->join('ta', 'call.ta_id', '=', 'ta.id')
                        ->join('district', 'ta.district_id', '=', 'district.id')
                        ->select(DB::raw('count(*) as user_count, district.id'))
                        ->groupBy('district.id')
                        ->get();

            return json_encode($cases);
        }

        public function getPartnerGrantt(Request $request)
        {
          // code...
          $id = $request->input('id');

          if ($id) {
            $partners = DB::table('counsellor')->join('partner', 'counsellor.id', '=', 'partner.id')
                          ->where('counsellor.id','=',$id)
                      ->get();

          }
          return view('admin.reports.partnergrant', compact($partners));
        }

        public $neutral_id = 0;
        public function getActivePartner(Request $request)
        {
          $id = $request->input('id');

          if ($id) {
            $Issues = Call::with(['ta.district'])
                        ->where('date', '<=', date("Y-m-d") ." 00:00:00")
                        ->where('partner_id', '=', $id )
                        ->orderBy('date','DESC')
                        ->paginate(5);
            $partners = DB::table('counsellor')->join('partner', 'counsellor.id', '=', 'partner.id')
                          ->where('counsellor.id','=',$id)
                      ->get();
            $Partnerissues = Partner::withCount(['calls as pend' => function ($query) {$query->where('status', 3);},
                      'calls as res' => function ($query){$query->where('status', 2);},
                      'calls as tot',
                      'calls as unres' => function ($query){$query->where('status', 1);}])
                        ->where('partner.id','=',$id)
                        ->orderBy('tot','DESC')
                        ->get();


          }
            // code...
            return view('admin.activepartner', compact('partners','Partnerissues','Issues'));
        }

        public function getDataPartnerCases(Request $request )
        {
            $id = $request->input('id');
          // code...
          $Partnerissues = Partner::withCount(['calls as unref' => function ($query) {$query->where('status', 0);},
                    'calls as res' => function ($query){$query->where('status', 2);},
                    'calls as unres' => function ($query){$query->where('status', 1);}])
                      ->where('partner.id','=',$id)
                      ->orderBy('name','DESC')
                      ->get();

          return json_encode($Partnerissues);
        }

        public function getboardPartner($value='')
        {
          // code...
            return view('admin.reports.partnerboard');
        }

        public function getProfile($value='')
        {
          // code...
          return view('admin.user.profile');
        }
        public function getNotifications($value='')
        {
          // code...
          return view('admin.user.notification');
        }

        public function getPartners($value='')
        {
          // code...
          $partners = DB::table('counsellor')->join('partner', 'counsellor.id', '=', 'partner.id')
                    ->orderBy('name','ASC')
                    ->get();
          $user = User::where('type', '=', 'partner')
                       ->paginate(5);

          $user->setPath(url()->current());
          $data['partners']= $user;

          return view('admin.user.partner', compact('partners', 'data'));
        }
        public function setcalendar($value='')
        {
          // code...

          return view('admin.reports.calendar');
        }

        // update accountSetting
        protected function updateProfile(Request $request)
        {
          Validator::make($request->all(), [
              'firstname' => 'required|string|max:255',
              'lastname' => 'required|string|max:255',
              'username' => 'required|string|max:255',
              'email' => 'required|string|email|max:255|unique:users',
              'newpassword' => 'required|string|min:6|confirmed',
          ]);
          // code...
          $id = Auth::user()->id;
          $user = User::find($id);
          $user->firstname = $request->input('firstname');
          $user->lastname = $request->input('lastname');
          $user->username = $request->input('username');
          $user->password =  Hash::make($request->input('newpassword'));
          $user->update();

          return view('admin.index')->with(['message' => Auth::user()->username.'you have updated your profile successfully']);
        }


        //
        public function getPartnerProjects(Request $request)
        {
          // code...
          $id = $request->input('id');
          if ($id) {
            // code...
            $partners = DB::table('counsellor')->join('partner', 'counsellor.id', '=', 'partner.id')
                          ->where('counsellor.id','=',$id)
                      ->get();
          }
          return view('admin.reports.partnerprojects', compact('partners'));
        }

        // json data functions
        public function getcaseperformace($value='')
        {
          // code...
           Partner::withCount(['calls as pend' => function ($query) {$query->where('status', 3);},
                       'calls as unres' => function ($query){$query->where('status', 1);},
                       'calls as res' => function ($query){$query->where('status', 2);}])
                     ->orderBy('name','ASC')
                     ->get();
                $casep = Call::select(DB::raw('count(*) as pend',function ($query) {$query->where('status', 3);} ))
                                ->select(DB::raw('count(*) as unres',function ($query) {$query->where('status', 2);} ))
                                ->select(DB::raw('count(*) as res',function ($query) {$query->where('status', 1);} ))
                                ->get();
          return $casep;
        }

        public function getLeaderboard($value='')
        {
          // code...
          $counsellorboard =  Counsellor::withCount(['call as comp' => function ($query) {$query->where('completeness', 'complete');},
                'call as incomp' => function ($query){$query->where('completeness',  'incomplete');}])
                ->where('type','=', 'common')
                ->orderBy('username','ASC')
                ->get();

          return $counsellorboard;
        }


        public function getAllUser($value='')
        {
          // code...
          $users = User::select('username as key', 'email as value')->get();
          return json_encode($users);
        }

        public function PlotGraphline(Request $request)
        {
          # code...
          // Route assigned name "partner.issues"...
          $date = new Carbon;
          $date->subMonth(7);
          // code...
          $id = $request->input('id');
          if ($id) {
            // code...
            $barGraph = Call::select('date', \DB::raw('count(call.id) as val'))
                        ->where('partner_id','=', $id)
                        ->where( 'date', '>',  $date->toDateTimeString())
                        ->groupBy('date')
                        ->orderBy('date','ASC')
                        ->limit(7)
                        ->get();
          }

          return json_encode($barGraph);
          //
        }

  }
