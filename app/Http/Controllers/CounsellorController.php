<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use App\User as User;
use App\Call as Call;
use App\Casee as Casee;
use App\subissue as Subissue;
use App\Issue as Issue;
use App\Partner as Partner;
use App\District as District;
use App\Intervention as Intervention;
use App\Referral as Referral;
use App\Perpetrator as Perpetrator;
use App\Client as Client;
use App\Outcome as Outcome;
use App\WorkerStatus as Worker;
use App\Scheme as Scheme;
use App\LabourType as Labour;
use App\AllianceOne as AllianceOne;
use App\AlpCommitte as Committee;
use App\Counsellor as Counsellor;

class CounsellorController extends Controller
{


  public function index(Request $request)
  {

    if (url('/gbv/') == url()->current()) {
      $cases = Call::with(['ta.district'])
      ->where('date', '<=', date("Y-m-d") ." 00:00:00")
      ->where('partner_id', '!=', '134' )
      ->paginate(5);
      $cases->setPath(url()->current());
      $data['cases']= $cases;
      $title = "GENDER BASED VIOLENCE";
      $pagename = "gbv";
      $caption = "";
      // return json_encode($cases );
      return view('counsellor.gbv.index',compact('data', 'cases', 'title', 'pagename', 'caption'));
    }
    elseif (url('/allianceone/') == url()->current()) {
      $cases = Call::with(['ta.district'])
      ->where('date', '>=', date("Y-m-d") ." 00:00:00")
      ->where('partner_id', '=', '134' )
      ->paginate(5);
      $cases->setPath(url()->current());
      $data['cases']= $cases;
      $title = "ALLIANCE ONE";
      $pagename = "allianceone";
      $caption = "";
      return view('counsellor.gbv.index',compact('data','cases', 'title', 'pagename', 'caption'));
    }
    elseif (url('/congoma/') == url()->current()) {
      $cases = Call::with(['ta.district'])
      ->where('date', '>=', date("Y-m-d") ." 00:00:00")
      ->where('partner_id', '=', '2' )
      ->paginate(10);
      $cases->setPath(url()->current());
      $data['cases']= $cases;
      $caption = " Coucil for Non Government Organisations in Malawi ";
      $title = "CONGOMA";
      $pagename = "congoma";
      return view('counsellor.gbv.index',compact('data','cases', 'title', 'pagename', 'caption'));
    }

  }

  public function registerNewCall($value='')
{
  # code...
  $districts = District::all();
  $interventions= Intervention::all();
  $issues  = Issue::with(['subissues'])->get();
  $refferals = Referral::all();
  $perpetrators =Perpetrator::all();
  $counsellors =User::all()->where('type', '>=',"common");

  if (url('/gbv/call') == url()->current()) {

    $partners = Partner::where('id', '!=', '134')->get();
    return view('counsellor.gbv.callForm',compact('districts','issues','interventions','refferals','perpetrators', 'partners','counsellors' ));
  }
  elseif (url('/allianceone/call') == url()->current()) {

    $schemes = Scheme::all();
    $labour_types = Labour::all();
    $workers = Worker::all();
    $partners = Partner::find('134');

    return view('counsellor.OtherPartners.alliance1callForm',compact('districts','issues','workers','schemes','labour_types','interventions','refferals','perpetrators', 'counsellors', 'partners'));
  }
  elseif (url('/congoma/call') == url()->current()) {

    $partners = Partner::find('2');

    return view('counsellor.OtherPartners.CongomacallForm',compact('districts','issues','interventions','refferals','perpetrators', 'partners','counsellors' ));
  }
}


  public function getcounsellor($value='')
  {
    # code...
    $counsellors =User::all()->where('type', '>=',"common");

    return json_encode($counsellors);
  }

  public function addCall(){
    $districts = District::all();
    $interventions= Intervention::all();
    $schemes = Scheme::all();
    $labour_types = Labour::all();
    $workers = Worker::all();
    $issues  = Issue::with(['subissues'])->get();
    $refferals = Referral::all();
    $perpetrators =Perpetrator::all();

    return view('registerCall',compact('districts','issues','workers','schemes','labour_types','interventions','refferals','perpetrators'));
  }


  public function registerCall(Request $request){

    if ($request->input('brief') !="") {
      // code...
      // changing the date format
      $ini_date = date_create($request->input('date'));
      $final_date = date_format($ini_date,"Y-m-d");

      // checking sucess
      $success = "false";
      if($request->input('partner') == "Alliance One International"){
        $partner = 134;
      }
      else{
        $partner =$request->input('partner');
      }

      $client_name = $request->input('fullname');
      $client_gender = $request->input('gender');
      $client_age = $request->input('age');
      $client_phone_number =$request->input('phonenumber');
      $district =$request->input('district');
      $traditional = $request->input('traditional');
      $intervention_ref =$request->input('intervention_ref');
      $perpetrator =$request->input('perpetrator');
      $counsellor =Auth::user()->username;

      $startTime =$request->input('starttime');
      $endTime =$request->input('endtime');
      $date =$final_date;
      $nature = $request->input('nature');
      $callCompleteness =$request->input('callCompleteness');

      $subissues =$request->input('subissue');
      $other_subissue=$request->input('other_subissue');
      $interventions =$request->input('intervention');
      $refferals =$request->input('referrals');

      $other_subissue_specify=$request->input('other_subissue');
      $brief =$request->input('brief');

      //Alliance one input data
      $labour_type = $request->input('lab_type');
      $status = $request->input('Status');
      $worker_status = $request->input('worker_status');
      $alp_committee = $request->input('ALP_committee');
      $scheme = $request->input('scheme');
      $alliance_once = $request->input('alliance');
      $pilot = $request->input('pilot');
      $call_id = 0;

      $client = Client::firstOrCreate(
        ['name' => $client_name],
        ['age' => $client_age],
        ['gender' => $client_gender],
        ['phonenumber' => $client_phone_number]
      );
      $call= new Call;
      $call->starttime = $startTime;
      $call->endTime = $endTime;
      $call->date =$date .' ' .$startTime;
      $call->nature = $nature;
      $call->counsellor_username = $counsellor;
      $call->starttime = $startTime;
      $call->perpetrator_id = $perpetrator;
      $call->partner_id =$partner;
      $call->ta_id = 23;
      $call->comments = $brief;
      $call->completeness=$callCompleteness;
      if(!empty($partner)){
        $call->status = 1;
        $call->partner_id =$partner;
        $call->referral_date = date("Y-m-d H:i:s");
        if($nature = "positive feedback"){
          $call->status = 2;
        }
      }else{
        $call->status = 0;
        if($nature = "positive feedback"){
          $call->status = 2;
        }
      }
      $client->calls()->save($call);

      if ($client->calls()->save($call)) {
        # code...
        $success = "true";

      }

      if(!empty($referrals)){
        foreach($referrals as $referral){
          $call->referrals()->attach($referral);
        }
      }
      if(!empty($interventions)){
        foreach($interventions as $intervention){
          $call->interventions()->attach($intervention);
        }
      }
      if(!empty($subissues)){
        foreach($subissues as $subissue){
          $call->subissues()->attach($subissue);
        }
      }

      //check if allianceOne partner id == 134
      if($partner == 134)
      {
        $alliance_one = new AllianceOne;
        $alliance_one->scheme_id = $scheme;
        $alliance_one->worker_id = $worker_status;
        $alliance_one->committee_id = $alp_committee;
        $alliance_one->labour_id =  $labour_type;
        $call->allianceOne()->save($alliance_one);
      }

      if($refferals != null || $intervention_ref == 4){

        //
        $case= new Casee;
        $case->status  ="open";
        $call->casee()->save($case);

        $case->assignedBy()->attach(Auth::User()->username);

      }

      if ($success) {
        return redirect()->back()->with('callCode',$call->id)->withMessage('Call registered successfully!');
      }
      else{

        return redirect()->back()->with('error','Please fill all the fields on the form!')->withInput();
      }
    }
    else {
      return redirect()->back()->with('error','Please fill all the fields on the form!');
    }

  }
  public function drawDashboard($value='')
  {
    // code...
    $dashIssues = Counsellor::withCount(['call as com' => function ($query) {$query->where('completeness', 'complete');},
      'call as incomp' => function ($query){$query->where('completeness', 'incomplete');}
    ])
    ->where('id','=', Auth::id())
    ->orderBy('username','ASC')
    ->get();

    return json_encode($dashIssues);
  }

  public function getCouncellorInfo($value='')
  {
    // code...
    $id = request('id');

    if ($id) {
      // code...
      $counsellors = Counsellor::find($id);
      $Issues = Call::with(['ta.district'])
                  ->where('date', '<=', date("Y-m-d") ." 00:00:00")
                  ->where('counsellor_username', '=', $counsellors->username )
                  ->paginate(5);
      $Issues->setPath(url()->current());
      $data['issues']= $Issues;
      $counsellorStats = Counsellor::withCount(['call as com' => function ($query) {$query->where('completeness', 'complete');},
                    'call as incomp' => function ($query){$query->where('completeness', 'incomplete');},
                    'call as total'
                  ])
                  ->where('id','=', $id)
                  ->orderBy('username','ASC')
                  ->get();
    }
    if (url('/counsellorStats')== url()->current()) {
      // code...
      $pageDescription = "counsellorStats";
      $data['issues']= $Issues;
      return view('supervisor.activecounsellor', compact('Issues', 'counsellors','counsellorStats', 'pageDescription','data'));
    }
    elseif (url('/counsellorDetails')== url()->current()) {
      // code...
      $pageDescription = "counsellorDetails";
      return view('supervisor.activecounsellor', compact('Issues', 'counsellors','counsellorStats', 'pageDescription','data'));
    }

  }



}
