<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use App\User as User;
use App\Call as Call;
use App\Casee as Casee;
use App\subissue as Subissue;
use App\Issue as Issue;whatever
use App\Partner as Partner;
use App\District as District;
use App\Intervention as Intervention;
use App\Referral as Referral;
use App\Perpetrator as Perpetrator;
use App\Cfm as Cfm;
use App\Client as Client;
use App\Outcome as Outcome;

class PartnerController extends Controller
{
  public function newCase(){
    $districts = District::all();
    $issues = Partner::with(['issues.subissues'])->where('partner.id','=',Auth::id())->first();
    $perpetrators = Perpetrator::all();
    $cfmChannels = Cfm::all();
    $reasons = ['unknown', 'concerned child','concerned adult','victimised adult','victimised child','followup','information seeking'];

    return view('/addCase',compact('cfmChannels','perpetrators','districts','issues', 'reasons'));

  }

  public function saveCase(Request $request){
    $ini_date = date_create($request->input('date'));
    $final_date = date_format($ini_date,"Y-m-d");

    $client_name = $request->input('clientname');
    $client_gender = $request->input('gender');
    $client_age = $request->input('age');
    $perpetrator = $request->input('perpetrator');
    $district = $request->input('district');
    $nature = $request->input('nature');
    $T_A = $request->input('tradional_Authority');
    $partner = $request->input('partner');
    $subissue = $request->input('subissue');
    $cfm = $request->input('cfm');
    $case_brief = $request->input('brief');
    $outcome = $request->input('outcome');
    $action_taken = $request->input('action');
    $startTime = date_default_timezone_set('Africa/Blantyre'); echo date("H:i:s");
    $client = Client::firstOrCreate(
      ['name' => $client_name],
      ['age' => $client_age],
      ['gender' => $client_gender]
    );
    $call = new Call();
    $call->date = $final_date.' '.$startTime;;
    $call->nature = $nature;
    $call->counsellor_username = Auth::user()->username;
    $call->ta_id = $district;
    $call->comments = $case_brief;
    $call->perpetrator_id = $perpetrator;
    $call->partner_id = Auth::id();
    $call->referral_date = date("y-m-d h:m:1");
    $call->status = 1;
    $call->cfm_id = $cfm;
    if($request->all()){
      $call->completeness = "complete";
    }
    else {
      $call->completeness = "incomplete";
    }
    $client->calls()->save($call);
    $call->subissues()->attach($subissue);
    if($action_taken != null && $outcome != null){
      $theOutcome = new Outcome();
      $theOutcome->action_taken = $action_taken;
      $theOutcome->outcome = $outcome;
      $call->outcome()->save($theOutcome);

      return redirect::to('/home')->with(['message' => 'Case successfully Saved!']);
    }
    return redirect::to('/caseClose?id=' .$call->id)->with(['message' => 'Case successfully Saved!']);
  }


// analytics
  public function analyticsDraw($value='')
  {
    // code...
    return view('partner.analytics');
  }

  public function PlotGraphdash($value='')
  {
    # code...
    // Route assigned name "partner.issues"...
    $dashIssues = Partner::withCount(['calls as pend' => function ($query) {$query->where('status', 3);},
    'calls as unres' => function ($query){$query->where('status', 1);},
    'calls as res' => function ($query){$query->where('status', 2);}])
    ->where('id','=', Auth::id())
    ->orderBy('name','ASC')
    ->get();


    return json_encode($dashIssues);
  }

  public function PlotGraphline($value='')
  {
    # code...
    // Route assigned name "partner.issues"...
    $date = new Carbon;
    $date->subMonth(10);
    $barGraph = Call::select('date', \DB::raw('count(call.id) as val'))
    ->where('partner_id','=', Auth::id())
    ->where( 'date', '>',  $date->toDateTimeString())
    ->groupBy('date')
    ->orderBy('date','ASC')
    ->limit(10)
    ->get();

    return json_encode($barGraph);
  }

  public function PlotGraphbar($value='')
  {
    # code...
    // Route assigned name "partner.issues"...
    $date = new Carbon;
    $date->subYear(1);
    $barGraph = Call::select('date', \DB::raw('count(call.id) as val'))
    ->where('partner_id','=', Auth::id())
    ->where( 'date', '>',  $date->toDateTimeString())
    ->groupBy('date')
    ->orderBy('date','ASC')
    ->limit(10)
    ->get();

    return json_encode($barGraph);
  }

<<<<<<< HEAD
  public function extras(){
    /*
    *this returns $results[] that has
    *the quering partner's calls
    *grouped monthly for a period of 8 months
    *and later grouped by unresolved, resolved and pending.
    */

    $frame = -8;
    $results = array();

    for(;$frame < 1;$frame++){
    $previous = strtotime($frame .' months +1 day');
    $date = date('Y-m', $previous);
    $q = Partner::withCount(['calls as pend' => function ($query) use($date) {$query->where('status', 3)->isTimeFrame($date);},
    'calls as unres' => function ($query) use($date) {$query->where('status', 1)->isTimeFrame($date);},
    'calls as res' => function ($query) use($date) {$query->where('status', 2)->isTimeFrame($date);}])
    ->get();
    $q = json_decode($q);
    $q[] = array('date' => $date);
    $results[] = $q;
  }





  /*
  *this returns $results[] that has
  *the quering partner's calls
  *for the current month and the previous month
  *grouped by client age with a range of 10 from 0 to 90 years.
  */

  for($range = 0;$range < 90;$range =+ 10){

  $previous = strtotime('-1 month +1 day');
  $date = date('Y-m', $previous);

  $q = Partner::withCount(['calls as thisMonth' => function ($query) {$query->isTimeFrame(date('Y-m'));},
  'calls as lastMonth' => function ($query) use($date) {$query->isTimeFrame($date);}])
  ->whereHas('clients', function ($query) use($range) {$query->where('age','>', $range)->where('age','<=', $range + 10);})
  ->first();
  $results[] = $q;
}





/*
*this returns $results[] that has
*the quering partner's calls
*for the current month and the previous month
*grouped by client gender.
*/

$previous = strtotime('-1 month +1 day');
$date = date('Y-m', $previous);

$females = Partner::withCount(['calls as thisMonth' => function ($query) {$query->isTimeFrame(date('Y-m'));},
'calls as lastMonth' => function ($query) {$query->isTimeFrame(date('Y-m', strtotime('-1 month +1 day')));}])
->whereHas('clients', function ($query) {$query->where('gender','female');})
->first();

$males = Partner::withCount(['calls as thisMonth' => function ($query) {$query->isTimeFrame(date('Y-m'));},
'calls as lastMonth' => function ($query) {$query->isTimeFrame(date('Y-m', strtotime('-1 month +1 day')));}])
->whereHas('clients', function ($query) {$query->where('gender','male');})
->first();

$results = array('females' => $females, 'males' => $males);




/*
*this returns $results[] that has
*the quering partner's calls
*for the current month and the previous month
*grouped by district.
*/

$q = district::withCount(['calls as thisMonth' => function ($query) {$query->isTimeFrame(date('Y-m'));},
'calls as lastMonth' => function ($query) {$query->isTimeFrame(date('Y-m', strtotime('-1 month +1 day')));}])
->get();
return $q;





/*
*this returns $results[] that has
*the quering partner's calls
*for the current month and the previous month
*grouped by issue.
*/

$partner = Partner::with(['issues'])->first();
foreach($partner->issues as $issue){
  $id = $issue->id;
  $thisMonth = Call::whereHas('subissues', function($query) use($id) {
    $query->where('issue_id', $id);
  })->isTimeFrame(date('Y-m'))->count();

  $lastMonth = Call::whereHas('subissues', function($query) use($id) {
    $query->where('issue_id', $id);
  })->isTimeFrame(date('Y-m', strtotime('-1 month +1 day')))->count();
  $results[$issue->name] = ['thismonth' => $thisMonth, 'lastMonth' => $thisMonth];
}




/*
*this returns $results[] that has
*the quering partner's calls
*for the current month and the previous month
*grouped by subissue.
*/
$results = Subissue::withCount(['calls as thisMonth' =>function($query){
  $query->isTimeFrame(date('Y-m'));
}, 'calls as lastMonth' => function($query){
  $query->isTimeFrame(date('Y-m', strtotime('-1 month +1 day')));
}])->get();
return $results;

}
=======
  public function piegender($value='')
  {
    // code...
    return json_encode($value );
  }

  public function partnersubissuesDraw($value='')
  {
    // code...
    $value = Subissue::withCount(['calls as value'])
          ->orderBy('value','DESC')
          ->get();
    return json_encode($value );
  }


  //
  public function partnerissuesDraw($value='')
  {
    // code...
    $value = Subissue::withCount(['calls as value'])
          ->orderBy('value','DESC')
          ->get();
    return json_encode($value );
  }

  public function getIntegrations($value='')
  {
    // code...
    return view('partner.integrations');
  }

  public function createReport($value='')
  {
    // code...
    // code...
    return view('partner.reports');
  }
>>>>>>> b9598ffb5b4ee67d053df41eaa4b36eafd7d85e1
}
