<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use App\User as User;
use App\Call as Call;
use App\District as District;
use App\Counsellor as Counsellor;
use App\Casee as Casee;
use App\Subissue as Subissue;
use App\Issue as Issue;
use App\Partner as Partner;

class HomeController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Http\Response
  *
  *Homepage for users.
  */
  public function index(){

    /*
    *homepage for supervisors.
    */
    if (Auth::User()->type === "supervisor") {

      $partnerIssues = Subissue::withCount(['calls as value'])
      ->orderBy('value','DESC')
      ->paginate(5);
      $partnerIssues->setPath(url()->current());
      $datapa['partnerIssues']= $partnerIssues;

      $issues = Partner::withCount(['calls as unref' => function ($query) {$query->where('status', 0);},
      'calls as ref' => function ($query){$query->where('status', 1);},
      'calls as res' => function ($query){$query->where('status', 2);}])
      ->orderBy('name','ASC')
      ->paginate(5);
      $issues->setPath(url()->current());
      $data['issues']= $issues;
      $totalCases = Casee::count();
      $totalCalls = Call::count();

    
      return view('supervisor.index',compact('issues','partnerIssues','totalCases', 'totalCalls','data', 'datapa'));
    }
    /*
    *homepage for partners
    */
    elseif(Auth::User()->type === "partner"){

      $partnerIssues = Subissue::withCount(['calls as value' => function ($query) {
        $query->where('call.partner_id','=',Auth::id());
      }])
      ->orderBy('value','DESC')
      ->get();


      return view('partner.index',compact('partnerIssues'));

    }
    elseif(Auth::user()->type === "admin"){
      $counsellors = User::orderBy('type','DESC')->get();
      $partnerIssues = Subissue::withCount(['calls as value'])
      ->orderBy('value','DESC')
      ->get();

      $issues = Partner::withCount(['calls as unref' => function ($query) {$query->where('status', 0);},
                  'calls as res' => function ($query){$query->where('status', 2);},
                  'calls as tot',
                  'calls as unres' => function ($query){$query->where('status', 1);}])
                  ->orderBy('tot','DESC')
                  ->paginate(5);
      $partners = DB::table('counsellor')->join('partner', 'counsellor.id', '=', 'partner.id')
                ->orderBy('name','DESC')
                ->get();
      $counsellors_count =  User::where('type','=', 'common')->count();
      $partners_count = Partner::count();
      $totalCases = Casee::count();

      return view('admin.index', compact('counsellors','issues','partnerIssues','partners_count','counsellors_count','totalCases','partners'));
    }
    elseif(Auth::user()->type === "common"){
      $cases = Call::with(['ta.district'])
      ->where('date', '>=', date("Y-m-d") ." 00:00:00")
      ->paginate(10);
      return view('counsellor.home',compact('cases'));
    }

  }

  // profile details
  public function ProfileIndex(Request $request)
  {
    // code...

    return view('auth.profile');
  }
}
