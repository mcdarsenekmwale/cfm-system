<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use App\Cfm as Cfm;
use App\User as User;
use App\Call as Call;
use App\Casee as Casee;
use App\subissue as Subissue;
use App\Issue as Issue;
use App\Partner as Partner;
use App\District as District;
use App\Intervention as Intervention;
use App\Referral as Referral;
use App\Perpetrator as Perpetrator;
use App\Client as Client;
use App\Outcome as Outcome;
use App\WorkerStatus as Worker;
use App\Scheme as Scheme;
use App\LabourType as Labour;
use App\AllianceOne as AllianceOne;
use App\AlpCommitte as Committee;
use App\Counsellor as Counsellor;
class RegistrationController extends Controller
{
    //

    // form functions
    public function createCall($value='')
    {
      // code...
      $districts = District::all();
      $interventions= Intervention::all();
      $issues  = Issue::with(['subissues'])->get();
      $refferals = Referral::all();
      $perpetrators =Perpetrator::all();
      $schemes = Scheme::all();
      $labour_types = Labour::all();
      $workers = Worker::all();
      $partners = Partner::all();
      $counsellors =User::all()->where('type', '>=',"supervisor");
      return view('supervisor.register.newcall', compact('districts','issues','workers','schemes','labour_types','interventions','refferals','perpetrators', 'counsellors', 'partners'));
    }

    public function createCase($value='')
    {
      // code...
      $districts = District::all();
      $issues = Partner::with(['issues.subissues'])->where('partner.id','=',134)->first();
      $perpetrators = Perpetrator::all();
      $cfmChannels = Cfm::all();
      $reasons = ['unknown', 'concerned child','concerned adult','victimised adult','victimised child','followup','information seeking'];

        return view('supervisor.register.newcase',compact('cfmChannels','perpetrators','districts','issues', 'reasons'));
    }
}
