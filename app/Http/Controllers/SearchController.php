<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Cfm as Cfm;
use App\User as User;
use App\Call as Call;
use App\Casee as Casee;
use App\subissue as Subissue;
use App\Issue as Issue;
use App\Partner as Partner;
use App\District as District;
use App\Intervention as Intervention;
use App\Referral as Referral;
use App\Perpetrator as Perpetrator;
use App\Client as Client;
use App\Outcome as Outcome;
use App\WorkerStatus as Worker;
use App\Scheme as Scheme;
use App\LabourType as Labour;
use App\AllianceOne as AllianceOne;
use App\AlpCommitte as Committee;
use App\Counsellor as Counsellor;

class SearchController extends Controller
{
    //
    public function index($value='')
    {
      $keyword = request('search');
      // code...

      if ($keyword) {
        // code...
        $result = Call::find("%{$keyword}%");
        $value = Call::with(['ta.district'])
              ->where('comments','LIKE', "%{$keyword}%")
              ->get();
        $user = User::where('username','LIKE', "%{$keyword}%")
            ->get();
      }
      else {
        // code...
        $result = " The input field was empty";

      }
      $results = compact('user','value','result');
      return json_encode($results );
    }


    public function typehead(Request  $request)
    {
        $result= User::where('username', 'LIKE', "%{$request->input('query')}%")->get();
        return response()->json($result);
    }

    function searching() {
       $search = request( 'search' );
       if($search != ""){
       $user = User::where ( 'username', 'LIKE', '%' . $search . '%' )
              ->orWhere ( 'firstname', 'LIKE', '%' . $search . '%' )
              ->orWhere ( 'lastname', 'LIKE', '%' . $search . '%' )
              ->orWhere ( 'email', 'LIKE', '%' . $search . '%' )
              ->get();
              $result = Call::with(['ta.district'])
                     ->where ( 'counsellor_username', 'LIKE', '%' . $search . '%' )
                     ->orWhere ( 'comments', 'LIKE', '%' . $search . '%' )
                     ->orWhere ( 'nature', 'LIKE', '%' . $search . '%' )
                     ->get();
       if (count ( $user ) > 0)
           return $user;
        return view('supervisor.search')->withDetails ( $user )->withQuery ( $search );
       }
        return $user;
        return view('supervisor.search')->withMessage ( 'No Details found. Try to search again !' );
      }
}
