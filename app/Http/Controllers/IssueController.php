<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use App\User as User;
use App\Call as Call;
use App\Casee as Casee;
use App\subissue as Subissue;
use App\Issue as Issue;
use App\Partner as Partner;
use App\Intervention as Intervention;
use App\Referral as Referral;
use App\Outcome as Outcome;

class IssueController extends Controller
{

  //
public function viewAllCases($value='')
{
  // code...
  $status = 0;
  $cases = Call::with(['ta.district'])
  ->where('partner_id', '=', Auth::user()->id )
  ->paginate(10);
  $cases->setPath(url()->current());
  $data['cases']= $cases;
  $caseStatus = "All Cases";
  return view('partner.cases', compact('status', 'cases','caseStatus','data'));
}

public function viewCall()
{
    # code...
    if(request('id')){
      $id = request('id');
      $callDetails = Call::with(['client','subissues.issue','partner','ta.district','interventions','referrals','outcome'])
                          ->where('call.id', '=', $id)
                          ->first();
      $outcomeCount = Outcome::where('call_id', '=', $id)
                              ->count();
      if(Auth::User()->type == "common"){
        return  json_encode($callDetails);
      }
      else {
        return  json_encode($callDetails);
      }
    }
}

  public function viewCases(){

      if(request('id')){
        $id = request('id');
        $callDetails = Call::with(['client','subissues.issue','partner','ta.district','interventions','referrals','outcome'])
                            ->where('call.id', '=', $id)
                            ->first();
        $outcomeCount = Outcome::where('call_id', '=', $id)
                                ->count();
        if(Auth::User()->type == "admin"){
            return view('admin.casedetails', compact('outcomeCount', 'callDetails'));
      }elseif(Auth::User()->type == "supervisor"){
          return view('supervisor.casedetails', compact('outcomeCount', 'callDetails'));
        }
    }
    if(request('status')){
      $status = request('status') - 1;
      $cases = Call::with(['partner', 'ta.district'])
               ->where('status', $status)
               ->orderBy('date','DESC')
               ->paginate(10);
      $cases->setPath(url()->current().'?status='.request('status'));
      $data['cases']= $cases;

      if(Auth::User()->type == "supervisor"){

        return view('supervisor.cases', compact('status', 'cases','data'));

      }

      elseif(Auth::User()->type == "partner"){
        if (request('status')==1) {
          // code...
          $caseStatus = "Pending Cases";
        }
        elseif (request('status')==2) {
          // code...
            $caseStatus = "Unresolved Cases";
        }
        elseif (request('status')==3) {
          // code...
              $caseStatus = "resolved Cases";
        }
        else {
              $caseStatus = "WPF Cases";
        }
        return view('partner.cases', compact('status', 'cases','caseStatus','data'));
      }
    }
    else {
      $cases = Call::with(['partner', 'ta.district'])
               ->orderBy('date','DESC')
               ->paginate(10);
      $cases->setPath(url()->current());
      $data['cases']= $cases;

        return view('supervisor.cases', compact('status', 'cases','data'));
    }
    if(request('subissue')){
      $subissue = request('subissue');
      $status = 4;
      $cases = Call::addSelect('call.id', 'call.comments', 'call.date', 'partner.name as partner', 'district.name as district', 'call.referral_date as referral_date')
        ->join('call_subissue', 'call_subissue.call_id', '=', 'call.id')
        ->join('subissue', 'subissue.id', '=', 'call_subissue.subissue_id')
        ->join('district', 'call.district_id', '=', 'district.id')
        ->join('partner', 'call.partner_id', '=', 'partner.id')
        ->where('subissue.id', $subissue)
        ->orderBy('date', 'DESC')
        ->paginate(10);
        $cases->setPath(url()->current().'?status='.request('status'));
        $data['cases']= $cases;

        if(Auth::User()->type == "supervisor" || Auth::User()->type == "common"){

        return view('viewCases', compact('status', 'cases','caseStatus','data'));

      }elseif(Auth::User()->type == "partner"){

        return view('partner.cases', compact('status', 'cases','caseStatus','data'));
      }else{
        return Redirect::back();
      }
    }
  }

  public function viewCalls()
  {
      # code...
        $calls = Call::with(['client','subissues.issue','partner','ta.district','interventions','referrals','outcome'])
                          ->orderBy('date','DESC')
                          ->paginate(10);
                  $calls->setPath(url()->current().'?status='.request('status'));
                  $data['calls']= $calls;

          return  view('supervisor.calls', compact('calls','data'));

  }

  public function edit(){
    if(request('id')){
      $id = request('id');
      $callDetails = Call::with(['client','subissues.issue','partner','ta.district','interventions','referrals','outcome'])
                          ->where('call.id', '=', $id)
                          ->first();
      $outcomeCount = Outcome::where('call_id', '=', $id)
                              ->count();
      if(Auth::User()->type == "partner"){
        return view('partner.edit', compact('outcomeCount', 'callDetails'));
      }
      elseif(Auth::User()->type == "supervisor"){
        $partners = Partner::all();
        return view('supervisor.register.edit', compact('partners','outcomeCount', 'callDetails'));
      }
      elseif(Auth::User()->type == "admin"){
        $partners = Partner::all();
          return view('admin.edit', compact('outcomeCount', 'callDetails','partners'));
      }
  }
}

  public function closeCase(Request $request){
    if(request('id')){
      $id = request('id');
      if ($request->input('partner') == 0) {
        // code...
        $partner = Auth::user()->id;
        $partner_name =Partner::find($partner);
        $name  = $partner_name->name;
        $messages = 'Case successfully edited by '.$name;
      }
      else{
        $partner = $request->input('partner');
        $partner_name =Partner::find($partner);
        $name  = $partner_name->name;
        $messages = 'Case successfully assigned to ' .$name;
      }

      $case_comments = $request->input('brief');
      $action_taken = $request->input('action');
      $outCome = $request->input('outcome');

      $case_date = $request->input('date');
      $status = $request->input('status');
      $case = Call::find($id );
      $case->comments =$case_comments;
      $case->status =1;
      $case->partner_id = $partner;
      $case->referral_date = date("Y-m-d H:i:s");
      $case->save();

      return  Redirect::to('/caseEdit?id='.$id)->with(['message' =>''.$messages]);
    }
  }
  public function delete(){

    if(request('id')){
      $id = request('id');
      $call = Call::find($id);
      return $call;
      $call->delete();
      return redirect()->back()->with(['message' => "Case deleted Sucessfully!"]);
    }
  }

// case trending
  public function getCaseTrend($value='')
  {
    # code...
    // Route assigned name "partner.issues"...
    $date = new Carbon;
    $date->subMonth(10);
          $barGraph = Call::select('date', \DB::raw('count(call.id) as val'))
                      ->where( 'date', '>',  $date->toDateTimeString())
                      ->groupBy('date')
                      ->orderBy('date','ASC')
                      ->limit(10)
                      ->get();

    return json_encode($barGraph);
  }
}
