<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Cfm as Cfm;
use App\User as User;
use App\Call as Call;
use App\Casee as Casee;
use App\subissue as Subissue;
use App\Issue as Issue;
use App\Partner as Partner;
use App\District as District;
use App\Intervention as Intervention;
use App\Referral as Referral;
use App\Perpetrator as Perpetrator;
use App\Client as Client;
use App\Outcome as Outcome;
use App\WorkerStatus as Worker;
use App\Scheme as Scheme;
use App\LabourType as Labour;
use App\AllianceOne as AllianceOne;
use App\AlpCommitte as Committee;
use App\Counsellor as Counsellor;
use App\Http\Controllers\AdminController as AdminController;
class SupervisorController extends Controller
{
    //
// Account Settings
    public function getProfile($value='')
    {
      // code...
      return view('supervisor.accountSetting.profile');
    }
    public function getNotifications($value='')
    {
      // code...
      return view('supervisor.accountSetting.notification');
    }

    public function accountSettings($value='')
    {
      // code...
      return view('supervisor.accountSetting.account');
    }

    public function getRequests($value='')
    {
      // code...\
        return view('supervisor.reports.request');
    }

    public function collectionMaps($value='')
    {
      // code...

      return view('supervisor.analytics.maps');
    }

    public function collectionCharts($value='')
    {
      // code...
      $userd = new AdminController;
      $userdistribution = $userd->getUserDistribution( );

      return view('supervisor.analytics.charts', compact('userdistribution'));
    }

    //
    public function getCouncellor ($value='')
    {
      // code...
      if (url('/supervisor.active') == url()->current()) {
        $counsellors = User::where('type','=', 'common')
        ->where('state','=','1')
        ->paginate(10);
        $counsellors->setPath(url()->current());
        $data['counsellor']= $counsellors;
      }
      elseif (url('/supervisor.online') == url()->current()) {
        // code...
        $counsellors = User::where('type','=', 'common')
        ->where('online','=','online')
        ->paginate(10);
        $counsellors->setPath(url()->current());
        $data['counsellor']= $counsellors;
      }

      return view('supervisor.counsellors', compact('counsellors', 'data' ));
    }

    public function getCases($value='')
    {
      // code...
      // code...
      $cases = Call::with(['ta.district'])
              ->orderBy('date','DESC')
              ->paginate(10);
      $cases->setPath(url()->current());
      $data['cases']= $cases;
      return view('supervisor.cases', compact('cases', 'data'));
    }


    public function getCalls($value='')
    {
      // code...
      $calls = Call::with(['ta.district'])
              ->orderBy('date','ASC')
              ->paginate(10);
      $calls->setPath(url()->current());
      $data['calls']= $calls;
      return view('supervisor.calls', compact('calls', 'data'));
    }

    public function newReports ($value='')
    {
      // code...
      return view('supervisor.reports.new');
    }

    public function getCalendar($value='')
    {
      // code...
        return view('supervisor.reports.calendar');
    }

    public function getPartners($value='')
    {
      // code...
      $partners = DB::table('counsellor')->join('partner', 'counsellor.id', '=', 'partner.id')
                ->orderBy('name','ASC')
                ->get();
      $user = User::where('type', '=', 'partner')
                   ->paginate(5);

      $user->setPath(url()->current());
      $data['partners']= $user;

      return view('supervisor.partner', compact('partners', 'data'));
    }
// counsellor
    public function PlotGraphline(Request $request)
    {
      # code...
      // Route assigned name "partner.issues"...
      $date = new Carbon;
      $date->subMonth(7);
      // code...
      $id = $request->input('id');
      $counsellors = Counsellor::find($id);
      if ($id) {
        // code...
        $barGraph = Call::select('date', \DB::raw('count(call.id) as val'))
                    ->where('counsellor_username','=', $counsellors->username)
                    ->where( 'date', '>',  $date->toDateTimeString())
                    ->groupBy('date')
                    ->orderBy('date','ASC')
                    ->limit(7)
                    ->get();
      }

      return json_encode($barGraph);
      //
    }
    public function getDataPartnerCases(Request $request )
    {
        $id = $request->input('id');
      // code...
      $Partnerissues = Counsellor::withCount(['call as com' => function ($query) {$query->where('completeness', 'complete');},
                    'call as incomp' => function ($query){$query->where('completeness', 'incomplete');}
                  ])
                  ->where('id','=', $id)
                  ->orderBy('username','ASC')
                  ->get();

      return json_encode($Partnerissues);
    }

}
