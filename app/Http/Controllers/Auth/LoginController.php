<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User as User;
Use DB;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticated(Request $request){

      $email = $request->input('email');
      $password = $request->input('password');

      if (Auth::attempt(['email' => $email, 'password' => $password, 'state' => "active"])) {
        // The user is active, not suspended, and exists.
        return redirect::to('/home');
      }else{
        return redirect::back()->with(['error' => 'The Username and Password combination is incorrect!']);
      }

        //DB::table('counsellor')->where('id',Auth::User()->id)->update(['online' =>'online']);

    }

    public function logout(){
        Auth::logout();

        return redirect::to('/login');
    }
}
