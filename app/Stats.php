<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stats extends Model
{
  public $table ='stats';
  public $timestamps=false;
}
