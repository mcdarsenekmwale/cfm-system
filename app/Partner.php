<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\PartnerModelScope;

class Partner extends Model
{
	public $table="partner";
	public $timestamps=false;

	public static function boot(){
			parent::boot();
			static::addGlobalScope(new PartnerModelScope);
	}

    public function calls(){
        return $this->hasMany('App\Call', 'partner_id');
    }
		public function clients(){
    	return $this->belongsToMany('App\Client','call', 'partner_id', 'client_id');
    }
    public function issues(){
    	return $this->belongsToMany('App\Issue','issue_partner', 'partner_id', 'issue_id');
    }
    public function user(){
        return $this->belongsTo('App\User','id');
    }
    public function partners(){
    	return $this->belongsToMany('App\Partner','partner_manager', 'manager_id', 'partner_id');
    }
}
