<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlpCommittee extends Model
{
  public $table="alp_committee";
  public $timestamps=false;
}
