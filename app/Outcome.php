<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outcome extends Model
{
    public $table ="outcome";
    public $timestamps=false;

    public function call(){
        return $this->belongsTo('App\Call', 'call_id');
    }
}
