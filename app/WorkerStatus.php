<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkerStatus extends Model
{
  public $table="worker_status";
  public $timestamps=false;
}
