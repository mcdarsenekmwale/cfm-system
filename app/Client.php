<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
  public $table="client";
	public $timestamps=false;
  protected $fillable = ['name','age','gender'];

  public function calls(){
      return $this->hasMany('App\Call', 'client_id');
  }

  public function scopeIsAgeFrame($query, $frame){
    $end = $frame + 10;
    if($frame = 0){
      return $query->where('ag','<=',$end);
    }elseif($frame = 50){
      return $query->where('age','>',$frame);
    }else{
      return $query->where('ag','>',$frame)->where('age','<=',$end);
    }
  }
}
