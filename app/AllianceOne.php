<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllianceOne extends Model
{
  public $table="alliance_one";
  public $timestamps=false;
}
