<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HelplineCounsellors extends Model
{
  public $table ='helpline_counsellors';
  public $timestamps=false;

  public function Calls(){
    return $this->hasMany('App\HelplineCalls', 'counsellor_username');
  }
}
