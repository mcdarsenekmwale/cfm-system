<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\User as User;
use App\Call as Call;
use App\Partner as Partner;
use App\District as District;
use App\Counsellor as Counsellor;
use App\Intervention as Intervention;
use App\Referral as Referral;
use App\Perpetrator as Perpetrator;
use App\Cfm as Cfm;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['layouts.navBar','index','partner.index', 'admin.user.account','admin.edit','admin.casedetails','supervisor.accountSetting.account','supervisor.register.edit'], function($view){

            $totalOpenCases = Call::isCases(0)->count();
            $totalClosedCases  = Call::isCases(1)->count();
            $totalResolvedCases  = Call::isCases(2)->count();
            $totalPendingCases  = Call::isCases(3)->count();
            $latestOpenCases = Call::select('id', 'comments')
                ->isCases(0)
                ->orderBy('date','ASC')
                ->paginate(10);
            $latestClosedCases = Call::select('id', 'comments')
                ->isCases(1)
                ->orderBy('date','ASC')
                ->paginate(10);
            $latestResolvedCases = Call::select('id', 'comments')
                ->isCases(2)
                ->orderBy('date','ASC')
                ->paginate(6);

            //$try = Call::select(DB::raw('count(*) as total, status'))->groupBy('status')->count();
             $view->withTotalOpenCases($totalOpenCases)
                 ->withTotalClosedCases($totalClosedCases)
                 ->withTotalResolvedCases($totalResolvedCases)
                 ->withTotalPendingCases($totalPendingCases)
                 ->withLatestOpenCases($latestOpenCases)
                 ->withLatestClosedCases($latestClosedCases)
                 ->withLatestResolvedCases($latestResolvedCases);
        });
        view()->composer(['partner.index','partner.cases','partner.reports','partner.edit','partner.analytics','partner.integrations', 'auth.profile'], function($view){

          $districts = District::all();
          $issues = Partner::with(['issues.subissues'])->where('partner.id','=',Auth::id())->first();
          $perpetrators = Perpetrator::all();
          $cfmChannels = Cfm::all();
          $reasons = ['unknown', 'concerned child','concerned adult','victimised adult','victimised child','followup','information seeking'];
             $view->withDistricts($districts)
                  ->withIssues($issues)
                  ->withPerpetrators($perpetrators)
                  ->withcfmChannels($cfmChannels)
                  ->withReasons($reasons);
        });

        view()->composer(['admin.index'], function($view){


          $counsellors_count = Counsellor::count();
          $partners_count = Partner::count();
          $totalOpenCases = Call::isCases(0)->count();
          $totalClosedCases  = Call::isCases(1)->count();
          $totalCases = $totalOpenCases + $totalClosedCases;
             $view->withPartners_count($partners_count)
                  ->withCounsellors_count($counsellors_count)
                  ->withTotalCases($totalCases);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
