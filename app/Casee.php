<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Casee extends Model
{
    public $table ='case';

    public function call(){
        return $this->belongsTo('App\Call', 'call_id');
    }
    public function usernames(){
        return $this->belongsToMany('App\Counsellor', 'counsellor_case', 'case_id', 'username');
    }
    public function assignedBy(){
        return $this->belongsToMany('App\Counsellor', 'counsellor_case', 'case_id', 'assigned_by');
    }
}
