<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HelplineCalls extends Model
{
  public $table ='helpline_calls';
  public $timestamps=false;

  public function Counsellors(){
    return $this->belongsTo('App\HelplineCounsellors', 'counsellor_username');
  }
}
