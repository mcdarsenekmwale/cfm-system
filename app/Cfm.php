<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cfm extends Model
{
  public $table ='cfm_channel';
  public $timestamps=false;

  public function calls(){
      return $this->hasMany('App\Call', 'cfm_id');
  }
}
