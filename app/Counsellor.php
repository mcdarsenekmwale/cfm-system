<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Counsellor extends Model
{
    public $table ="counsellor";
	public $timestamps = false;

    public function calls(){
        return $this->hasMany('App\Call', 'counsellor_username', 'username');
    }

}
