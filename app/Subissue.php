<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Call as Call;
use App\Issue as Issue;
use Illuminate\Support\Facades\Auth;

class Subissue extends Model
{
    public $table="subissue";
	public $timestamps=false;

    public function issue(){
        return $this->belongsTo('App\Issue', 'issue_id');
    }
    public function calls(){
        return $this->belongsToMany('App\Call', 'call_subissue', 'subissue_id' , 'call_id');
    }
    public function anotherCalls(){
        return $this->belongsToMany('App\Call', 'call_subissue', 'subissue_id' , 'call_id');
    }
}
