<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scheme extends Model
{
  public $table="scheme";
  public $timestamps=false;
}
