<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referral extends Model
{
    public $table="referral";
	public $timestamps=false;

    public function calls(){
        return $this->belongsToMany('App\Call', 'call_referral', 'referral_id' , 'call_id');
    }
}
