<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Scopes\CallScope;
use App\Scopes\PartnerScope;
use App\Scopes\CounsellorScope;
use App\Scopes\SubissueScope;
use App\Ta;

class Call extends Model
{
    public $table ='call';
    public $timestamps=false;

    public static function boot(){
        parent::boot();
        static::addGlobalScope(new CallScope);
        static::addGlobalScope(new PartnerScope);
        static::addGlobalScope(new CounsellorScope);
    }

    public function partner(){
        return $this->belongsTo('App\Partner', 'partner_id');
    }
    public function allianceOne(){
        return $this->hasOne('App\AllianceOne', 'call_id');
    }
    public function perpetrator(){
        return $this->belongsTo('App\Perpetrator', 'perpetrator_id');
    }
    public function counsellor(){
        return $this->belongsTo('App\Counsellor', 'counsellor_username', 'username');
    }
    public function client(){
        return $this->belongsTo('App\Client', 'client_id');
    }
    public function ta(){
        return $this->belongsTo('App\Ta', 'ta_id');
    }
    public function cfm(){
        return $this->belongsTo('App\Cfm', 'cfm_id');
    }
    public function casee(){
        return $this->hasOne('App\Casee', 'call_id');
    }
    public function outcome(){
        return $this->hasOne('App\Outcome', 'call_id');
    }
    public function interventions(){
        return $this->belongsToMany('App\Intervention', 'call_intervention', 'call_id', 'intervention_id');
    }
    public function referrals(){
        return $this->belongsToMany('App\Referral', 'call_referral', 'call_id', 'referral_id');
    }
    public function subissues(){
        return $this->belongsToMany('App\Subissue', 'call_subissue', 'call_id' ,'subissue_id');
    }
    public function scopeIsCases($query, $status){
        return $query->where('status',$status);
    }
    public function scopeIsAllCases($query, $status){
        if($status != 4){
        return $query->addSelect('call.id', 'call.comments', 'call.date', 'partner.name as partner', 'district.name as district', 'call.referral_date as referral_date')
        ->join('call_subissue', 'call_subissue.call_id', '=', 'call.id')
        ->join('subissue', 'subissue.id', '=', 'call_subissue.subissue_id')
        ->join('district', 'call.district_id', '=', 'district.id')
        ->join('partner', 'call.partner_id', '=', 'partner.id')
        ->where('status', $status)
        ->orderBy('date', 'DESC');
        }else{
        return $query->addSelect('call.id', 'call.comments', 'call.date', 'partner.name as partner', 'district.name as district', 'call.referral_date as referral_date')
        ->join('call_subissue', 'call_subissue.call_id', '=', 'call.id')
        ->join('subissue', 'subissue.id', '=', 'call_subissue.subissue_id')
        ->join('district', 'call.district_id', '=', 'district.id')
        ->join('partner', 'call.partner_id', '=', 'partner.id')
        ->orderBy('date', 'DESC');
        }
    }
    public function scopeIsCallSubissueCount($query){
      return $query->select("subissue.name  as issue", \DB::raw('count(call.id) as total'))
          ->join('call_subissue','call_subissue.call_id', '=','call.id')
          ->join('subissue','subissue.id', '=','call_subissue.subissue_id')
          ->groupBy('subissue.name')->orderBy('total','DESC');
    }
    public function scopeIsTimeFrame($query, $date){
      return $query->where('date', '>=', $date ."-01 00-00-00")->where('date', '<=', $date ."-31 00-00-00");
    }

    public function scopeIsAgeFrame($query, $frame){
      $end = $frame + 10;
      if($frame = 0){
        return $query->where('age','<=',$end);
      }elseif($frame = 50){
        return $query->where('age','>',$frame);
      }else{
        return $query->where('age','>',$frame)->where('age','<=',$end);
      }
    }

}
