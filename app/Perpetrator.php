<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perpetrator extends Model
{
    public $table ="perpetrator";
    public $timestamps=false;

    public function call(){
        return $this->hasMany('App\Call', 'perpetrator_id');
    }
}
