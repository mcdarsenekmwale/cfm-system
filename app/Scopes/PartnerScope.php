<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use App\Partner as Manager;

class PartnerScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {

      if(Auth::check()){
        if(Auth::user()->type == 'partner'){

          $manager = Manager::whereHas('partners', function($query){
                      $query->where('manager_id',Auth::id());
          })->first();
          if(!empty($manager)){
            $manager->load('partners:partner_id');
            $ids[] = Auth::id();
            foreach($manager->partners as $partner){
                $ids[] = $partner->partner_id;
              }
            $builder->whereIn('call.partner_id', $ids);
          }else{
            $builder->where('call.partner_id','=',Auth::id());
          }

        }
      }
    }

}
