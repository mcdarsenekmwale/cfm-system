<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ta extends Model
{
  protected $table = 'ta';

  public function district(){
      return $this->belongsTo('App\District', 'district_id');
  }
  public function calls(){
      return $this->hasMany('App\Call', 'ta_id');
  }
}
