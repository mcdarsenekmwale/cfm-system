<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LabourType extends Model
{
  public $table="labour_type";
  public $timestamps=false;
}
