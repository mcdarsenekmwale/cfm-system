<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    public $table ='district';

    public function calls(){
        return $this->hasManyThrough('App\Call','App\Ta' ,'district_id','ta_id');
    }
    public function tas(){
        return $this->hasMany('App\Ta', 'district_id');
    }
}
