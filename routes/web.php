<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| YONECO ICT 2018
*/

use App\User as User;
use App\Call as Call;

Auth::routes();

Route::get('/logout','Auth\LoginController@logout');
Route::get('/log',function(){
	return view('log');
});
/*
|General Routes
*/
Route::middleware(['auth'])->group(function(){
	Route::get('/typeahead','SearchController@typehead')->name('typehead');
	Route::any('/search','SearchController@index')->name('search');
	Route::any('/searching','SearchController@searching')->name('searching');
	Route::get('/','HomeController@index')->name('index');
	Route::get('/home','HomeController@index')->name('home');
	Route::get('/viewCases','IssueController@viewCases')->name('viewCases');
	Route::get('/allCases','IssueController@viewAllCases')->name('allCases');
	Route::get('/casedetails','IssueController@viewCall')->name('View');
	Route::post('/casedetails','IssueController@viewCases')->name('calldetails');
	Route::get('/profile','HomeController@ProfileIndex')->name('profile');
	Route::get('/caseEdit', 'IssueController@edit')->name('caseEdit');
	Route::get('/callDelete', 'IssueController@delete')->name('callDelete');
	Route::get('/callEdit', 'IssueController@edit')->name('callEdit');

	Route::get('/allusers', 'AdminController@getAllUser')->name('allusers');
	Route::get('/casetrend', 'IssueController@getCaseTrend')->name('casetrend');
	Route::post('savecall', 'CounsellorController@registerCall')->name('savecall');
	Route::post('/savecase', 'PartnerController@saveCase')->name('savecase');
	Route::get('/counsellorStats', 'CounsellorController@getCouncellorInfo')->name('counsellorStats');
	Route::get('/counsellorDetails', 'CounsellorController@getCouncellorInfo')->name('counsellorDetails');

	// counsellor data counsellorcase
	Route::get('/admin.counsellorcase', 'SupervisorController@getDataPartnerCases')->name('counsellorcase');
	Route::get('/admin.counsellordailytrend', 'SupervisorController@PlotGraphline')->name('counsellordailytrend');
});
/*
| supervisor Routes and
| Partner Routes
*/
Route::group(['middleware' => ['auth', 'types:admin, supervisor, partner'] ], function() {
	Route::post('saveEdit', 'IssueController@closeCase')->name('saveEdit');

	Route::get('/caseDelete', 'IssueController@delete')->name('caseDelete');


});


/*
| Partner Routes
|
*/
Route::middleware(['auth', 'partner'])->group(function () {

	Route::get('/partner','HomeController@index')->name('index');
	Route::get('/partnerHome','HomeController@index')->name('home');
	Route::post('/caseResolve', 'PartnerController@resolveCase')->name('ResolveCase');
	Route::get('/addCase', 'PartnerController@newCase')->name('addNewCase');

	Route::get('/analytics','PartnerController@analyticsDraw')->name('analytics');
	 Route::get('/partner.newreport','PartnerController@createReport')->name('partner.newreport');
	Route::get('/integrations','PartnerController@getIntegrations')->name('integrations');
	// case status
	Route::get('/partner.issues.dash', 'PartnerController@PlotGraphdash')->name('dash');
	// monthly
	Route::get('/partner.issues.line', 'PartnerController@PlotGraphline')->name('line');
	// monthly
<<<<<<< HEAD
	Route::get('/partner.issues.bar', 'PartnerController@PlotGraph3')->name('graph');

	Route::get('/extra','PartnerController@extras');
=======
	Route::get('/partner.issues.bar', 'PartnerController@PlotGraphbar')->name('graph');
	Route::get('/partner.piegender', 'PartnerController@piegender')->name('piegender');
	Route::get('/partner.lineissue', 'PartnerController@partnersubissuesDraw')->name('lineissue');
	Route::get('/partner.linesubissue', 'PartnerController@partnersubissuesDraw')->name('linesubissue');


>>>>>>> b9598ffb5b4ee67d053df41eaa4b36eafd7d85e1
});

/*
| Administrator Routes
|
*/
Route::middleware(['auth', 'admin'])->group(function () {

	Route::get('/admin','HomeController@index')->name('index');
	Route::get('/adminHome','HomeController@index')->name('home');
	Route::get('/addUser', function(){
		return view('addUser');
	});


	// Forms
		Route::prefix('forms')->group(function () {
			Route::get('/newcall','AdminController@createCall')->name('newcall');
			Route::get('/newcase', 'AdminController@createCase')->name('newcase');
			Route::get('/newpartner', 'AdminController@createPartner')->name('newpartner');
			Route::get('/newuser', 'AdminController@createUser')->name('newuser');
			Route::post('saveuser' , 'AdminController@saveUser')->name('saveuser');
			Route::post('admin.savecall', 'CounsellorController@registerCall')->name('admin.savecall');
	});

	Route::prefix('users')->group(function () {
		Route::get('/account','AdminController@accountSettings')->name('account');
		Route::get('/case', 'AdminController@createCase')->name('case');
		Route::get('/partner-list', 'AdminController@getPartners')->name('partner-list');
		Route::get('/users', 'AdminController@CheckUser')->name('users');
		Route::get('/admin.profile', 'AdminController@getProfile')->name('admin.profile');
		Route::get('/notifications', 'AdminController@getNotifications')->name('notifications');

	});

	Route::prefix('tables')->group(function () {
		Route::get('/counsellors','AdminController@viewCounsellors')->name('counsellors');
		Route::get('/cases', 'AdminController@viewCases')->name('cases');
		Route::get('/partners', 'AdminController@viewsPartners')->name('partners');
		Route::get('/calls', 'AdminController@viewCalls')->name('calls');
		Route::get('/supervisors', 'AdminController@viewSupervisors')->name('supervisors');
	});

	Route::prefix('collections')->group(function () {
		Route::get('/maps','AdminController@collectionMaps')->name('maps');
		Route::get('/charts', 'AdminController@collectionCharts')->name('charts');

	});

	Route::prefix('reports')->group(function () {
		Route::get('/newreport',function ($value='') {
			// code...
			return view( 'admin.reports.new');
		})->name('newreport');
		Route::get('/boardPartner', 'AdminController@getboardPartner')->name('boardPartner');
		Route::get('/calendar', 'AdminController@setcalendar')->name('calendar');
		Route::get('/partnergrantt', 'AdminController@getPartnerGrantt')->name('partnergrantt');
		Route::get('/partnerprojects', 'AdminController@getPartnerProjects')->name('partnerprojects');
		Route::get('/request', 'AdminController@getRequests')->name('request');
	});
	Route::get('/partnerprojects', 'AdminController@getPartnerProjects')->name('partnerprojects');
	Route::get('/activepartner', 'AdminController@getActivePartner')->name('activepartner');
	Route::get('/admin.userdistribution', 'AdminController@getUserDistribution')->name('adminuser');
	Route::get('/admin.casedistribution', 'AdminController@getCaseDistribution')->name('admincases');
	Route::get('/admin.caseperformace', 'AdminController@getcaseperformace')->name('caseperformace');
	Route::get('/admin.counsellorboard', 'AdminController@getLeaderboard')->name('counsellorboard');
	Route::get('/admin.partnercase', 'AdminController@getDataPartnerCases')->name('partnercase');
	Route::get('/admin.partnerdailytrend', 'AdminController@PlotGraphline')->name('partnerdailytrend');
	Route::post('/updateaccount', 'AdminController@updateProfile')->name('updateaccount');
	Route::post('/addUser', 'AdminController@addUser')->name('addUser');
	Route::get('/viewUsers', 'AdminController@viewUsers')->name('ViewUsers');
	Route::get('/deleteUser', 'AdminController@deleteUser')->name('deleteUser');
	Route::get('/editUser', 'AdminController@editUser')->name('editUser');
	Route::post('/editUser', 'AdminController@edit')->name('edit');
	Route::get('/passwordReset', 'AdminController@passwordReset')->name('passwordReset');
});

/*
| supervisor Routes
|
*/
Route::middleware(['auth', 'supervisor'])->group(function () {
<<<<<<< HEAD
	Route::get('/caseDelete', 'IssueController@delete')->name('CaseDelete');
	Route::get('/viewStats', 'StatsController@stats')->name('viewStats');
	Route::get('/extras', 'StatsController@extras');
=======

	// Register
		Route::prefix('register')->group(function () {
			Route::get('/call','RegistrationController@createCall')->name('call');
			Route::get('/case', 'RegistrationController@createCase')->name('case');
	});
	// AccountSetting
		Route::prefix('accountSetting')->group(function () {
			Route::get('/account','SupervisorController@accountSettings')->name('supervisor.account');
			Route::get('/supervisor.profile', 'SupervisorController@getProfile')->name('supervisor.profile');
			Route::get('/supervisor.notifications', 'SupervisorController@getNotifications')->name('supervisor.notifications');

	});

	Route::prefix('analytics')->group(function () {
		Route::get('/supervisor.maps','SupervisorController@collectionMaps')->name('supervisor.maps');
		Route::get('/supervisor.charts', 'SupervisorController@collectionCharts')->name('supervisor.charts');

	});

	Route::get('/supervisor.partners', 'SupervisorController@getPartners')->name('supervisor.partners');
	Route::get('/supervisor.calendar', 'SupervisorController@getCalendar')->name('supervisor.calendar');
	Route::get('/supervisor.newreport', 'SupervisorController@newReports')->name('supervisor.newreport');
	Route::get('/supervisor.request', 'SupervisorController@getRequests')->name('supervisor.request');
	Route::get('/supervisor.active', 'SupervisorController@getCouncellor')->name('supervisor.active');
	Route::get('/supervisor.online', 'SupervisorController@getCouncellor')->name('supervisor.online');
	Route::get('/supervisor.allcases', 'IssueController@viewCases')->name('supervisor.allcases');
	Route::get('/supervisor.allcalls', 'IssueController@viewCalls')->name('supervisor.allcalls');
	Route::get('/supervisor.unresolvedcases', 'IssueController@viewCases')->name('supervisor.unresolvedcases');
	Route::get('/supervisor.resolvedcases', 'IssueController@viewCases')->name('supervisor.resolvedcases');
	Route::get('/supervisor.pendingcases', 'IssueController@viewCases')->name('supervisor.pendingcases');
	Route::get('/supervisor.refferedcases', 'IssueController@viewCases')->name('supervisor.refferedcases');
	Route::get('/supervisor.unrefferedcases', 'IssueController@viewCases')->name('supervisor.unrefferedcases');
	ROute::get('/viewStats', 'TithandizaneController@stats')->name('viewStats');
>>>>>>> b9598ffb5b4ee67d053df41eaa4b36eafd7d85e1
});

/*
| Counsellor Routes
|
*/
Route::middleware(['auth', 'counsellor'])->group(function () {

	// gbv councellor
	Route::prefix('gbv')->group(function () {
			Route::get('/','CounsellorController@index')->name('gbvcases');
	    Route::get('/call', 'CounsellorController@registerNewCall')->name('gbvcall');
	    Route::get('/councellor', 'CounsellorController@getCouncellor')->name('councellor');
			Route::post('savecall', 'CounsellorController@registerCall')->name('savecall');
			Route::get('/logout','Auth\LoginController@logout');
	});

	//allianceone councellor
	Route::prefix('allianceone')->group(function () {
		Route::get('/','CounsellorController@index')->name('allianceOneCases');
	    Route::get('/call', 'CounsellorController@registerNewCall')->name('allianceOneCall');
	    Route::get('/councellor', 'CounsellorController@getCouncellor')->name('councellor');
	    Route::post('savecall', 'CounsellorController@registerCall')->name('savecall');
			Route::get('/logout','Auth\LoginController@logout');
	});

	// congoma councellor
	Route::prefix('congoma')->group(function () {
		Route::get('/','CounsellorController@index')->name('CongomaCases');
	    Route::get('/call', 'CounsellorController@registerNewCall')->name('CongomaCall');
	    Route::get('/councellor', 'CounsellorController@getCouncellor')->name('councellor');
	    Route::post('savecall', 'CounsellorController@registerCall')->name('savecall');
			Route::get('/logout','Auth\LoginController@logout');
	});

	Route::get('/recentcalls','IssueController@viewCall')->name('View');
	// dashboard_counsellor

	Route::get('/councellor.issues.dash', 'CounsellorController@drawDashboard')->name('addCall');
	Route::get('/test', function ($value='')
	{
		// code...
		return view('counsellor.councellor.gbv.layout.test');
	});
	Route::get('/addCall', 'CounsellorController@addCall')->name('addCall');
	Route::post('/caseClose', 'IssueController@closeCase')->name('closeCase');
});
